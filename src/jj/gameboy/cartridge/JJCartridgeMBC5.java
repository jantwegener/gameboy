package jj.gameboy.cartridge;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import jj.gameboy.JJGameBoyException;

/**
 * This class represents the MBC5.
 * 
 * @author Jan-Thierry Wegener
 */
public class JJCartridgeMBC5 extends JJCartridgeMBC {
	
	private boolean isRamEnabled = false;
	
	private int lowerRomBits = 0;
	
	private int upperRomBits = 0;
	
	public JJCartridgeMBC5(JJCartridge cartridge, JJCartridgeROM rom, int numRomBank, int numRamBank, int ramBankSize, boolean hasBattery) {
		super(cartridge, rom, numRomBank, numRamBank, ramBankSize, hasBattery);
	}

	@Override
	public void write(int address, int value) {
		int offset;
		
		// normally, the ROM cannot be written to
		// however, some addresses have some special meaning
		if (0 <= address && address <= 0x1FFF) {
			// 0000-1FFF - RAM and Timer Enable (Write Only)
			// 00h  Disable RAM (default)
			// 0Ah  Enable RAM
			if ((value & 0x0F) == 0x0A) {
				isRamEnabled = true;
			} else {
				isRamEnabled = false;
				if (hasBattery()) {
					getCartridge().saveRAM();
				}
			}
		} else if (0x2000 <= address && address <= 0x2FFF) {
			lowerRomBits = 0xFF & value;
			setRomBankPointer((upperRomBits << 8) | lowerRomBits); 
		} else if (0x3000 <= address && address <= 0x3FFF) {
			upperRomBits = 0x01 & value;
			setRomBankPointer((upperRomBits << 8) | lowerRomBits); 
		} else if (0x4000 <= address && address <= 0x5FFF) {
			setRamBank(value & 0x0F);
		} else if (0xA000 <= address && address <= 0xBFFF) {
			if (isRamEnabled()) {
				// A000-BFFF - RAM Bank 00-03, if any (Read/Write)
				offset = 0xA000;
				getSRAM().write(address - offset, value);
			}
		} else {
			System.out.printf("Unexpected write access: %x - %d%n", address, value);
			throw new JJGameBoyException(String.format("Unexpected write access: %x - %d", address, value));
		}
	}
	
	@Override
	protected boolean isRamEnabled() {
		return isRamEnabled;
	}
	
	@Override
	public void dumpState(OutputStream out) throws IOException {
		out.write(isRamEnabled ? 1 : 0);
		out.write(lowerRomBits);
		out.write(upperRomBits);
		
		super.dumpState(out);
	}
	
	@Override
	public void reloadState(InputStream in) throws IOException {
		isRamEnabled = in.read() == 1;
		lowerRomBits = in.read();
		upperRomBits = in.read();
		
		super.reloadState(in);
	}
	
}
