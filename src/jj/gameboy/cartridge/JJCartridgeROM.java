package jj.gameboy.cartridge;

import java.io.IOException;

import jj.gameboy.io.rom.JJBasicROM;

/**
 * This class represents the ROMs of the cartridge.
 * 
 * @author Jan-Thierry Wegener
 */
public class JJCartridgeROM extends JJBasicROM {
	
	public JJCartridgeROM(String romFile) throws IOException {
		super(romFile);
	}
	
	public JJCartridgeROM(int sizeInB, int[] initArray) {
		super(sizeInB, initArray);
	}
	
}
