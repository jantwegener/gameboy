package jj.gameboy.cartridge;

import static jj.gameboy.io.ram.JJAbstractRAM.KB;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import jj.gameboy.JJGameBoyException;
import jj.gameboy.unit.JJIUnit;
import jj.gameboy.unit.JJSaveStateUnit;

/**
 * An abstract parent class for the MBC.
 * 
 * @author Jan-Thierry Wegener
 */
public abstract class JJCartridgeMBC implements JJIUnit, JJSaveStateUnit {

	/**
	 * The pointer to the banks higher than 0.
	 */
	private int romBankPointer = 1;
	
	/**
	 * Needed to return the correct ROM banks.
	 */
	private int romBankWiring;
	
	/**
	 * The pointer to the RAM banks.
	 */
	private int ramBankPointer = 0;
	
	/**
	 * Needed to return the correct RAM banks.
	 */
	private int ramBankWiring;
	
	private JJCartridgeROM[] romBank;

	private JJCartridgeSRAM[] sram;
	
	private JJCartridge cartridge;
	
	private boolean hasBattery;
	
	public JJCartridgeMBC(JJCartridge cartridge, JJCartridgeROM rom, int numRomBank, int numRamBank, int ramBankSize, boolean hasBattery) {
		this.cartridge = cartridge;
		this.hasBattery = hasBattery;
		
		// does not work for ROM sizes 0x52h - 0x54
		romBankWiring = numRomBank - 1;
		ramBankWiring = numRamBank - 1;
		
		romBank = new JJCartridgeROM[numRomBank];
		
		// init rom
		for (int i = 0; i < romBank.length; i++) {
			int start = i * 16 * KB;
			int end = start + 16 * KB;
			int[] data = rom.readWindow(start, end);
			romBank[i] = new JJCartridgeROM(16 * KB, data);
		}
		
		romBankPointer = 1;
		
		// init ram
		JJCartridgeSRAM tmpRAM = null;
		if (hasBattery && cartridge.existsSaveFile()) {
			try {
				tmpRAM = new JJCartridgeSRAM(cartridge.getSaveFile());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		if (tmpRAM == null) {
			if (numRamBank == 1) {
				tmpRAM = new JJCartridgeSRAM(ramBankSize);
			} else if (numRamBank > 1) {
				tmpRAM = new JJCartridgeSRAM(numRamBank * 8 * KB);
			}
		}
		
		sram = new JJCartridgeSRAM[numRamBank];
		if (numRamBank == 1) {
			sram[0] = tmpRAM;
		} else if (numRamBank > 1) {
			for (int i = 0; i < numRamBank; i++) {
				int start = i * 8 * KB;
				int end = start + 8 * KB;
				int[] data = tmpRAM.readWindow(start, end);
				sram[i] = new JJCartridgeSRAM(8 * KB, data);
			}
		}
		
		ramBankPointer = 0;
	}
	
	/**
	 * Returns the original cartridge where the MBC is located on.
	 * 
	 * @return The cartridge.
	 */
	protected JJCartridge getCartridge() {
		return cartridge;
	}
	
	/**
	 * Returns true when the cartridge has a battery, false otherwise.
	 * 
	 * @return true when the cartridge has a battery, false otherwise.
	 */
	public boolean hasBattery() {
		return hasBattery;
	}
	
	@Override
	public int read(int address) {
		int retval;
		
		int offset;
		if (0 <= address && address <= 0x3FFF) {
			// 0000-3FFF - ROM Bank 00 (Read Only)
			offset = 0;
			retval = getRom0().read(address - offset);
		} else if (0x4000 <= address && address <= 0x7FFF) {
			// 4000-7FFF - ROM Bank 01-7F (Read Only)
			offset = 0x4000;
			retval = getRom().read(address - offset);
		} else if (0xA000 <= address && address <= 0xBFFF) {
			if (isRamEnabled()) {
				// A000-BFFF - RAM Bank 00-03, if any (Read/Write)
				offset = 0xA000;
				retval = getSRAM().read(address - offset);
			} else {
				// not allowed to access RAM while not enabled
				System.err.println("RAM must be enabled before accessing it.");
				retval = 0xFF;
			}
		} else {
			System.out.printf("Unexpected read access: %x%n", address);
			throw new JJGameBoyException(String.format("Unexpected read access: %x", address));
		}
		
		return retval;
	}
	
	protected abstract boolean isRamEnabled();
	
	public int getRomBankPointer() {
		return romBankPointer;
	}
	
	protected void setRomBankPointer(int p) {
		romBankPointer = p;
	}
	
	protected JJCartridgeROM getRom0() {
		return romBank[0];
	}
	
	protected JJCartridgeROM getRom() {
		return romBank[romBankPointer & romBankWiring];
	}
	
	public JJCartridgeSRAM getSRAM() {
		return sram[ramBankPointer & ramBankWiring];
	}
	
	protected void setRamBank(int p) {
		ramBankPointer = p;
	}
	
	public int getRamBankPointer() {
		return ramBankPointer;
	}

	public InputStream getInputStream() {
		InputStream in = new InputStream() {
			int romBankPointer = 0;
			int ramBankPointer = 0;
			
			int romAddress = 0;
			int ramAddress = 0;
			
			@Override
			public int read() throws IOException {
				int retval = -1;
				
				if (romBankPointer < romBank.length) {
					retval = romBank[romBankPointer].read(romAddress++); 

					if (romAddress >= romBank[romBankPointer].size()) {
						romBankPointer++;
						romAddress = 0;
					}
				} else if (ramBankPointer < sram.length) {
					retval = sram[ramBankPointer].read(ramAddress++); 

					if (ramAddress >= sram[ramBankPointer].size()) {
						ramBankPointer++;
						ramAddress = 0;
					}
				}
				
				return retval;
			}
		};
		
		return in;
	}
	
	public InputStream getRamInputStream() {
		InputStream in = new InputStream() {
			int ramBankPointer = 0;
			int ramAddress = 0;
			
			@Override
			public int read() throws IOException {
				int retval = -1;
				
				if (ramBankPointer < sram.length) {
					retval = sram[ramBankPointer].read(ramAddress++); 

					if (ramAddress >= sram[ramBankPointer].size()) {
						ramBankPointer++;
						ramAddress = 0;
					}
				}
				
				return retval;
			}
		};
		
		return in;
	}
	
	/**
	 * Dumps the cartridge to the output stream.
	 * 
	 * @param out The output stream to that the cartridge is dumped to.
	 * 
	 * @throws IOException If an {@link IOException} occurs.
	 */
	public void dumpState(OutputStream out) throws IOException {
		out.write(hasBattery ? 1 : 0);
		out.write(romBankPointer);
		out.write(romBankWiring);
		out.write(ramBankPointer);
		out.write(ramBankWiring);
		
		dumpRAM(out);
	}
	
	/**
	 * Dumps the ram to the output stream.
	 * 
	 * @param out The output stream to that the RAM is dumped to.
	 * 
	 * @throws IOException IOException If an {@link IOException} occurs.
	 */
	protected void dumpRAM(OutputStream out) throws IOException {
		for (int i = 0; i < sram.length; i++) {
			sram[i].dumpState(out);
		}
	}
	
	/**
	 * Reloads the cartridge from the input stream.
	 * 
	 * @param in The stream to load the cartridge from.
	 * 
	 * @throws IOException If an {@link IOException} occurs.
	 */
	public void reloadState(InputStream in) throws IOException {
		hasBattery = in.read() == 1;
		romBankPointer = in.read();
		romBankWiring = in.read();
		ramBankPointer = in.read();
		ramBankWiring = in.read();
		
		reloadRAM(in);
	}
	
	/**
	 * Reloads the RAM from the input stream.
	 * 
	 * @param in The stream to load the RAM from.
	 * 
	 * @throws IOException If an {@link IOException} occurs.
	 */
	protected void reloadRAM(InputStream in) throws IOException {
		for (int i = 0; i < sram.length; i++) {
			sram[i].reloadState(in);
		}
	}
	
}
