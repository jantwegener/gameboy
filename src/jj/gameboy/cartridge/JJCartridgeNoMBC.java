package jj.gameboy.cartridge;

import jj.gameboy.JJGameBoyException;

/**
 * This class represents the no MBC chips.
 * 
 * @author Jan-Thierry Wegener
 */
public class JJCartridgeNoMBC extends JJCartridgeMBC {

	public JJCartridgeNoMBC(JJCartridge cartridge, JJCartridgeROM rom, int numRomBank, int numRamBank, int ramBankSize, boolean hasBattery) {
		super(cartridge, rom, numRomBank, numRamBank, ramBankSize, hasBattery);
	}

	@Override
	public int read(int address) {
		int retval;
		
		int offset;
		if (0 <= address && address <= 0x3FFF) {
			// 0000-3FFF   16KB ROM Bank 00     (in cartridge, fixed at bank 00)
			offset = 0;
			retval = getRom0().read(address - offset);
		} else if (0x4000 <= address && address <= 0x7FFF) {
			// 4000-7FFF   16KB ROM Bank 01..NN (in cartridge, switchable bank number)
			offset = 0x4000;
			retval = getRom().read(address - offset);
		} else {
			System.out.printf("Unexpected read access: %x%n", address);
			throw new JJGameBoyException(String.format("Unexpected read access: %x", address));
		}
		
		return retval;
	}

	@Override
	public void write(int address, int value) {
		int offset;
		
		// normally, the ROM cannot be written to
		// however, some addresses have some special meaning
		if (0 <= address && address <= 0x3FFF) {
			// 0000-3FFF   16KB ROM Bank 00     (in cartridge, fixed at bank 00)
			// we simply ignore this write attempt
			// for example Tetris tries to write here
		} else if (0x4000 <= address && address <= 0x7FFF) {
			// 4000-7FFF   16KB ROM Bank 01..NN (in cartridge, switchable bank number)
			offset = 0x4000;
			getRom().write(address - offset, value);
		} else {
			System.out.printf("Unexpected write access: %x - %d%n", address, value);
			throw new JJGameBoyException(String.format("Unexpected write access: %x - %d", address, value));
		}
	}
	
	@Override
	protected boolean isRamEnabled() {
		return false;
	}
	
}
