package jj.gameboy.cartridge;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import jj.gameboy.JJGameBoyException;

/**
 * This class represents the MBC1.
 * 
 * @author Jan-Thierry Wegener
 */
public class JJCartridgeMBC1 extends JJCartridgeMBC {

	private static final int RAM_BANKING_MODE = 1;
	private static final int ROM_BANKING_MODE = 0;

	private boolean isRamEnabled = false;
	
	/**
	 * Keeps track of the current banking mode, either ROM or RAM.
	 */
	private int bankingMode = 0;
	
	/**
	 * The upper bits when switching between ROM and RAM mode.
	 */
	private int upperBits = 0;
	
	/**
	 * To keep track when switching between ROM and RAM mode.
	 */
	private int oldRamBank;
	
	public JJCartridgeMBC1(JJCartridge cartridge, JJCartridgeROM rom, int numRomBank, int numRamBank, int ramBankSize, boolean hasBattery) {
		super(cartridge, rom, numRomBank, numRamBank, ramBankSize, hasBattery);
	}
	
	protected boolean isRamEnabled() {
		return isRamEnabled;
	}
	
	public int read(int address) {
		// When the 0x0000-0x3FFF address range is accessed, the effective bank number depends on the MODE register.
		// In MODE 0b0 the bank number is always 0, but in MODE 0b1 it�s formed by shifting the BANK2 register value left by 5 bits
		int retval;
		if (0 <= address && address <= 0x3FFF) {
			switch (bankingMode) {
				case ROM_BANKING_MODE:
					// normal access to the 0 bank
					retval = super.read(address);
					break;
				case RAM_BANKING_MODE:
					// weird access to bank another bank
					// clear lower 5 bits
					int origRomBankPointer = getRomBankPointer();
					setRomBankPointer(upperBits);
					retval = super.getRom().read(address);
					// shift back
					setRomBankPointer(origRomBankPointer);
					break;
				default:
					throw new JJGameBoyException("Unknown banking mode: " + bankingMode);
			}
		} else {
			retval = super.read(address);
		}
		
		return retval;
	}
	
	@Override
	public void write(int address, int value) {
		int offset;
		
		// normally, the ROM cannot be written to
		// however, some addresses have some special meaning
		if (0 <= address && address <= 0x1FFF) {
			// 0000-1FFF - RAM Enable (Write Only)
			// 00h  Disable RAM (default)
			// 0Ah  Enable RAM
			if ((value & 0x0F) == 0x0A) {
				isRamEnabled = true;
			} else {
				isRamEnabled = false;
				if (hasBattery()) {
					getCartridge().saveRAM();
				}
			}
		} else if (0x2000 <= address && address <= 0x3FFF) {
			// 2000-3FFF - ROM Bank Number (Write Only)
			value = value & 0x1F;
			if (value == 0x00) {
				value = 0x01;
			}
			
			// clear lower 5 bits
			int pointer = 0x60 & getRomBankPointer();
			// set lower 5 bits
			pointer = pointer | value;
			setRomBankPointer(pointer);
		} else if (0x4000 <= address && address <= 0x5FFF) {
			// 4000-5FFF - RAM Bank Number - or - Upper Bits of ROM Bank Number (Write Only)
			switch (bankingMode) {
				case ROM_BANKING_MODE:
					// clear upper 2 bits
					int pointer = 0x1F & getRomBankPointer();
					// set the upper bits
					pointer = ((value & 0x03) << 5) | pointer;
					setRomBankPointer(pointer);
					break;
				case RAM_BANKING_MODE:
					setRamBank(value & 0x03);
					break;
			}
		} else if (0x6000 <= address && address <= 0x7FFF) {
			// 6000-7FFF - ROM/RAM Mode Select (Write Only)
			// 00h = ROM Banking Mode (up to 8KByte RAM, 2MByte ROM) (default)
			// 01h = RAM Banking Mode (up to 32KByte RAM, 512KByte ROM)
			// The program may freely switch between both modes,
			// the only limitation is that only RAM Bank 00h can be used during Mode 0,
			// and only ROM Banks 00-1Fh can be used during Mode 1.
			bankingMode = value & 0x01;
			if (bankingMode == ROM_BANKING_MODE) {
				// in this mode we can only use RAM Bank 0
				// but we keep the bank pointer if we switch back
				oldRamBank = getRamBankPointer();
				setRamBank(0x00);
			} else {
				// in this mode we can only use ROM Banks 00-1Fh
				int pointer = 0x1F & getRomBankPointer();
				// but keep the upper bits
				upperBits = 0x60 & getRomBankPointer();
				setRomBankPointer(pointer);
				// switch back to the previous RAM bank
				setRamBank(oldRamBank);
			}
		} else if (0xA000 <= address && address <= 0xBFFF) {
			if (isRamEnabled()) {
				// A000-BFFF - RAM Bank 00-03, if any (Read/Write)
				offset = 0xA000;
				getSRAM().write(address - offset, value);
			}
		} else {
			System.out.printf("Unexpected write access: %x - %d%n", address, value);
			throw new JJGameBoyException(String.format("Unexpected write access: %x - %d", address, value));
		}
	}
	
	@Override
	public void dumpState(OutputStream out) throws IOException {
		out.write(isRamEnabled ? 1 : 0);
		out.write(bankingMode);
		out.write(oldRamBank);
		out.write(upperBits);
		
		super.dumpState(out);
	}
	
	@Override
	public void reloadState(InputStream in) throws IOException {
		isRamEnabled = in.read() == 1;
		bankingMode = in.read();
		oldRamBank = in.read();
		upperBits = in.read();
		
		super.reloadState(in);
	}
	
}
