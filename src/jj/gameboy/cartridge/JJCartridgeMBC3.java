package jj.gameboy.cartridge;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import jj.gameboy.JJGameBoyException;

/**
 * This class represents the MBC3.
 * 
 * @author Jan-Thierry Wegener
 */
public class JJCartridgeMBC3 extends JJCartridgeMBC {
	
	private static final int RAM_MODE = 0;
	
	private static final int RTC_MODE = 1;
	
	private boolean isRamEnabled = false;
	
	private int lowerBitsRom = 0;
	
	private int ramMode = 0;
	
	public JJCartridgeMBC3(JJCartridge cartridge, JJCartridgeROM rom, int numRomBank, int numRamBank, int ramBankSize, boolean hasBattery) {
		super(cartridge, rom, numRomBank, numRamBank, ramBankSize, hasBattery);
	}

	protected boolean isRamEnabled() {
		return isRamEnabled;
	}
	
	@Override
	public void write(int address, int value) {
		int offset;
		
		// normally, the ROM cannot be written to
		// however, some addresses have some special meaning
		if (0 <= address && address <= 0x1FFF) {
			// 0000-1FFF - RAM and Timer Enable (Write Only)
			// 00h  Disable RAM (default)
			// 0Ah  Enable RAM
			if ((value & 0x0F) == 0x0A) {
				isRamEnabled = true;
			} else {
				isRamEnabled = false;
				if (hasBattery()) {
					getCartridge().saveRAM();
				}
			}
		} else if (0x2000 <= address && address <= 0x3FFF) {
			switch (ramMode) {
				case RAM_MODE:
					// 2000-3FFF - ROM Bank Number (Write Only)
					lowerBitsRom = (value & 0x7F);
					if (lowerBitsRom == 0) {
						lowerBitsRom = 1;
					}
					setRomBankPointer(lowerBitsRom);
					break;
				case RTC_MODE:
					// TODO implement
					System.out.println("RTC - " + value);
					break;
			}
		} else if (0x4000 <= address && address <= 0x5FFF) {
			// 4000-5FFF - RAM Bank Number - or - RTC Register Select (Write Only)
			if (value <= 0x07) {
				setRamBank(value);
				ramMode = RAM_MODE;
			} else {
				// TODO implement RTC register
				System.out.println("RTC: " + value);
				ramMode = RTC_MODE;
			}
		} else if (0x6000 <= address && address <= 0x7FFF) {
			// 6000-7FFF - Latch Clock Data (Write Only)
			// TODO
		} else if (0xA000 <= address && address <= 0xBFFF) {
			if (isRamEnabled()) {
				// A000-BFFF - RAM Bank 00-03, if any (Read/Write)
				offset = 0xA000;
				getSRAM().write(address - offset, value);
			}
		} else {
			System.out.printf("Unexpected write access: %x - %d%n", address, value);
			throw new JJGameBoyException(String.format("Unexpected write access: %x - %d", address, value));
		}
	}
	
	@Override
	public void dumpState(OutputStream out) throws IOException {
		out.write(isRamEnabled ? 1 : 0);
		out.write(lowerBitsRom);
		out.write(ramMode);
		
		super.dumpState(out);
	}
	
	@Override
	public void reloadState(InputStream in) throws IOException {
		isRamEnabled = in.read() == 1;
		lowerBitsRom = in.read();
		ramMode = in.read();
		
		super.reloadState(in);
	}
	
}
