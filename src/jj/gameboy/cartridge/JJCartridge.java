package jj.gameboy.cartridge;

import static jj.gameboy.io.ram.JJAbstractRAM.KB;
import static jj.gameboy.io.ram.JJAbstractRAM.MB;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.LinkOption;

import jj.gameboy.JJGameBoyException;
import jj.gameboy.unit.JJIUnit;
import jj.gameboy.unit.JJSaveStateUnit;

/**
 * The class representing the GameBoy.
 * 
 * @author Jan-Thierry Wegener
 */
public class JJCartridge implements JJIUnit, JJSaveStateUnit {
	
	private static final String SAVE_FILE_ENDING = ".jjs";

	private JJCartridgeAudioOutput audioOutput;
	
	private JJCartridgeMBC mbc;
	
	private JJCartridgeROM rom;
	
	private String romFile;
	
	public JJCartridge(String romFile) throws IOException {
		this.romFile = romFile;
		
		rom = new JJCartridgeROM(romFile);
		
		// init mbc and its rom bank
		mbc = getCartridgeMBC();
		
		// init audio
		audioOutput = new JJCartridgeAudioOutput();
	}
	
	/**
	 * Called to write the RAM to the save file.
	 */
	protected void saveRAM() {
		File saveFile = new File(romFile + SAVE_FILE_ENDING);
		try {
			File tmpSaveFile = File.createTempFile(romFile, SAVE_FILE_ENDING);
			try (BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(tmpSaveFile));
					InputStream input = mbc.getRamInputStream()) {
				int val = 0;
				while ((val = input.read()) >= 0) {
					output.write(val);
				}
			}
			if (!tmpSaveFile.renameTo(saveFile)) {
				if (saveFile.exists()) {
					File renameFile = new File(saveFile + ".bak");
					if (renameFile.exists()) {
						renameFile.delete();
					}
					saveFile.renameTo(renameFile);
					tmpSaveFile.renameTo(saveFile);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public boolean isColorGameBoyOnly() {
		int cgbFlag = rom.read(0x0143);
		return cgbFlag == 0xC0;
	}
	
	public String getTitle() {
		// 0134-0143
		byte[] b = rom.readWindowAsByte(0x0134, 0x0134 + 16);
		int len = 0;
		while (len < b.length && b[len] > 0) {
			len++;
		}
		String title = new String(b, 0, len);
		return title;
	}
	
	public String getManufacturerCode() {
		// 013F-0142
		byte[] b = rom.readWindowAsByte(0x013F, 0x013F + 4);
		int len = 0;
		while (len < b.length && b[len] > 0) {
			len++;
		}
		String title = new String(b, 0, len);
		return title;
	}
	
	private JJCartridgeMBC getCartridgeMBC() {
		int type = getCartridgeType();
		JJCartridgeMBC retval;
		
		// https://gbdev.io/pandocs/#mbc3
		switch (type) {
		
			case 0x00:
				// ROM ONLY
				retval = new JJCartridgeNoMBC(this, rom, getRomBanks(), getExternalRamBanks(), getRamSize(), false);
				break;
				
			case 0x01:
				// MBC1
				retval = new JJCartridgeMBC1(this, rom, getRomBanks(), getExternalRamBanks(), getRamSize(), false);
				break;
				
			case 0x02:
				// MBC1+RAM
				retval = new JJCartridgeMBC1(this, rom, getRomBanks(), getExternalRamBanks(), getRamSize(), false);
				break;
				
			case 0x03:
				// MBC1+RAM+BATTERY
				retval = new JJCartridgeMBC1(this, rom, getRomBanks(), getExternalRamBanks(), getRamSize(), true);
				break;
				
			case 0x05:
				// MBC2
				retval = new JJCartridgeMBC2(this, rom, getRomBanks(), getExternalRamBanks(), getRamSize(), false);
				break;
				
			case 0x06:
				// MBC2+BATTERY
				retval = new JJCartridgeMBC2(this, rom, getRomBanks(), getExternalRamBanks(), getRamSize(), true);
				break;
				
			case 0x08:
				// ROM+RAM
				throw new JJCartridgeUnsupportedMBCException(type);
				
			case 0x09:
				// ROM+RAM+BATTERY
				throw new JJCartridgeUnsupportedMBCException(type);
				
			case 0x11:
				// MBC3
				retval = new JJCartridgeMBC3(this, rom, getRomBanks(), getExternalRamBanks(), getRamSize(), false);
				break;
				
			case 0x12:
				// MBC3+RAM
				retval = new JJCartridgeMBC3(this, rom, getRomBanks(), getExternalRamBanks(), getRamSize(), false);
				break;
				
			case 0x13:
				// MBC3+RAM+BATTERY
				retval = new JJCartridgeMBC3(this, rom, getRomBanks(), getExternalRamBanks(), getRamSize(), true);
				break;
				
			case 0x19:
				// MBC5
				retval = new JJCartridgeMBC5(this, rom, getRomBanks(), getExternalRamBanks(), getRamSize(), false);
				break;
				
			case 0x1A:
				// MBC5+RAM
				retval = new JJCartridgeMBC5(this, rom, getRomBanks(), getExternalRamBanks(), getRamSize(), false);
				retval = null;
				break;
				
			case 0x1B:
				// MBC5+RAM+BATTERY
				retval = new JJCartridgeMBC5(this, rom, getRomBanks(), getExternalRamBanks(), getRamSize(), true);
				retval = null;
				break;
				
			default:
				throw new JJCartridgeUnsupportedMBCException(type);
		}
		
		return retval;
	}
	
	public int getCartridgeType() {
		//		 00h  ROM ONLY                 19h  MBC5
		//		 01h  MBC1                     1Ah  MBC5+RAM
		//		 02h  MBC1+RAM                 1Bh  MBC5+RAM+BATTERY
		//		 03h  MBC1+RAM+BATTERY         1Ch  MBC5+RUMBLE
		//		 05h  MBC2                     1Dh  MBC5+RUMBLE+RAM
		//		 06h  MBC2+BATTERY             1Eh  MBC5+RUMBLE+RAM+BATTERY
		//		 08h  ROM+RAM                  20h  MBC6
		//		 09h  ROM+RAM+BATTERY          22h  MBC7+SENSOR+RUMBLE+RAM+BATTERY
		//		 0Bh  MMM01
		//		 0Ch  MMM01+RAM
		//		 0Dh  MMM01+RAM+BATTERY
		//		 0Fh  MBC3+TIMER+BATTERY
		//		 10h  MBC3+TIMER+RAM+BATTERY   FCh  POCKET CAMERA
		//		 11h  MBC3                     FDh  BANDAI TAMA5
		//		 12h  MBC3+RAM                 FEh  HuC3
		//		 13h  MBC3+RAM+BATTERY         FFh  HuC1+RAM+BATTERY
		return rom.read(0x0147);
	}
	
	public String getCartridgeTypeDescription() {
		String retval;
		int type = getCartridgeType();
		switch (type) {
			case 0x00:
				retval = "ROM ONLY";
				break;
			case 0x01:
				retval = "MBC1";
				break;
			case 0x02:
				retval = "MBC1+RAM";
				break;
			case 0x03:
				retval = "MBC1+RAM+BATTERY";
				break;
			case 0x05:
				retval = "MBC2";
				break;
			case 0x06:
				retval = "MBC2+BATTERY";
				break;
			case 0x08:
				retval = "ROM+RAM";
				break;
			case 0x09:
				retval = "ROM+RAM+BATTERY";
				break;
			case 0x0B:
				retval = "MMM01";
				break;
			case 0x0C:
				retval = "MMM01+RAM";
				break;
			case 0x0D:
				retval = "MMM01+RAM+BATTERY";
				break;
			case 0x0F:
				retval = "MBC3+TIMER+BATTERY";
				break;
			case 0x10:
				retval = "MBC3+TIMER+RAM+BATTERY";
				break;
			case 0x11:
				retval = "MBC3";
				break;
			case 0x12:
				retval = "MBC3+RAM";
				break;
			case 0x13:
				retval = "MBC3+RAM+BATTERY";
				break;
			case 0x19:
				retval = "MBC5";
				break;
			case 0x1A:
				retval = "MBC5+RAM";
				break;
			case 0x1B:
				retval = "MBC5+RAM+BATTERY";
				break;
			case 0x1C:
				retval = "MBC5+RUMBLE";
				break;
			case 0x1D:
				retval = "MBC5+RUMBLE+RAM";
				break;
			case 0x1E:
				retval = "MBC5+RUMBLE+RAM+BATTERY";
				break;
			case 0x20:
				retval = "MBC6";
				break;
			case 0x22:
				retval = "MBC7+SENSOR+RUMBLE+RAM+BATTERY";
				break;
			case 0xFC:
				retval = "POCKET CAMERA";
				break;
			case 0xFD:
				retval = "BANDAI TAMA5";
				break;
			case 0xFE:
				retval = "HuC3";
				break;
			case 0xFF:
				retval = "HuC1+RAM+BATTERY";
				break;
			default:
				throw new JJCartridgeUnsupportedMBCException(type);
		}
		return retval;
	}

	/**
	 * Returns the size of the actual rom in byte.
	 * 
	 * @return The size of the rom in byte.
	 */
	public int getRomBanks() {
		//		 00h -  32KByte (no ROM banking)
		//		 01h -  64KByte (4 banks)
		//		 02h - 128KByte (8 banks)
		//		 03h - 256KByte (16 banks)
		//		 04h - 512KByte (32 banks)
		//		 05h -   1MByte (64 banks)  - only 63 banks used by MBC1
		//		 06h -   2MByte (128 banks) - only 125 banks used by MBC1
		//		 07h -   4MByte (256 banks)
		//		 08h -   8MByte (512 banks)
		//		 52h - 1.1MByte (72 banks)
		//		 53h - 1.2MByte (80 banks)
		//		 54h - 1.5MByte (96 banks)
		int b = rom.read(0x0148);
		int banks = -1;
		switch (b) {
			case 0x00:
				// 00h -  32KByte (no ROM banking)
				banks = 2;
				break;
			case 0x01:
				// 01h -  64KByte (4 banks)
				banks = 4;
				break;
			case 0x02:
				// 02h - 128KByte (8 banks)
				banks = 8;
				break;
			case 0x03:
				// 03h - 256KByte (16 banks)
				banks = 16;
				break;
			case 0x04:
				// 04h - 512KByte (32 banks)
				banks = 32;
				break;
			case 0x05:
				// 05h -   1MByte (64 banks)  - only 63 banks used by MBC1
				banks = 64;
				break;
			case 0x06:
				// 06h -   2MByte (128 banks) - only 125 banks used by MBC1
				banks = 128;
				break;
			case 0x07:
				// 07h -   4MByte (256 banks)
				banks = 256;
				break;
			case 0x08:
				// 08h -   8MByte (512 banks)
				banks = 512;
				break;
			case 0x52:
				// 52h - 1.1MByte (72 banks)
				banks = 72;
				break;
			case 0x53:
				// 53h - 1.2MByte (80 banks)
				banks = 80;
				break;
			case 0x54:
				// 54h - 1.5MByte (96 banks)
				banks = 96;
				break;
		}
		return banks;
	}
	
	/**
	 * Returns the size of the actual rom in byte.
	 * 
	 * @return The size of the rom in byte.
	 */
	public int getRomSize() {
		//		 00h -  32KByte (no ROM banking)
		//		 01h -  64KByte (4 banks)
		//		 02h - 128KByte (8 banks)
		//		 03h - 256KByte (16 banks)
		//		 04h - 512KByte (32 banks)
		//		 05h -   1MByte (64 banks)  - only 63 banks used by MBC1
		//		 06h -   2MByte (128 banks) - only 125 banks used by MBC1
		//		 07h -   4MByte (256 banks)
		//		 08h -   8MByte (512 banks)
		//		 52h - 1.1MByte (72 banks)
		//		 53h - 1.2MByte (80 banks)
		//		 54h - 1.5MByte (96 banks)
		int b = rom.read(0x0148);
		int size = -1;
		switch (b) {
			case 0x00:
				// 00h -  32KByte (no ROM banking)
				size = 32 * KB;
				break;
			case 0x01:
				// 01h -  64KByte (4 banks)
				size = 64 * KB;
				break;
			case 0x02:
				// 02h - 128KByte (8 banks)
				size = 128 * KB;
				break;
			case 0x03:
				// 03h - 256KByte (16 banks)
				size = 256 * KB;
				break;
			case 0x04:
				// 04h - 512KByte (32 banks)
				size = 512 * KB;
				break;
			case 0x05:
				// 05h -   1MByte (64 banks)  - only 63 banks used by MBC1
				size = 1 * MB;
				break;
			case 0x06:
				// 06h -   2MByte (128 banks) - only 125 banks used by MBC1
				size = 2 * MB;
				break;
			case 0x07:
				// 07h -   4MByte (256 banks)
				size = 4 * MB;
				break;
			case 0x08:
				// 08h -   8MByte (512 banks)
				size = 8 * MB;
				break;
			case 0x52:
				// 52h - 1.1MByte (72 banks)
				size = 16 * 72 * KB;
				break;
			case 0x53:
				// 53h - 1.2MByte (80 banks)
				size = 16 * 80 * KB;
				break;
			case 0x54:
				// 54h - 1.5MByte (96 banks)
				size = 16 * 96 * KB;
				break;
		}
		return size;
	}
	
	/**
	 * Returns the number of external ram banks. Normally, each bank has 8 kb.
	 * 
	 * @return The number of external ram banks.
	 */
	public int getExternalRamBanks() {
		//		 00h - None
		//		 01h - 2 KBytes
		//		 02h - 8 Kbytes
		//		 03h - 32 KBytes (4 banks of 8KBytes each)
		//		 04h - 128 KBytes (16 banks of 8KBytes each)
		//		 05h - 64 KBytes (8 banks of 8KBytes each)int b = rom.read(0x149);
		int b = rom.read(0x149);
		int size = -1;
		switch (b) {
			case 0x00:
				// 00h - None
				size = 0;
				break;
			case 0x01:
				// 01h - 2 KBytes
				size = 1;
				break;
			case 0x02:
				// 02h - 8 Kbytes
				size = 1;
				break;
			case 0x03:
				// 03h - 32 KBytes (4 banks of 8KBytes each)
				size = 4;
				break;
			case 0x04:
				// 128 KBytes (16 banks of 8KBytes each)
				size = 16;
				break;
			case 0x05:
				// 05h - 64 KBytes (8 banks of 8KBytes each)
				size = 8;
				break;
		}
		
		return size;
	}
	
	/**
	 * Returns the size of the ram (if any).
	 * 
	 * @return The size of the ram, can be 0.
	 */
	public int getRamSize() {
		//	 00h - None
		//	 01h - 2 KBytes
		//	 02h - 8 Kbytes
		//	 03h - 32 KBytes (4 banks of 8KBytes each)
		//	 04h - 128 KBytes (16 banks of 8KBytes each)
		//	 05h - 64 KBytes (8 banks of 8KBytes each)
		int b = rom.read(0x149);
		int size = -1;
		switch (b) {
			case 0x00:
				// 00h - None
				size = 0 * KB;
				break;
			case 0x01:
				// 01h - 2 KBytes
				size = 2 * KB;
				break;
			case 0x02:
				// 02h - 8 Kbytes
				size = 8 * KB;
				break;
			case 0x03:
				// 03h - 32 KBytes (4 banks of 8KBytes each)
				size = 32 * KB;
				break;
			case 0x04:
				// 128 KBytes (16 banks of 8KBytes each)
				size = 128 * KB;
				break;
			case 0x05:
				// 05h - 64 KBytes (8 banks of 8KBytes each)
				size = 64 * KB;
				break;
		}
		
		return size;
	}
	
	public JJCartridgeAudioOutput getAudioOutput() {
		return audioOutput;
	}
	
	@Override
	public int read(int address) {
		int retval;
		
		// offset is handled by MBC
		if (0 <= address && address <= 0x3FFF) {
			// 0000-3FFF   16KB ROM Bank 00     (in cartridge, fixed at bank 00)
			retval = mbc.read(address);
		} else if (0x4000 <= address && address <= 0x7FFF) {
			// 4000-7FFF   16KB ROM Bank 01..NN (in cartridge, switchable bank number)
			retval = mbc.read(address);
		} else if (0xA000 <= address && address <= 0xBFFF) {
			// A000-BFFF   8KB External RAM     (in cartridge, switchable bank, if any)
			retval = mbc.read(address);
		} else {
			System.out.printf("Unexpected read access: %x%n", address);
			throw new JJGameBoyException(String.format("Unexpected read access: %x", address));
		}
		
		return retval;
	}

	@Override
	public void write(int address, int value) {
		
		// offset is handled by MBC
		if (0 <= address && address <= 0x3FFF) {
			// 0000-3FFF   16KB ROM Bank 00     (in cartridge, fixed at bank 00)
			mbc.write(address, value);
		} else if (0x4000 <= address && address <= 0x7FFF) {
			// 4000-7FFF   16KB ROM Bank 01..NN (in cartridge, switchable bank number)
			mbc.write(address, value);
		} else if (0xA000 <= address && address <= 0xBFFF) {
			// A000-BFFF   8KB External RAM     (in cartridge, switchable bank, if any)
			mbc.write(address, value);
		} else {
			System.out.printf("Unexpected write access: %x - %d%n", address, value);
			throw new JJGameBoyException(String.format("Unexpected write access: %x - %d", address, value));
		} 
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Cartridge: (0x").append(String.format("%02x", getCartridgeType())).append(") ").append(getTitle()).append(" ").append(getManufacturerCode()).append(", ");
		sb.append("romBanks: (").append(mbc.getRomBankPointer()).append("/").append(getRomBanks()).append("), ");
		sb.append("ramBanks: (").append(mbc.getRamBankPointer()).append("/").append(getExternalRamBanks()).append(")");
		return sb.toString();
	}
	
	public String getSaveFile() {
		return romFile + SAVE_FILE_ENDING;
	}
	
	public boolean existsSaveFile() {
		return Files.exists(FileSystems.getDefault().getPath(getSaveFile()), LinkOption.NOFOLLOW_LINKS);
	}
	
	/**
	 * Dumps the cartridge to the output stream.
	 * 
	 * @param out The output stream to that the cartridge is dumped to.
	 * 
	 * @throws IOException If an {@link IOException} occurs.
	 */
	public void dumpState(OutputStream out) throws IOException {
		mbc.dumpState(out);
	}
	
	/**
	 * Reloads the cartridge from the input stream.
	 * 
	 * @param in The stream to load the cartridge from.
	 * 
	 * @throws IOException If an {@link IOException} occurs.
	 */
	public void reloadState(InputStream in) throws IOException {
		mbc.reloadState(in);
	}
	
}
