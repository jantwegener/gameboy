package jj.gameboy.cartridge;

import jj.gameboy.io.ram.JJAbstractRAM;

/**
 * Not implemented yet. Maybe never to come :D
 * 
 * @author jan-thierry.wegener
 *
 */
public class JJCartridgeAudioOutput extends JJAbstractRAM {

	public JJCartridgeAudioOutput() {
		super(0);
	}

	/**
	 * Always returns 0.
	 */
	@Override
	public int read(int address) {
		return 0;
	}
	
	/**
	 * Does nothing.
	 */
	@Override
	public void write(int address, int value) {
	}

}
