package jj.gameboy.cartridge;

import jj.gameboy.JJGameBoyException;

/**
 * This class represents the exception for unsupported MBCs. This is mainly for debugging reasons
 * and making errors visible during the development phase.
 * 
 * @author Jan-Thierry Wegener
 */
public class JJCartridgeUnsupportedMBCException extends JJGameBoyException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public JJCartridgeUnsupportedMBCException(int type) {
		super(String.format("Unsupported MBC type=%x", type));
	}

}
