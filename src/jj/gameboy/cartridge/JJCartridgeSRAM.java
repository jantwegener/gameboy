package jj.gameboy.cartridge;

import java.io.IOException;

import jj.gameboy.io.ram.JJAbstractRAM;

/**
 * This class represents the SRAM of the cartridge.
 * 
 * @author Jan-Thierry Wegener
 */
public class JJCartridgeSRAM extends JJAbstractRAM {
	
	public JJCartridgeSRAM(int sizeInB) {
		super(sizeInB);
	}
	
	public JJCartridgeSRAM(String file) throws IOException {
		super(file);
	}

	public JJCartridgeSRAM(int sizeInB, int[] data) {
		super(sizeInB, data);
	}
	
}
