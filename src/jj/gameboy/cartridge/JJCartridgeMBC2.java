package jj.gameboy.cartridge;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import jj.gameboy.JJGameBoyException;

/**
 * This class represents the MBC2.
 * 
 * @author Jan-Thierry Wegener
 */
public class JJCartridgeMBC2 extends JJCartridgeMBC  {
	
	private boolean isRamEnabled = false;
	
	public JJCartridgeMBC2(JJCartridge cartridge, JJCartridgeROM rom, int numRomBank, int numRamBank, int ramBankSize, boolean hasBattery) {
		super(cartridge, rom, numRomBank, 1, 512, hasBattery);
		
		setRamBank(0);
	}
	
	public int read(int address) {
		int retval;
		if (0xA000 <= address && address <= 0xBFFF) {
			address = ((address - 0xA000) % 512) + 0xA000;
			retval = 0xF0 | super.read(address);
		} else {
			retval = super.read(address);
		}
		
		return retval;
	}

	@Override
	public void write(int address, int value) {
		int offset;
		
		// normally, the ROM cannot be written to
		// however, some addresses have some special meaning
		if (0 <= address && address <= 0x3FFF) {
			// 0000-3FFF - RAM Enable (Write Only)
			// 00h  Disable RAM (default)
			// 0Ah  Enable RAM
			// The least significant bit of the upper address byte must be zero to enable/disable cart RAM
			if ((address & 0x100) == 0x00) {
				if ((value & 0x0F) == 0x0A) {
					isRamEnabled = true;
				} else {
					isRamEnabled = false;
					if (hasBattery()) {
						getCartridge().saveRAM();
					}
				}
			} else
			// 0000-3FFF - ROM Bank Number (Write Only)
			// Writing a value (XXXXBBBB - X = Don't cares, B = bank select bits) into 2000-3FFF area will select an appropriate ROM bank at 4000-7FFF.
			// The least significant bit of the upper address byte must be one to select a ROM bank.
			if ((address & 0x100) > 0x00) {
				value = value & 0x0F;
				if (value == 0x00) {
					value = 0x01;
				}
				
				setRomBankPointer(value);
			}
		} else if (0x4000 <= address && address <= 0x5FFF) {
			// 4000-7FFF - ROM Bank 01-0F (Read Only)
			System.err.printf("MBC2: Writing not supported %04x%n", address);
		} else if (0x6000 <= address && address <= 0x7FFF) {
			// unused
			System.err.printf("MBC2: Writing not supported %04x%n", address);
		} else if (0xA000 <= address && address <= 0xBFFF) {
			if (isRamEnabled()) {
				// A000-BFFF - RAM Bank 00-03, if any (Read/Write)
				offset = 0xA000;
				value = 0x0F & value;
				getSRAM().write(address - offset, value);
			}
		} else {
			System.out.printf("Unexpected write access: %x - %d%n", address, value);
			throw new JJGameBoyException(String.format("Unexpected write access: %x - %d", address, value));
		}
	}
	
	@Override
	protected boolean isRamEnabled() {
		return isRamEnabled;
	}
	
	@Override
	public void dumpState(OutputStream out) throws IOException {
		out.write(isRamEnabled ? 1 : 0);
		
		super.dumpState(out);
	}
	
	@Override
	public void reloadState(InputStream in) throws IOException {
		isRamEnabled = in.read() == 1;
		
		super.reloadState(in);
	}
	
}
