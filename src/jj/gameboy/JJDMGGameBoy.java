package jj.gameboy;

import java.io.IOException;

import jj.gameboy.util.JJInstructionTracerHelper;

public class JJDMGGameBoy extends JJGameBoy {
	
	private static JJDMGGameBoy INSTANCE;
	
	public JJDMGGameBoy(String bootRomFilePath) {
		super(bootRomFilePath);
		INSTANCE = this;
	}
	
	public static JJDMGGameBoy getInstance() {
		return INSTANCE;
	}
	
	public static void main(String[] args) throws IOException {
		initLogger();
		
		String bootRomFile = "boot_roms\\dmg0_rom.bin";
		
//		String romFile = ... test file;
		JJGameBoy gameboy = new JJDMGGameBoy(bootRomFile);
		gameboy.init();
		
//		if (Boolean.TRUE) {
//			// disable pausing for easier debugging
//			gameboy.setPauseWhenFocusLost(false);
//			
//			// mainly for debugging and fast start without selecting the rom from the menu
//			// boot the game boy
//			gameboy.startUp(romFile);
//			// booting finished, internal rom is disabled, game rom is in place
//			// let's start the game loop
//			gameboy.startGame();
//		}
	}
	
	private static void initLogger() throws IOException {
		JJInstructionTracerHelper.getInstance().addRange(0x60, 0x60);
		JJInstructionTracerHelper.getInstance().setRangeLogging(false);
//		JJInstructionTracerHelper.getInstance().addOpcode(0xCB);
//		JJInstructionTracerHelper.getInstance().addOpcode(0x00);
		JJInstructionTracerHelper.getInstance().setOpcodeLogging(false);
//		JJInstructionTracerHelper.getInstance().setTrailing(200);
//		JJInstructionTracerHelper.getInstance().setAnd(false);
		JJInstructionTracerHelper.getInstance().setBufferSize(10000);
		JJInstructionTracerHelper.getInstance().setAllLogging(false);
//		JJInstructionTracerHelper.getInstance().setStandardLogCallRet();
	}
	
}
