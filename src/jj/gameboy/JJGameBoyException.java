package jj.gameboy;

/**
 * A general class for exceptions within the GameBoy.
 * 
 * @author Jan-Thierry Wegener
 *
 */
public class JJGameBoyException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public JJGameBoyException(Exception e) {
		super(e);
	}
	
	public JJGameBoyException(String msg) {
		super(msg);
	}
	
}
