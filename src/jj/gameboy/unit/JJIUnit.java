package jj.gameboy.unit;

public interface JJIUnit {
	
	/**
	 * Reads from the unit.
	 */
	public int read(int address);
	
	/**
	 * Writes to the unit.
	 */
	public void write(int address, int value);
	
}
