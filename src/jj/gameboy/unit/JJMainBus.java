package jj.gameboy.unit;

import static jj.gameboy.io.ram.JJAbstractRAM.KB;

import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import jj.gameboy.JJGameBoy;
import jj.gameboy.JJGameBoyException;
import jj.gameboy.cartridge.JJCartridge;
import jj.gameboy.input.JJController;
import jj.gameboy.io.JJIOPort;
import jj.gameboy.io.ram.JJHRAM;
import jj.gameboy.io.ram.JJVRAM;
import jj.gameboy.io.ram.JJWRAM;
import jj.gameboy.io.rom.JJBootRom;

public class JJMainBus implements JJIUnit {
	
	/**
	 * The address for firing an interrupt.
	 */
	public static final int ADDRESS_INTERRUPT = 0xFF0F;
	
	private static final int WRAM_BANK_NUM = 8;

	private static final int WRAM_BANK_SIZE = 4 * KB;
	
	private static final int HRAM_SIZE = 4 * KB;

	private JJCPU cpu;
	
	private JJPPU ppu;
	
	private JJAPU apu;
	
	private JJBootRom bootRom;
	
	private JJCartridge cartridge;
	
	private JJController controller;
	
	// working ram
	private JJWRAM[] wram;
	// high ram
	private JJHRAM hram;
	// video ram
	private JJVRAM vram;
	
	
	// I/O port
	JJIOPort ioport;

	// flags
	boolean bootingFlag = true;

	int wramBankPointer = 1;
	int sramBankPointer = 0;
	
	public JJMainBus() {
		// io port
		ioport = new JJIOPort(this);
		cpu = new JJCPU(this);
		ppu = new JJPPU(this);
		apu = new JJAPUSoundEngine(this);
//		apu = new JJAPUExact(this);
		
		controller = new JJController(this);
		
		// ram
		wram = new JJWRAM[WRAM_BANK_NUM];
		for (int i = 0; i < WRAM_BANK_NUM; i++) {
			wram[i] = new JJWRAM(WRAM_BANK_SIZE);
		}
		hram = new JJHRAM(HRAM_SIZE);
		vram = new JJVRAM();
		
	}
	
	public void reset() {
		cpu.reset();
		ppu.reset();
		apu.reset();
		
		vram.reset();
		
		sramBankPointer = 0;
		wramBankPointer = 1;
	}
	
	public JJCPU getCPU() {
		return cpu;
	}

	public JJPPU getPPU() {
		return ppu;
	}

	public JJAPU getAPU() {
		return apu;
	}
	
	public JJCartridge getCartridge() {
		return cartridge;
	}
	
	public JJIOPort getIOPort() {
		return ioport;
	}

	public JJController getController() {
		return controller;
	}
	
	/**
	 * Reads a 16 bit number and returns it.
	 * 
	 * @param address The address in the memory.
	 * 
	 * @return A 16 bit number.
	 */
	public int read16(int address) {
		int v1 = read(address);
		int v2 = read(address + 1);
		int retval = (v2 << 8) | v1;
		return retval;
	}
	
	public void write16(int address, int value) {
		int v1 = (value & 0x00FF);
		int v2 = (value & 0xFF00) >> 8;
		write(address, v1);
		write(address + 1, v2);
	}
	
	/**
	 * Reads the given address as a signed byte.
	 * 
	 * @param address The address in the memory.
	 * 
	 * @return A signed 8 bit number.
	 */
	public int read7(int address) {
		int retval = (byte) read(address);
		return retval;
	}
	
	/**
	 * Reads the given address as an unsigned byte.
	 * 
	 * @param address The address in the memory.
	 * 
	 * @return An unsigned 8 bit number.
	 */
	public int read8(int address) {
		int retval = read(address);
		return retval;
	}
	
	public int[] readRange(int start, int end) {
		int[] pxl = new int[end - start + 1];
		for (int i = 0; i < pxl.length; i++) {
			pxl[i] = read(start + i);
		}
		return pxl;
	}

	@Override
	public int read(int address) {
		/*
		 * 0000-3FFF   16KB ROM Bank 00     (in cartridge, fixed at bank 00)
		 * 4000-7FFF   16KB ROM Bank 01..NN (in cartridge, switchable bank number)
		 * 8000-9FFF   8KB Video RAM (VRAM) (switchable bank 0-1 in CGB Mode)
		 * A000-BFFF   8KB External RAM     (in cartridge, switchable bank, if any)
		 * C000-CFFF   4KB Work RAM Bank 0 (WRAM)
		 * D000-DFFF   4KB Work RAM Bank 1 (WRAM)  (switchable bank 1-7 in CGB Mode)
		 * E000-FDFF   Same as C000-DDFF (ECHO)    (typically not used)
		 * FE00-FE9F   Sprite Attribute Table (OAM)
		 * FEA0-FEFF   Not Usable
		 * FF00-FF7F   I/O Ports
		 * FF80-FFFE   High RAM (HRAM)
		 * FFFF        Interrupt Enable Register
		 */
		
		int retval = -1;
		int offset = 0;

		// during booting time, we only read from the boot rom
		if (bootingFlag) {
			if (0 <= address && address < 0x0100) {
				offset = 0;
				retval = bootRom.read(address - offset);
				return retval;
			}
		}
		
		if (0 <= address && address <= 0x7FFF) {
			// 0000-7FFF 16KB ROM Bank 00, 01..NN
			offset = 0;
			retval = cartridge.read(address - offset);
		} else if (address <= 0x9FFF) {
			// 8000-9FFF   8KB Video RAM (VRAM) (switchable bank 0-1 in CGB Mode)
			offset = 0x8000;
			retval = vram.read(address - offset);
		} else if (address <= 0xBFFF) {
			// A000-BFFF   8KB External RAM     (in cartridge, switchable bank, if any)
			// offset is handled by cartridge
			offset = 0x00;
			retval = cartridge.read(address - offset);
		} else if (address <= 0xCFFF) {
			// C000-CFFF   4KB Work RAM Bank 0 (WRAM)
			offset = 0xC000;
			retval = wram[0].read(address - offset);
		} else if (address <= 0xDFFF) {
			// D000-DFFF   4KB Work RAM Bank 1 (WRAM)  (switchable bank 1-7 in CGB Mode)
			offset = 0xD000;
			retval = wram[wramBankPointer].read(address - offset);
		} else if (address <= 0xFDFF) {
			// E000-FDFF   Same as C000-DDFF (ECHO)    (typically not used)
			offset = 0xE000;
			// do an echo
			retval = read(address - offset + 0xC000);
		} else if (address <= 0xFE9F) {
			// FE00-FE9F   Sprite Attribute Table (OAM)
			offset = 0xFE00;
			retval = ppu.readOAM(address - offset);
		} else if (address <= 0xFEFF) {
			// FEA0-FEFF   Not Usable
			offset = 0xFEA0;
		} else if (address <= 0xFF7F) {
			// FF00-FF7F   I/O Ports
			offset = 0xFF00;
			retval = ioport.read(address - offset);
		} else if (address <= 0xFFFE) {
			// FF80-FFFE   High RAM (HRAM)
			offset = 0xFF80;
			retval = hram.read(address - offset);
		} else if (0xFFFF == address) {
			// FFFF        Interrupt Enable Register
			offset = 0xFFFF;
			retval = cpu.getEnabledInterrupts();
		}
		
		retval = retval & 0xFF;
		
		return retval;
	}
	
	@Override
	public void write(int address, int value) {
		int offset = 0;
		
		if (0 <= address && address <= 0x7FFF) {
			// 0000-7FFF   16KB ROM Bank 00, 01..NN
			offset = 0;
			cartridge.write(address - offset, value);
		} else if (0x8000 <= address && address <= 0x9FFF) {
			// 8000-9FFF   8KB Video RAM (VRAM) (switchable bank 0-1 in CGB Mode)
			offset = 0x8000;
			int oldVal = vram.read(address - offset);
			vram.write(address - offset, value);
			if (oldVal != value) {
				ppu.dirtyRam(address);
			}
		} else if (0xA000 <= address && address <= 0xBFFF) {
			// A000-BFFF   8KB External RAM     (in cartridge, switchable bank, if any)
			// offset is handled by cartridge
			offset = 0x00;
			cartridge.write(address - offset, value);
		} else if (0xC000 <= address && address <= 0xCFFF) {
			// C000-CFFF   4KB Work RAM Bank 0 (WRAM)
			offset = 0xC000;
			wram[0].write(address - offset, value);
		} else if (0xD000 <= address && address <= 0xDFFF) {
			// D000-DFFF   4KB Work RAM Bank 1 (WRAM)  (switchable bank 1-7 in CGB Mode)
			offset = 0xD000;
			wram[wramBankPointer].write(address - offset, value);
		} else if (0xE000 <= address && address <= 0xFDFF) {
			// E000-FDFF   Same as C000-DDFF (ECHO)    (typically not used)
			offset = 0xE000;
			// do an echo
			write(address - offset + 0xC000, value);
		} else if (0xFE00 <= address && address <= 0xFE9F) {
			// FE00-FE9F   Sprite Attribute Table (OAM)
			offset = 0xFE00;
			ppu.writeOAM(address - offset, value);
		} else if (0xFEA0 <= address && address <= 0xFEFF) {
			// FEA0-FEFF   Not Usable
			offset = 0xFEA0;
		} else if (0xFF00 <= address && address <= 0xFF7F) {
			// FF00-FF7F   I/O Ports
			offset = 0xFF00;
			ioport.write(address - offset, value);
		} else if (0xFF80 <= address && address <= 0xFFFE) {
			// FF80-FFFE   High RAM (HRAM)
			offset = 0xFF80;
			hram.write(address - offset, value);
		} else if (0xFFFF == address) {
			// FFFF        Interrupt Enable Register
			offset = 0xFFFF;
			cpu.enableInterrupts(value);
		}
	}

	public void connectCartridge(JJCartridge cartridge) {
		this.cartridge = cartridge;
	}
	
	public void powerOn(JJBootRom bootRom) {
		this.bootRom = bootRom;
		bootingFlag = true;
	}
	
	public void disableInternalRom() {
		bootingFlag = false;
	}
	
	public boolean isBooting() {
		return bootingFlag;
	}

	public void dumpState(ZipOutputStream out) throws IOException {
		ZipEntry gbEntry = new ZipEntry("gb.jj");
		out.putNextEntry(gbEntry);
		out.write(JJGameBoy.VERSION);
		out.closeEntry();
		
		ZipEntry cpuEntry = new ZipEntry("cpu.jj");
		out.putNextEntry(cpuEntry);
		cpu.dumpState(out);
		out.closeEntry();
		
		ZipEntry ppuEntry = new ZipEntry("ppu.jj");
		out.putNextEntry(ppuEntry);
		ppu.dumpState(out);
		out.closeEntry();
		
		ZipEntry apuEntry = new ZipEntry("apu.jj");
		out.putNextEntry(apuEntry);
		apu.dumpState(out);
		out.closeEntry();
		
		ZipEntry cartridgeEntry = new ZipEntry("cartridge.jj");
		out.putNextEntry(cartridgeEntry);
		cartridge.dumpState(out);
		out.closeEntry();
		
		ZipEntry wramEntry = new ZipEntry("wram.jj");
		out.putNextEntry(wramEntry);
		for (int i = 0; i < wram.length; i++) {
			wram[i].dumpState(out);
		}
		out.closeEntry();
		
		ZipEntry hramEntry = new ZipEntry("hram.jj");
		out.putNextEntry(hramEntry);
		hram.dumpState(out);
		out.closeEntry();
		
		ZipEntry vramEntry = new ZipEntry("vram.jj");
		out.putNextEntry(vramEntry);
		vram.dumpState(out);
		out.closeEntry();
	}

	public void reloadState(ZipInputStream in) throws IOException {
		ZipEntry entry;
		while ((entry = in.getNextEntry()) != null) {
			if (entry.getName().equals("gb.jj")) {
				int version = in.read();
				if (!isSupportedVersion(version)) {
					throw new JJGameBoyException("Unsupported state file version " + version + ".");
				}
			} else if (entry.getName().equals("cpu.jj")) {
				cpu.reloadState(in);
			} else if (entry.getName().equals("ppu.jj")) {
				ppu.reloadState(in);
			} else if (entry.getName().equals("apu.jj")) {
				apu.reloadState(in);
			} else if (entry.getName().equals("cartridge.jj")) {
				cartridge.reloadState(in);
			} else if (entry.getName().equals("wram.jj")) {
				for (int i = 0; i < wram.length; i++) {
					wram[i].reloadState(in);
				}
			} else if (entry.getName().equals("hram.jj")) {
				hram.reloadState(in);
			} else if (entry.getName().equals("vram.jj")) {
				vram.reloadState(in);
			}
		}
	}
	
	private boolean isSupportedVersion(int version) {
		return version <= JJGameBoy.VERSION;
	}
	
}
