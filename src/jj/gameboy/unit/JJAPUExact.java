package jj.gameboy.unit;

import jj.gameboy.output.sound.JJAmplifier;
import jj.gameboy.output.sound.JJAmplifierExactOneLine;

/**
 * The audio processing unit (APU).
 * 
 * SO1 = right speaker
 * S02 = left speaker
 * 
 * @author jan-thierry.wegener
 *
 */
public class JJAPUExact extends JJAPU {
	
	public JJAPUExact(JJMainBus bus) {
		super(bus);
	}

	@Override
	protected JJAmplifier initAmplifier() {
		return new JJAmplifierExactOneLine(this);
	}
		
}
