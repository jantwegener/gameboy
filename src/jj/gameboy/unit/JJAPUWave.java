package jj.gameboy.unit;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import jj.gameboy.JJGameBoyException;
import jj.gameboy.output.sound.JJAmplifier;
import jj.gameboy.output.sound.JJAmplifierThread;

/**
 * The audio processing unit (APU).
 * 
 * SO1 = right speaker
 * S02 = left speaker
 * 
 * @author jan-thierry.wegener
 *
 */
public class JJAPUWave extends JJAPU {
	
	/**
	 * The wave duty for channel 1 and 2.
	 */
	private static final int[] WAVE_DUTY = new int[] {
			0b10000000,
			0b11000000,
			0b11110000,
			0b11111100
	};
	
	private JJMainBus bus;
	
	private JJAmplifierThread amplifier;
	
	private static final int SOUND_ALL_ON_FLAG = 0x80;
	private static final int SOUND_4_ON_FLAG = 0x08;
	private static final int SOUND_3_ON_FLAG = 0x04;
	private static final int SOUND_2_ON_FLAG = 0x02;
	private static final int SOUND_1_ON_FLAG = 0x01;
	
	private int cpuClock = 0;
	
	private int frameSequenceTicker = 0;
	
	// wave ram is from 0x30 - 0x3f
	private int[] wavePatternRam = new int[32];
	/**
	 * The pointer to the correct position in the wave ram.
	 */
	private int wavePatternCounter = 0;
	/**
	 * The pointer to the correct position in the duty wave of channel 1.
	 */
	private int channel1DutyCounter = 0;
	/**
	 * The pointer to the correct position in the duty wave of channel 2.
	 */
	private int channel2DutyCounter = 0;
	
	
	/**
	 * Keeps track how often the sweep method is called. This is need to ensure that channelXSweepTime is respected.
	 */
	private int sweepTimeCounter = 0;
	
	private int[] frequencyTimer = new int[4];
	
	private boolean[] channelCtrl = new boolean[4];
	
	private boolean soundEnabled = false;
	private boolean sound4Enabled = false;
	private boolean sound3Enabled = false;
	private boolean sound2Enabled = false;
	private boolean sound1Enabled = false;
	
	private boolean channel1Enabled = false;
	private boolean channel2Enabled = false;
	private boolean channel3Enabled = false;
	private boolean channel4Enabled = false;
	
	private boolean outputVinSO2Left = false;
	private int so2LeftVolume = 0;
	private boolean outputVinSO1Right = false;
	private int so1RightVolume = 0;

	private boolean outputSound1SO1;
	private boolean outputSound2SO1;
	private boolean outputSound3SO1;
	private boolean outputSound4SO1;
	private boolean outputSound1SO2;
	private boolean outputSound2SO2;
	private boolean outputSound3SO2;
	private boolean outputSound4SO2;
	
	private int channel1WavePattern;
	private int channel1SoundLength;
	
	private int channel1InitialVolumeEnvelope;
	private boolean channel1VolumeEnvelopeDirectionIncrease;
	private int channel1VolumeEnvelopeNumberSweep;
	
	private int channel1Frequency;
	private boolean channel1RestartSound;
	private boolean channel1CounterSelection;

	private int channel1SweepTime;
	private int channel1SweepIncrease;
	private int channel1SweepNumber;
	
	private int channel2WavePattern;
	private int channel2SoundLength;
	
	private int channel2InitialVolumeEnvelope;
	private boolean channel2VolumeEnvelopeDirectionIncrease;
	private int channel2VolumeEnvelopeNumberSweep;

	private int channel2Frequency;
	private boolean channel2RestartSound;
	private boolean channel2CounterSelection;

	private int channel3Frequency;
	private boolean channel3RestartSound;
	private boolean channel3CounterSelection;

	private int channel3SoundLength = 0;
	private int channel3OutputLevel = 0;

	private int channel4InitialVolumeEnvelope;
	private boolean channel4VolumeEnvelopeDirectionIncrease;
	private int channel4VolumeEnvelopeNumberSweep;

	private int channel4SoundLength;

	private boolean channel4RestartSound;
	private boolean channel4CounterSelection;
	
	private int channel4ShiftClockFrequency;
	private int channel4CounterWidth;
	private int channel4DividingRatio;
	
	
	public JJAPUWave(JJMainBus bus) {
		super(bus);
		
		this.bus = bus;
		
		amplifier = new JJAmplifierThread();
//		amplifier.start();
		
		for (int i = 0; i < channelCtrl.length; i++) {
			channelCtrl[i] = true;
		}
	}
	
	protected void writeToAmplifier(int cpuCycle) {
		// write to amplifier if sound is enabled
		// and it is either continuous or the counter length is still positive
		if (channelCtrl[0] && sound1Enabled && (!channel1CounterSelection || channel1SoundLength > 0)) {
			amplifier.write(this, 0, cpuCycle);
		}
		// write to amplifier if sound is enabled
		// and it is either continuous or the counter length is still positive
		if (channelCtrl[1] && sound2Enabled && (!channel2CounterSelection || channel2SoundLength > 0)) {
			amplifier.write(this, 1, cpuCycle);
		}
		// write to amplifier if sound is enabled
		// and it is either continuous or the counter length is still positive
		if (channelCtrl[2] && sound3Enabled && (!channel3CounterSelection || channel3SoundLength > 0)) {
			amplifier.write(this, 2, cpuCycle);
		}
		// write to amplifier if sound is enabled
		// and it is either continuous or the counter length is still positive
		if (channelCtrl[3] && sound4Enabled && (!channel4CounterSelection || channel4SoundLength > 0)) {
//			int output = 
			
		}
	}

	@Override
	protected JJAmplifier initAmplifier() {
		// TODO
		return null;
	}
	
}
