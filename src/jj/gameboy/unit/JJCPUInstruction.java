package jj.gameboy.unit;

import jj.gameboy.io.JJUnsupportedOpcodeException;
import jj.gameboy.util.JJInstructionTracerHelper;

/**
 * A class representing the instruction set of the CPU.
 * 
 * @author jan-thierry.wegener
 */
public class JJCPUInstruction {
	
	private JJCPU cpu;
	private JJMainBus bus;
	
	private int previousPC = 0;
	
	private String[] asmCode = new String[0xFFFF];
	private String[] asmParam = new String[0xFFFF];
	
	public JJCPUInstruction(JJCPU cpu, JJMainBus bus) {
		this.cpu = cpu;
		this.bus = bus;
	}
	
	public String[] getAsmCode() {
		return asmCode;
	}
	
	public String[] getAsmParam() {
		return asmParam;
	}
	
	public void resetASM() {
		asmCode = new String[0xFFFF];
		asmParam = new String[0xFFFF];
	}
	
	public int getPreviousPC() {
		return previousPC;
	}
	
	public int next() {
		int pc = cpu.getPC();
		int sp = cpu.getSP();
		int opcode = bus.read(pc);
		
		int cycle = handleOpcode(pc, sp, opcode);
		previousPC = pc;
		return cycle;
	}
	
	/**
	 * Handles the opcode.
	 * 
	 * @param pc The pointer of the currently processed opcode. The pointer is increased at the beginning of this code.
	 * @param sp The current stack pointer.
	 * @param opcode The current opcode where pc is pointing to.
	 * 
	 * @return The number of cycles passed.
	 */
	public int handleOpcode(int pc, int sp, int opcode) {
		// the number of cycles passed after the instruction
		int cycle = 0;
		
		// for debugging reason
		int currPC = pc;
		// increase the pointer to the next byte
		pc++;
		
		int v1;
		int v2;
		
		// for an easy break point
		if (currPC == 0x100) {
			System.out.printf("pc: 0x%x, opcode: 0x%x%n", currPC, opcode);
//			System.out.printf("pc: 0x%x, opcode: 0x%x, 0xFF05: 0x%02x%n", currPC, opcode, bus.read(0xff05));
			System.out.println(cpu);
			System.out.println();
		}
		
		boolean isLogging = JJInstructionTracerHelper.getInstance().isLogging(currPC, opcode);
		
		String cpuTrace = "";
		if (isLogging) {
			cpuTrace = cpu.toString();
		}
		
		String asm = "";
		String param = "";
		try {
			switch (opcode) {
				case 0x00:
					// nop
					if (isLogging) {
						asm = "nop";
					}
					cycle = 4;
					break;
					
				case 0x01:
					// LD BC, d16
					if (isLogging) {
						asm = "LD BC, d16";
						param = String.format("0x%04x", bus.read16(pc));
					}
					cycle = 12;
					v1 = bus.read(pc++);
					v2 = bus.read(pc++);
					cpu.setRegBC(v2, v1);
					break;
					
				case 0x02:
					// LD (BC), A
					if (isLogging) {
						asm = "LD (BC), A";
					}
					cycle = 8;
					v1 = cpu.getRegBC();
					v2 = cpu.getRegA();
					bus.write(v1, v2);
					break;
					
				case 0x03:
					// INC BC
					if (isLogging) {
						asm = "INC BC";
					}
					cycle = 8;
					v1 = cpu.getRegBC();
					v1++;
					cpu.setRegBC(v1);
					break;
					
				case 0x04:
					// INC B
					if (isLogging) {
						asm = "INC B";
					}
					cycle = 4;
					v1 = cpu.getRegB();
					v1++;
					cpu.setRegB(v1);
					
					cpu.setFlagZero((v1 & 0xFF) == 0);
					cpu.setFlagHalf((v1 & 0x0F) == 0x00);
					cpu.setFlagSub(false);
					break;
					
				case 0x05:
					// DEC B
					if (isLogging) {
						asm = "DEC B";
					}
					cycle = 4;
					v1 = cpu.getRegB();
					v1--;
					cpu.setRegB(v1);
					
					cpu.setFlagZero((v1 & 0xFF) == 0);
					cpu.setFlagHalf((v1 & 0x0F) == 0x0F);
					cpu.setFlagSub(true);
					break;
					
				case 0x06:
					// LD B,d8
					if (isLogging) {
						asm = "LD B,d8";
						param = String.format("0x%02x", bus.read8(pc));
					}
					cycle = 8;
					v1 = bus.read(pc++);
					cpu.setRegB(v1);
					break;
					
				case 0x07:
					// RLCA
					if (isLogging) {
						asm = "RLCA";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v1 = v1 << 1 | (v1 >> 7);
					cpu.setRegA(v1);
					
					cpu.setFlags(false, false, false, (v1 & 0x0100) > 0);
					break;
					
				case 0x08:
					// LD (a16),SP
					if (isLogging) {
						asm = "LD (a16),SP";
						param = String.format("0x%04x", bus.read16(pc));
					}
					cycle = 20;
					v1 = bus.read(pc++);
					v2 = bus.read(pc++);
					v1 = (v2 << 8) | v1;
					bus.write(v1, sp & 0xFF);
					bus.write(v1 + 1, (sp & 0xFF00) >> 8);
					break;
					
				case 0x09:
					// ADD HL,BC
					if (isLogging) {
						asm = "ADD HL,BC";
					}
					cycle = 8;
					v1 = cpu.getRegHL();
					v2 = cpu.getRegBC();
					v2 = v1 + v2;
					cpu.setRegHL(v2);
					// - 0 H C
					cpu.setFlagSub(false);
					cpu.setFlagHalf((v1 & 0x0FFF) > (v2 & 0x0FFF));
					cpu.setFlagCarry(v2 > 0xFFFF);
					break;
					
				case 0x0a:
					// LD A,(BC)
					if (isLogging) {
						asm = "LD A,(BC)";
					}
					cycle = 8;
					v1 = cpu.getRegBC();
					v2 = bus.read(v1);
					cpu.setRegA(v2);
					break;
					
				case 0x0b:
					// DEC BC
					if (isLogging) {
						asm = "DEC BC";
					}
					cycle = 8;
					v1 = cpu.getRegBC();
					v1--;
					cpu.setRegBC(v1);
					break;
	
				case 0x0C:
					// INC C
					if (isLogging) {
						asm = "INC C";
					}
					cycle = 4;
					v1 = cpu.getRegC();
					v1++;
					cpu.setRegC(v1);
					
					cpu.setFlagZero((v1 & 0xFF) == 0);
					cpu.setFlagHalf((v1 & 0x0F) == 0x00);
					cpu.setFlagSub(false);
					break;
					
				case 0x0D:
					// DEC C
					if (isLogging) {
						asm = "DEC C";
					}
					cycle = 4;
					v1 = cpu.getRegC();
					v1--;
					cpu.setRegC(v1);
					
					cpu.setFlagZero((v1 & 0xFF) == 0);
					cpu.setFlagSub(true);
					cpu.setFlagHalf((v1 & 0x0F) == 0x0F);
					break;
					
				case 0x0E:
					// LD C,d8
					if (isLogging) {
						asm = "LD C,d8";
						param = String.format("0x%02x", bus.read8(pc));
					}
					cycle = 8;
					v1 = bus.read(pc++);
					cpu.setRegC(v1);
					break;
					
				case 0x0F:
					// RRCA
					if (isLogging) {
						asm = "RRCA";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = (v1 >> 1) | (v1 << 7);
					cpu.setRegA(v2);
					
					cpu.setFlags(false, false, false, (v1 & 0x01) > 0); 
					break;
					
				case 0x10:
					// STOP 0
					if (isLogging) {
						asm = "STOP 0";
					}
					cycle = 4;
					// TODO implement: Enter CPU very low power mode. Also used to switch between double and normal speed CPU modes in GBC.
					break;
					
				case 0x11:
					// LD DE,d16
					if (isLogging) {
						asm = "LD DE,d16";
						param = String.format("0x%04x", bus.read16(pc));
					}
					cycle = 12;
					v1 = bus.read(pc++);
					v2 = bus.read(pc++);
					cpu.setRegDE(v2, v1);
					break;
					
				case 0x12:
					// LD (DE),A
					if (isLogging) {
						asm = "LD (DE),A";
					}
					cycle = 8;
					v1 = cpu.getRegA();
					v2 = cpu.getRegDE();
					bus.write(v2, v1);
					break;
					
				case 0x13:
					// INC DE
					if (isLogging) {
						asm = "INC DE";
					}
					cycle = 8;
					v1 = cpu.getRegDE();
					v1++;
					cpu.setRegDE(v1);
					break;
					
				case 0x14:
					// INC D
					if (isLogging) {
						asm = "INC D";
					}
					cycle = 4;
					v1 = cpu.getRegD();
					v1++;
					cpu.setRegD(v1);
					
					cpu.setFlagZero((v1 & 0xFF) == 0);
					cpu.setFlagSub(false);
					cpu.setFlagHalf((v1 & 0x0F) == 0x00);
					break;
					
				case 0x15:
					// DEC D
					if (isLogging) {
						asm = "DEC D";
					}
					cycle = 4;
					v1 = cpu.getRegD();
					v1--;
					cpu.setRegD(v1);
					
					cpu.setFlagZero((v1 & 0xFF) == 0);
					cpu.setFlagSub(true);
					cpu.setFlagHalf((v1 & 0x0F) == 0x0F);
					break;
					
				case 0x16:
					// LD D,d8
					if (isLogging) {
						asm = "LD D,d8";
						param = String.format("0x%02x", bus.read8(pc));
					}
					cycle = 8;
					v1 = bus.read(pc++);
					cpu.setRegD(v1);
					break;
					
				case 0x17:
					// RLA
					if (isLogging) {
						asm = "RLA";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = (v1 & 0x80) >> 7;
					v1 = (v1 << 1) | (cpu.isCarry() ? 1 : 0);
					cpu.setRegA(v1);
					
					cpu.setFlags(false, false, false, v2 > 0);
					break;
					
				case 0x18:
					// JR r8
					if (isLogging) {
						asm = "JR r8";
						param = String.format("0x%02x (%d)", (pc + bus.read7(pc) + 1), bus.read7(pc));
					}
					cycle = 12;
					v1 = bus.read(pc++);
					pc += (byte) v1;
					break;
					
				case 0x19:
					// ADD HL,DE
					if (isLogging) {
						asm = "ADD HL,DE";
					}
					cycle = 8;
					v1 = cpu.getRegHL();
					v2 = cpu.getRegDE();
					v2 = v1 + v2;
					cpu.setRegHL(v2);
					// - 0 H C
					cpu.setFlagSub(false);
					cpu.setFlagHalf((v1 & 0x0FFF) > (v2 & 0x0FFF));
					cpu.setFlagCarry(v2 > 0xFFFF);
					break;
					
				case 0x1A:
					// LD A,(DE)
					if (isLogging) {
						asm = "LD A,(DE)";
					}
					cycle = 8;
					v1 = cpu.getRegDE();
					v2 = bus.read(v1);
					cpu.setRegA(v2);
					break;
	
				case 0x1B:
					// DEC DE
					if (isLogging) {
						asm = "DEC DE";
					}
					cycle = 8;
					v1 = cpu.getRegDE();
					v1--;
					cpu.setRegDE(v1);
					break;
					
				case 0x1C:
					// INC E
					if (isLogging) {
						asm = "INC E";
					}
					cycle = 4;
					v1 = cpu.getRegE();
					v1++;
					cpu.setRegE(v1);
					
					cpu.setFlagZero((v1 & 0xFF) == 0);
					cpu.setFlagHalf((v1 & 0x0F) == 0x00);
					cpu.setFlagSub(false);
					break;
					
				case 0x1D:
					// DEC E
					if (isLogging) {
						asm = "DEC E";
					}
					cycle = 4;
					v1 = cpu.getRegE();
					v1--;
					cpu.setRegE(v1);
					
					cpu.setFlagZero((v1 & 0xFF) == 0);
					cpu.setFlagSub(true);
					cpu.setFlagHalf((v1 & 0x0F) == 0x0F);
					break;
					
				case 0x1E:
					// LD E,d8
					if (isLogging) {
						asm = "LD E,d8";
						param = String.format("0x%02x", bus.read8(pc));
					}
					cycle = 8;
					v1 = bus.read(pc++);
					cpu.setRegE(v1);
					break;
					
				case 0x1F:
					// RRA
					if (isLogging) {
						asm = "RRA";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = (cpu.isCarry() ? 0x80 : 0) | (v1 >> 1);
					cpu.setRegA(v2);
					// 0 0 0 C
					cpu.setFlags(false, false, false, (v1 & 0x01) > 0); 
					break;
					
				case 0x20:
					// JR NZ,r8
					if (isLogging) {
						asm = "JR NZ,r8";
						param = String.format("0x%02x (%d)", (pc + bus.read7(pc) + 1), bus.read7(pc));
					}
					cycle = 8;
					v1 = bus.read(pc++);
					if (!cpu.isZero()) {
						cycle += 4;
						pc += (byte) v1;
					}
					break;
	
				case 0x21:
					// LD HL,d16
					if (isLogging) {
						asm = "LD HL,d16";
						param = String.format("0x%04x", bus.read16(pc));
					}
					cycle = 12;
					v1 = bus.read(pc++);
					v2 = bus.read(pc++);
					cpu.setRegHL(v2, v1);
					break;
					
				case 0x22:
					// LD (HL+),A
					if (isLogging) {
						asm = "LD (HL+),A";
					}
					cycle = 8;
					v1 = cpu.getRegA();
					v2 = cpu.getRegHL();
					bus.write(v2, v1);
					v2++;
					cpu.setRegHL(v2);
					break;
					
				case 0x23:
					// INC HL
					if (isLogging) {
						asm = "INC HL";
					}
					cycle = 8;
					v1 = cpu.getRegHL();
					v1++;
					cpu.setRegHL(v1);
					break;
					
				case 0x24:
					// INC H
					if (isLogging) {
						asm = "INC H";
					}
					cycle = 4;
					v1 = cpu.getRegH();
					v1++;
					cpu.setRegH(v1);
					
					cpu.setFlagZero((v1 & 0xFF) == 0);
					cpu.setFlagSub(false);
					cpu.setFlagHalf((v1 & 0x0F) == 0x00);
					break;
					
				case 0x25:
					// DEC H
					if (isLogging) {
						asm = "DEC H";
					}
					cycle = 4;
					v1 = cpu.getRegH();
					v1--;
					cpu.setRegH(v1);
					
					cpu.setFlagZero((v1 & 0xFF) == 0);
					cpu.setFlagSub(true);
					cpu.setFlagHalf((v1 & 0x0F) == 0x0F);
					break;
					
				case 0x26:
					// LD H,d8
					if (isLogging) {
						asm = "LD H,d8";
						param = String.format("0x%02x", bus.read8(pc));
					}
					cycle = 8;
					v1 = bus.read(pc++);
					cpu.setRegH(v1);
					break;
					
				case 0x27:
					// DAA
					if (isLogging) {
						asm = "DAA";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					
					if (!cpu.isSubtraction()) {
						if (cpu.isCarry() || v1 > 0x99) {
							v1 = (v1 + 0x60) & 0xFF;
							cpu.setFlagCarry(true);
						}
						if (cpu.isHalfCarry() || (v1 & 0xF) > 0x9) {
							v1 = (v1 + 0x06) & 0xFF;
							cpu.setFlagHalf(false);
						}
					}
					else if (cpu.isCarry() && cpu.isHalfCarry()) {
						v1 = (v1 + 0x9A) & 0xFF;
						cpu.setFlagHalf(false);
					}
					else if (cpu.isCarry()) {
						v1 = (v1 + 0xA0) & 0xFF;
					}
					else if (cpu.isHalfCarry()) {
						v1 = (v1 + 0xFA) & 0xFF;
						cpu.setFlagHalf(false);
					}
					
					cpu.setRegA(v1);
					cpu.setFlagZero(cpu.getRegA() == 0);
					break;
					
				case 0x28:
					// JR Z,r8
					if (isLogging) {
						asm = "JR Z,r8";
						param = String.format("0x%02x (%d)", (pc + bus.read7(pc) + 1), bus.read7(pc));
					}
					cycle = 8;
					v1 = bus.read(pc++);
					if (cpu.isZero()) {
						cycle += 4;
						pc += (byte) v1;
					}
					break;
					
				case 0x29:
					// ADD HL,HL
					if (isLogging) {
						asm = "ADD HL,HL";
					}
					cycle = 8;
					v1 = cpu.getRegHL();
					v2 = cpu.getRegHL();
					v2 = v1 + v2;
					cpu.setRegHL(v2);
					// - 0 H C
					cpu.setFlagSub(false);
					cpu.setFlagHalf((v1 & 0x0FFF) > (v2 & 0x0FFF));
					cpu.setFlagCarry(v2 > 0xFFFF);
					break;
					
				case 0x2A:
					// LD A,(HL+)
					if (isLogging) {
						asm = "LD A,(HL+)";
					}
					cycle = 8;
					v1 = cpu.getRegHL();
					v2 = bus.read(v1);
					cpu.setRegA(v2);
					v1++;
					cpu.setRegHL(v1);
					break;
					
				case 0x2B:
					// DEC HL
					if (isLogging) {
						asm = "DEC HL";
					}
					cycle = 8;
					v1 = cpu.getRegHL();
					v1--;
					cpu.setRegHL(v1);
					break;
					
				case 0x2C:
					// INC L
					if (isLogging) {
						asm = "INC L";
					}
					cycle = 4;
					v1 = cpu.getRegL();
					v1++;
					cpu.setRegL(v1);
					cpu.setFlagZero((v1 & 0xFF) == 0);
					cpu.setFlagHalf((v1 & 0x0F) == 0x00);
					cpu.setFlagSub(false);
					break;
					
				case 0x2D:
					// DEC L
					if (isLogging) {
						asm = "DEC L";
					}
					cycle = 4;
					v1 = cpu.getRegL();
					v1--;
					cpu.setRegL(v1);
					
					cpu.setFlagZero((v1 & 0xFF) == 0);
					cpu.setFlagSub(true);
					cpu.setFlagHalf((v1 & 0x0F) == 0x0F);
					break;
					
				case 0x2E:
					// LD L,d8
					if (isLogging) {
						asm = "LD L,d8";
						param = String.format("0x%02x", bus.read8(pc));
					}
					cycle = 8;
					v1 = bus.read(pc++);
					cpu.setRegL(v1);
					break;
					
				case 0x2F:
					// CPL
					if (isLogging) {
						asm = "CPL";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = v1 ^ 0xFF;
					cpu.setRegA(v2);
					// - 1 1 -
					cpu.setFlagSub(true);
					cpu.setFlagHalf(true);
					break;
					
				case 0x30:
					// JR NC,r8
					if (isLogging) {
						asm = "JR NC,r8";
						param = String.format("0x%02x (%d)", (pc + bus.read7(pc) + 1), bus.read7(pc));
					}
					cycle = 8;
					v1 = bus.read(pc++);
					if (!cpu.isCarry()) {
						cycle += 4;
						pc += (byte) v1;
					}
					break;
					
				case 0x31:
					// LD SP,d16
					if (isLogging) {
						asm = "LD SP,d16";
						param = String.format("0x%04x", bus.read16(pc));
					}
					cycle = 12;
					v1 = bus.read(pc++);
					v2 = bus.read(pc++);
					sp = ((v2 << 8) | v1);
					break;
	
				case 0x32:
					// LD (HL-),A
					if (isLogging) {
						asm = "LD (HL-),A";
					}
					cycle = 8;
					v1 = cpu.getRegA();
					v2 = cpu.getRegHL();
					bus.write(v2, v1);
					v2--;
					cpu.setRegHL(v2);
					break;
					
				case 0x33:
					// INC SP
					if (isLogging) {
						asm = "INC SP";
					}
					cycle = 8;
					sp++;
					break;
					
				case 0x34:
					// INC (HL)
					if (isLogging) {
						asm = "INC (HL)";
					}
					cycle = 12;
					v2 = cpu.getRegHL();
					v1 = bus.read(v2);
					v1++;
					bus.write(v2, v1);
					
					cpu.setFlagZero((v1 & 0xFF) == 0);
					cpu.setFlagSub(false);
					cpu.setFlagHalf((v1 & 0x0F) == 0x00);
					break;
					
				case 0x35:
					// DEC (HL)
					if (isLogging) {
						asm = "DEC (HL)";
					}
					cycle = 4;
					v2 = cpu.getRegHL();
					v1 = bus.read(v2);
					v1--;
					bus.write(v2, v1);
					
					cpu.setFlagZero((v1 & 0xFF) == 0);
					cpu.setFlagSub(true);
					cpu.setFlagHalf((v1 & 0x0F) == 0x0F);
					break;
					
				case 0x36:
					// LD (HL),d8
					if (isLogging) {
						asm = "LD (HL),d8";
						param = String.format("0x%02x", bus.read8(pc));
					}
					cycle = 12;
					v1 = cpu.getRegHL();
					v2 = bus.read(pc++);
					bus.write(v1, v2);
					break;
					
				case 0x37:
					// SCF
					if (isLogging) {
						asm = "SCF";
					}
					cycle = 4;
					cpu.setFlagSub(false);
					cpu.setFlagHalf(false);
					cpu.setFlagCarry(true);
					break;
					
				case 0x38:
					// JR C,r8
					if (isLogging) {
						asm = "JR C,r8";
						param = String.format("0x%02x (%d)", (pc + bus.read7(pc) + 1), bus.read7(pc));
					}
					cycle = 8;
					v1 = bus.read(pc++);
					if (cpu.isCarry()) {
						cycle += 4;
						pc += (byte) v1;
					}
					break;
					
				case 0x39:
					// ADD HL,SP
					if (isLogging) {
						asm = "ADD HL,SP";
					}
					cycle = 8;
					v1 = cpu.getRegHL();
					v2 = cpu.getSP();
					v2 = v1 + v2;
					cpu.setRegHL(v2);
					// - 0 H C
					cpu.setFlagSub(false);
					cpu.setFlagHalf((v1 & 0x0FFF) > (v2 & 0x0FFF));
					cpu.setFlagCarry(v2 > 0xFFFF);
					break;
					
				case 0x3A:
					// LD A,(HL-)
					if (isLogging) {
						asm = "LD A,(HL-)";
					}
					cycle = 8;
					v1 = cpu.getRegHL();
					v2 = bus.read(v1);
					cpu.setRegA(v2);
					v1--;
					cpu.setRegHL(v1);
					break;
					
				case 0x3B:
					// DEC SP
					if (isLogging) {
						asm = "DEC SP";
					}
					cycle = 8;
					sp--;
					break;
					
				case 0x3C:
					// INC A
					if (isLogging) {
						asm = "INC A";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v1++;
					cpu.setRegA(v1);
					
					cpu.setFlagZero((v1 & 0xFF) == 0);
					cpu.setFlagHalf((v1 & 0x0F) == 0x00);
					cpu.setFlagSub(false);
					break;
					
				case 0x3D:
					// DEC A
					if (isLogging) {
						asm = "DEC A";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v1--;
					cpu.setRegA(v1);
					
					cpu.setFlagZero((v1 & 0xFF) == 0);
					cpu.setFlagSub(true);
					cpu.setFlagHalf((v1 & 0x0F) == 0x0F);
					break;
					
				case 0x3E:
					// LD A,d8
					if (isLogging) {
						asm = "LD A,d8";
						param = String.format("0x%02x", bus.read8(pc));
					}
					cycle = 8;
					v1 = bus.read(pc++);
					cpu.setRegA(v1);
					break;
					
				case 0x3F:
					// CCF
					if (isLogging) {
						asm = "CCF";
					}
					cycle = 4;
					// - 0 0 C
					cpu.setFlagSub(false);
					cpu.setFlagHalf(false);
					cpu.setFlagCarry(!cpu.isCarry());
					break;
					
				case 0x40:
					// LD B,B
					if (isLogging) {
						asm = "LD B,B";
					}
					cycle = 4;
					v1 = cpu.getRegB();
					cpu.setRegB(v1);
					break;
					
				case 0x41:
					// LD B,C
					if (isLogging) {
						asm = "LD B,C";
					}
					cycle = 4;
					v1 = cpu.getRegC();
					cpu.setRegB(v1);
					break;
					
				case 0x42:
					// LD B,D
					if (isLogging) {
						asm = "LD B,D";
					}
					cycle = 4;
					v1 = cpu.getRegD();
					cpu.setRegB(v1);
					break;
					
				case 0x43:
					// LD B,E
					if (isLogging) {
						asm = "LD B,E";
					}
					cycle = 4;
					v1 = cpu.getRegE();
					cpu.setRegB(v1);
					break;
					
				case 0x44:
					// LD B,H
					if (isLogging) {
						asm = "LD B,H";
					}
					cycle = 4;
					v1 = cpu.getRegH();
					cpu.setRegB(v1);
					break;
					
				case 0x45:
					// LD B,L
					if (isLogging) {
						asm = "LD B,L";
					}
					cycle = 4;
					v1 = cpu.getRegL();
					cpu.setRegB(v1);
					break;
					
				case 0x46:
					// LD B,(HL)
					if (isLogging) {
						asm = "LD B,(HL)";
					}
					cycle = 4;
					v2 = cpu.getRegHL();
					v1 = bus.read(v2);
					cpu.setRegB(v1);
					break;
					
				case 0x47:
					// LD B,A
					if (isLogging) {
						asm = "LD B,A";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					cpu.setRegB(v1);
					break;
					
				case 0x48:
					// LD C,B
					if (isLogging) {
						asm = "LD C,B";
					}
					cycle = 4;
					v1 = cpu.getRegB();
					cpu.setRegC(v1);
					break;
					
				case 0x49:
					// LD C,C
					if (isLogging) {
						asm = "LD C,C";
					}
					cycle = 4;
					v1 = cpu.getRegC();
					cpu.setRegC(v1);
					break;
					
				case 0x4A:
					// LD C,D
					if (isLogging) {
						asm = "LD C,D";
					}
					cycle = 4;
					v1 = cpu.getRegD();
					cpu.setRegC(v1);
					break;
					
				case 0x4B:
					// LD C,E
					if (isLogging) {
						asm = "LD C,E";
					}
					cycle = 4;
					v1 = cpu.getRegE();
					cpu.setRegC(v1);
					break;
					
				case 0x4C:
					// LD C,H
					if (isLogging) {
						asm = "LD C,H";
					}
					cycle = 4;
					v1 = cpu.getRegH();
					cpu.setRegC(v1);
					break;
					
				case 0x4D:
					// LD C,L
					if (isLogging) {
						asm = "LD C,L";
					}
					cycle = 4;
					v1 = cpu.getRegL();
					cpu.setRegC(v1);
					break;
					
				case 0x4E:
					// LD C,(HL)
					if (isLogging) {
						asm = "LD C,(HL)";
					}
					cycle = 4;
					v2 = cpu.getRegHL();
					v1 = bus.read(v2);
					cpu.setRegC(v1);
					break;
					
				case 0x4F:
					// LD C,A
					if (isLogging) {
						asm = "LD C,A";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					cpu.setRegC(v1);
					break;
					
				case 0x50:
					// LD D,B
					if (isLogging) {
						asm = "LD D,B";
					}
					cycle = 4;
					v1 = cpu.getRegB();
					cpu.setRegD(v1);
					break;
					
				case 0x51:
					// LD D,C
					if (isLogging) {
						asm = "LD D,C";
					}
					cycle = 4;
					v1 = cpu.getRegC();
					cpu.setRegD(v1);
					break;
					
				case 0x52:
					// LD D,D
					if (isLogging) {
						asm = "LD D,D";
					}
					cycle = 4;
					v1 = cpu.getRegD();
					cpu.setRegD(v1);
					break;
					
				case 0x53:
					// LD D,E
					if (isLogging) {
						asm = "LD D,E";
					}
					cycle = 4;
					v1 = cpu.getRegE();
					cpu.setRegD(v1);
					break;
					
				case 0x54:
					// LD D,H
					if (isLogging) {
						asm = "LD D,H";
					}
					cycle = 4;
					v1 = cpu.getRegH();
					cpu.setRegD(v1);
					break;
					
				case 0x55:
					// LD D,L
					if (isLogging) {
						asm = "LD D,L";
					}
					cycle = 4;
					v1 = cpu.getRegL();
					cpu.setRegD(v1);
					break;
					
				case 0x56:
					// LD D,(HL)
					if (isLogging) {
						asm = "LD D,(HL)";
					}
					cycle = 4;
					v2 = cpu.getRegHL();
					v1 = bus.read(v2);
					cpu.setRegD(v1);
					break;
					
				case 0x57:
					// LD D,A
					if (isLogging) {
						asm = "LD D,A";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					cpu.setRegD(v1);
					break;
					
				case 0x58:
					// LD E,B
					if (isLogging) {
						asm = "LD E,B";
					}
					cycle = 4;
					v1 = cpu.getRegB();
					cpu.setRegE(v1);
					break;
					
				case 0x59:
					// LD E,C
					if (isLogging) {
						asm = "LD E,C";
					}
					cycle = 4;
					v1 = cpu.getRegC();
					cpu.setRegE(v1);
					break;
					
				case 0x5A:
					// LD E,D
					if (isLogging) {
						asm = "LD E,D";
					}
					cycle = 4;
					v1 = cpu.getRegD();
					cpu.setRegE(v1);
					break;
					
				case 0x5B:
					// LD E,E
					if (isLogging) {
						asm = "LD E,E";
					}
					cycle = 4;
					v1 = cpu.getRegE();
					cpu.setRegE(v1);
					break;
					
				case 0x5C:
					// LD E,H
					if (isLogging) {
						asm = "LD E,H";
					}
					cycle = 4;
					v1 = cpu.getRegH();
					cpu.setRegE(v1);
					break;
					
				case 0x5D:
					// LD E,L
					if (isLogging) {
						asm = "LD E,L";
					}
					cycle = 4;
					v1 = cpu.getRegL();
					cpu.setRegE(v1);
					break;
					
				case 0x5E:
					// LD E,(HL)
					if (isLogging) {
						asm = "LD E,(HL)";
					}
					cycle = 8;
					v2 = cpu.getRegHL();
					v1 = bus.read(v2);
					cpu.setRegE(v1);
					break;
					
				case 0x5F:
					// LD E,A
					if (isLogging) {
						asm = "LD E,A";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					cpu.setRegE(v1);
					break;
					
				case 0x60:
					// LD H,B
					if (isLogging) {
						asm = "LD H,B";
					}
					cycle = 4;
					v1 = cpu.getRegB();
					cpu.setRegH(v1);
					break;
					
				case 0x61:
					// LD H,C
					if (isLogging) {
						asm = "LD H,C";
					}
					cycle = 4;
					v1 = cpu.getRegC();
					cpu.setRegH(v1);
					break;
					
				case 0x62:
					// LD H,D
					if (isLogging) {
						asm = "LD H,D";
					}
					cycle = 4;
					v1 = cpu.getRegD();
					cpu.setRegH(v1);
					break;
					
				case 0x63:
					// LD H,E
					if (isLogging) {
						asm = "LD H,E";
					}
					cycle = 4;
					v1 = cpu.getRegE();
					cpu.setRegH(v1);
					break;
					
				case 0x64:
					// LD H,H
					if (isLogging) {
						asm = "LD H,H";
					}
					cycle = 4;
					v1 = cpu.getRegH();
					cpu.setRegH(v1);
					break;
					
				case 0x65:
					// LD H,L
					if (isLogging) {
						asm = "LD H,L";
					}
					cycle = 4;
					v1 = cpu.getRegL();
					cpu.setRegH(v1);
					break;
					
				case 0x66:
					// LD H,(HL)
					if (isLogging) {
						asm = "LD H,(HL)";
					}
					cycle = 8;
					v1 = cpu.getRegHL();
					v2 = bus.read(v1);
					cpu.setRegH(v2);
					break;
					
				case 0x67:
					// LD H,A
					if (isLogging) {
						asm = "LD H,A";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					cpu.setRegH(v1);
					break;
					
				case 0x68:
					// LD L,B
					if (isLogging) {
						asm = "LD L,B";
					}
					cycle = 4;
					v1 = cpu.getRegB();
					cpu.setRegL(v1);
					break;
					
				case 0x69:
					// LD L,C
					if (isLogging) {
						asm = "LD L,C";
					}
					cycle = 4;
					v1 = cpu.getRegC();
					cpu.setRegL(v1);
					break;
					
				case 0x6A:
					// LD L,D
					if (isLogging) {
						asm = "LD L,D";
					}
					cycle = 4;
					v1 = cpu.getRegD();
					cpu.setRegL(v1);
					break;
					
				case 0x6B:
					// LD L,E
					if (isLogging) {
						asm = "LD L,E";
					}
					cycle = 4;
					v1 = cpu.getRegE();
					cpu.setRegL(v1);
					break;
					
				case 0x6C:
					// LD L,H
					if (isLogging) {
						asm = "LD L,H";
					}
					cycle = 4;
					v1 = cpu.getRegH();
					cpu.setRegL(v1);
					break;
					
				case 0x6D:
					// LD L,L
					if (isLogging) {
						asm = "LD L,L";
					}
					cycle = 4;
					v1 = cpu.getRegL();
					cpu.setRegL(v1);
					break;
					
				case 0x6E:
					// LD L,(HL)
					if (isLogging) {
						asm = "LD L,(HL)";
					}
					cycle = 4;
					v2 = cpu.getRegHL();
					v1 = bus.read(v2);
					cpu.setRegL(v1);
					break;
					
				case 0x6F:
					// LD L,A
					if (isLogging) {
						asm = "LD L,A";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					cpu.setRegL(v1);
					break;
					
				case 0x70:
					// LD (HL),B
					if (isLogging) {
						asm = "LD (HL),B";
					}
					cycle = 4;
					v1 = cpu.getRegB();
					v2 = cpu.getRegHL();
					bus.write(v2, v1);
					break;
					
				case 0x71:
					// LD (HL),C
					if (isLogging) {
						asm = "LD (HL),C";
					}
					cycle = 4;
					v1 = cpu.getRegC();
					v2 = cpu.getRegHL();
					bus.write(v2, v1);
					break;
					
				case 0x72:
					// LD (HL),D
					if (isLogging) {
						asm = "LD (HL),D";
					}
					cycle = 4;
					v1 = cpu.getRegD();
					v2 = cpu.getRegHL();
					bus.write(v2, v1);
					break;
					
				case 0x73:
					// LD (HL),E
					if (isLogging) {
						asm = "LD (HL),E";
					}
					cycle = 4;
					v1 = cpu.getRegE();
					v2 = cpu.getRegHL();
					bus.write(v2, v1);
					break;
					
				case 0x74:
					// LD (HL),H
					if (isLogging) {
						asm = "LD (HL),H";
					}
					cycle = 4;
					v1 = cpu.getRegH();
					v2 = cpu.getRegHL();
					bus.write(v2, v1);
					break;
					
				case 0x75:
					// LD (HL),L
					if (isLogging) {
						asm = "LD (HL),L";
					}
					cycle = 4;
					v1 = cpu.getRegL();
					v2 = cpu.getRegHL();
					bus.write(v2, v1);
					break;
					
				case 0x76:
					// HALT
					if (isLogging) {
						asm = "HALT";
					}
					cycle = 4;
					// TODO 
					break;
					
				case 0x77:
					// LD (HL),A
					if (isLogging) {
						asm = "LD (HL),A";
					}
					cycle = 8;
					v1 = cpu.getRegHL();
					v2 = cpu.getRegA();
					bus.write(v1, v2);
					break;
					
				case 0x78:
					// LD A,B
					if (isLogging) {
						asm = "LD A,B";
					}
					cycle = 4;
					v1 = cpu.getRegB();
					cpu.setRegA(v1);
					break;
					
				case 0x79:
					// LD A,C
					if (isLogging) {
						asm = "LD A,C";
					}
					cycle = 4;
					v1 = cpu.getRegC();
					cpu.setRegA(v1);
					break;
					
				case 0x7A:
					// LD A,D
					if (isLogging) {
						asm = "LD A,D";
					}
					cycle = 4;
					v1 = cpu.getRegD();
					cpu.setRegA(v1);
					break;
					
				case 0x7B:
					// LD A,E
					if (isLogging) {
						asm = "LD A,E";
					}
					cycle = 4;
					v1 = cpu.getRegE();
					cpu.setRegA(v1);
					break;
					
				case 0x7C:
					// LD A,H
					if (isLogging) {
						asm = "LD A,H";
					}
					cycle = 4;
					v1 = cpu.getRegH();
					cpu.setRegA(v1);
					break;
					
				case 0x7D:
					// LD A,L
					if (isLogging) {
						asm = "LD A,L";
					}
					cycle = 4;
					v1 = cpu.getRegL();
					cpu.setRegA(v1);
					break;
					
				case 0x7E:
					// LD A,(HL)
					if (isLogging) {
						asm = "LD A,(HL)";
					}
					cycle = 4;
					v1 = cpu.getRegHL();
					v2 = bus.read(v1);
					cpu.setRegA(v2);
					break;
					
				case 0x7F:
					// LD A,A
					if (isLogging) {
						asm = "LD A,A";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					cpu.setRegA(v1);
					break;
					
				case 0x80:
					// ADD A,B
					if (isLogging) {
						asm = "ADD A,B";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegB();
					v2 = v1 + v2;
					cpu.setRegA(v2);
					
					cpu.setFlags((v2 & 0xFF) == 0, false, (v2 & 0x0F) < (v1 & 0x0F), v2 > 0xFF);
					break;
					
				case 0x81:
					// ADD A,c
					if (isLogging) {
						asm = "ADD A,C";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegC();
					v2 = v1 + v2;
					cpu.setRegA(v2);
					
					cpu.setFlags((v2 & 0xFF) == 0, false, (v2 & 0x0F) < (v1 & 0x0F), v2 > 0xFF);
					break;
					
				case 0x82:
					// ADD A,D
					if (isLogging) {
						asm = "ADD A,D";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegD();
					v2 = v1 + v2;
					cpu.setRegA(v2);
					
					cpu.setFlags((v2 & 0xFF) == 0, false, (v2 & 0x0F) < (v1 & 0x0F), v2 > 0xFF);
					break;
					
				case 0x83:
					// ADD A,E
					if (isLogging) {
						asm = "ADD A,E";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegE();
					v2 = v1 + v2;
					cpu.setRegA(v2);
					
					cpu.setFlags((v2 & 0xFF) == 0, false, (v2 & 0x0F) < (v1 & 0x0F), v2 > 0xFF);
					break;
					
				case 0x84:
					// ADD A,H
					if (isLogging) {
						asm = "ADD A,H";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegH();
					v2 = v1 + v2;
					cpu.setRegA(v2);
					
					cpu.setFlags((v2 & 0xFF) == 0, false, (v2 & 0x0F) < (v1 & 0x0F), v2 > 0xFF);
					break;
					
				case 0x85:
					// ADD A,L
					if (isLogging) {
						asm = "ADD A,L";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegL();
					v2 = v1 + v2;
					cpu.setRegA(v2);
					
					cpu.setFlags((v2 & 0xFF) == 0, false, (v2 & 0x0F) < (v1 & 0x0F), v2 > 0xFF);
					break;
					
				case 0x86:
					// ADD A,(HL)
					if (isLogging) {
						asm = "ADD A,(HL)";
					}
					cycle = 8;
					v1 = cpu.getRegA();
					v2 = cpu.getRegHL();
					v2 = bus.read(v2);
					v2 = v1 + v2;
					cpu.setRegA(v2);
					
					cpu.setFlags((v2 & 0xFF) == 0, false, (v2 & 0x0F) < (v1 & 0x0F), v2 > 0xFF);
					break;
					
				case 0x87:
					// ADD A,A
					if (isLogging) {
						asm = "ADD A,A";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegA();
					v2 = v1 + v2;
					cpu.setRegA(v2);
					
					cpu.setFlags((v2 & 0xFF) == 0, false, (v2 & 0x0F) < (v1 & 0x0F), v2 > 0xFF);
					break;
					
				case 0x88:
					// ADC A,B
					if (isLogging) {
						asm = "ADC A,B";
					}
					cycle = 4;
					v1 = cpu.getRegB();
					v2 = cpu.getRegA();
					
					// half carry
					cpu.setFlagHalf((v1 & 0x0F) + (v2 & 0x0F) + (cpu.isCarry() ? 1 : 0) > 0x0F);
					
					v1 = v1 + v2 + (cpu.isCarry() ? 1 : 0);
					cpu.setRegA(v1);
					// Z 0 H C
					cpu.setFlags((v1 & 0xFF) == 0, false, cpu.isHalfCarry(), v1 > 0xFF);
					break;
					
				case 0x89:
					// ADC A,C
					if (isLogging) {
						asm = "ADC A,C";
					}
					v1 = cpu.getRegC();
					v2 = cpu.getRegA();
					
					// half carry
					cpu.setFlagHalf((v1 & 0x0F) + (v2 & 0x0F) + (cpu.isCarry() ? 1 : 0) > 0x0F);
					
					v1 = v1 + v2 + (cpu.isCarry() ? 1 : 0);
					cpu.setRegA(v1);
					// Z 0 H C
					cpu.setFlags((v1 & 0xFF) == 0, false, cpu.isHalfCarry(), v1 > 0xFF);
					break;
					
				case 0x8A:
					// ADC A,D
					if (isLogging) {
						asm = "ADC A,D";
					}
					cycle = 4;
					v1 = cpu.getRegD();
					v2 = cpu.getRegA();
					
					// half carry
					cpu.setFlagHalf((v1 & 0x0F) + (v2 & 0x0F) + (cpu.isCarry() ? 1 : 0) > 0x0F);
					
					v1 = v1 + v2 + (cpu.isCarry() ? 1 : 0);
					cpu.setRegA(v1);
					// Z 0 H C
					cpu.setFlags((v1 & 0xFF) == 0, false, cpu.isHalfCarry(), v1 > 0xFF);
					break;
					
				case 0x8B:
					// ADC A,E
					if (isLogging) {
						asm = "ADC A,E";
					}
					cycle = 4;
					v1 = cpu.getRegE();
					v2 = cpu.getRegA();
					
					// half carry
					cpu.setFlagHalf((v1 & 0x0F) + (v2 & 0x0F) + (cpu.isCarry() ? 1 : 0) > 0x0F);
					
					v1 = v1 + v2 + (cpu.isCarry() ? 1 : 0);
					cpu.setRegA(v1);
					// Z 0 H C
					cpu.setFlags((v1 & 0xFF) == 0, false, cpu.isHalfCarry(), v1 > 0xFF);
					break;
					
				case 0x8C:
					// ADC A,H
					if (isLogging) {
						asm = "ADC A,H";
					}
					cycle = 4;
					v1 = cpu.getRegH();
					v2 = cpu.getRegA();
					
					// half carry
					cpu.setFlagHalf((v1 & 0x0F) + (v2 & 0x0F) + (cpu.isCarry() ? 1 : 0) > 0x0F);
					
					v1 = v1 + v2 + (cpu.isCarry() ? 1 : 0);
					cpu.setRegA(v1);
					// Z 0 H C
					cpu.setFlags((v1 & 0xFF) == 0, false, cpu.isHalfCarry(), v1 > 0xFF);
					break;
					
				case 0x8D:
					// ADC A,L
					if (isLogging) {
						asm = "ADC A,L";
					}
					cycle = 4;
					v1 = cpu.getRegL();
					v2 = cpu.getRegA();
					
					// half carry
					cpu.setFlagHalf((v1 & 0x0F) + (v2 & 0x0F) + (cpu.isCarry() ? 1 : 0) > 0x0F);
					
					v1 = v1 + v2 + (cpu.isCarry() ? 1 : 0);
					cpu.setRegA(v1);
					// Z 0 H C
					cpu.setFlags((v1 & 0xFF) == 0, false, cpu.isHalfCarry(), v1 > 0xFF);
					break;
					
				case 0x8E:
					// ADC A,(HL)
					if (isLogging) {
						asm = "ADC A,(HL)";
						param = String.format("0x%02x", bus.read8(cpu.getRegHL()));
					}
					cycle = 8;
					v1 = cpu.getRegHL();
					v1 = bus.read(v1);
					v2 = cpu.getRegA();
					
					// half carry
					cpu.setFlagHalf((v1 & 0x0F) + (v2 & 0x0F) + (cpu.isCarry() ? 1 : 0) > 0x0F);
					
					v1 = v1 + v2 + (cpu.isCarry() ? 1 : 0);
					cpu.setRegA(v1);
					// Z 0 H C
					cpu.setFlags((v1 & 0xFF) == 0, false, cpu.isHalfCarry(), v1 > 0xFF);
					break;
					
				case 0x8F:
					// ADC A,A
					if (isLogging) {
						asm = "ADC A,A";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegA();
					
					// half carry
					cpu.setFlagHalf((v1 & 0x0F) + (v2 & 0x0F) + (cpu.isCarry() ? 1 : 0) > 0x0F);
					
					v1 = v1 + v2 + (cpu.isCarry() ? 1 : 0);
					cpu.setRegA(v1);
					// Z 0 H C
					cpu.setFlags((v1 & 0xFF) == 0, false, cpu.isHalfCarry(), v1 > 0xFF);
					break;
					
				case 0x90:
					// SUB B
					if (isLogging) {
						asm = "SUB B";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegB();
					v2 = v1 - v2;
					cpu.setRegA(v2);
					
					cpu.setFlags((v2 & 0xFF) == 0, true, (v1 & 0x0F) < (v2 & 0x0F), v2 < 0);
					break;
	
				case 0x91:
					// SUB C
					if (isLogging) {
						asm = "SUB C";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegC();
					v2 = v1 - v2;
					cpu.setRegA(v2);
					
					cpu.setFlags((v2 & 0xFF) == 0, true, (v1 & 0x0F) < (v2 & 0x0F), v2 < 0);
					break;
	
				case 0x92:
					// SUB D
					if (isLogging) {
						asm = "SUB D";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegD();
					v2 = v1 - v2;
					cpu.setRegA(v2);
					
					cpu.setFlags((v2 & 0xFF) == 0, true, (v1 & 0x0F) < (v2 & 0x0F), v2 < 0);
					break;
					
				case 0x93:
					// SUB E
					if (isLogging) {
						asm = "SUB E";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegE();
					v2 = v1 - v2;
					cpu.setRegA(v2);
					
					cpu.setFlags((v2 & 0xFF) == 0, true, (v1 & 0x0F) < (v2 & 0x0F), v2 < 0);
					break;
					
				case 0x94:
					// SUB H
					if (isLogging) {
						asm = "SUB H";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegH();
					v2 = v1 - v2;
					cpu.setRegA(v2);
					
					cpu.setFlags((v2 & 0xFF) == 0, true, (v1 & 0x0F) < (v2 & 0x0F), v2 < 0);
					break;
					
				case 0x95:
					// SUB L
					if (isLogging) {
						asm = "SUB L";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegL();
					v2 = v1 - v2;
					cpu.setRegA(v2);
					
					cpu.setFlags((v2 & 0xFF) == 0, true, (v1 & 0x0F) < (v2 & 0x0F), v2 < 0);
					break;
					
				case 0x96:
					// SUB (HL)
					if (isLogging) {
						asm = "SUB (HL)";
					}
					cycle = 8;
					v1 = cpu.getRegA();
					v2 = cpu.getRegHL();
					v2 = bus.read(v2);
					v2 = v1 - v2;
					cpu.setRegA(v2);
					
					cpu.setFlags((v2 & 0xFF) == 0, true, (v1 & 0x0F) < (v2 & 0x0F), v2 < 0);
					break;
					
				case 0x97:
					// SUB A
					if (isLogging) {
						asm = "SUB A";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegA();
					v2 = v1 - v2;
					cpu.setRegA(v2);
					
					cpu.setFlags((v2 & 0xFF) == 0, true, (v1 & 0x0F) < (v2 & 0x0F), v2 < 0);
					break;
					
				case 0x98:
					// SBC A,B
					if (isLogging) {
						asm = "SBC A,B";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegB();
					v2 = v1 - v2 - (cpu.isCarry() ? 1 : 0);
					cpu.setRegA(v2);
					
					cpu.setFlags((v2 & 0xFF) == 0, true, (v1 & 0x0f) - (cpu.getRegB() & 0x0f) - (cpu.isCarry() ? 1 : 0) < 0, v2 < 0);
					break;
					
				case 0x99:
					// SBC A,C
					if (isLogging) {
						asm = "SBC A,C";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegC();
					v2 = v1 - v2 - (cpu.isCarry() ? 1 : 0);
					cpu.setRegA(v2);
					
					cpu.setFlags((v2 & 0xFF) == 0, true, (v1 & 0x0f) - (cpu.getRegC() & 0x0f) - (cpu.isCarry() ? 1 : 0) < 0, v2 < 0);
					break;
					
				case 0x9A:
					// SBC A,D
					if (isLogging) {
						asm = "SBC A,D";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegD();
					v2 = v1 - v2 - (cpu.isCarry() ? 1 : 0);
					cpu.setRegA(v2);
					
					cpu.setFlags((v2 & 0xFF) == 0, true, (v1 & 0x0f) - (cpu.getRegD() & 0x0f) - (cpu.isCarry() ? 1 : 0) < 0, v2 < 0);
					break;
					
				case 0x9B:
					// SBC A,E
					if (isLogging) {
						asm = "SBC A,E";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegE();
					v2 = v1 - v2 - (cpu.isCarry() ? 1 : 0);
					cpu.setRegA(v2);
					
					cpu.setFlags((v2 & 0xFF) == 0, true, (v1 & 0x0f) - (cpu.getRegE() & 0x0f) - (cpu.isCarry() ? 1 : 0) < 0, v2 < 0);
					break;
					
				case 0x9C:
					// SBC A,H
					if (isLogging) {
						asm = "SBC A,H";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegH();
					v2 = v1 - v2 - (cpu.isCarry() ? 1 : 0);
					cpu.setRegA(v2);
					
					cpu.setFlags((v2 & 0xFF) == 0, true, (v1 & 0x0f) - (cpu.getRegH() & 0x0f) - (cpu.isCarry() ? 1 : 0) < 0, v2 < 0);
					break;
					
				case 0x9D:
					// SBC A,L
					if (isLogging) {
						asm = "SBC A,L";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegL();
					v2 = v1 - v2 - (cpu.isCarry() ? 1 : 0);
					cpu.setRegA(v2);
					
					cpu.setFlags((v2 & 0xFF) == 0, true, (v1 & 0x0f) - (cpu.getRegL() & 0x0f) - (cpu.isCarry() ? 1 : 0) < 0, v2 < 0);
					break;
					
				case 0x9E:
					// SBC A,(HL)
					if (isLogging) {
						asm = "SBC A,(HL)";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegHL();
					v2 = bus.read(v2);
					v2 = v1 - v2 - (cpu.isCarry() ? 1 : 0);
					cpu.setRegA(v2);
					
					cpu.setFlags((v2 & 0xFF) == 0, true, (v1 & 0x0f) - (bus.read(cpu.getRegHL()) & 0x0f) - (cpu.isCarry() ? 1 : 0) < 0, v2 < 0);
					break;
					
				case 0x9F:
					// SBC A,A
					if (isLogging) {
						asm = "SBC A,A";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegA();
					v2 = v1 - v2 - (cpu.isCarry() ? 1 : 0);
					cpu.setRegA(v2);
					
					cpu.setFlags((v2 & 0xFF) == 0, true, (v1 & 0x0f) - (cpu.getRegA() & 0x0f) - (cpu.isCarry() ? 1 : 0) < 0, v2 < 0);
					break;
					
				case 0xA0:
					// AND B
					if (isLogging) {
						asm = "AND B";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegB();
					v1 = v1 & v2;
					cpu.setRegA(v1);
					
					cpu.setFlags((v1 & 0xFF) == 0, false, true, false);
					break;
					
				case 0xA1:
					// AND C
					if (isLogging) {
						asm = "AND C";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegC();
					v1 = v1 & v2;
					cpu.setRegA(v1);
					
					cpu.setFlags((v1 & 0xFF) == 0, false, true, false);
					break;
					
				case 0xA2:
					// AND D
					if (isLogging) {
						asm = "AND D";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegD();
					v1 = v1 & v2;
					cpu.setRegA(v1);
					
					cpu.setFlags((v1 & 0xFF) == 0, false, true, false);
					break;
					
				case 0xA3:
					// AND E
					if (isLogging) {
						asm = "AND E";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegE();
					v1 = v1 & v2;
					cpu.setRegA(v1);
					
					cpu.setFlags((v1 & 0xFF) == 0, false, true, false);
					break;
					
				case 0xA4:
					// AND H
					if (isLogging) {
						asm = "AND H";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegH();
					v1 = v1 & v2;
					cpu.setRegA(v1);
					
					cpu.setFlags((v1 & 0xFF) == 0, false, true, false);
					break;
					
				case 0xA5:
					// AND L
					if (isLogging) {
						asm = "AND L";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegL();
					v1 = v1 & v2;
					cpu.setRegA(v1);
					
					cpu.setFlags((v1 & 0xFF) == 0, false, true, false);
					break;
					
				case 0xA6:
					// AND (HL)
					if (isLogging) {
						asm = "AND (HL)";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegHL();
					v2 = bus.read(v2);
					v1 = v1 & v2;
					cpu.setRegA(v1);
					
					cpu.setFlags((v1 & 0xFF) == 0, false, true, false);
					break;
					
				case 0xA7:
					// AND A
					if (isLogging) {
						asm = "AND A";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegA();
					v1 = v1 & v2;
					cpu.setRegA(v1);
					
					cpu.setFlags((v1 & 0xFF) == 0, false, true, false);
					break;
					
				case 0xA8:
					// XOR B
					if (isLogging) {
						asm = "XOR B";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegB();
					v1 = v1 ^ v2;
					cpu.setRegA(v1);
					
					cpu.setFlags((v1 & 0xFF) == 0, false, false, false);
					break;
					
				case 0xA9:
					// XOR C
					if (isLogging) {
						asm = "XOR C";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegC();
					v1 = v1 ^ v2;
					cpu.setRegA(v1);
					
					cpu.setFlags((v1 & 0xFF) == 0, false, false, false);
					break;
					
				case 0xAA:
					// XOR D
					if (isLogging) {
						asm = "XOR D";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegD();
					v1 = v1 ^ v2;
					cpu.setRegA(v1);
					
					cpu.setFlags((v1 & 0xFF) == 0, false, false, false);
					break;
					
				case 0xAB:
					// XOR E
					if (isLogging) {
						asm = "XOR E";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegE();
					v1 = v1 ^ v2;
					cpu.setRegA(v1);
					
					cpu.setFlags((v1 & 0xFF) == 0, false, false, false);
					break;
					
				case 0xAC:
					// XOR H
					if (isLogging) {
						asm = "XOR H";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegH();
					v1 = v1 ^ v2;
					cpu.setRegA(v1);
					
					cpu.setFlags((v1 & 0xFF) == 0, false, false, false);
					break;
					
				case 0xAD:
					// XOR L
					if (isLogging) {
						asm = "XOR L";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegL();
					v1 = v1 ^ v2;
					cpu.setRegA(v1);
					
					cpu.setFlags((v1 & 0xFF) == 0, false, false, false);
					break;
					
				case 0xAE:
					// XOR (HL)
					if (isLogging) {
						asm = "XOR (HL)";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegHL();
					v2 = bus.read(v2);
					v1 = v1 ^ v2;
					cpu.setRegA(v1);
					
					cpu.setFlags((v1 & 0xFF) == 0, false, false, false);
					break;
					
				case 0xAF:
					// XOR A
					if (isLogging) {
						asm = "XOR A";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v1 = v1 ^ v1;
					cpu.setRegA(v1);
					
					cpu.setFlags((v1 & 0xFF) == 0, false, false, false);
					break;
					
				case 0xB0:
					// OR B
					if (isLogging) {
						asm = "OR B";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegB();
					v1 = v1 | v2;
					cpu.setRegA(v1);
					
					cpu.setFlags((v1 & 0xFF) == 0, false, false, false);
					break;
					
				case 0xB1:
					// OR C
					if (isLogging) {
						asm = "OR C";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegC();
					v1 = v1 | v2;
					cpu.setRegA(v1);
					
					cpu.setFlags((v1 & 0xFF) == 0, false, false, false);
					break;
					
				case 0xB2:
					// OR D
					if (isLogging) {
						asm = "OR D";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegD();
					v1 = v1 | v2;
					cpu.setRegA(v1);
					
					cpu.setFlags((v1 & 0xFF) == 0, false, false, false);
					break;
					
				case 0xB3:
					// OR E
					if (isLogging) {
						asm = "OR E";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegE();
					v1 = v1 | v2;
					cpu.setRegA(v1);
					
					cpu.setFlags((v1 & 0xFF) == 0, false, false, false);
					break;
					
				case 0xB4:
					// OR H
					if (isLogging) {
						asm = "OR H";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegH();
					v1 = v1 | v2;
					cpu.setRegA(v1);
					
					cpu.setFlags((v1 & 0xFF) == 0, false, false, false);
					break;
					
				case 0xB5:
					// OR L
					if (isLogging) {
						asm = "OR L";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegL();
					v1 = v1 | v2;
					cpu.setRegA(v1);
					
					cpu.setFlags((v1 & 0xFF) == 0, false, false, false);
					break;
					
				case 0xB6:
					// OR (HL)
					if (isLogging) {
						asm = "OR (HL)";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegHL();
					v2 = bus.read(v2);
					v1 = v1 | v2;
					cpu.setRegA(v1);
					
					cpu.setFlags((v1 & 0xFF) == 0, false, false, false);
					break;
					
				case 0xB7:
					// OR A
					if (isLogging) {
						asm = "OR A";
					}
					cycle = 4;
					v1 = cpu.getRegA();
					v2 = cpu.getRegA();
					v1 = v1 | v2;
					cpu.setRegA(v1);
					
					cpu.setFlags((v1 & 0xFF) == 0, false, false, false);
					break;
					
				case 0xB8:
					// CP B
					if (isLogging) {
						asm = "CP B";
					}
					cycle = 8;
					v1 = cpu.getRegB();
					v2 = cpu.getRegA();
					v1 = v2 - v1;
					
					cpu.setFlags((v1 & 0xFF) == 0, true, (v2 & 0x0F) < (v1 & 0x0F), v1 < 0);
					break;
					
				case 0xB9:
					// CP C
					if (isLogging) {
						asm = "CP C";
					}
					cycle = 8;
					v1 = cpu.getRegC();
					v2 = cpu.getRegA();
					v1 = v2 - v1;
					
					cpu.setFlags((v1 & 0xFF) == 0, true, (v2 & 0x0F) < (v1 & 0x0F), v1 < 0);
					break;
					
				case 0xBA:
					// CP D
					if (isLogging) {
						asm = "CP D";
					}
					cycle = 8;
					v1 = cpu.getRegD();
					v2 = cpu.getRegA();
					v1 = v2 - v1;
					
					cpu.setFlags((v1 & 0xFF) == 0, true, (v2 & 0x0F) < (v1 & 0x0F), v1 < 0);
					break;
					
				case 0xBB:
					// CP E
					if (isLogging) {
						asm = "CP E";
					}
					cycle = 8;
					v1 = cpu.getRegE();
					v2 = cpu.getRegA();
					v1 = v2 - v1;
					
					cpu.setFlags((v1 & 0xFF) == 0, true, (v2 & 0x0F) < (v1 & 0x0F), v1 < 0);
					break;
					
				case 0xBC:
					// CP H
					if (isLogging) {
						asm = "CP H";
					}
					cycle = 8;
					v1 = cpu.getRegH();
					v2 = cpu.getRegA();
					v1 = v2 - v1;
					
					cpu.setFlags((v1 & 0xFF) == 0, true, (v2 & 0x0F) < (v1 & 0x0F), v1 < 0);
					break;
					
				case 0xBD:
					// CP L
					if (isLogging) {
						asm = "CP L";
					}
					cycle = 8;
					v1 = cpu.getRegL();
					v2 = cpu.getRegA();
					v1 = v2 - v1;
					
					cpu.setFlags((v1 & 0xFF) == 0, true, (v2 & 0x0F) < (v1 & 0x0F), v1 < 0);
					break;
					
				case 0xBE:
					// CP (HL)
					if (isLogging) {
						asm = "CP (HL)";
						param = String.format("0x%02x", bus.read(cpu.getRegHL()));
					}
					cycle = 8;
					v1 = cpu.getRegHL();
					v1 = bus.read(v1);
					v2 = cpu.getRegA();
					v1 = v2 - v1;
					
					cpu.setFlags((v1 & 0xFF) == 0, true, (v2 & 0x0F) < (v1 & 0x0F), v1 < 0);
					break;
					
				case 0xBF:
					// CP A
					if (isLogging) {
						asm = "CP A";
					}
					cycle = 8;
					v1 = cpu.getRegA();
					v2 = cpu.getRegA();
					v1 = v2 - v1;
					
					cpu.setFlags((v1 & 0xFF) == 0, true, (v2 & 0x0F) < (v1 & 0x0F), v1 < 0);
					break;
					
				case 0xC0:
					// RET NZ
					if (isLogging) {
						asm = "RET NZ";
					}
					cycle = 8;
					if (!cpu.isZero()) {
						v2 = bus.read(sp++);
						v1 = bus.read(sp++);
						cycle += 12;
						pc = (v1 << 8) | v2;
					}
					break;
					
				case 0xC1:
					// POP BC
					if (isLogging) {
						asm = "POP BC";
					}
					cycle = 12;
					v1 = bus.read(sp++);
					v2 = bus.read(sp++);
					cpu.setRegBC(v2, v1);
					break;
					
				case 0xC2:
					// JP NZ,a16
					if (isLogging) {
						asm = "JP NZ,a16";
						param = String.format("0x%04x", bus.read16(pc));
					}
					cycle = 16;
					v1 = bus.read(pc++);
					v2 = bus.read(pc++);
					if (!cpu.isZero()) {
						cycle += 4;
						pc = (v2 << 8) | v1;
					}
					break;
					
				case 0xC3:
					// JP a16
					if (isLogging) {
						asm = "JP a16";
						param = String.format("0x%04x", bus.read16(pc));
					}
					cycle = 16;
					v1 = bus.read(pc++);
					v2 = bus.read(pc++);
					pc = (v2 << 8) | v1;
					break;
					
				case 0xC4:
					// CALL NZ,a16
					if (isLogging) {
						asm = "CALL NZ,a16";
						param = String.format("0x%04x", bus.read16(pc));
					}
					cycle = 12;
					if (!cpu.isZero()) {
						cycle += 12;
						
						v1 = ((pc+2) & 0xFF00) >> 8;
						v2 = ((pc+2) & 0x00FF);
						bus.write(--sp, v1);
						bus.write(--sp, v2);
						v1 = bus.read(pc++);
						v2 = bus.read(pc++);
						
						pc = (v2 << 8) | v1;
					} else {
						v1 = bus.read(pc++);
						v2 = bus.read(pc++);
					}
					break;
					
				case 0xC5:
					// PUSH BC
					if (isLogging) {
						asm = "PUSH BC";
					}
					cycle = 16;
					v1 = cpu.getRegB();
					v2 = cpu.getRegC();
					sp--;
					bus.write(sp, v1);
					sp--;
					bus.write(sp, v2);
					break;
					
				case 0xC6:
					// ADD d8
					if (isLogging) {
						asm = "ADD d8";
						param = String.format("0x%02x", bus.read8(pc));
					}
					cycle = 8;
					v1 = cpu.getRegA();
					v2 = bus.read(pc++);
					v2 = v1 + v2;
					cpu.setRegA(v2);
					
					cpu.setFlags((v2 & 0xFF) == 0, false, (v2 & 0x0F) < (v1 & 0x0F), v2 > 0xFF);
					break;
					
				case 0xC7:
					// RST 00H
					if (isLogging) {
						asm = "RST 00H";
					}
					cycle = 16;
					v1 = (pc & 0xFF00) >> 8;
					v2 = (pc & 0xFF);
					sp--;
					bus.write(sp, v1);
					sp--;
					bus.write(sp, v2);
					pc = 0x00;
					break;
					
				case 0xC8:
					// RET Z
					if (isLogging) {
						asm = "RET Z";
					}
					cycle = 8;
					if (cpu.isZero()) {
						v2 = bus.read(sp++);
						v1 = bus.read(sp++);
						cycle += 12;
						pc = (v1 << 8) | v2;
					}
					break;
					
				case 0xC9:
					// RET
					if (isLogging) {
						asm = "RET";
					}
					cycle = 16;
					v2 = bus.read(sp++);
					v1 = bus.read(sp++);
					pc = (v1 << 8) | v2;
					break;
					
				case 0xCA:
					// JP Z,a16
					if (isLogging) {
						asm = "JP Z,a16";
						param = String.format("0x%04x", bus.read16(pc));
					}
					cycle = 16;
					v1 = bus.read(pc++);
					v2 = bus.read(pc++);
					if (cpu.isZero()) {
						cycle += 4;
						pc = (v2 << 8) | v1;
					}
					break;
					
				case 0xCB:
					// PREFIX CB
					cycle = 4;
					cpu.setPC(pc);
					String[] a = new String[] {"CB "};
					cycle += prefixCB(currPC, a);
					asm = a[0];
					pc = cpu.getPC();
					break;
					
				case 0xCC:
					// CALL Z,a16
					if (isLogging) {
						asm = "CALL Z,a16";
						param = String.format("0x%04x", bus.read16(pc));
					}
					cycle = 12;
					if (cpu.isZero()) {
						cycle += 12;
						
						v1 = ((pc+2) & 0xFF00) >> 8;
						v2 = ((pc+2) & 0x00FF);
						bus.write(--sp, v1);
						bus.write(--sp, v2);
						v1 = bus.read(pc++);
						v2 = bus.read(pc++);
						
						pc = (v2 << 8) | v1;
					} else {
						v1 = bus.read(pc++);
						v2 = bus.read(pc++);
					}
					break;
					
				case 0xCD:
					// CALL a16
					if (isLogging) {
						asm = "CALL a16";
						param = String.format("0x%04x", bus.read16(pc));
					}
					cycle = 24;
					// push pc on stack
					v1 = ((pc+2) & 0xFF00) >> 8;
					v2 = ((pc+2) & 0x00FF);
					bus.write(--sp, v1);
					bus.write(--sp, v2);
					v1 = bus.read(pc++);
					v2 = bus.read(pc++);
					
					pc = (v2 << 8) | v1;
					break;
					
				case 0xCE:
					// ADC A,d8
					if (isLogging) {
						asm = "ADC A,d8";
						param = String.format("0x%02x", bus.read8(pc));
					}
					cycle = 8;
					v1 = bus.read(pc++);
					v2 = cpu.getRegA();
					
					// half carry
					cpu.setFlagHalf((v1 & 0x0F) + (v2 & 0x0F) + (cpu.isCarry() ? 1 : 0) > 0x0F);
					
					v1 = v1 + v2 + (cpu.isCarry() ? 1 : 0);
					cpu.setRegA(v1);
					// Z 0 H C
					cpu.setFlags((v1 & 0xFF) == 0, false, cpu.isHalfCarry(), v1 > 0xFF);
					break;
					
				case 0xCF:
					// RST 08H
					if (isLogging) {
						asm = "RST 08H";
					}
					cycle = 16;
					// push pc on stack
					v1 = ((pc) & 0xFF00) >> 8;
					v2 = ((pc) & 0x00FF);
					bus.write(--sp, v1);
					bus.write(--sp, v2);
					
					pc = 0x08;
					break;
					
				case 0xD0:
					// RET NC
					if (isLogging) {
						asm = "RET NC";
					}
					cycle = 8;
					if (!cpu.isCarry()) {
						v2 = bus.read(sp++);
						v1 = bus.read(sp++);
						cycle += 12;
						pc = (v1 << 8) | v2;
					}
					break;
					
				case 0xD1:
					// POP DE
					if (isLogging) {
						asm = "POP DE";
					}
					cycle = 12;
					v1 = bus.read(sp++);
					v2 = bus.read(sp++);
					cpu.setRegDE(v2, v1);
					break;
					
				case 0xD2:
					// JP NC,a16
					if (isLogging) {
						asm = "JP NC,a16";
						param = String.format("0x%04x", bus.read16(pc));
					}
					cycle = 16;
					v1 = bus.read(pc++);
					v2 = bus.read(pc++);
					if (!cpu.isCarry()) {
						cycle += 4;
						pc = (v2 << 8) | v1;
					}
					break;
					
				case 0xD4:
					// CALL NC,a16
					if (isLogging) {
						asm = "CALL NC,a16";
						param = String.format("0x%04x", bus.read16(pc));
					}
					cycle = 12;
					if (!cpu.isCarry()) {
						cycle += 12;
						
						v1 = ((pc+2) & 0xFF00) >> 8;
						v2 = ((pc+2) & 0x00FF);
						bus.write(--sp, v1);
						bus.write(--sp, v2);
						v1 = bus.read(pc++);
						v2 = bus.read(pc++);
						
						pc = (v2 << 8) | v1;
					} else {
						v1 = bus.read(pc++);
						v2 = bus.read(pc++);
					}
					break;
					
				case 0xD5:
					// PUSH DE
					if (isLogging) {
						asm = "PUSH DE";
					}
					cycle = 16;
					v1 = cpu.getRegD();
					v2 = cpu.getRegE();
					sp--;
					bus.write(sp, v1);
					sp--;
					bus.write(sp, v2);
					break;
					
				case 0xD6:
					// SUB d8
					if (isLogging) {
						asm = "SUB d8";
						param = String.format("0x%02x", bus.read8(pc));
					}
					cycle = 8;
					v1 = cpu.getRegA();
					v2 = bus.read(pc++);
					v2 = v1 - v2;
					cpu.setRegA(v2);
					
					cpu.setFlags((v2 & 0xFF) == 0, true, (v1 & 0x0F) < (v2 & 0x0F), v2 < 0);
					break;
					
				case 0xD7:
					// RST 10H
					if (isLogging) {
						asm = "RST 10H";
					}
					cycle = 16;
					v1 = (pc & 0xFF00) >> 8;
					v2 = (pc & 0xFF);
					sp--;
					bus.write(sp, v1);
					sp--;
					bus.write(sp, v2);
					pc = 0x10;
					break;
					
				case 0xD8:
					// RET C
					if (isLogging) {
						asm = "RET C";
					}
					cycle = 8;
					if (cpu.isCarry()) {
						v2 = bus.read(sp++);
						v1 = bus.read(sp++);
						cycle += 12;
						pc = (v1 << 8) | v2;
					}
					break;
					
				case 0xD9:
					// RETI
					cycle = 0;
					if (isLogging) {
						asm = "RETI";
						param = String.format("0x%04x (0x%04x)", sp, bus.read16(sp));
					}
					
					// 1st RET (0xC9)
					cycle += 16;
					v2 = bus.read(sp++);
					v1 = bus.read(sp++);
					pc = (v1 << 8) | v2;
					
					// 2nd EMI (0xFB)
					cycle += 4;
					cpu.enableIME();
					break;
					
				case 0xDA:
					// JP C,a16
					if (isLogging) {
						asm = "JP C,a16";
						param = String.format("0x%04x", bus.read16(pc));
					}
					cycle = 16;
					v1 = bus.read(pc++);
					v2 = bus.read(pc++);
					if (cpu.isCarry()) {
						cycle += 4;
						pc = (v2 << 8) | v1;
					}
					break;
					
				case 0xDC:
					// CALL C,a16
					if (isLogging) {
						asm = "CALL C,a16";
						param = String.format("0x%04x", bus.read16(pc));
					}
					cycle = 12;
					if (cpu.isCarry()) {
						cycle += 12;
						
						v1 = ((pc+2) & 0xFF00) >> 8;
						v2 = ((pc+2) & 0x00FF);
						bus.write(--sp, v1);
						bus.write(--sp, v2);
						v1 = bus.read(pc++);
						v2 = bus.read(pc++);
						
						pc = (v2 << 8) | v1;
					} else {
						v1 = bus.read(pc++);
						v2 = bus.read(pc++);
					}
					break;
					
				case 0xDE:
					// SBC d8
					if (isLogging) {
						asm = "SBC d8";
						param = String.format("0x%02x", bus.read8(pc));
					}
					cycle = 8;
					v1 = cpu.getRegA();
					v2 = bus.read(pc++);
					v2 = v1 - v2 - (cpu.isCarry() ? 1 : 0);
					cpu.setRegA(v2);
	
					
					cpu.setFlags((v2 & 0xFF) == 0, true, (v1 & 0x0f) - (bus.read(pc - 1) & 0x0f) - (cpu.isCarry() ? 1 : 0) < 0, v2 < 0);
					break;
					
				case 0xDF:
					// RST 18H
					if (isLogging) {
						asm = "RST 18H";
					}
					cycle = 16;
					// push pc on stack
					v1 = ((pc) & 0xFF00) >> 8;
					v2 = ((pc) & 0x00FF);
					bus.write(--sp, v1);
					bus.write(--sp, v2);
					
					pc = 0x18;
					break;
					
				case 0xE0:
					// LDH (a8),A
					if (isLogging) {
						asm = "LDH (a8),A";
						param = String.format("0xFF%02x", bus.read8(pc));
					}
					cycle = 12;
					v1 = bus.read(pc++) + 0xFF00;
					v2 = cpu.getRegA();
					bus.write(v1, v2);
					break;
					
				case 0xE1:
					// POP HL
					if (isLogging) {
						asm = "POP HL";
					}
					cycle = 12;
					v1 = bus.read(sp++);
					v2 = bus.read(sp++);
					cpu.setRegHL(v2, v1);
					break;
					
				case 0xE2:
					// LD (C),A
					if (isLogging) {
						asm = "LD (C),A";
					}
					cycle = 8;
					// write to IO port C
					v1 = cpu.getRegC() + 0xFF00;
					v2 = cpu.getRegA();
					bus.write(v1, v2);
					break;
					
				case 0xE3:
					throw new JJUnsupportedOpcodeException("Invalid opcode: 0xE3");
					
				case 0xE4:
					throw new JJUnsupportedOpcodeException("Invalid opcode: 0xE4");
					
				case 0xE5:
					// PUSH HL
					if (isLogging) {
						asm = "PUSH HL";
					}
					cycle = 16;
					v1 = cpu.getRegH();
					v2 = cpu.getRegL();
					sp--;
					bus.write(sp, v1);
					sp--;
					bus.write(sp, v2);
					break;
					
				case 0xE6:
					// AND d8
					if (isLogging) {
						asm = "AND d8";
						param = String.format("0x%02x", bus.read8(pc));
					}
					cycle = 8;
					v1 = cpu.getRegA();
					v2 = bus.read(pc++);
					v1 = v1 & v2;
					cpu.setRegA(v1);
					
					cpu.setFlags((v1 & 0xFF) == 0, false, true, false);
					break;
					
				case 0xE7:
					// RST 20H
					if (isLogging) {
						asm = "RST 20H";
					}
					cycle = 16;
					v1 = (pc & 0xFF00) >> 8;
					v2 = (pc & 0xFF);
					sp--;
					bus.write(sp, v1);
					sp--;
					bus.write(sp, v2);
					pc = 0x20;
					break;
					
				case 0xE8:
					// ADD SP,r8
					if (isLogging) {
						asm = "ADD SP,r8";
						param = String.format("0x%02x (%d)", bus.read(pc), bus.read7(pc));
					}
					cycle = 16;
					v1 = bus.read7(pc++);
					v2 = (sp + v1) & 0xFFFF;
					v2 = sp ^ ((byte) v1) ^ v2;
					sp = sp + v1;
					cpu.setSP(sp);
					
					cpu.setFlags(false, false, (v2 & 0x10) > 0, (v2 & 0x100) > 0);
					break;
					
				case 0xEA:
					// LD (a16),A
					if (isLogging) {
						asm = "LD (a16),A";
						param = String.format("0x%04x", bus.read16(pc));
					}
					cycle = 16;
					v1 = bus.read(pc++);
					v2 = bus.read(pc++);
					v1 = (v2 << 8) | v1;
					v2 = cpu.getRegA();
					bus.write(v1, v2);
					break;
					
				case 0xE9:
					// JP HL
					if (isLogging) {
						asm = "JP HL";
						param = String.format("%04x", cpu.getRegHL());
					}
					cycle = 4;
					v1 = cpu.getRegHL();
					pc = v1;
					break;
					
				case 0xEE:
					// XOR d8
					if (isLogging) {
						asm = "XOR d8";
						param = String.format("0x%02x", bus.read8(pc));
					}
					cycle = 8;
					v1 = cpu.getRegA();
					v2 = bus.read(pc++);
					v1 = v1 ^ v2;
					cpu.setRegA(v1);
					
					cpu.setFlags((v1 & 0xFF) == 0, false, false, false);
					break;
					
				case 0xEF:
					// RST 28H
					if (isLogging) {
						asm = "RST 28H";
					}
					cycle = 16;
					// push pc on stack
					v1 = ((pc) & 0xFF00) >> 8;
					v2 = ((pc) & 0x00FF);
					bus.write(--sp, v1);
					bus.write(--sp, v2);
					
					pc = 0x28;
					break;
					
				case 0xF0:
					// LDH A,(a8)
					if (isLogging) {
						asm = "LDH A,(a8)";
						param = String.format("0xFF%02x", bus.read8(pc));
					}
					cycle = 12;
					v1 = bus.read(pc++) + 0xFF00;
					v2 = bus.read(v1);
					cpu.setRegA(v2);
					break;
					
				case 0xF1:
					// POP AF
					if (isLogging) {
						asm = "POP AF";
					}
					cycle = 12;
					v1 = bus.read(sp++);
					v2 = bus.read(sp++);
					cpu.setRegAF(v2, v1);
					break;
					
				case 0xF2:
					// LD A,(C)
					if (isLogging) {
						asm = "LD A,(C)";
					}
					cycle = 8;
					v1 = cpu.getRegC() + 0xFF00;
					v2 = bus.read(v1);
					cpu.setRegA(v2);
					break;
					
				case 0xF3:
					// DI
					if (isLogging) {
						asm = "DI";
					}
					cycle = 4;
					cpu.disableIME();
					break;
					
				case 0xF5:
					// PUSH AF
					if (isLogging) {
						asm = "PUSH AF";
					}
					cycle = 16;
					v1 = cpu.getRegA();
					v2 = cpu.getRegF();
					sp--;
					bus.write(sp, v1);
					sp--;
					bus.write(sp, v2);
					break;
					
				case 0xF6:
					// OR d8
					if (isLogging) {
						asm = "OR d8";
						param = String.format("0x%02x", bus.read8(pc));
					}
					cycle = 8;
					v1 = cpu.getRegA();
					v2 = bus.read(pc++);
					v1 = v1 | v2;
					cpu.setRegA(v1);
					
					cpu.setFlags((v1 & 0xFF) == 0, false, false, false);
					break;
					
				case 0xF7:
					// RST 30H
					if (isLogging) {
						asm = "RST 30H";
					}
					cycle = 16;
					v1 = (pc & 0xFF00) >> 8;
					v2 = (pc & 0xFF);
					sp--;
					bus.write(sp, v1);
					sp--;
					bus.write(sp, v2);
					pc = 0x30;
					break;
					
				case 0xF8:
					// LD HL,SP+r8
					if (isLogging) {
						asm = "LD HL,SP+r8";
						param = String.format("0x%02x (%d)", bus.read(pc), bus.read7(pc));
					}
					cycle = 12;
					v1 = bus.read7(pc++);
					cpu.setRegHL(sp + v1);
					
					v2 = sp ^ v1 ^ cpu.getRegHL();
					
					cpu.setFlags(false, false, (v2 & 0x10) > 0, (v2 & 0x0100) > 0);
					break;
					
				case 0xF9:
					// LD SP,HL
					if (isLogging) {
						asm = "LD SP,HL";
					}
					cycle = 8;
					v1 = cpu.getRegHL();
					sp = v1;
					break;
					
				case 0xFA:
					// LD A,(a16)
					if (isLogging) {
						asm = "LD A,(a16)";
						param = String.format("0x%04x", bus.read16(pc));
					}
					v1 = bus.read(pc++);
					v2 = bus.read(pc++);
					v1 = (v2 << 8) | v1;
					v2 = bus.read(v1);
					cpu.setRegA(v2);
					break;
					
				case 0xFB:
					// EI
					if (isLogging) {
						asm = "EI";
					}
					cycle = 4;
					cpu.enableIME();
					break;
					
				case 0xFC:
					throw new JJUnsupportedOpcodeException("Invalid opcode: 0xFC");
					
				case 0xFE:
					// CP d8
					if (isLogging) {
						asm = "CP d8";
						param = String.format("0x%02x", bus.read8(pc));
					}
					cycle = 8;
					v1 = bus.read(pc++);
					v2 = cpu.getRegA();
					v1 = v2 - v1;
					
					cpu.setFlags((v1 & 0xFF) == 0, true, (v2 & 0x0F) < (v1 & 0x0F), v1 < 0);
					break;
					
				case 0xFF:
					// RST 38H
					if (isLogging) {
						asm = "RST 38H";
					}
					cycle = 16;
					// push pc on stack
					v1 = ((pc) & 0xFF00) >> 8;
					v2 = ((pc) & 0x00FF);
					bus.write(--sp, v1);
					bus.write(--sp, v2);
					
					pc = 0x38;
					break;
					
				default:
					System.out.printf("Unknown opcode: %x%n", opcode);
					throw new JJUnsupportedOpcodeException(opcode, cpu);
			}
		} finally {
			if (isLogging) {
				if (param.equals("")) {
					param = "\t";
				}
				JJInstructionTracerHelper.getInstance().addTrace(String.format("%04x: %s \t-\t%s\t-\t%s", currPC, asm, param, cpuTrace));
			}
		}
		
		cpu.setPC(pc);
		cpu.setSP(sp);
		
		asmCode[currPC] = asm;
		asmParam[currPC] = param;
		
		return cycle;
	}
	
	private int prefixCB(int currPC, String[] asmTrace) {
		int pc = cpu.getPC();
		int opcode = bus.read(pc++);
		// the number of cycles passed after the instruction
		int cycle = 0;
		
		int v1;
		int v2;
		
		String asm = "";
		String param = "";
		switch (opcode) {
		
			case 0x00:
				// RLC B
				asm = "RLC B";
				cycle = 8;
				v1 = cpu.getRegB();
				v2 = (v1 & 0x80) >> 7;
				v1 = (v1 << 1) | v2;
				cpu.setRegB(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0);
				break;
				
			case 0x01:
				// RLC C
				asm = "RLC C";
				cycle = 8;
				v1 = cpu.getRegC();
				v2 = (v1 & 0x80) >> 7;
				v1 = (v1 << 1) | v2;
				cpu.setRegC(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0);
				break;
				
			case 0x02:
				// RLC D
				asm = "RLC D";
				cycle = 8;
				v1 = cpu.getRegD();
				v2 = (v1 & 0x80) >> 7;
				v1 = (v1 << 1) | v2;
				cpu.setRegD(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0);
				break;
				
			case 0x03:
				// RLC E
				asm = "RLC E";
				cycle = 8;
				v1 = cpu.getRegE();
				v2 = (v1 & 0x80) >> 7;
				v1 = (v1 << 1) | v2;
				cpu.setRegE(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0);
				break;
				
			case 0x04:
				// RLC H
				asm = "RLC H";
				cycle = 8;
				v1 = cpu.getRegH();
				v2 = (v1 & 0x80) >> 7;
				v1 = (v1 << 1) | v2;
				cpu.setRegH(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0);
				break;
				
			case 0x05:
				// RLC L
				asm = "RLC L";
				cycle = 8;
				v1 = cpu.getRegL();
				v2 = (v1 & 0x80) >> 7;
				v1 = (v1 << 1) | v2;
				cpu.setRegL(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0);
				break;
				
			case 0x06:
				// RLC (HL)
				asm = "RLC (HL)";
				cycle = 8;
				v1 = cpu.getRegHL();
				v1 = bus.read(v1);
				v2 = (v1 & 0x80) >> 7;
				v1 = (v1 << 1) | v2;
				bus.write(cpu.getRegHL(), v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0);
				break;
				
			case 0x07:
				// RLC A
				asm = "RLC A";
				cycle = 8;
				v1 = cpu.getRegA();
				v2 = (v1 & 0x80) >> 7;
				v1 = (v1 << 1) | v2;
				cpu.setRegA(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0);
				break;
				
			case 0x08:
				// RRC B
				asm = "RRC B";
				cycle = 8;
				v1 = cpu.getRegB();
				v2 = (v1 & 0x01);
				v1 = (v2 << 7) | v1 >> 1;
				cpu.setRegB(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0);
				break;
				
			case 0x09:
				// RRC C
				asm = "RRC C";
				cycle = 8;
				v1 = cpu.getRegC();
				v2 = (v1 & 0x01);
				v1 = (v2 << 7) | v1 >> 1;
				cpu.setRegC(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0);
				break;
				
			case 0x0A:
				// RRC D
				asm = "RRC D";
				cycle = 8;
				v1 = cpu.getRegD();
				v2 = (v1 & 0x01);
				v1 = (v2 << 7) | v1 >> 1;
				cpu.setRegD(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0);
				break;
				
			case 0x0B:
				// RRC E
				asm = "RRC E";
				cycle = 8;
				v1 = cpu.getRegE();
				v2 = (v1 & 0x01);
				v1 = (v2 << 7) | v1 >> 1;
				cpu.setRegE(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0);
				break;
				
			case 0x0C:
				// RRC H
				asm = "RRC H";
				cycle = 8;
				v1 = cpu.getRegH();
				v2 = (v1 & 0x01);
				v1 = (v2 << 7) | v1 >> 1;
				cpu.setRegH(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0);
				break;
				
			case 0x0D:
				// RRC L
				asm = "RRC L";
				cycle = 8;
				v1 = cpu.getRegL();
				v2 = (v1 & 0x01);
				v1 = (v2 << 7) | v1 >> 1;
				cpu.setRegL(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0);
				break;
				
			case 0x0E:
				// RRC (HL)
				asm = "RRC (HL)";
				cycle = 8;
				v1 = cpu.getRegHL();
				v1 = bus.read(v1);
				v2 = (v1 & 0x01);
				v1 = (v2 << 7) | v1 >> 1;
				bus.write(cpu.getRegHL(), v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0);
				break;
				
			case 0x0F:
				// RRC A
				asm = "RRC A";
				cycle = 8;
				v1 = cpu.getRegA();
				v2 = (v1 & 0x01);
				v1 = (v2 << 7) | v1 >> 1;
				cpu.setRegA(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0);
				break;
				
			case 0x10:
				// RL B
				asm = "RL B";
				cycle = 8;
				v1 = cpu.getRegB();
				v2 = (v1 & 0x80) >> 7;
				v1 = (v1 << 1) | (cpu.isCarry() ? 1 : 0);
				cpu.setRegB(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0);
				break;
				
			case 0x11:
				// RL C
				asm = "RL C";
				cycle = 8;
				v1 = cpu.getRegC();
				v2 = (v1 & 0x80) >> 7;
				v1 = (v1 << 1) | (cpu.isCarry() ? 1 : 0);
				cpu.setRegC(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0);
				break;
				
			case 0x12:
				// RL D
				asm = "RL D";
				cycle = 8;
				v1 = cpu.getRegD();
				v2 = (v1 & 0x80) >> 7;
				v1 = (v1 << 1) | (cpu.isCarry() ? 1 : 0);
				cpu.setRegD(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0);
				break;
				
			case 0x13:
				// RL E
				asm = "RL E";
				cycle = 8;
				v1 = cpu.getRegE();
				v2 = (v1 & 0x80) >> 7;
				v1 = (v1 << 1) | (cpu.isCarry() ? 1 : 0);
				cpu.setRegE(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0);
				break;
				
			case 0x14:
				// RL H
				asm = "RL H";
				cycle = 8;
				v1 = cpu.getRegH();
				v2 = (v1 & 0x80) >> 7;
				v1 = (v1 << 1) | (cpu.isCarry() ? 1 : 0);
				cpu.setRegH(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0);
				break;
				
			case 0x15:
				// RL L
				asm = "RL L";
				cycle = 8;
				v1 = cpu.getRegL();
				v2 = (v1 & 0x80) >> 7;
				v1 = (v1 << 1) | (cpu.isCarry() ? 1 : 0);
				cpu.setRegL(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0);
				break;
				
			case 0x16:
				// RL (HL)
				asm = "RL (HL)";
				cycle = 8;
				v1 = cpu.getRegHL();
				v1 = bus.read(v1);
				v2 = (v1 & 0x80) >> 7;
				v1 = (v1 << 1) | (cpu.isCarry() ? 1 : 0);
				bus.write(cpu.getRegHL(), v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0);
				break;
				
			case 0x17:
				// RL A
				asm = "RL A";
				cycle = 8;
				v1 = cpu.getRegA();
				v2 = (v1 & 0x80) >> 7;
				v1 = (v1 << 1) | (cpu.isCarry() ? 1 : 0);
				cpu.setRegA(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0);
				break;
				
			case 0x18:
				// RR B
				asm = "RR B";
				cycle = 8;
				v1 = cpu.getRegB();
				v2 = (v1 & 0x01);
				v1 = (cpu.isCarry() ? 0x80 : 0) | (v1 >> 1);
				cpu.setRegB(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0);
				break;
				
			case 0x19:
				// RR C
				asm = "RR C";
				cycle = 8;
				v1 = cpu.getRegC();
				v2 = (v1 & 0x01);
				v1 = (cpu.isCarry() ? 0x80 : 0) | (v1 >> 1);
				cpu.setRegC(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0);
				break;
				
			case 0x1A:
				// RR D
				asm = "RR D";
				cycle = 8;
				v1 = cpu.getRegD();
				v2 = (v1 & 0x01);
				v1 = (cpu.isCarry() ? 0x80 : 0) | (v1 >> 1);
				cpu.setRegD(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0);
				break;
				
			case 0x1B:
				// RR E
				asm = "RR E";
				cycle = 8;
				v1 = cpu.getRegE();
				v2 = (v1 & 0x01);
				v1 = (cpu.isCarry() ? 0x80 : 0) | (v1 >> 1);
				cpu.setRegE(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0);
				break;
				
			case 0x1C:
				// RR H
				asm = "RR H";
				cycle = 8;
				v1 = cpu.getRegH();
				v2 = (v1 & 0x01);
				v1 = (cpu.isCarry() ? 0x80 : 0) | (v1 >> 1);
				cpu.setRegH(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0);
				break;
				
			case 0x1D:
				// RR L
				asm = "RR L";
				cycle = 8;
				v1 = cpu.getRegL();
				v2 = (v1 & 0x01);
				v1 = (cpu.isCarry() ? 0x80 : 0) | (v1 >> 1);
				cpu.setRegL(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0);
				break;
				
			case 0x1E:
				// RR (HL)
				asm = "RR (HL)";
				cycle = 8;
				v1 = cpu.getRegHL();
				v1 = bus.read(v1);
				v2 = (v1 & 0x01);
				v1 = (cpu.isCarry() ? 0x80 : 0) | (v1 >> 1);
				bus.write(cpu.getRegHL(), v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0);
				break;
				
			case 0x1F:
				// RR A
				asm = "RR A";
				cycle = 8;
				v1 = cpu.getRegA();
				v2 = (v1 & 0x01);
				v1 = (cpu.isCarry() ? 0x80 : 0) | (v1 >> 1);
				cpu.setRegA(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0);
				break;
				
			case 0x20:
				// SLA B
				asm = "SLA B";
				cycle = 8;
				v1 = cpu.getRegB();
				v1 = v1 << 1;
				cpu.setRegB(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v1 > 0xFF);
				break;
				
			case 0x21:
				// SLA C
				asm = "SLA C";
				cycle = 8;
				v1 = cpu.getRegC();
				v1 = v1 << 1;
				cpu.setRegC(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v1 > 0xFF);
				break;
				
			case 0x22:
				// SLA D
				asm = "SLA D";
				cycle = 8;
				v1 = cpu.getRegD();
				v1 = v1 << 1;
				cpu.setRegD(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v1 > 0xFF);
				break;
				
			case 0x23:
				// SLA E
				asm = "SLA E";
				cycle = 8;
				v1 = cpu.getRegE();
				v1 = v1 << 1;
				cpu.setRegE(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v1 > 0xFF);
				break;
				
			case 0x24:
				// SLA H
				asm = "SLA H";
				cycle = 8;
				v1 = cpu.getRegH();
				v1 = v1 << 1;
				cpu.setRegH(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v1 > 0xFF);
				break;
				
			case 0x25:
				// SLA L
				asm = "SLA L";
				cycle = 8;
				v1 = cpu.getRegL();
				v1 = v1 << 1;
				cpu.setRegL(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v1 > 0xFF);
				break;
				
			case 0x26:
				// SLA (HL)
				asm = "SLA (HL)";
				cycle = 8;
				v2 = cpu.getRegHL();
				v1 = bus.read(v2);
				v1 = v1 << 1;
				bus.write(v2, v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v1 > 0xFF);
				break;
				
			case 0x27:
				// SLA A
				asm = "SLA A";
				cycle = 8;
				v1 = cpu.getRegA();
				v1 = v1 << 1;
				cpu.setRegA(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v1 > 0xFF);
				break;
				
			case 0x28:
				// SRA B
				asm = "SRA B";
				cycle = 8;
				v1 = cpu.getRegB();
				v2 = (v1 & 0x01);
				v1 = (v1 & 0x80) | (v1 >> 1);
				cpu.setRegB(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0x00);
				break;
				
			case 0x29:
				// SRA C
				asm = "SRA C";
				cycle = 8;
				v1 = cpu.getRegC();
				v2 = (v1 & 0x01);
				v1 = (v1 & 0x80) | (v1 >> 1);
				cpu.setRegC(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0x00);
				break;
				
			case 0x2A:
				// SRA D
				asm = "SRA D";
				cycle = 8;
				v1 = cpu.getRegD();
				v2 = (v1 & 0x01);
				v1 = (v1 & 0x80) | (v1 >> 1);
				cpu.setRegD(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0x00);
				break;
				
			case 0x2B:
				// SRA E
				asm = "SRA E";
				cycle = 8;
				v1 = cpu.getRegE();
				v2 = (v1 & 0x01);
				v1 = (v1 & 0x80) | (v1 >> 1);
				cpu.setRegE(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0x00);
				break;
				
			case 0x2C:
				// SRA H
				asm = "SRA H";
				cycle = 8;
				v1 = cpu.getRegH();
				v2 = (v1 & 0x01);
				v1 = (v1 & 0x80) | (v1 >> 1);
				cpu.setRegH(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0x00);
				break;
				
			case 0x2D:
				// SRA L
				asm = "SRA L";
				cycle = 8;
				v1 = cpu.getRegL();
				v2 = (v1 & 0x01);
				v1 = (v1 & 0x80) | (v1 >> 1);
				cpu.setRegL(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0x00);
				break;
				
			case 0x2E:
				// SRA (HL)
				asm = "SRA (HL)";
				cycle = 8;
				v1 = cpu.getRegHL();
				v1 = bus.read(v1);
				v2 = (v1 & 0x01);
				v1 = (v1 & 0x80) | (v1 >> 1);
				bus.write(cpu.getRegHL(), v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0x00);
				break;
				
			case 0x2F:
				// SRA A
				asm = "SRA A";
				cycle = 8;
				v1 = cpu.getRegA();
				v2 = (v1 & 0x01);
				v1 = (v1 & 0x80) | (v1 >> 1);
				cpu.setRegA(v1);
				cpu.setFlags((v1 & 0xFF) == 0, false, false, v2 > 0x00);
				break;
				
			case 0x30:
				// SWAP B
				asm = "SWAP B";
				cycle = 8;
				v1 = cpu.getRegB();
				v2 = (v1 & 0xF0) >> 4;
				v1 = (v1 & 0x0F) << 4;
				v1 = v1 | v2;
				cpu.setRegB(v1);
				// Z 0 0 0
				cpu.setFlags((v1 & 0xFF) == 0, false, false, false);
				break;
				
			case 0x31:
				// SWAP C
				asm = "SWAP C";
				cycle = 8;
				v1 = cpu.getRegC();
				v2 = (v1 & 0xF0) >> 4;
				v1 = (v1 & 0x0F) << 4;
				v1 = v1 | v2;
				cpu.setRegC(v1);
				// Z 0 0 0
				cpu.setFlags((v1 & 0xFF) == 0, false, false, false);
				break;
				
			case 0x32:
				// SWAP D
				asm = "SWAP D";
				cycle = 8;
				v1 = cpu.getRegD();
				v2 = (v1 & 0xF0) >> 4;
				v1 = (v1 & 0x0F) << 4;
				v1 = v1 | v2;
				cpu.setRegD(v1);
				// Z 0 0 0
				cpu.setFlags((v1 & 0xFF) == 0, false, false, false);
				break;
				
			case 0x33:
				// SWAP E
				asm = "SWAP E";
				cycle = 8;
				v1 = cpu.getRegE();
				v2 = (v1 & 0xF0) >> 4;
				v1 = (v1 & 0x0F) << 4;
				v1 = v1 | v2;
				cpu.setRegE(v1);
				// Z 0 0 0
				cpu.setFlags((v1 & 0xFF) == 0, false, false, false);
				break;
				
			case 0x34:
				// SWAP H
				asm = "SWAP H";
				cycle = 8;
				v1 = cpu.getRegH();
				v2 = (v1 & 0xF0) >> 4;
				v1 = (v1 & 0x0F) << 4;
				v1 = v1 | v2;
				cpu.setRegH(v1);
				// Z 0 0 0
				cpu.setFlags((v1 & 0xFF) == 0, false, false, false);
				break;
				
			case 0x35:
				// SWAP L
				asm = "SWAP L";
				cycle = 8;
				v1 = cpu.getRegL();
				v2 = (v1 & 0xF0) >> 4;
				v1 = (v1 & 0x0F) << 4;
				v1 = v1 | v2;
				cpu.setRegL(v1);
				// Z 0 0 0
				cpu.setFlags((v1 & 0xFF) == 0, false, false, false);
				break;
				
			case 0x36:
				// SWAP (HL)
				asm = "SWAP (HL)";
				cycle = 16;
				v1 = cpu.getRegHL();
				v1 = bus.read(v1);
				v2 = (v1 & 0xF0) >> 4;
				v1 = (v1 & 0x0F) << 4;
				v1 = v1 | v2;
				v2 = cpu.getRegHL();
				bus.write(v2, v1);
				// Z 0 0 0
				cpu.setFlags((v1 & 0xFF) == 0, false, false, false);
				break;
				
			case 0x37:
				// SWAP A
				asm = "SWAP A";
				cycle = 8;
				v1 = cpu.getRegA();
				v2 = (v1 & 0xF0) >> 4;
				v1 = (v1 & 0x0F) << 4;
				v1 = v1 | v2;
				cpu.setRegA(v1);
				// Z 0 0 0
				cpu.setFlags((v1 & 0xFF) == 0, false, false, false);
				break;
				
			case 0x38:
				// SRL B
				asm = "SRL B";
				cycle = 8;
				v1 = cpu.getRegB();
				v2 = v1 >> 1;
				cpu.setRegB(v2);
				// Z 0 0 C
				cpu.setFlags((v2 & 0xFF) == 0, false, false, (v1 & 0x01) > 0);
				break;
				
			case 0x39:
				// SRL C
				asm = "SRL C";
				cycle = 8;
				v1 = cpu.getRegC();
				v2 = v1 >> 1;
				cpu.setRegC(v2);
				// Z 0 0 C
				cpu.setFlags((v2 & 0xFF) == 0, false, false, (v1 & 0x01) > 0);
				break;
				
			case 0x3A:
				// SRL D
				asm = "SRL D";
				cycle = 8;
				v1 = cpu.getRegD();
				v2 = v1 >> 1;
				cpu.setRegD(v2);
				// Z 0 0 C
				cpu.setFlags((v2 & 0xFF) == 0, false, false, (v1 & 0x01) > 0);
				break;
				
			case 0x3B:
				// SRL E
				asm = "SRL E";
				cycle = 8;
				v1 = cpu.getRegE();
				v2 = v1 >> 1;
				cpu.setRegE(v2);
				// Z 0 0 C
				cpu.setFlags((v2 & 0xFF) == 0, false, false, (v1 & 0x01) > 0);
				break;
				
			case 0x3C:
				// SRL H
				asm = "SRL H";
				cycle = 8;
				v1 = cpu.getRegH();
				v2 = v1 >> 1;
				cpu.setRegH(v2);
				// Z 0 0 C
				cpu.setFlags((v2 & 0xFF) == 0, false, false, (v1 & 0x01) > 0);
				break;
				
			case 0x3D:
				// SRL L
				asm = "SRL L";
				cycle = 8;
				v1 = cpu.getRegL();
				v2 = v1 >> 1;
				cpu.setRegL(v2);
				// Z 0 0 C
				cpu.setFlags((v2 & 0xFF) == 0, false, false, (v1 & 0x01) > 0);
				break;
				
			case 0x3E:
				// SRL (HL)
				asm = "SRL (HL)";
				cycle = 8;
				v1 = cpu.getRegHL();
				v1 = bus.read(v1);
				v2 = v1 >> 1;
				bus.write(cpu.getRegHL(), v2);
				// Z 0 0 C
				cpu.setFlags((v2 & 0xFF) == 0, false, false, (v1 & 0x01) > 0);
				break;
				
			case 0x3F:
				// SRL A
				asm = "SRL A";
				cycle = 8;
				v1 = cpu.getRegA();
				v2 = v1 >> 1;
				cpu.setRegA(v2);
				// Z 0 0 C
				cpu.setFlags((v2 & 0xFF) == 0, false, false, (v1 & 0x01) > 0);
				break;
				
			case 0x40:
				// BIT 0,B
				asm = "BIT 0,B";
				cycle = 8;
				v1 = cpu.getRegB();
				v1 = v1 & 0x01;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x41:
				// BIT 0,C
				asm = "BIT 0,C";
				cycle = 8;
				v1 = cpu.getRegC();
				v1 = v1 & 0x01;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x42:
				// BIT 0,D
				asm = "BIT 0,D";
				cycle = 8;
				v1 = cpu.getRegD();
				v1 = v1 & 0x01;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x43:
				// BIT 0,E
				asm = "BIT 0,E";
				cycle = 8;
				v1 = cpu.getRegE();
				v1 = v1 & 0x01;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x44:
				// BIT 0,H
				asm = "BIT 0,H";
				cycle = 8;
				v1 = cpu.getRegH();
				v1 = v1 & 0x01;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x45:
				// BIT 0,L
				asm = "BIT 0,L";
				cycle = 8;
				v1 = cpu.getRegL();
				v1 = v1 & 0x01;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x46:
				// BIT 0,(HL)
				asm = "BIT 0,(HL)";
				cycle = 8;
				v1 = cpu.getRegHL();
				v1 = bus.read(v1);
				v1 = v1 & 0x01;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x47:
				// BIT 0,A
				asm = "BIT 0,A";
				cycle = 8;
				v1 = cpu.getRegA();
				v1 = v1 & 0x01;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x48:
				// BIT 1,B
				asm = "BIT 1,B";
				cycle = 8;
				v1 = cpu.getRegB();
				v1 = v1 & 0x02;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x49:
				// BIT 1,C
				asm = "BIT 1,C";
				cycle = 8;
				v1 = cpu.getRegC();
				v1 = v1 & 0x02;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x4A:
				// BIT 1,D
				asm = "BIT 1,D";
				cycle = 8;
				v1 = cpu.getRegD();
				v1 = v1 & 0x02;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x4B:
				// BIT 1,E
				asm = "BIT 1,E";
				cycle = 8;
				v1 = cpu.getRegE();
				v1 = v1 & 0x02;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x4C:
				// BIT 1,H
				asm = "BIT 1,H";
				cycle = 8;
				v1 = cpu.getRegH();
				v1 = v1 & 0x02;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x4D:
				// BIT 1,L
				asm = "BIT 1,L";
				cycle = 8;
				v1 = cpu.getRegL();
				v1 = v1 & 0x02;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x4E:
				// BIT 1,(HL)
				asm = "BIT 1,(HL)";
				cycle = 8;
				v1 = cpu.getRegHL();
				v1 = bus.read(v1);
				v1 = v1 & 0x02;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x4F:
				// BIT 1,A
				asm = "BIT 1,A";
				cycle = 8;
				v1 = cpu.getRegA();
				v1 = v1 & 0x02;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x50:
				// BIT 2,B
				asm = "BIT 2,B";
				cycle = 8;
				v1 = cpu.getRegB();
				v1 = v1 & 0x04;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x51:
				// BIT 2,C
				asm = "BIT 2,C";
				cycle = 8;
				v1 = cpu.getRegC();
				v1 = v1 & 0x04;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x52:
				// BIT 2,D
				asm = "BIT 2,D";
				cycle = 8;
				v1 = cpu.getRegD();
				v1 = v1 & 0x04;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x53:
				// BIT 2,E
				asm = "BIT 2,E";
				cycle = 8;
				v1 = cpu.getRegE();
				v1 = v1 & 0x04;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x54:
				// BIT 2,H
				asm = "BIT 2,H";
				cycle = 8;
				v1 = cpu.getRegH();
				v1 = v1 & 0x04;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x55:
				// BIT 2,L
				asm = "BIT 2,L";
				cycle = 8;
				v1 = cpu.getRegL();
				v1 = v1 & 0x04;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x56:
				// BIT 2,(HL)
				asm = "BIT 2,(HL)";
				cycle = 8;
				v1 = cpu.getRegHL();
				v1 = bus.read(v1);
				v1 = v1 & 0x04;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x57:
				// BIT 2,A
				asm = "BIT 2,A";
				cycle = 8;
				v1 = cpu.getRegA();
				v1 = v1 & 0x04;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x58:
				// BIT 3,B
				asm = "BIT 3,B";
				cycle = 8;
				v1 = cpu.getRegB();
				v1 = v1 & 0x08;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x59:
				// BIT 3,C
				asm = "BIT 3,C";
				cycle = 8;
				v1 = cpu.getRegC();
				v1 = v1 & 0x08;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x5A:
				// BIT 3,D
				asm = "BIT 3,D";
				cycle = 8;
				v1 = cpu.getRegD();
				v1 = v1 & 0x08;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x5B:
				// BIT 3,E
				asm = "BIT 3,E";
				cycle = 8;
				v1 = cpu.getRegE();
				v1 = v1 & 0x08;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x5C:
				// BIT 3,H
				asm = "BIT 3,H";
				cycle = 8;
				v1 = cpu.getRegH();
				v1 = v1 & 0x08;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x5D:
				// BIT 3,L
				asm = "BIT 3,L";
				cycle = 8;
				v1 = cpu.getRegL();
				v1 = v1 & 0x08;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x5E:
				// BIT 3,(HL)
				asm = "BIT 3,(HL)";
				cycle = 8;
				v1 = cpu.getRegHL();
				v1 = bus.read(v1);
				v1 = v1 & 0x08;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x5F:
				// BIT 3,A
				asm = "BIT 3,A";
				cycle = 8;
				v1 = cpu.getRegA();
				v1 = v1 & 0x08;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x60:
				// BIT 4,B
				asm = "BIT 4,B";
				cycle = 8;
				v1 = cpu.getRegB();
				v1 = v1 & 0x10;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x61:
				// BIT 4,C
				asm = "BIT 4,C";
				cycle = 8;
				v1 = cpu.getRegC();
				v1 = v1 & 0x10;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x62:
				// BIT 4,D
				asm = "BIT 4,D";
				cycle = 8;
				v1 = cpu.getRegD();
				v1 = v1 & 0x10;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x63:
				// BIT 4,E
				asm = "BIT 4,E";
				cycle = 8;
				v1 = cpu.getRegE();
				v1 = v1 & 0x10;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x64:
				// BIT 4,H
				asm = "BIT 4,H";
				cycle = 8;
				v1 = cpu.getRegH();
				v1 = v1 & 0x10;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x65:
				// BIT 4,L
				asm = "BIT 4,L";
				cycle = 8;
				v1 = cpu.getRegL();
				v1 = v1 & 0x10;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x66:
				// BIT 4,(HL)
				asm = "BIT 4,(HL)";
				cycle = 8;
				v1 = cpu.getRegHL();
				v1 = bus.read(v1);
				v1 = v1 & 0x10;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x67:
				// BIT 4,A
				asm = "BIT 4,A";
				cycle = 8;
				v1 = cpu.getRegA();
				v1 = v1 & 0x10;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x68:
				// BIT 5,B
				asm = "BIT 5,B";
				cycle = 8;
				v1 = cpu.getRegB();
				v1 = v1 & 0x20;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x69:
				// BIT 5,C
				asm = "BIT 5,C";
				cycle = 8;
				v1 = cpu.getRegC();
				v1 = v1 & 0x20;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x6A:
				// BIT 5,D
				asm = "BIT 5,D";
				cycle = 8;
				v1 = cpu.getRegD();
				v1 = v1 & 0x20;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x6B:
				// BIT 5,E
				asm = "BIT 5,E";
				cycle = 8;
				v1 = cpu.getRegE();
				v1 = v1 & 0x20;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x6C:
				// BIT 5,H
				asm = "BIT 5,H";
				cycle = 8;
				v1 = cpu.getRegH();
				v1 = v1 & 0x20;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x6D:
				// BIT 5,L
				asm = "BIT 5,L";
				cycle = 8;
				v1 = cpu.getRegL();
				v1 = v1 & 0x20;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x6E:
				// BIT 5,(HL)
				asm = "BIT 5,(HL)";
				cycle = 8;
				v1 = cpu.getRegHL();
				v1 = bus.read(v1);
				v1 = v1 & 0x20;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x6F:
				// BIT 5,A
				asm = "BIT 5,A";
				cycle = 8;
				v1 = cpu.getRegA();
				v1 = v1 & 0x20;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x70:
				// BIT 6,B
				asm = "BIT 6,B";
				cycle = 8;
				v1 = cpu.getRegB();
				v1 = v1 & 0x40;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x71:
				// BIT 6,C
				asm = "BIT 6,C";
				cycle = 8;
				v1 = cpu.getRegC();
				v1 = v1 & 0x40;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x72:
				// BIT 6,D
				asm = "BIT 6,D";
				cycle = 8;
				v1 = cpu.getRegD();
				v1 = v1 & 0x40;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x73:
				// BIT 6,E
				asm = "BIT 6,E";
				cycle = 8;
				v1 = cpu.getRegE();
				v1 = v1 & 0x40;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x74:
				// BIT 6,H
				asm = "BIT 6,H";
				cycle = 8;
				v1 = cpu.getRegH();
				v1 = v1 & 0x40;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x75:
				// BIT 6,L
				asm = "BIT 6,L";
				cycle = 8;
				v1 = cpu.getRegL();
				v1 = v1 & 0x40;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x76:
				// BIT 6,(HL)
				asm = "BIT 6,(HL)";
				cycle = 8;
				v1 = cpu.getRegHL();
				v1 = bus.read(v1);
				v1 = v1 & 0x40;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x77:
				// BIT 6,A
				asm = "BIT 6,A";
				cycle = 8;
				v1 = cpu.getRegA();
				v1 = v1 & 0x40;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x78:
				// BIT 7,B
				asm = "BIT 7,B";
				cycle = 8;
				v1 = cpu.getRegB();
				v1 = v1 & 0x80;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x79:
				// BIT 7,C
				asm = "BIT 7,C";
				cycle = 8;
				v1 = cpu.getRegC();
				v1 = v1 & 0x80;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x7A:
				// BIT 7,D
				asm = "BIT 7,D";
				cycle = 8;
				v1 = cpu.getRegD();
				v1 = v1 & 0x80;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x7B:
				// BIT 7,E
				asm = "BIT 7,E";
				cycle = 8;
				v1 = cpu.getRegE();
				v1 = v1 & 0x80;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x7C:
				// BIT 7,H
				asm = "BIT 7,H";
				cycle = 8;
				v1 = cpu.getRegH();
				v1 = v1 & 0x80;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x7D:
				// BIT 7,L
				asm = "BIT 7,L";
				cycle = 8;
				v1 = cpu.getRegL();
				v1 = v1 & 0x80;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x7E:
				// BIT 7,(HL)
				asm = "BIT 7,(HL)";
				cycle = 8;
				v1 = cpu.getRegHL();
				v1 = bus.read(v1);
				v1 = v1 & 0x80;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x7F:
				// BIT 7,A
				asm = "BIT 7,A";
				cycle = 8;
				v1 = cpu.getRegA();
				v1 = v1 & 0x80;
				cpu.setFlagZero(v1 == 0);
				cpu.setFlagSub(false);
				cpu.setFlagHalf(true);
				break;
				
			case 0x80:
				// RES 0,B
				asm = "RES 0,B";
				cycle = 8;
				v1 = cpu.getRegB();
				v1 = v1 & 0xFE;
				cpu.setRegB(v1);
				break;
				
			case 0x81:
				// RES 0,C
				asm = "RES 0,C";
				cycle = 8;
				v1 = cpu.getRegC();
				v1 = v1 & 0xFE;
				cpu.setRegC(v1);
				break;
				
			case 0x82:
				// RES 0,D
				asm = "RES 0,D";
				cycle = 8;
				v1 = cpu.getRegD();
				v1 = v1 & 0xFE;
				cpu.setRegD(v1);
				break;
				
			case 0x83:
				// RES 0,E
				asm = "RES 0,E";
				cycle = 8;
				v1 = cpu.getRegE();
				v1 = v1 & 0xFE;
				cpu.setRegE(v1);
				break;
				
			case 0x84:
				// RES 0,H
				asm = "RES 0,H";
				cycle = 8;
				v1 = cpu.getRegH();
				v1 = v1 & 0xFE;
				cpu.setRegH(v1);
				break;
				
			case 0x85:
				// RES 0,L
				asm = "RES 0,L";
				cycle = 8;
				v1 = cpu.getRegL();
				v1 = v1 & 0xFE;
				cpu.setRegL(v1);
				break;
				
			case 0x86:
				// RES 0,(HL)
				asm = "RES 0,(HL)";
				cycle = 16;
				v1 = cpu.getRegHL();
				v2 = bus.read(v1);
				v2 = v2 & 0xFE;
				bus.write(v1, v2);
				break;
				
			case 0x87:
				// RES 0,A
				asm = "RES 0,A";
				cycle = 8;
				v1 = cpu.getRegA();
				v1 = v1 & 0xFE;
				cpu.setRegA(v1);
				break;
				
			case 0x88:
				// RES 1,B
				asm = "RES 1,B";
				cycle = 8;
				v1 = cpu.getRegB();
				v1 = v1 & 0xFD;
				cpu.setRegB(v1);
				break;
				
			case 0x89:
				// RES 1,C
				asm = "RES 1,C";
				cycle = 8;
				v1 = cpu.getRegC();
				v1 = v1 & 0xFD;
				cpu.setRegC(v1);
				break;
				
			case 0x8A:
				// RES 1,D
				asm = "RES 1,D";
				cycle = 8;
				v1 = cpu.getRegD();
				v1 = v1 & 0xFD;
				cpu.setRegD(v1);
				break;
				
			case 0x8B:
				// RES 1,E
				asm = "RES 1,E";
				cycle = 8;
				v1 = cpu.getRegE();
				v1 = v1 & 0xFD;
				cpu.setRegE(v1);
				break;
				
			case 0x8C:
				// RES 1,H
				asm = "RES 1,H";
				cycle = 8;
				v1 = cpu.getRegH();
				v1 = v1 & 0xFD;
				cpu.setRegH(v1);
				break;
				
			case 0x8D:
				// RES 1,L
				asm = "RES 1,L";
				cycle = 8;
				v1 = cpu.getRegL();
				v1 = v1 & 0xFD;
				cpu.setRegL(v1);
				break;
				
			case 0x8E:
				// RES 1,(HL)
				asm = "RES 1,(HL)";
				cycle = 16;
				v1 = cpu.getRegHL();
				v2 = bus.read(v1);
				v2 = v2 & 0xFD;
				bus.write(v1, v2);
				break;
				
			case 0x8F:
				// RES 1,A
				asm = "RES 1,A";
				cycle = 8;
				v1 = cpu.getRegA();
				v1 = v1 & 0xFD;
				cpu.setRegA(v1);
				break;
				
			case 0x90:
				// RES 2,B
				asm = "RES 2,B";
				cycle = 8;
				v1 = cpu.getRegB();
				v1 = v1 & 0xFB;
				cpu.setRegB(v1);
				break;
				
			case 0x91:
				// RES 2,C
				asm = "RES 2,C";
				cycle = 8;
				v1 = cpu.getRegC();
				v1 = v1 & 0xFB;
				cpu.setRegC(v1);
				break;
				
			case 0x92:
				// RES 2,D
				asm = "RES 2,D";
				cycle = 8;
				v1 = cpu.getRegD();
				v1 = v1 & 0xFB;
				cpu.setRegD(v1);
				break;
				
			case 0x93:
				// RES 2,E
				asm = "RES 2,E";
				cycle = 8;
				v1 = cpu.getRegE();
				v1 = v1 & 0xFB;
				cpu.setRegE(v1);
				break;
				
			case 0x94:
				// RES 2,H
				asm = "RES 2,H";
				cycle = 8;
				v1 = cpu.getRegH();
				v1 = v1 & 0xFB;
				cpu.setRegH(v1);
				break;
				
			case 0x95:
				// RES 2,L
				asm = "RES 2,L";
				cycle = 8;
				v1 = cpu.getRegL();
				v1 = v1 & 0xFB;
				cpu.setRegL(v1);
				break;
				
			case 0x96:
				// RES 2,(HL)
				asm = "RES 2,(HL)";
				cycle = 16;
				v1 = cpu.getRegHL();
				v2 = bus.read(v1);
				v2 = v2 & 0xFB;
				bus.write(v1, v2);
				break;
				
			case 0x97:
				// RES 2,A
				asm = "RES 2,A";
				cycle = 8;
				v1 = cpu.getRegA();
				v1 = v1 & 0xFB;
				cpu.setRegA(v1);
				break;
				
			case 0x98:
				// RES 3,B
				asm = "RES 3,B";
				cycle = 8;
				v1 = cpu.getRegB();
				v1 = v1 & 0xF7;
				cpu.setRegB(v1);
				break;
				
			case 0x99:
				// RES 3,C
				asm = "RES 3,C";
				cycle = 8;
				v1 = cpu.getRegC();
				v1 = v1 & 0xF7;
				cpu.setRegC(v1);
				break;
				
			case 0x9A:
				// RES 3,D
				asm = "RES 3,D";
				cycle = 8;
				v1 = cpu.getRegD();
				v1 = v1 & 0xF7;
				cpu.setRegD(v1);
				break;
				
			case 0x9B:
				// RES 3,E
				asm = "RES 3,E";
				cycle = 8;
				v1 = cpu.getRegE();
				v1 = v1 & 0xF7;
				cpu.setRegE(v1);
				break;
				
			case 0x9C:
				// RES 3,H
				asm = "RES 3,H";
				cycle = 8;
				v1 = cpu.getRegH();
				v1 = v1 & 0xF7;
				cpu.setRegH(v1);
				break;
				
			case 0x9D:
				// RES 3,L
				asm = "RES 3,L";
				cycle = 8;
				v1 = cpu.getRegL();
				v1 = v1 & 0xF7;
				cpu.setRegL(v1);
				break;
				
			case 0x9E:
				// RES 3,(HL)
				asm = "RES 3,(HL)";
				cycle = 16;
				v1 = cpu.getRegHL();
				v2 = bus.read(v1);
				v2 = v2 & 0xF7;
				bus.write(v1, v2);
				break;
				
			case 0x9F:
				// RES 3,A
				asm = "RES 3,A";
				cycle = 8;
				v1 = cpu.getRegA();
				v1 = v1 & 0xF7;
				cpu.setRegA(v1);
				break;
				
			case 0xA0:
				// RES 4,B
				asm = "RES 4,B";
				cycle = 8;
				v1 = cpu.getRegB();
				v1 = v1 & 0xEF;
				cpu.setRegB(v1);
				break;
				
			case 0xA1:
				// RES 4,C
				asm = "RES 4,C";
				cycle = 8;
				v1 = cpu.getRegC();
				v1 = v1 & 0xEF;
				cpu.setRegC(v1);
				break;
				
			case 0xA2:
				// RES 4,D
				asm = "RES 4,D";
				cycle = 8;
				v1 = cpu.getRegD();
				v1 = v1 & 0xEF;
				cpu.setRegD(v1);
				break;
				
			case 0xA3:
				// RES 4,E
				asm = "RES 4,E";
				cycle = 8;
				v1 = cpu.getRegE();
				v1 = v1 & 0xEF;
				cpu.setRegE(v1);
				break;
				
			case 0xA4:
				// RES 4,H
				asm = "RES 4,H";
				cycle = 8;
				v1 = cpu.getRegH();
				v1 = v1 & 0xEF;
				cpu.setRegH(v1);
				break;
				
			case 0xA5:
				// RES 4,L
				asm = "RES 4,L";
				cycle = 8;
				v1 = cpu.getRegL();
				v1 = v1 & 0xEF;
				cpu.setRegL(v1);
				break;
				
			case 0xA6:
				// RES 4,(HL)
				asm = "RES 4,(HL)";
				cycle = 16;
				v1 = cpu.getRegHL();
				v2 = bus.read(v1);
				v2 = v2 & 0xEF;
				bus.write(v1, v2);
				break;
				
			case 0xA7:
				// RES 4,A
				asm = "RES 4,A";
				cycle = 8;
				v1 = cpu.getRegA();
				v1 = v1 & 0xEF;
				cpu.setRegA(v1);
				break;
				
			case 0xA8:
				// RES 5,B
				asm = "RES 5,B";
				cycle = 8;
				v1 = cpu.getRegB();
				v1 = v1 & 0xDF;
				cpu.setRegB(v1);
				break;
				
			case 0xA9:
				// RES 5,C
				asm = "RES 5,C";
				cycle = 8;
				v1 = cpu.getRegC();
				v1 = v1 & 0xDF;
				cpu.setRegC(v1);
				break;
				
			case 0xAA:
				// RES 5,D
				asm = "RES 5,D";
				cycle = 8;
				v1 = cpu.getRegD();
				v1 = v1 & 0xDF;
				cpu.setRegD(v1);
				break;
				
			case 0xAB:
				// RES 5,E
				asm = "RES 5,E";
				cycle = 8;
				v1 = cpu.getRegE();
				v1 = v1 & 0xDF;
				cpu.setRegE(v1);
				break;
				
			case 0xAC:
				// RES 5,H
				asm = "RES 5,H";
				cycle = 8;
				v1 = cpu.getRegH();
				v1 = v1 & 0xDF;
				cpu.setRegH(v1);
				break;
				
			case 0xAD:
				// RES 5,L
				asm = "RES 5,L";
				cycle = 8;
				v1 = cpu.getRegL();
				v1 = v1 & 0xDF;
				cpu.setRegL(v1);
				break;
				
			case 0xAE:
				// RES 5,(HL)
				asm = "RES 5,(HL)";
				cycle = 16;
				v1 = cpu.getRegHL();
				v2 = bus.read(v1);
				v2 = v2 & 0xDF;
				bus.write(v1, v2);
				break;
				
			case 0xAF:
				// RES 5,A
				asm = "RES 5,A";
				cycle = 8;
				v1 = cpu.getRegA();
				v1 = v1 & 0xDF;
				cpu.setRegA(v1);
				break;
				
			case 0xB0:
				// RES 6,B
				asm = "RES 6,B";
				cycle = 8;
				v1 = cpu.getRegB();
				v1 = v1 & 0xBF;
				cpu.setRegB(v1);
				break;
				
			case 0xB1:
				// RES 6,C
				asm = "RES 6,C";
				cycle = 8;
				v1 = cpu.getRegC();
				v1 = v1 & 0xBF;
				cpu.setRegC(v1);
				break;
				
			case 0xB2:
				// RES 6,D
				asm = "RES 6,D";
				cycle = 8;
				v1 = cpu.getRegD();
				v1 = v1 & 0xBF;
				cpu.setRegD(v1);
				break;
				
			case 0xB3:
				// RES 6,E
				asm = "RES 6,E";
				cycle = 8;
				v1 = cpu.getRegE();
				v1 = v1 & 0xBF;
				cpu.setRegE(v1);
				break;
				
			case 0xB4:
				// RES 6,H
				asm = "RES 6,H";
				cycle = 8;
				v1 = cpu.getRegH();
				v1 = v1 & 0xBF;
				cpu.setRegH(v1);
				break;
				
			case 0xB5:
				// RES 6,L
				asm = "RES 6,L";
				cycle = 8;
				v1 = cpu.getRegL();
				v1 = v1 & 0xBF;
				cpu.setRegL(v1);
				break;
				
			case 0xB6:
				// RES 6,(HL)
				asm = "RES 6,(HL)";
				cycle = 16;
				v1 = cpu.getRegHL();
				v2 = bus.read(v1);
				v2 = v2 & 0xBF;
				bus.write(v1, v2);
				break;
				
			case 0xB7:
				// RES 6,A
				asm = "RES 6,A";
				cycle = 8;
				v1 = cpu.getRegA();
				v1 = v1 & 0xBF;
				cpu.setRegA(v1);
				break;
				
			case 0xB8:
				// RES 7,B
				asm = "RES 7,B";
				cycle = 8;
				v1 = cpu.getRegB();
				v1 = v1 & 0x7F;
				cpu.setRegB(v1);
				break;
				
			case 0xB9:
				// RES 7,C
				asm = "RES 7,C";
				cycle = 8;
				v1 = cpu.getRegC();
				v1 = v1 & 0x7F;
				cpu.setRegC(v1);
				break;
				
			case 0xBA:
				// RES 7,D
				asm = "RES 7,D";
				cycle = 8;
				v1 = cpu.getRegD();
				v1 = v1 & 0x7F;
				cpu.setRegD(v1);
				break;
				
			case 0xBB:
				// RES 7,E
				asm = "RES 7,E";
				cycle = 8;
				v1 = cpu.getRegE();
				v1 = v1 & 0x7F;
				cpu.setRegE(v1);
				break;
				
			case 0xBC:
				// RES 7,H
				asm = "RES 7,H";
				cycle = 8;
				v1 = cpu.getRegH();
				v1 = v1 & 0x7F;
				cpu.setRegH(v1);
				break;
				
			case 0xBD:
				// RES 7,L
				asm = "RES 7,L";
				cycle = 8;
				v1 = cpu.getRegL();
				v1 = v1 & 0x7F;
				cpu.setRegL(v1);
				break;
				
			case 0xBE:
				// RES 7,(HL)
				asm = "RES 7,(HL)";
				cycle = 16;
				v1 = cpu.getRegHL();
				v2 = bus.read(v1);
				v2 = v2 & 0x7F;
				bus.write(v1, v2);
				break;
				
			case 0xBF:
				// RES 7,A
				asm = "RES 7,A";
				cycle = 8;
				v1 = cpu.getRegA();
				v1 = v1 & 0x7F;
				cpu.setRegA(v1);
				break;
				
			case 0xC0:
				// SET 0,B
				asm = "SET 0,B";
				cycle = 8;
				v1 = cpu.getRegB();
				v1 = v1 | 0x01;
				cpu.setRegB(v1);
				break;
				
			case 0xC1:
				// SET 0,C
				asm = "SET 0,C";
				cycle = 8;
				v1 = cpu.getRegC();
				v1 = v1 | 0x01;
				cpu.setRegC(v1);
				break;
				
			case 0xC2:
				// SET 0,D
				asm = "SET 0,D";
				cycle = 8;
				v1 = cpu.getRegD();
				v1 = v1 | 0x01;
				cpu.setRegD(v1);
				break;
				
			case 0xC3:
				// SET 0,E
				asm = "SET 0,E";
				cycle = 8;
				v1 = cpu.getRegE();
				v1 = v1 | 0x01;
				cpu.setRegE(v1);
				break;
				
			case 0xC4:
				// SET 0,H
				asm = "SET 0,H";
				cycle = 8;
				v1 = cpu.getRegH();
				v1 = v1 | 0x01;
				cpu.setRegH(v1);
				break;
				
			case 0xC5:
				// SET 0,L
				asm = "SET 0,L";
				cycle = 8;
				v1 = cpu.getRegL();
				v1 = v1 | 0x01;
				cpu.setRegL(v1);
				break;
				
			case 0xC6:
				// SET 0,(HL)
				asm = "SET 0,(HL)";
				cycle = 16;
				v1 = cpu.getRegHL();
				v2 = bus.read(v1);
				v2 = v2 | 0x01;
				bus.write(v1, v2);
				break;
				
			case 0xC7:
				// SET 0,A
				asm = "SET 0,A";
				cycle = 8;
				v1 = cpu.getRegA();
				v1 = v1 | 0x01;
				cpu.setRegA(v1);
				break;
				
			case 0xC8:
				// SET 1,B
				asm = "SET 1,B";
				cycle = 8;
				v1 = cpu.getRegB();
				v1 = v1 | 0x02;
				cpu.setRegB(v1);
				break;
				
			case 0xC9:
				// SET 1,C
				asm = "SET 1,C";
				cycle = 8;
				v1 = cpu.getRegC();
				v1 = v1 | 0x02;
				cpu.setRegC(v1);
				break;
				
			case 0xCA:
				// SET 1,D
				asm = "SET 1,D";
				cycle = 8;
				v1 = cpu.getRegD();
				v1 = v1 | 0x02;
				cpu.setRegD(v1);
				break;
				
			case 0xCB:
				// SET 1,E
				asm = "SET 1,E";
				cycle = 8;
				v1 = cpu.getRegE();
				v1 = v1 | 0x02;
				cpu.setRegE(v1);
				break;
				
			case 0xCC:
				// SET 1,H
				asm = "SET 1,H";
				cycle = 8;
				v1 = cpu.getRegH();
				v1 = v1 | 0x02;
				cpu.setRegH(v1);
				break;
				
			case 0xCD:
				// SET 1,L
				asm = "SET 1,L";
				cycle = 8;
				v1 = cpu.getRegL();
				v1 = v1 | 0x02;
				cpu.setRegL(v1);
				break;
				
			case 0xCE:
				// SET 1,(HL)
				asm = "SET 1,(HL)";
				cycle = 16;
				v1 = cpu.getRegHL();
				v2 = bus.read(v1);
				v2 = v2 | 0x02;
				bus.write(v1, v2);
				break;
				
			case 0xCF:
				// SET 1,A
				asm = "SET 1,A";
				cycle = 8;
				v1 = cpu.getRegA();
				v1 = v1 | 0x02;
				cpu.setRegA(v1);
				break;
				
			case 0xD0:
				// SET 2,B
				asm = "SET 2,B";
				cycle = 8;
				v1 = cpu.getRegB();
				v1 = v1 | 0x04;
				cpu.setRegB(v1);
				break;
				
			case 0xD1:
				// SET 2,C
				asm = "SET 2,C";
				cycle = 8;
				v1 = cpu.getRegC();
				v1 = v1 | 0x04;
				cpu.setRegC(v1);
				break;
				
			case 0xD2:
				// SET 2,D
				asm = "SET 2,D";
				cycle = 8;
				v1 = cpu.getRegD();
				v1 = v1 | 0x04;
				cpu.setRegD(v1);
				break;
				
			case 0xD3:
				// SET 2,E
				asm = "SET 2,E";
				cycle = 8;
				v1 = cpu.getRegE();
				v1 = v1 | 0x04;
				cpu.setRegE(v1);
				break;
				
			case 0xD4:
				// SET 2,H
				asm = "SET 2,H";
				cycle = 8;
				v1 = cpu.getRegH();
				v1 = v1 | 0x04;
				cpu.setRegH(v1);
				break;
				
			case 0xD5:
				// SET 2,L
				asm = "SET 2,L";
				cycle = 8;
				v1 = cpu.getRegL();
				v1 = v1 | 0x04;
				cpu.setRegL(v1);
				break;
				
			case 0xD6:
				// SET 2,(HL)
				asm = "SET 2,(HL)";
				cycle = 16;
				v1 = cpu.getRegHL();
				v2 = bus.read(v1);
				v2 = v2 | 0x04;
				bus.write(v1, v2);
				break;
				
			case 0xD7:
				// SET 2,A
				asm = "SET 2,A";
				cycle = 8;
				v1 = cpu.getRegA();
				v1 = v1 | 0x04;
				cpu.setRegA(v1);
				break;
				
			case 0xD8:
				// SET 3,B
				asm = "SET 3,B";
				cycle = 8;
				v1 = cpu.getRegB();
				v1 = v1 | 0x08;
				cpu.setRegB(v1);
				break;
				
			case 0xD9:
				// SET 3,C
				asm = "SET 3,C";
				cycle = 8;
				v1 = cpu.getRegC();
				v1 = v1 | 0x08;
				cpu.setRegC(v1);
				break;
				
			case 0xDA:
				// SET 3,D
				asm = "SET 3,D";
				cycle = 8;
				v1 = cpu.getRegD();
				v1 = v1 | 0x08;
				cpu.setRegD(v1);
				break;
				
			case 0xDB:
				// SET 3,E
				asm = "SET 3,E";
				cycle = 8;
				v1 = cpu.getRegE();
				v1 = v1 | 0x08;
				cpu.setRegE(v1);
				break;
				
			case 0xDC:
				// SET 3,H
				asm = "SET 3,H";
				cycle = 8;
				v1 = cpu.getRegH();
				v1 = v1 | 0x08;
				cpu.setRegH(v1);
				break;
				
			case 0xDD:
				// SET 3,L
				asm = "SET 3,L";
				cycle = 8;
				v1 = cpu.getRegL();
				v1 = v1 | 0x08;
				cpu.setRegL(v1);
				break;
				
			case 0xDE:
				// SET 3,(HL)
				asm = "SET 3,(HL)";
				cycle = 16;
				v1 = cpu.getRegHL();
				v2 = bus.read(v1);
				v2 = v2 | 0x08;
				bus.write(v1, v2);
				break;
				
			case 0xDF:
				// SET 3,A
				asm = "SET 3,A";
				cycle = 8;
				v1 = cpu.getRegA();
				v1 = v1 | 0x08;
				cpu.setRegA(v1);
				break;
				
			case 0xE0:
				// SET 4,B
				asm = "SET 4,B";
				cycle = 8;
				v1 = cpu.getRegB();
				v1 = v1 | 0x10;
				cpu.setRegB(v1);
				break;
				
			case 0xE1:
				// SET 4,C
				asm = "SET 4,C";
				cycle = 8;
				v1 = cpu.getRegC();
				v1 = v1 | 0x10;
				cpu.setRegC(v1);
				break;
				
			case 0xE2:
				// SET 4,D
				asm = "SET 4,D";
				cycle = 8;
				v1 = cpu.getRegD();
				v1 = v1 | 0x10;
				cpu.setRegD(v1);
				break;
				
			case 0xE3:
				// SET 4,E
				asm = "SET 4,E";
				cycle = 8;
				v1 = cpu.getRegE();
				v1 = v1 | 0x10;
				cpu.setRegE(v1);
				break;
				
			case 0xE4:
				// SET 4,H
				asm = "SET 4,H";
				cycle = 8;
				v1 = cpu.getRegH();
				v1 = v1 | 0x10;
				cpu.setRegH(v1);
				break;
				
			case 0xE5:
				// SET 4,L
				asm = "SET 4,L";
				cycle = 8;
				v1 = cpu.getRegL();
				v1 = v1 | 0x10;
				cpu.setRegL(v1);
				break;
				
			case 0xE6:
				// SET 4,(HL)
				asm = "SET 4,(HL)";
				cycle = 16;
				v1 = cpu.getRegHL();
				v2 = bus.read(v1);
				v2 = v2 | 0x10;
				bus.write(v1, v2);
				break;
				
			case 0xE7:
				// SET 4,A
				asm = "SET 4,A";
				cycle = 8;
				v1 = cpu.getRegA();
				v1 = v1 | 0x10;
				cpu.setRegA(v1);
				break;
				
			case 0xE8:
				// SET 5,B
				asm = "SET 5,B";
				cycle = 8;
				v1 = cpu.getRegB();
				v1 = v1 | 0x20;
				cpu.setRegB(v1);
				break;
				
			case 0xE9:
				// SET 5,C
				asm = "SET 5,C";
				cycle = 8;
				v1 = cpu.getRegC();
				v1 = v1 | 0x20;
				cpu.setRegC(v1);
				break;
				
			case 0xEA:
				// SET 5,D
				asm = "SET 5,D";
				cycle = 8;
				v1 = cpu.getRegD();
				v1 = v1 | 0x20;
				cpu.setRegD(v1);
				break;
				
			case 0xEB:
				// SET 5,E
				asm = "SET 5,E";
				cycle = 8;
				v1 = cpu.getRegE();
				v1 = v1 | 0x20;
				cpu.setRegE(v1);
				break;
				
			case 0xEC:
				// SET 5,H
				asm = "SET 5,H";
				cycle = 8;
				v1 = cpu.getRegH();
				v1 = v1 | 0x20;
				cpu.setRegH(v1);
				break;
				
			case 0xED:
				// SET 5,L
				asm = "SET 5,L";
				cycle = 8;
				v1 = cpu.getRegL();
				v1 = v1 | 0x20;
				cpu.setRegL(v1);
				break;
				
			case 0xEE:
				// SET 5,(HL)
				asm = "SET 5,(HL)";
				cycle = 16;
				v1 = cpu.getRegHL();
				v2 = bus.read(v1);
				v2 = v2 | 0x20;
				bus.write(v1, v2);
				break;
				
			case 0xEF:
				// SET 5,A
				asm = "SET 5,A";
				cycle = 8;
				v1 = cpu.getRegA();
				v1 = v1 | 0x20;
				cpu.setRegA(v1);
				break;
				
			case 0xF0:
				// SET 6,B
				asm = "SET 6,B";
				cycle = 8;
				v1 = cpu.getRegB();
				v1 = v1 | 0x40;
				cpu.setRegB(v1);
				break;
				
			case 0xF1:
				// SET 6,C
				asm = "SET 6,C";
				cycle = 8;
				v1 = cpu.getRegC();
				v1 = v1 | 0x40;
				cpu.setRegC(v1);
				break;
				
			case 0xF2:
				// SET 6,D
				asm = "SET 6,D";
				cycle = 8;
				v1 = cpu.getRegD();
				v1 = v1 | 0x40;
				cpu.setRegD(v1);
				break;
				
			case 0xF3:
				// SET 6,E
				asm = "SET 6,E";
				cycle = 8;
				v1 = cpu.getRegE();
				v1 = v1 | 0x40;
				cpu.setRegE(v1);
				break;
				
			case 0xF4:
				// SET 6,H
				asm = "SET 6,H";
				cycle = 8;
				v1 = cpu.getRegH();
				v1 = v1 | 0x40;
				cpu.setRegH(v1);
				break;
				
			case 0xF5:
				// SET 6,L
				asm = "SET 6,L";
				cycle = 8;
				v1 = cpu.getRegL();
				v1 = v1 | 0x40;
				cpu.setRegL(v1);
				break;
				
			case 0xF6:
				// SET 6,(HL)
				asm = "SET 6,(HL)";
				cycle = 16;
				v1 = cpu.getRegHL();
				v2 = bus.read(v1);
				v2 = v2 | 0x40;
				bus.write(v1, v2);
				break;
				
			case 0xF7:
				// SET 6,A
				asm = "SET 6,A";
				cycle = 8;
				v1 = cpu.getRegA();
				v1 = v1 | 0x40;
				cpu.setRegA(v1);
				break;
				
			case 0xF8:
				// SET 7,B
				asm = "SET 7,B";
				cycle = 8;
				v1 = cpu.getRegB();
				v1 = v1 | 0x80;
				cpu.setRegB(v1);
				break;
				
			case 0xF9:
				// SET 7,C
				asm = "SET 7,C";
				cycle = 8;
				v1 = cpu.getRegC();
				v1 = v1 | 0x80;
				cpu.setRegC(v1);
				break;
				
			case 0xFA:
				// SET 7,D
				asm = "SET 7,D";
				cycle = 8;
				v1 = cpu.getRegD();
				v1 = v1 | 0x80;
				cpu.setRegD(v1);
				break;
				
			case 0xFB:
				// SET 7,E
				asm = "SET 7,E";
				cycle = 8;
				v1 = cpu.getRegE();
				v1 = v1 | 0x80;
				cpu.setRegE(v1);
				break;
				
			case 0xFC:
				// SET 7,H
				asm = "SET 7,H";
				cycle = 8;
				v1 = cpu.getRegH();
				v1 = v1 | 0x80;
				cpu.setRegH(v1);
				break;
				
			case 0xFD:
				// SET 7,L
				asm = "SET 7,L";
				cycle = 8;
				v1 = cpu.getRegL();
				v1 = v1 | 0x80;
				cpu.setRegL(v1);
				break;
				
			case 0xFE:
				// SET 7,(HL)
				asm = "SET 7,(HL)";
				cycle = 16;
				v1 = cpu.getRegHL();
				v2 = bus.read(v1);
				v2 = v2 | 0x80;
				bus.write(v1, v2);
				break;
				
			case 0xFF:
				// SET 7,A
				asm = "SET 7,A";
				cycle = 8;
				v1 = cpu.getRegA();
				v1 = v1 | 0x80;
				cpu.setRegA(v1);
				break;
				
			default:
				throw new JJUnsupportedOpcodeException("Unknown opcode: CB", opcode, cpu);
		}
		
		cpu.setPC(pc);
		
		asmCode[currPC] = asm;
		asmParam[currPC] = param;
		
		asmTrace[0] += asm;
		
		return cycle;
	}

	public int callInterrupt(int address) {
		cpu.disableIME();
		
		if (JJInstructionTracerHelper.getInstance().isLogging(cpu.getPC(), -1)) {
			JJInstructionTracerHelper.getInstance().addTrace(String.format("%04x: %s \t-\t%s\t-\t%s", cpu.getPC(), "IRQ", String.valueOf(address), cpu.toString()));
		}
		
		// CALL
		int pc = cpu.getPC();
		int sp = cpu.getSP();
		
		int v1 = ((pc) & 0xFF00) >> 8;
		int v2 = ((pc) & 0x00FF);
		
		bus.write(--sp, v1);
		bus.write(--sp, v2);
		
		pc = address;
		
		cpu.setPC(pc);
		cpu.setSP(sp);
		
		// 12 cycles passed
		return 12;
	}
	
}
