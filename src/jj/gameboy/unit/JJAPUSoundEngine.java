package jj.gameboy.unit;

import jj.gameboy.output.sound.JJAmplifier;
import jj.gameboy.output.sound.JJAmplifierSoundEngine;

public class JJAPUSoundEngine extends JJAPU {
	
	public JJAPUSoundEngine(JJMainBus bus) {
		super(bus);
	}
	
	@Override
	protected JJAmplifier initAmplifier() {
		return new JJAmplifierSoundEngine(this);
	}
	
}
