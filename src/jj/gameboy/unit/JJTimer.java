package jj.gameboy.unit;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class JJTimer implements JJSaveStateUnit {
	
	public static final int ADDRESS_DIV = 0xFF04;
	public static final int ADDRESS_TIMA = 0xFF05;
	public static final int ADDRESS_TMA = 0xFF06;
	public static final int ADDRESS_TAC = 0xFF07;
	
	private JJMainBus bus;
	
	/**
	 * The clock speed.
	 * 
	 * <table>
	 * <tr><td>
	 * 00: CPU Clock / 1024 (DMG, CGB:   4096 Hz, SGB:   ~4194 Hz)
	 * </td></tr>
	 * <tr><td>
	 * 01: CPU Clock / 16   (DMG, CGB: 262144 Hz, SGB: ~268400 Hz)
	 * </td></tr>
	 * <tr><td>
	 * 10: CPU Clock / 64   (DMG, CGB:  65536 Hz, SGB:  ~67110 Hz)
	 * </td></tr>
	 * <tr><td>
	 * 11: CPU Clock / 256  (DMG, CGB:  16384 Hz, SGB:  ~16780 Hz)
	 * </td></tr>
	 * </table>
	 */
	private int clockSpeed = 3;
	
	private boolean isEnabled = false;
	
	private int regDivider = 0;
	private int regTimer = 0;
	
	/**
	 * The reloading state of TIMA.
	 */
	private boolean timaReloading = false;
	
	/**
	 * The overflow state of TIMA.
	 */
	private boolean timaOverflow = false;
	
	/**
	 * The value TMA.
	 */
	private int regTMA;
	
	public JJTimer(JJMainBus bus) {
		this.bus = bus;
	}
	
	public void reset() {
		clockSpeed = 3;
		regDivider = 0;
		regTimer = 0;
		isEnabled = false;
	}
	
	public void resetDIV() {
		// trigger timer increment by resetting the divider
		checkResetHighToLow();
		
		regDivider = 0;
	}
	
	/**
	 * Checks whether we have to trigger a timer increment due to the switch from high to low.
	 */
	private void checkResetHighToLow() {
		boolean prev = getTimerBit(regDivider, clockSpeed);
		boolean next = false;
		if (prev && !next) {
			// increase the timer
			incrementTimer();
		}
	}
	
	public int getDIV() {
		return regDivider >> 8;
	}
	
	public int getTAC() {
		return (isEnabled ? 0x04 : 0x00) | clockSpeed;
	}
	
	public int getTIMA() {
		return regTimer;
	}
	
	public void setTIMA(int value) {
		if (isEnabled) {
			// ignore while we were just reloading
			if (timaReloading) {
				return;
			}
			if (timaOverflow) {
				timaOverflow = false;
			}
		}
		regTimer = value;
	}
	
	public int getTMA() {
		return regTMA;
	}
	
	public void setTMA(int tma) {
		this.regTMA = tma;
		
		if (isEnabled && timaReloading) {
			regTimer = tma;
			timaReloading = false;
		}
	}
	
	public void setTimerSpeed(int speed) {
		int oldClockSpeed = clockSpeed;
		boolean oldEnabled = isEnabled;
		
		clockSpeed = speed & 0x03;
		isEnabled = (speed & 0x04) > 0;
		
		// weird additional timer increase
		boolean additionalTimerIncrease = false;
		if (!oldEnabled) {
			if (isEnabled) {
				additionalTimerIncrease = getTimerBit(regDivider, oldClockSpeed);
			} else {
				additionalTimerIncrease = getTimerBit(regDivider, oldClockSpeed) && getTimerBit(regDivider, clockSpeed);
			}
			
			if (additionalTimerIncrease) {
				incrementTimer();
			}
		}
	}
	
	/**
	 * Increments the counter depending on the instruction cycles in this "period".
	 * 
	 * @param cycles The number of cycles of the current instruction.
	 */
	public void handleCycles(int cycles) {
		// the divider is always increased
		int oldRegDivider = regDivider;
		regDivider = (regDivider + cycles) & 0xFFFF;
		
		if (timaReloading) {
			// deactivate reloading
			// one cycle after it happened
			timaReloading = false;
		}
		if (timaOverflow) {
			// spill over, thus, interrupt and reset
			bus.getIOPort().writeFromComponent(JJMainBus.ADDRESS_INTERRUPT, JJInterruptService.INTERRUPT_TIMER);
			// deactivate overflow
			timaOverflow = false;
			
			// set the new value
			regTimer = regTMA;
			// activate on more cycle of ignoring inputs to TIMA
			timaReloading = true;
		}
		
		if (isEnabled) {
			checkCounter(oldRegDivider, cycles);
		}
	}
	
	/**
	 * Checks the counter for incrementing the timer.
	 * 
	 * @param oldRegDivider The old value of the divider registry.
	 * @param cycles The number of cycles of the current instruction.
	 */
	private void checkCounter(int oldRegDivider, int cycles) {
		// we increment each machine cycle
		int machineCycles = cycles / 4;
		boolean prev = getTimerBit(oldRegDivider, clockSpeed);
		for (int i = 1; i <= machineCycles; i++) {
			boolean next = getTimerBit(oldRegDivider + 4 * i, clockSpeed);
			// test for a change from low to high
			if (prev && !next) {
				// increase the timer
				incrementTimer();
			}
			prev = next;
		}
	}
	
	/**
	 * Returns the bit depending on the currently set speed.
	 * 
	 * @param testCycles The cycles to test.
	 * 
	 * @return true if bit is 1, false if bit is 0.
	 */
	private boolean getTimerBit(int testCycles, int clockSpeed) {
		boolean retval = false;
		switch (clockSpeed) {
			case 0:
				// test bit 9
				retval = (testCycles & 0x0200) > 0;
				break;
			case 1:
				// test bit 3
				retval = (testCycles & 0x0008) > 0;
				break;
			case 2:
				// test bit 5
				retval = (testCycles & 0x0020) > 0;
				break;
			case 3:
				// test bit 7
				retval = (testCycles & 0x0080) > 0;
				break;
		}
		return retval;
	}
	
	private void incrementTimer() {
		regTimer++;
		if (regTimer > 0xFF) {
			// adding 1 results in 0
			regTimer = 0;
			
			timaOverflow = true;
		}
	}
	
	@Override
	public void dumpState(OutputStream out) throws IOException {
		out.write(clockSpeed);
		out.write(isEnabled ? 1 : 0);
		out.write(timaOverflow ? 1 : 0);
		out.write(timaReloading ? 1 : 0);
		out.write(regDivider);
		out.write(regTimer);
		out.write(regTMA);
	}
	
	@Override
	public void reloadState(InputStream in) throws IOException {
		clockSpeed = in.read();
		isEnabled = in.read() == 1;
		timaOverflow = in.read() == 1;
		timaReloading = in.read() == 1;
		regDivider = in.read();
		regTimer = in.read();
		regTMA = in.read();
	}
	
}
