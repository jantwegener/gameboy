package jj.gameboy.unit;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import jj.gameboy.util.JJInstructionTracerHelper;

/**
 * The central processing unit (CPU).
 * 
 * 
 * @author jan-thierry.wegener
 *
 */
public class JJCPU implements JJIProcessor, JJSaveStateUnit {
	
	// the different register of the CPU
	private int regA;
	private int regB;
	private int regC;
	private int regD;
	private int regE;
	private int regF;
	private int regH;
	private int regL;
	
	// internal pointer
	// program counter
	int pc = 0;
	// stack pointer
	int sp = 0xFFFE;
	
	// our helper class for the instructions
	private JJCPUInstruction instruction;
	// our helper class for interrupts
	private JJInterruptService interruptService;
	// our helper class for timer
	private JJTimer timer;
	
	public JJCPU(JJMainBus bus) {
		// helper
		instruction = new JJCPUInstruction(this, bus);
		
		interruptService = new JJInterruptService(this, bus);
		
		timer = new JJTimer(bus);
	}
	
	public void reset() {
		setPC(0);
		setSP(0xFFFE);
		setRegAF(0);
		setRegBC(0);
		setRegDE(0);
		setRegHL(0);
		
		timer.reset();
		interruptService.reset();
	}
	
	public int tick(int cpuClock) {
		int cycle;
		try {
			cycle = instruction.next();
			
			// handle the interrupts
			cycle += interruptService.handleInterrupt();
			
			// handle the timer
			timer.handleCycles(cycle);
		} catch (Exception e) {
			String file = "log\\rom.asm";
			System.out.println(this);
			writeASM(file);
			
			String queueFile = "log\\callQueue.asm";
			JJInstructionTracerHelper.getInstance().writeQueue(queueFile, true);
			
			throw e;
		}
	
		return cycle;
	}
	
	/**
	 * Enables the interrupts.
	 * 
	 * Bit	When 0			When 1
	 * 0	Vblank off		Vblank on
	 * 1	LCD stat off	LCD stat on
	 * 2	Timer off		Timer on
	 * 3	Serial off		Serial on
	 * 4	Joypad off		Joypad on
	 * 
	 * @param value The handed value.
	 */
	public void enableInterrupts(int value) {
		interruptService.enableInterrupts(value);
	}
	
	public int getEnabledInterrupts() {
		return interruptService.getEnabledInterrupts();
	}
	
	/**
	 * Writes the current state of the ASM code to the given file.
	 * 
	 * @param file The file name to write to.
	 */
	public void writeASM(String file) {
		String[] asmCode = instruction.getAsmCode();
		String[] asmParam = instruction.getAsmParam();
		
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
			for (int i = 0; i < asmCode.length; i++) {
				String asm = asmCode[i];
				if (asm != null) {
					String outline = String.format("%04x : %s", i, asm);
					writer.write(outline);
					String param = asmParam[i];
					if (param != null) {
						outline = String.format("\t\t%s", param);
						writer.write(outline);
					}
					writer.newLine();
				}
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
	public void resetASM() {
		instruction.resetASM();
	}
	
	public boolean isZero() {
		return (regF & 0x80) > 0;
	}
	
	public boolean isSubtraction() {
		return (regF & 0x40) > 0;
	}
	
	public boolean isHalfCarry() {
		return (regF & 0x20) > 0;
	}
	
	public boolean isCarry() {
		return (regF & 0x10) > 0;
	}
	
	public void setFlags(boolean zero, boolean sub, boolean half, boolean carry) {
		setFlagZero(zero);
		setFlagSub(sub);
		setFlagHalf(half);
		setFlagCarry(carry);
	}
	
	public void setFlagZero(boolean v) {
		if (v) {
			regF = 0x80 | regF;
		} else {
			regF = 0x7F & regF;
		}
	}
	
	public void setFlagSub(boolean v) {
		if (v) {
			regF = 0x40 | regF;
		} else {
			regF = 0xBF & regF;
		}
	}
	
	public void setFlagHalf(boolean v) {
		if (v) {
			regF = 0x20 | regF;
		} else {
			regF = 0xDF & regF;
		}
	}
	
	public void setFlagCarry(boolean v) {
		if (v) {
			regF = 0x10 | regF;
		} else {
			regF = 0xEF & regF;
		}
	}
	
	public int getPC() {
		return pc;
	}
	
	public void setPC(int pc) {
		this.pc = pc & 0xFFFF;
	}
	
	public void setPC(int v1, int v2) {
		this.pc = ((v1 << 8) | v2)  & 0xFFFF;
	}
	
	public void setSP(int v1, int v2) {
		this.sp = ((v1 << 8) | v2)  & 0xFFFF;
	}
	
	public void setSP(int val) {
		this.sp = val & 0xFFFF;
	}
	
	public int getSP() {
		return this.sp;
	}
	
	public int getRegAF() {
		return regA << 8 | regF;
	}
	
	public int getRegBC() {
		return regB << 8 | regC;
	}
	
	public int getRegDE() {
		return regD << 8 | regE;
	}
	
	public int getRegHL() {
		return regH << 8 | regL;
	}
	
	public int getRegA() {
		return regA;
	}

	public int getRegB() {
		return regB;
	}

	public int getRegC() {
		return regC;
	}

	public int getRegD() {
		return regD;
	}

	public int getRegE() {
		return regE;
	}

	public int getRegF() {
		return regF;
	}

	public int getRegH() {
		return regH;
	}

	public int getRegL() {
		return regL;
	}
	
	public void setRegAF(int val) {
		regA = (val & 0x0000FF00) >> 8;
		regF = (val & 0x000000F0);
	}
	
	public void setRegAF(int v1, int v2) {
		regA = v1 & 0xFF;
		regF = v2 & 0xF0;
	}
	
	public void setRegBC(int val) {
		regB = (val & 0x0000FF00) >> 8;
		regC = (val & 0x000000FF);
	}
	
	public void setRegBC(int v1, int v2) {
		regB = v1 & 0xFF;
		regC = v2 & 0xFF;
	}
	
	public void setRegDE(int val) {
		regD = (val & 0x0000FF00) >> 8;
		regE = (val & 0x000000FF);
	}
	
	public void setRegDE(int v1, int v2) {
		regD = v1 & 0xFF;
		regE = v2 & 0xFF;
	}
	
	public void setRegHL(int val) {
		regH = (val & 0x0000FF00) >> 8;
		regL = (val & 0x000000FF);
	}
	
	public void setRegHL(int v1, int v2) {
		regH = v1 & 0xFF;
		regL = v2 & 0xFF;
	}
	
	public void setRegA(int regA) {
		this.regA = regA & 0xFF;
	}

	public void setRegB(int regB) {
		this.regB = regB & 0xFF;
	}

	public void setRegC(int regC) {
		this.regC = regC & 0xFF;
	}

	public void setRegD(int regD) {
		this.regD = regD & 0xFF;
	}

	public void setRegE(int regE) {
		this.regE = regE & 0xFF;
	}

	public void setRegF(int regF) {
		this.regF = regF & 0xF0;
	}

	public void setRegH(int regH) {
		this.regH = regH & 0xFF;
	}

	public void setRegL(int regL) {
		this.regL = regL & 0xFF;
	}
	
	public void enableIME() {
		interruptService.enableIME();
	}
	
	public void disableIME() {
		interruptService.disableIME();
	}
	
	public JJCPUInstruction getInstruction() {
		return instruction;
	}
	
	public JJInterruptService getInterruptService() {
		return interruptService;
	}
	
	public JJTimer getTimer() {
		return timer;
	}
	
	public String toString() {
		StringBuffer b = new StringBuffer();
		
//		b.append(this.getClass().getName()).append(": ");
//		b.append("A:0x").append(Integer.toHexString(getRegA())).append("; ");
//		b.append("B:0x").append(Integer.toHexString(getRegB())).append("; ");
//		b.append("C:0x").append(Integer.toHexString(getRegC())).append("; ");
//		b.append("D:0x").append(Integer.toHexString(getRegD())).append("; ");
//		b.append("E:0x").append(Integer.toHexString(getRegE())).append("; ");
//		b.append("F:0x").append(Integer.toHexString(getRegF())).append("; ");
//		b.append("H:0x").append(Integer.toHexString(getRegH())).append("; ");
//		b.append("L:0x").append(Integer.toHexString(getRegL())).append("; ");
		b.append("AF:0x").append(Integer.toHexString(getRegAF())).append("; ");
		b.append("BC:0x").append(Integer.toHexString(getRegBC())).append("; ");
		b.append("DE:0x").append(Integer.toHexString(getRegDE())).append("; ");
		b.append("HL:0x").append(Integer.toHexString(getRegHL())).append("; ");
		b.append("PC:0x").append(Integer.toHexString(getPC())).append("; ");
		b.append("SP:0x").append(Integer.toHexString(getSP())).append("; ");
		
		return b.toString();
	}
	
	/**
	 * Dumps the CPU to the output stream.
	 * 
	 * @param out The output stream to that the CPU is dumped to.
	 * 
	 * @throws IOException If an {@link IOException} occurs.
	 */
	public void dumpState(OutputStream out) throws IOException {
		out.write(regA);
		out.write(regB);
		out.write(regC);
		out.write(regD);
		out.write(regE);
		out.write(regF);
		out.write(regH);
		out.write(regL);
		out.write((pc >> 8) & 0xFF);
		out.write((pc >> 0) & 0xFF);
		out.write((sp >> 8) & 0xFF);
		out.write((sp >> 0) & 0xFF);
		
		interruptService.dumpState(out);
		timer.dumpState(out);
	}
	
	/**
	 * Reloads the CPU from the input stream.
	 * 
	 * @param in The stream to load the CPU from.
	 * 
	 * @throws IOException If an {@link IOException} occurs.
	 */
	public void reloadState(InputStream in) throws IOException {
		regA = in.read();
		regB = in.read();
		regC = in.read();
		regD = in.read();
		regE = in.read();
		regF = in.read();
		regH = in.read();
		regL = in.read();
		pc = (in.read() << 8) | in.read();
		sp = (in.read() << 8) | in.read();
		
		interruptService.reloadState(in);
		timer.reloadState(in);
	}
	
}
