package jj.gameboy.unit;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface JJSaveStateUnit {
	
	/**
	 * Dumps the state of the unit to the output stream.
	 * 
	 * @param out The output stream to dump the state to.
	 * 
	 * @throws IOException If an {@link IOException} occurs.
	 */
	public void dumpState(OutputStream out) throws IOException;
	
	/**
	 * Reloads the state from the input stream.
	 * 
	 * @param in The input stream to load from.
	 * 
	 * @throws IOException If an {@link IOException} occurs.
	 */
	public void reloadState(InputStream in) throws IOException;
	
}
