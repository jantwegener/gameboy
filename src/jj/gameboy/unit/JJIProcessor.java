package jj.gameboy.unit;

public interface JJIProcessor {
	
	/**
	 * Makes another tick cycle depending on how much time passed by the cpu.
	 * 
	 * @param cpuClock The time passed in the cpu.
	 */
	public int tick(int cpuClock);
	
}
