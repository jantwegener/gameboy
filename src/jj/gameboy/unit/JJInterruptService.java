package jj.gameboy.unit;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class JJInterruptService implements JJSaveStateUnit {

	public static final int ADDRESS_ISR_VERTICAL_BLANK = 0x40;
	public static final int ADDRESS_ISR_LCD_STATUS = 0x48;
	public static final int ADDRESS_ISR_TIMER_OVERFLOW = 0x50;
	public static final int ADDRESS_ISR_SERIAL_LINK = 0x58;
	public static final int ADDRESS_ISR_JOYPAD_PRESSED = 0x60;
	
	public static final int INTERRUPT_VBLANK = 0x01;
	public static final int INTERRUPT_LCD_STAT = 0x02;
	public static final int INTERRUPT_TIMER = 0x04;
	public static final int INTERRUPT_SERIAL_LINK = 0x08;
	public static final int INTERRUPT_JOYPAD = 0x10;
	
	private JJCPU cpu;
	
	private JJMainBus bus;
	
	// stores the interrupts
	int interrupt = 0x00;
	
	// IME
	private boolean ime = false;
	// interrupts
	private boolean isInterruptVblankEnabled = false;
	private boolean isInterruptLCDStatEnabled = false;
	private boolean isInterruptTimerEnabled = false;
	private boolean isInterruptSerialEnabled = false;
	private boolean isInterruptJoypadEnabled = false;
	
	// delay of one instruction
	private int delay = 0;
	
	int interruptEnabledRegister = 0;
	
	public JJInterruptService(JJCPU cpu, JJMainBus bus) {
		this.cpu = cpu;
		this.bus = bus;
	}
	
	public void reset() {
		ime = false;
		interrupt = 0;
		delay = 0;
		// same as interruptEnabledRegister = 0; and setting the interrupts correctly
		enableInterrupts(0);
	}
	
	/**
	 * Enables the interrupts.
	 * 
	 * Bit	When 0			When 1
	 * 0	Vblank off		Vblank on
	 * 1	LCD stat off	LCD stat on
	 * 2	Timer off		Timer on
	 * 3	Serial off		Serial on
	 * 4	Joypad off		Joypad on
	 * 
	 * @param value The handed value.
	 */
	public void enableInterrupts(int value) {
		interruptEnabledRegister = value;
		
		isInterruptVblankEnabled = (value & INTERRUPT_VBLANK) > 0;
		isInterruptLCDStatEnabled = (value & INTERRUPT_LCD_STAT) > 0;
		isInterruptTimerEnabled = (value & INTERRUPT_TIMER) > 0;
		isInterruptSerialEnabled = (value & INTERRUPT_SERIAL_LINK) > 0;
		isInterruptJoypadEnabled = (value & INTERRUPT_JOYPAD) > 0;
	}
	
	public int getEnabledInterrupts() {
		return interruptEnabledRegister;
	}
	
	public int getInterruptFlags() {
		return interrupt;
	}
	
	public int handleInterrupt() {
		int cycle = 0;
		
		if (isIME()) {
			if (delay > 0) {
				delay--;
				
				cycle = 4;
			} else if (isInterruptVblankEnabled && isInterruptVBlank()) {
				cycle = cpu.getInstruction().callInterrupt(ADDRESS_ISR_VERTICAL_BLANK);
				acknowledgeInterrupt(INTERRUPT_VBLANK);
			} else if (isInterruptLCDStatEnabled && isInterruptLCDStat()) {
				cycle = cpu.getInstruction().callInterrupt(ADDRESS_ISR_LCD_STATUS);
				acknowledgeInterrupt(INTERRUPT_LCD_STAT);
			} else if (isInterruptTimerEnabled && isInterruptTimer()) {
				cycle = cpu.getInstruction().callInterrupt(ADDRESS_ISR_TIMER_OVERFLOW);
				acknowledgeInterrupt(INTERRUPT_TIMER);
			} else if (isInterruptSerialEnabled && isInterruptSerial()) {
				cycle = cpu.getInstruction().callInterrupt(ADDRESS_ISR_SERIAL_LINK);
				acknowledgeInterrupt(INTERRUPT_SERIAL_LINK);
			} else if (isInterruptJoypadEnabled && isInterruptJoypad()) {
				cycle = cpu.getInstruction().callInterrupt(ADDRESS_ISR_JOYPAD_PRESSED);
				acknowledgeInterrupt(INTERRUPT_JOYPAD);
			}
		}
		
		return cycle;
	}
	
	public boolean isInterruptVBlank() {
		return (interrupt & INTERRUPT_VBLANK) > 0;
	}
	
	public boolean isInterruptLCDStat() {
		return (interrupt & INTERRUPT_LCD_STAT) > 0;
	}
	
	public boolean isInterruptTimer() {
		return (interrupt & INTERRUPT_TIMER) > 0;
	}
	
	public boolean isInterruptSerial() {
		return (interrupt & INTERRUPT_SERIAL_LINK) > 0;
	}
	
	public boolean isInterruptJoypad() {
		return (interrupt & INTERRUPT_JOYPAD) > 0;
	}
	
	public void enableIME() {
		ime = true;
		delay = 1;
	}
	
	public void disableIME() {
		ime = false;
	}
	
	public boolean isIME() {
		return ime;
	}
	
//	public int setInterrupt(int value) {
//		interrupt = interrupt | value;
//		return interrupt;
//	}
	
	public int setInterrupt(int value) {
		value = isIME() ? value & interruptEnabledRegister : 0;
		int tmp = interrupt | value;
		if (tmp != interrupt) {
			interrupt = tmp;
		}
		return interrupt;
	}
	
	public void acknowledgeInterrupt(int value) {
		interrupt = interrupt & ~value;
	}
	
	/**
	 * Directly written from CPU.
	 * 
	 * @param value The value to write into interrupt.
	 */
	public void writeInterrupt(int value) {
		interrupt = value;
	}
	
	public String toString() {
		StringBuffer b = new StringBuffer();
		b.append("enabled (");
		b.append("vblank: ").append(isInterruptVblankEnabled);
		b.append(", lcd: ").append(isInterruptLCDStatEnabled);
		b.append(", timer: ").append(isInterruptTimerEnabled);
		b.append(", serial: ").append(isInterruptSerialEnabled);
		b.append(", joypad: ").append(isInterruptJoypadEnabled);
		b.append(")").append("; ");
		b.append("set (");
		b.append("vblank: ").append(isInterruptVBlank());
		b.append(", lcd: ").append(isInterruptLCDStat());
		b.append(", timer: ").append(isInterruptTimer());
		b.append(", serial: ").append(isInterruptSerial());
		b.append(", joypad: ").append(isInterruptJoypad());
		b.append(")");
		return b.toString();
	}

	@Override
	public void dumpState(OutputStream out) throws IOException {
		out.write(delay);
		out.write(ime ? 1 : 0);
		out.write(interrupt);
		out.write(interruptEnabledRegister);
		
	}

	@Override
	public void reloadState(InputStream in) throws IOException {
		delay = in.read();
		ime = in.read() == 1;
		interrupt = in.read();
		interruptEnabledRegister = in.read();
		enableInterrupts(interruptEnabledRegister);
	}
	
}
