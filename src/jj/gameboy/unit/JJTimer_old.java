package jj.gameboy.unit;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class JJTimer_old implements JJSaveStateUnit {
	
	public static final int ADDRESS_DIV = 0xFF04;
	public static final int ADDRESS_TIMA = 0xFF05;
	public static final int ADDRESS_TMA = 0xFF06;
	public static final int ADDRESS_TAC = 0xFF07;
	
	private JJMainBus bus;
	
	/**
	 * The clock speed.
	 * 
	 * <table>
	 * <tr><td>
	 * 00: CPU Clock / 1024 (DMG, CGB:   4096 Hz, SGB:   ~4194 Hz)
	 * </td></tr>
	 * <tr><td>
	 * 01: CPU Clock / 16   (DMG, CGB: 262144 Hz, SGB: ~268400 Hz)
	 * </td></tr>
	 * <tr><td>
	 * 10: CPU Clock / 64   (DMG, CGB:  65536 Hz, SGB:  ~67110 Hz)
	 * </td></tr>
	 * <tr><td>
	 * 11: CPU Clock / 256  (DMG, CGB:  16384 Hz, SGB:  ~16780 Hz)
	 * </td></tr>
	 * </table>
	 */
	private int clockSpeed;
	
	private int remainderCycles = 0;
	private int remainderDividerCycles = 0;
	
	/**
	 * The counter.
	 */
	private int counter;
	
	private boolean isEnabled = false;
	
	private int regDivider = 0;
	
	public JJTimer_old(JJMainBus bus) {
		this.bus = bus;
	}
	
	public void reset() {
		counter = 0;
		regDivider = 0;
		isEnabled = false;
	}
	
	public void resetDIV() {
		remainderDividerCycles = 0;
		regDivider = 0;
		
		remainderCycles = 0;
		counter = 0;
	}
	
	public int getDIV() {
		return regDivider;
	}
	
	public int getTAC() {
		return (isEnabled ? 0x04 : 0x00) | clockSpeed;
	}
	
	public int getTIMA() {
		return counter;
	}
	
	public void setTIMA(int value) {
		counter = value;
	}
	
	public void setTimerSpeed(int speed) {
		clockSpeed = speed & 0x03;
		isEnabled = (speed & 0x04) > 0;
	}
	
	/**
	 * Increments the counter depending on the instruction cycles in this "period".
	 * 
	 * @param cycles The number of cycles of the current instruction.
	 */
	public void handleCycles(int cycles) {
		if (isEnabled) {
			incrementCounter(cycles);
		}
		// the divider is always increased
		incrementDivider(cycles);
	}
	
	/**
	 * Increments the counter depending on the instruction cycles in this "period".
	 * 
	 * @param cycles The number of cycles of the current instruction.
	 */
	private void incrementCounter(int cycles) {
		remainderCycles += cycles;
		
		int threshold = getCounterThreshold();
		while (remainderCycles >= threshold) {
				// increase counter
				incrementCounter();
				// decrease remainder
				remainderCycles = remainderCycles - threshold;
		}
	}
	
	/**
	 * Returns the threshold when the counter is increased.
	 * 
	 * @return The threshold.
	 */
	private int getCounterThreshold() {
		int retval = 0;
		switch (clockSpeed) {
			case 0:
				retval = 1024;
				break;
			case 1:
				retval = 16;
				break;
			case 2:
				retval = 64;
				break;
			case 3:
				retval = 256;
				break;
		}
		return retval;
	}
	
	/**
	 * Increments the divider counter depending on the instruction cycles in this "period".
	 * 
	 * @param cycles The number of cycles of the current instruction.
	 */
	private void incrementDivider(int cycles) {
		remainderDividerCycles += cycles;
		
		while (remainderDividerCycles >= 64) {
			// increase counter
			incrementDivider();
			// decrease remainder
			remainderDividerCycles = remainderDividerCycles - 64;
		}
	}
	
	private void incrementCounter() {
//		counter++;
		counter = bus.getIOPort().read(ADDRESS_TIMA - 0xFF00) + 1;
		if (counter >= 0xFF) {
			// spill over, thus, interrupt and reset
			bus.getIOPort().writeFromComponent(JJMainBus.ADDRESS_INTERRUPT, JJInterruptService.INTERRUPT_TIMER);
			counter = bus.read(ADDRESS_TMA);
		}
		bus.getIOPort().writeFromComponent(ADDRESS_TIMA, counter);
	}
	
	private void incrementDivider() {
		regDivider++;
	}
	
	@Override
	public void dumpState(OutputStream out) throws IOException {
		out.write(clockSpeed);
		out.write(counter);
		out.write(isEnabled ? 1 : 0);
		out.write(regDivider);
		out.write(remainderCycles);
		out.write(remainderDividerCycles);
	}
	
	@Override
	public void reloadState(InputStream in) throws IOException {
		clockSpeed = in.read();
		counter = in.read();
		isEnabled = in.read() == 1;
		regDivider = in.read();
		remainderCycles = in.read();
		remainderDividerCycles = in.read();
	}
	
}
