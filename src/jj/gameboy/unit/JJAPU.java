package jj.gameboy.unit;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import jj.gameboy.JJGameBoyException;
import jj.gameboy.output.sound.JJAmplifier;

/**
 * The base class for the APU.
 * 
 * @author Jan-Thierry Wegener
 */
public abstract class JJAPU implements JJIUnit, JJSaveStateUnit {

	/**
	 * The wave duty for channel 1 and 2.
	 */
	private static final int[] WAVE_DUTY = new int[] {
			0b10000000,
			0b11000000,
			0b11110000,
			0b11111100
	};
	
	private JJMainBus bus;
	
	private JJAmplifier amplifier;
	
	private static final int SOUND_ALL_ON_FLAG = 0x80;
	private static final int SOUND_4_ON_FLAG = 0x08;
	private static final int SOUND_3_ON_FLAG = 0x04;
	private static final int SOUND_2_ON_FLAG = 0x02;
	private static final int SOUND_1_ON_FLAG = 0x01;
	
	private int cpuClock = 0;
	
	private int frameSequenceTicker = 0;
	
	// wave ram is from 0x30 - 0x3f
	private int[] wavePatternRam = new int[32];
	/**
	 * The pointer to the correct position in the wave ram.
	 */
	private int wavePatternCounter = 0;
	/**
	 * The pointer to the correct position in the duty wave of channel 1.
	 */
	private int channel1DutyCounter = 0;
	/**
	 * The pointer to the correct position in the duty wave of channel 2.
	 */
	private int channel2DutyCounter = 0;
	
	
	/**
	 * Keeps track how often the sweep method is called. This is need to ensure that channelXSweepTime is respected.
	 */
	private int sweepTimeCounter = 0;
	
	private int[] frequencyTimer = new int[4];
	
	private boolean[] channelCtrl = new boolean[4];
	
	private boolean soundEnabled = false;
	private boolean sound4Enabled = false;
	private boolean sound3Enabled = false;
	private boolean sound2Enabled = false;
	private boolean sound1Enabled = false;
	
	private boolean channel1Enabled = false;
	private boolean channel2Enabled = false;
	private boolean channel3Enabled = false;
	private boolean channel4Enabled = false;
	
	private boolean outputVinSO2Left = false;
	private int so2LeftVolume = 0;
	private boolean outputVinSO1Right = false;
	private int so1RightVolume = 0;

	private boolean outputSound1SO1;
	private boolean outputSound2SO1;
	private boolean outputSound3SO1;
	private boolean outputSound4SO1;
	private boolean outputSound1SO2;
	private boolean outputSound2SO2;
	private boolean outputSound3SO2;
	private boolean outputSound4SO2;
	
	protected int channel1WavePattern;
	protected int channel1SoundLength;
	
	protected int channel1InitialVolumeEnvelope;
	private boolean channel1VolumeEnvelopeDirectionIncrease;
	private int channel1VolumeEnvelopeNumberSweep;
	
	private int channel1Frequency;
	private boolean channel1RestartSound;
	protected boolean channel1CounterSelection;

	private int channel1SweepTime;
	private int channel1SweepIncrease;
	private int channel1SweepNumber;
	
	protected int channel2WavePattern;
	protected int channel2SoundLength;
	
	protected int channel2InitialVolumeEnvelope;
	private boolean channel2VolumeEnvelopeDirectionIncrease;
	private int channel2VolumeEnvelopeNumberSweep;

	private int channel2Frequency;
	private boolean channel2RestartSound;
	protected boolean channel2CounterSelection;

	private int channel3Frequency;
	private boolean channel3RestartSound;
	protected boolean channel3CounterSelection;

	protected int channel3SoundLength = 0;
	protected int channel3OutputLevel = 0;

	private int channel4InitialVolumeEnvelope;
	private boolean channel4VolumeEnvelopeDirectionIncrease;
	private int channel4VolumeEnvelopeNumberSweep;

	protected int channel4SoundLength;

	protected boolean channel4RestartSound;
	protected boolean channel4CounterSelection;
	
	private int channel4ShiftClockFrequency;
	private int channel4CounterWidth;
	private int channel4DividingRatio;
	
	public JJAPU(JJMainBus bus) {
		this.bus = bus;
		
		amplifier = initAmplifier();
		amplifier.start();
		
		for (int i = 0; i < channelCtrl.length; i++) {
			channelCtrl[i] = true;
		}
	}
	
	/**
	 * Initializes the amplifier.
	 * 
	 * @return The initialized amplifier.
	 */
	protected abstract JJAmplifier initAmplifier();
	
	public void reset() {
		amplifier.reset();

		soundEnabled = false;
		sound4Enabled = false;
		sound3Enabled = false;
		sound2Enabled = false;
		sound1Enabled = false;
		
		channel1Enabled = false;
		channel2Enabled = false;
		channel3Enabled = false;
		channel4Enabled = false;
		
		for (int i = 0; i < channelCtrl.length; i++) {
			channelCtrl[i] = true;
		}
	}
	
	public void flushAmplifier() {
		for (int i = 0; i < 4; i++) {
			amplifier.flush(i);
		}
	}
	
	public boolean getChannelCtrl(int channel) {
		return channelCtrl[channel];
	}
	
	public boolean isSoundEnabled(int channel) {
		boolean retval = false;
		switch (channel) {
			
			case 0:
				retval = sound1Enabled;
				break;
				
			case 1:
				retval = sound2Enabled;
				break;
				
			case 2:
				retval = sound3Enabled;
				break;
				
			case 3:
				retval = sound4Enabled;
				break;
				
		}
		return retval;
	}
	
	public boolean isChannelCounterSelection(int channel) {
		boolean retval = false;
		switch (channel) {
			
			case 0:
				retval = channel1CounterSelection;
				break;
				
			case 1:
				retval = channel2CounterSelection;
				break;
				
			case 2:
				retval = channel3CounterSelection;
				break;
				
			case 3:
				retval = channel4CounterSelection;
				break;
				
		}
		return retval;
	}
	
	public int getChannelSoundLength(int channel) {
		int retval = 0;
		switch (channel) {
			
			case 0:
				retval = channel1SoundLength;
				break;
				
			case 1:
				retval = channel2SoundLength;
				break;
				
			case 2:
				retval = channel3SoundLength;
				break;
				
			case 3:
				retval = channel4SoundLength;
				break;
				
		}
		return retval;
	}
	
	public int getChannel3OutputLevel() {
		return channel3OutputLevel;
	}
	
	/**
	 * Returns the wave pattern ram.
	 * 
	 * @return The wave pattern ram.
	 */
	public int[] getWavePatternRam() {
		return wavePatternRam;
	}
	
	public JJAmplifier getAmplifier() {
		return amplifier;
	}
	
	@Override
	public int read(int address) {
		return 0;
	}

	@Override
	public void write(int address, int value) {
		
		switch (address) {
			
			case 0x10:
				// NR10 - Channel 1 (Tone & Sweep) Sweep register (R/W)
				// Bit 6-4 - Sweep Time
				// Bit 3   - Sweep Increase/Decrease
				//            0: Addition    (frequency increases)
				//            1: Subtraction (frequency decreases)
				// Bit 2-0 - Number of sweep shift (n: 0-7)
				channel1SweepTime = (value & 0x70) >> 4;
				channel1SweepIncrease = (value & 0x08) > 0 ? -1 : 1;
				channel1SweepNumber = (value & 0x07);
				
				// reset the sweep counter
				sweepTimeCounter = 0;
				break;
				
			case 0x11:
				// NR11 - Channel 1 (Tone & Sweep) Sound length/Wave pattern duty (R/W)
				// Bit 7-6 - Wave Pattern Duty (Read/Write)
				// Bit 5-0 - Sound length data (Write Only) (t1: 0-63)
				channel1WavePattern = (value & 0xC0) >> 6;
				// Sound Length = (64-t1)*(1/256) seconds The Length value is used only if Bit 6 in NR14 is set.
				channel1SoundLength = value & 0x3F;
				break;
				
			case 0x12:
				// NR12 - Channel 1 (Tone & Sweep) Volume Envelope (R/W)
				// Bit 7-4 - Initial Volume of envelope (0-0Fh) (0=No Sound)
				// Bit 3   - Envelope Direction (0=Decrease, 1=Increase)
				// Bit 2-0 - Number of envelope sweep (n: 0-7)
				//           (If zero, stop envelope operation.)
				channel1InitialVolumeEnvelope = (value & 0xF0) >> 4;
				channel1VolumeEnvelopeDirectionIncrease = (value & 0x08) > 0;
				channel1VolumeEnvelopeNumberSweep = (value & 0x07);
				break;
				
			case 0x13:
				// NR13 - Channel 1 (Tone & Sweep) Frequency lo (Write Only)
				channel1Frequency = value;
				break;
				
			case 0x14:
				// NR14 - Channel 1 (Tone & Sweep) Frequency hi (R/W)
				// Bit 7   - Initial (1=Restart Sound)     (Write Only)
				// Bit 6   - Counter/consecutive selection (Read/Write)
				//           (1=Stop output when length in NR11 expires)
				// Bit 2-0 - Frequency's higher 3 bits (x) (Write Only)
				channel1Frequency = ((value & 0x03) << 8) | (channel1Frequency);
				channel1RestartSound = (value & 0x80) > 0;
				channel1CounterSelection = (value & 0x40) > 0;
				
				// initialize the period
				channel1DutyCounter = 0;
				frequencyTimer[0] = 2048 - channel1Frequency;
				
				sound1Enabled = true;
				channel1Enabled = true;
				break;
				
			case 0x16:
				// NR21 Channel 2 (Tone) Sound Length/Wave Pattern Duty (R/W)
				// Bit 7-6 - Wave Pattern Duty (Read/Write)
				// Bit 5-0 - Sound length data (Write Only) (t1: 0-63)
				channel2WavePattern = (value & 0xC0) >> 6;
				// Sound Length = (64-t1)*(1/256) seconds The Length value is used only if Bit 6 in NR14 is set.
				channel2SoundLength = value & 0x3F;
				break;
				
			case 0x17:
				// NR22 - Channel 2 (Tone) Volume Envelope (R/W)
				// Bit 7-4 - Initial Volume of envelope (0-0Fh) (0=No Sound)
				// Bit 3   - Envelope Direction (0=Decrease, 1=Increase)
				// Bit 2-0 - Number of envelope sweep (n: 0-7)
				//           (If zero, stop envelope operation.)
				channel2InitialVolumeEnvelope = (value & 0xF0) >> 4;
				channel2VolumeEnvelopeDirectionIncrease = (value & 0x08) > 0;
				channel2VolumeEnvelopeNumberSweep = (value & 0x07);
				break;
				
			case 0x18:
				// NR23 - Channel 2 (Tone) Frequency lo data (W)
				channel2Frequency = value;
				break;
				
			case 0x19:
				// NR24 - Channel 2 (Tone) Frequency hi data (R/W)
				// Bit 7   - Initial (1=Restart Sound)     (Write Only)
				// Bit 6   - Counter/consecutive selection (Read/Write)
		        //   (1=Stop output when length in NR21 expires)
				// Bit 2-0 - Frequency's higher 3 bits (x) (Write Only)
				channel2Frequency = ((value & 0x03) << 8) | (channel1Frequency);
				channel2RestartSound = (value & 0x80) > 0;
				channel2CounterSelection = (value & 0x40) > 0;
				
				// initialize the period
				channel2DutyCounter = 0;
				frequencyTimer[1] = 2048 - channel2Frequency;
				
				sound2Enabled = true;
				channel2Enabled = true;
				break;
				
			case 0x1A:
				// NR30 - Channel 3 (Wave Output) Sound on/off (R/W)
				// Bit 7 - Sound Channel 3 Off  (0=Stop, 1=Playback)  (Read/Write)
				channel3Enabled = (value & 0x80) > 0;
				break;
				
			case 0x1B:
				// FF1B - NR31 - Channel 3 Sound Length
				// Bit 7-0 - Sound length (t1: 0 - 255)
				channel3SoundLength = value;
				break;
				
			case 0x1C:
				// FF1C - NR32 - Channel 3 Select output level (R/W)
				// Bit 6-5 - Select output level (Read/Write)
				switch (value) {
					// 0: Mute (No sound)
					// 1: 100% Volume (Produce Wave Pattern RAM Data as it is)
					// 2:  50% Volume (Produce Wave Pattern RAM data shifted once to the right)
					// 3:  25% Volume (Produce Wave Pattern RAM data shifted twice to the right)
					case 0:
						// 4 shifts
						channel3OutputLevel = 4;
						break;
						
					case 0x20:
						// 0 shifts
						channel3OutputLevel = 0;
						break;
						
					case 0x40:
						// 1 shift
						channel3OutputLevel = 1;
						break;
						
					case 0x60:
						// 2 shifts
						channel3OutputLevel = 2;
						break;
				}
				break;
				
			case 0x1D:
				// NR33 - Channel 3 (Wave Output) Frequency's lower data (W)
				channel3Frequency = value;
				break;
				
			case 0x1E:
				// NR34 - Channel 3 (Wave Output) Frequency's higher data (R/W)
				// Bit 7   - Initial (1=Restart Sound)     (Write Only)
				// Bit 6   - Counter/consecutive selection (Read/Write)
		        //   (1=Stop output when length in NR31 expires)
				// Bit 2-0 - Frequency's higher 3 bits (x) (Write Only)
				channel3Frequency = ((value & 0x03) << 8) | (channel1Frequency);
				channel3RestartSound = (value & 0x80) > 0;
				channel3CounterSelection = (value & 0x40) > 0;
				
				// initialize the period
				frequencyTimer[2] = 2 * (2048 - channel3Frequency);
				
				sound3Enabled = true;
				channel3Enabled = true;
				break;

			case 0x20:
				// FF20 - NR41 - Channel 4 Sound Length (R/W)
				// Bit 5-0 - Sound length data (t1: 0-63)
				channel4SoundLength = value;
				break;
				
			case 0x21:
				// FF21 - NR42 - Channel 4 Volume Envelope (R/W)
				// Bit 7-4 - Initial Volume of envelope (0-0Fh) (0=No Sound)
				// Bit 3   - Envelope Direction (0=Decrease, 1=Increase)
				// Bit 2-0 - Number of envelope sweep (n: 0-7)
				//           (If zero, stop envelope operation.)
				channel4InitialVolumeEnvelope = (value & 0xF0) >> 4;
				channel4VolumeEnvelopeDirectionIncrease = (value & 0x08) > 0;
				channel4VolumeEnvelopeNumberSweep = (value & 0x07);
				break;
				
			case 0x22:
				// 	NR43 - Channel 4 (Noise) Polynomial Counter (R/W)
				// Bit 7-4 - Shift Clock Frequency (s)
				// Bit 3   - Counter Step/Width (0=15 bits, 1=7 bits)
				// Bit 2-0 - Dividing Ratio of Frequencies (r)
				channel4ShiftClockFrequency = (value & 0xF0) >> 4;
				channel4CounterWidth = (value & 0x08) > 0 ? 7 : 15;
				channel4DividingRatio = (value & 0x07);
				break;
				
			case 0x23:
				// FF23 - NR44 - Channel 4 Counter/consecutive; Inital (R/W)
				// Bit 7   - Initial (1=Restart Sound)     (Write Only)
				// Bit 6   - Counter/consecutive selection (Read/Write)
				//           (1=Stop output when length in NR41 expires)
				channel4RestartSound = (value & 0x80) > 0;
				channel4CounterSelection = (value & 0x40) > 0;
				
				sound4Enabled = true;
				channel4Enabled = true;
				break;
				
			case 0x24:
				// NR50 - Channel control / ON-OFF / Volume (R/W)
				// Bit 7   - Output Vin to SO2 terminal (1=Enable)
				// Bit 6-4 - SO2 output level (volume)  (0-7)
				// Bit 3   - Output Vin to SO1 terminal (1=Enable)
				// Bit 2-0 - SO1 output level (volume)  (0-7)
				outputVinSO2Left = (value & 0x80) > 0;
				so2LeftVolume = (value & 0x70);
				outputVinSO1Right = (value & 0x08) > 0;
				so1RightVolume = (value & 0x07);
				break;
				
			case 0x25:
				// NR51 - Selection of Sound output terminal (R/W)
				// Bit 7 - Output sound 4 to SO2 terminal
				// Bit 6 - Output sound 3 to SO2 terminal
				// Bit 5 - Output sound 2 to SO2 terminal
				// Bit 4 - Output sound 1 to SO2 terminal
				// Bit 3 - Output sound 4 to SO1 terminal
				// Bit 2 - Output sound 3 to SO1 terminal
				// Bit 1 - Output sound 2 to SO1 terminal
				// Bit 0 - Output sound 1 to SO1 terminal
				outputSound1SO1 = (value & 0x01) > 0;
				outputSound2SO1 = (value & 0x02) > 0;
				outputSound3SO1 = (value & 0x04) > 0;
				outputSound4SO1 = (value & 0x08) > 0;
				outputSound1SO2 = (value & 0x10) > 0;
				outputSound2SO2 = (value & 0x20) > 0;
				outputSound3SO2 = (value & 0x40) > 0;
				outputSound4SO2 = (value & 0x80) > 0;
				break;
			
			case 0x26:
				// FF26 - NR52 - Sound on/off
				soundEnabled = (value & SOUND_ALL_ON_FLAG) > 0;
				sound4Enabled = (value & SOUND_4_ON_FLAG) > 0;
				sound3Enabled = (value & SOUND_3_ON_FLAG) > 0;
				sound2Enabled = (value & SOUND_2_ON_FLAG) > 0;
				sound1Enabled = (value & SOUND_1_ON_FLAG) > 0;
				break;

			case 0x30:
				// Wave Pattern RAM
			case 0x31:
				// Wave Pattern RAM
			case 0x32:
				// Wave Pattern RAM
			case 0x33:
				// Wave Pattern RAM
			case 0x34:
				// Wave Pattern RAM
			case 0x35:
				// Wave Pattern RAM
			case 0x36:
				// Wave Pattern RAM
			case 0x37:
				// Wave Pattern RAM
			case 0x38:
				// Wave Pattern RAM
			case 0x39:
				// Wave Pattern RAM
			case 0x3A:
				// Wave Pattern RAM
			case 0x3B:
				// Wave Pattern RAM
			case 0x3C:
				// Wave Pattern RAM
			case 0x3D:
				// Wave Pattern RAM
			case 0x3E:
				// Wave Pattern RAM
			case 0x3F:
				// Wave Pattern RAM
				// 0x30 is the offset
				// each entry has 4 bits
				int addressLow = 2 * (address - 0x30) + 0;
				int addressHigh = 2 * (address - 0x30) + 1;
				wavePatternRam[addressLow] = (value & 0x0F);
				wavePatternRam[addressHigh] = (value & 0xF0) >> 4;
				break;
				
			default:
				System.out.printf("Unknown APU (w): 0x%x - %d%n", address, value);
				throw new JJGameBoyException(String.format("Unknown i/o port (w): 0x%x - %d", address, value));
				
		}
	}
	
	private void frameSequencer() {
		//	Step   Length Ctr  Vol Env     Sweep
		//	---------------------------------------
		//	0      Clock       -           -
		//	1      -           -           -
		//	2      Clock       -           Clock
		//	3      -           -           -
		//	4      Clock       -           -
		//	5      -           -           -
		//	6      Clock       -           Clock
		//	7      -           Clock       -
		//	---------------------------------------
		//	Rate   256 Hz      64 Hz       128 Hz
		if ((frameSequenceTicker % 2) == 0) {
			if (channel1CounterSelection) {
				channel1SoundLength--;
			}
			if (channel2CounterSelection) {
				channel2SoundLength--;
			}
			if (channel3CounterSelection) {
				channel3SoundLength--;
			}
		}
		if ((frameSequenceTicker % 8) == 7) {
			volumeSweep();
		}
		if ((frameSequenceTicker % 8) == 2 || (frameSequenceTicker % 8) == 6) {
			if (channel1SweepNumber > 0 && channel1SweepTime > 0) {
				frequencySweep();
			}
		}

		frameSequenceTicker++;
	}

	private void tickTimer(int channel, int cpuCycle) {
		if (channel == 0) {
			if (frequencyTimer[0] <= 0) {
				frequencyTimer[0] = 2048 - channel1Frequency;
				dutyIncrementChannel1();
			}
		}
		if (channel == 1) {
			if (frequencyTimer[1] <= 0) {
				frequencyTimer[1] = 2048 - channel2Frequency;
				dutyIncrementChannel2();
			}
		}
		if (channel == 2) {
			if (frequencyTimer[2] <= 0) {
				frequencyTimer[2] = 2 * (2048 - channel3Frequency);
				waveIncrement();
			}
		}
		if (channel == 3) {
			if (frequencyTimer[3] <= 0) {
				frequencyTimer[3] = channel4ShiftClockFrequency;
			}
		}
		
		frequencyTimer[channel] -= cpuCycle;
	}
	
	private void dutyIncrementChannel1() {
		channel1DutyCounter++;
		if (channel1DutyCounter >= 8) {
			channel1DutyCounter = 0;
		}
	}
	
	private void dutyIncrementChannel2() {
		channel2DutyCounter++;
		if (channel2DutyCounter >= 8) {
			channel2DutyCounter = 0;
		}
	}
	
	private void waveIncrement() {
		wavePatternCounter++;
		if (wavePatternCounter >= wavePatternRam.length) {
			wavePatternCounter = 0;
		}
	}
	
	private void frequencySweep() {
		// X(t) = X(t-1) +/- X(t-1)/2^n
		sweepTimeCounter++;
		if ((sweepTimeCounter % channel1SweepTime) == 0) {
			channel1SweepNumber--;
			
			channel1Frequency = channel1Frequency + channel1SweepIncrease * channel1Frequency / 2;
		}
	}
	
	private void volumeSweep() {
		if (channel1VolumeEnvelopeNumberSweep > 0) {
			channel1VolumeEnvelopeNumberSweep--;
			channel1InitialVolumeEnvelope = channel1VolumeEnvelopeDirectionIncrease ? channel1InitialVolumeEnvelope << 1 : channel1InitialVolumeEnvelope >> 1;
		}
		if (channel2VolumeEnvelopeNumberSweep > 0) {
			channel2VolumeEnvelopeNumberSweep--;
			channel2InitialVolumeEnvelope = channel2VolumeEnvelopeDirectionIncrease ? channel2InitialVolumeEnvelope << 1 : channel2InitialVolumeEnvelope >> 1;
		}
		if (channel4VolumeEnvelopeNumberSweep > 0) {
			channel4VolumeEnvelopeNumberSweep--;
			channel4InitialVolumeEnvelope = channel4VolumeEnvelopeDirectionIncrease ? channel4InitialVolumeEnvelope << 1 : channel4InitialVolumeEnvelope >> 1;
		}
	}
	
	protected int getWaveForm() {
		int pattern = wavePatternRam[wavePatternCounter];
		return pattern;
	}
	
	/**
	 * Returns the current duty, so either 0 or 1.
	 * 
	 * @param channel The channel to get the duty for. Either 0 for channel 1 or 1 for channel 2.
	 * 
	 * @return 0 or 1.
	 */
	protected int getWaveDuty(int channel) {
		int pos = channel == 0 ? channel1DutyCounter : channel2DutyCounter;
		int duty = channel == 0 ? channel1WavePattern : channel2WavePattern;
		
		int pattern = WAVE_DUTY[duty];
		int retval = (pattern >> pos) & 0x01;
		return retval;
	}
	
	public void toggleSoundChannel(int channel) {
		channelCtrl[channel] = !channelCtrl[channel];
		flushAmplifier();
	}
	
	/**
	 * Returns the channel frequency.
	 * 
	 * @param channel The selected channel.
	 * 
	 * @return The frequency for the selected channel.
	 */
	public int getFrequency(int channel) {
		int retval = 0;
		switch (channel) {
			case 0:
				retval = channel1Frequency;
				break;
			case 1:
				retval = channel2Frequency;
				break;
			case 2:
				retval = channel3Frequency;
				break;
		}
		return retval;
	}
	
	/**
	 * Returns the volume of the given channel.
	 * <p>
	 * Channel 3 is special in the sense that the volume returned is always 1.
	 * The volume has to be calculated in the computation of the custom wave.
	 * </p>
	 * 
	 * @param channel The selected channel.
	 * 
	 * @return The volume of the selected channel.
	 */
	public int getVolume(int channel) {
		int retval = 0;
		switch (channel) {
			case 0:
				retval = channel1InitialVolumeEnvelope;
				break;
			case 1:
				retval = channel2InitialVolumeEnvelope;
				break;
			case 2:
				// this is special due to the calculation by the custom wave form
				retval = 1;
				break;
			case 3:
				// TODO
				retval = 0;
				break;
		}
		return retval;
	}
	
	public int tick(int cpuClockPassed) {
		if (!soundEnabled) {
			return 1;
		}
		
		cpuClock += cpuClockPassed;
		
		// 8192 for a 4 MHz processor
		// 2048 for a 1 MHz processor
		int mhz = 8192;
		if (cpuClock > mhz) {
			frameSequencer();
			cpuClock -= mhz;
		}
		
		for (int i = 0; i < frequencyTimer.length; i++) {
			tickTimer(i, cpuClockPassed);
		}
		
		// write to the amplifier if sound is enabled
		writeToAmplifier(cpuClockPassed);
		
		return 1;
	}
	
	/**
	 * Writes the sound to the amplifier.
	 * 
	 * @param cpuClockPassed The cpu clock that passed.
	 */
	protected void writeToAmplifier(int cpuClockPassed) {
		handleChannel1(cpuClockPassed);
		
		handleChannel2(cpuClockPassed);
		
		handleChannel3(cpuClockPassed);
		
		handleChannel4(cpuClockPassed);
	}
	
	protected void handleChannel1(int cpuClockPassed) {
		// write to amplifier if sound is enabled
		// and it is either continuous or the counter length is still positive
		if (isChannel1Writing()) {
			int output = channel1InitialVolumeEnvelope * getWaveDuty(0);
			getAmplifier().write(0, cpuClockPassed, (byte) output);
		} else {
			getAmplifier().writeNoop(0, cpuClockPassed);
		}
	}
	
	protected void handleChannel2(int cpuClockPassed) {
		// write to amplifier if sound is enabled
		// and it is either continuous or the counter length is still positive
		if (isChannel2Writing()) {
			int output = channel2InitialVolumeEnvelope * getWaveDuty(1);
			getAmplifier().write(1, cpuClockPassed, (byte) output);
		} else {
			getAmplifier().writeNoop(1, cpuClockPassed);
		}
	}
	
	protected void handleChannel3(int cpuClockPassed) {
		// write to amplifier if sound is enabled
		// and it is either continuous or the counter length is still positive
		if (isChannel3Writing()) {
			// normalize to 15
			int output = 2 * (getWaveForm() >> channel3OutputLevel);
			getAmplifier().write(2, cpuClockPassed, (byte) output);
		} else {
			getAmplifier().writeNoop(2, cpuClockPassed);
		}
	}
	
	protected void handleChannel4(int cpuClockPassed) {
		// write to amplifier if sound is enabled
		// and it is either continuous or the counter length is still positive
		if (isChannel4Writing()) {
//			int output = 
			getAmplifier().writeNoop(3, cpuClockPassed);
		} else {
			getAmplifier().writeNoop(3, cpuClockPassed);
		}
	}
	
	protected boolean isChannel1Writing() {
		return getChannelCtrl(0) && isSoundEnabled(0) && (!channel1CounterSelection || channel1SoundLength > 0);
	}
	
	protected boolean isChannel2Writing() {
		return getChannelCtrl(1) && isSoundEnabled(1) && (!channel2CounterSelection || channel2SoundLength > 0);
	}
	
	protected boolean isChannel3Writing() {
		return getChannelCtrl(2) && isSoundEnabled(2) && (!channel3CounterSelection || channel3SoundLength > 0);
	}
	
	protected boolean isChannel4Writing() {
		return getChannelCtrl(3) && isSoundEnabled(3) && (!channel4CounterSelection || channel4SoundLength > 0);
	}
	
	/**
	 * Dumps the APU to the output stream.
	 * 
	 * @param out The output stream to that the APU is dumped to.
	 * 
	 * @throws IOException If an {@link IOException} occurs.
	 */
	public void dumpState(OutputStream out) throws IOException {
		// TODO
	}
	
	/**
	 * Reloads the APU from the input stream.
	 * 
	 * @param in The stream to load the APU from.
	 * 
	 * @throws IOException If an {@link IOException} occurs.
	 */
	public void reloadState(InputStream in) throws IOException {
		// TODO
	}
	
}
