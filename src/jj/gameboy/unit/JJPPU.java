package jj.gameboy.unit;

import static jj.gameboy.output.JJScreen.WIDTH_VIEW_PIXEL_NUM;
import static jj.gameboy.unit.JJInterruptService.INTERRUPT_LCD_STAT;
import static jj.gameboy.unit.JJInterruptService.INTERRUPT_VBLANK;
import static jj.gameboy.unit.JJMainBus.ADDRESS_INTERRUPT;
import static jj.gameboy.output.JJPixelOrigin.TILE;
import static jj.gameboy.output.JJPixelOrigin.WINDOW;
import static jj.gameboy.output.JJPixelOrigin.SPRITE;

import java.awt.Dimension;
import java.awt.geom.Dimension2D;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import jj.gameboy.output.JJScreen;
import jj.gameboy.output.JJUnknownColorException;
import jj.gameboy.output.graphic.Sprite;
import jj.gameboy.output.graphic.Tile;

/**
 * The picture processing unit (PPU).
 * 
 * @author jan-thierry.wegener
 *
 */
public class JJPPU implements JJIProcessor, JJSaveStateUnit {
	
	// minimal address of the tile set data (for the first tile)
	private static final int MIN_TILE_SET_DATA_ADDRESS = 0x8000;
	// maximal address of the tile set data (for the first tile)
	private static final int MAX_TILE_SET_DATA_ADDRESS = 0x8800;
	// minimal address of the tile set data (for the first entry)
	private static final int MIN_MAP_ADDRESS = 0x9800;
	// minimal address of the tile set data (for the first entry)
	private static final int MAX_MAP_ADDRESS = 0x9C00;
	
	// width of the map
	private static final int MAP_WIDTH = 0x20;
	// height of the map
	private static final int MAP_HEIGHT = 0x20;
	
	/**
	 * The maximal number of sprites.
	 */
	private static final int MAX_SPRITE_NUM = 40;
	
	/**
	 * The maximal clock cycle for a scanline.
	 */
	private static final int MAX_H_BLANK_CYCLE = 456;
	/**
	 * The maximal clock cycle for data transfer.
	 */
	private static final int MAX_DATA_TRANSFER_CYCLE = 252;
	/**
	 * The maximal clock cycle for OAM search.
	 */
	private static final int MAX_OAM_SEARCH_CYCLE = 80;
	/**
	 * The maximal visible line.
	 */
	private static final int MAX_VISIBLE_LINE = 144;
	/**
	 * The maximal V-Blank line.
	 */
	private static final int MAX_V_BLANK_LINE = 154;
	
	/**
	 * The width in pixels of a sprite or tile.
	 */
	public static final int WIDTH_SPRITE_OR_TILE = 8;
	/**
	 * The height in pixels of a sprite or tile.
	 */
	public static final int HEIGHT_TILE = 8;
	
	/**
	 * The number of tiles per scanline.
	 */
	public static final int TILES_PER_SCANLINE = JJScreen.WIDTH_PIXEL_NUM / WIDTH_SPRITE_OR_TILE;
	
	
	// the number of bytes per tile
	private static final int BYTES_PER_TILE = 16;
	
	public static int MAX_TOTAL_TILE_SET_SIZE = 384;
	
	public static int CANVAS_BUFFER_SIZE = 0;

	public static final int TILE_SET_SIZE = 256;
	public static final int SPRITE_SET_SIZE = 40;
	
	public static final int TILE_WIDTH = 8;
	public static final int TILE_HEIGHT = 8;
	
	public static final int MAX_SHADE_NUM = 4;
	
	/**
	 * For CGB the maximal number of palettes.
	 * Not necessary a correct value!
	 */
	public static final int MAX_PALETTE_NUM = 640;
	
	// Mode Flag (in STAT)
	public static final int MODE_H_BLANK = 0;
	public static final int MODE_V_BLANK = 1;
	public static final int MODE_SEARCH_OAM = 2;
	public static final int MODE_DATA_TRANSFER = 3;
	
	// for handling the correct refresh rate
	private static final double FPS = 59.7275;
	private static final long MIN_TIME_BETWEEN_FRAME = (long) (1000 / FPS) * 1000;
	private long lastFrame = 0;
	
	private JJMainBus bus;
	
	private JJScreen screen;
	
	// maximum of 256 tiles
	private Tile[] tileSet = new Tile[TILE_SET_SIZE];
	// maximum of 40 sprites
	private Sprite[] spriteSet = new Sprite[SPRITE_SET_SIZE];
	// maximum of 256 sprites tiles
	private Tile[] spriteTileSet = new Tile[TILE_SET_SIZE];
	
	// register addresses
	// LCDC control register (0xff40)
	public static final int LCDC_ADDRESS = 0xFF40;
	// STAT status register (0xff41)
	public static final int STAT_ADDRESS = 0xFF41;
	// SCY vertical scroll register (0xff42)
	public static final int SCY_ADDRESS = 0xFF42;
	// SCX horizontal scroll register (0xff43)
	public static final int SCX_ADDRESS = 0xFF43;
	// LY scanline register (0xff44)
	public static final int LY_ADDRESS = 0xFF44;
	// LYC scanline compare register (0xff45)
	public static final int LYC_ADDRESS = 0xFF45;
	
	// register
	private int lcdc;
	private int stat;
	private int scy;
	private int scx;
	private int ly;
	private int lyc;
	private int wx;
	private int wy;
	// 
	private int currLcdMode = 0;
	
	// helper
	/**
	 * The color palette.
	 * 
	 * The four possible gray shades are:
	 *  		 0  White
	 *  		 1  Light gray
	 *  		 2  Dark gray
	 *  		 3  Black
	 */
	private int[] bgColorPalette;
	private int[][] objColorPalette;
	private static final int WHITE = 0;
	private static final int LIGHT_GRAY = 1;
	private static final int DARK_GRAY = 2;
	private static final int BLACK = 3;
	
	// the number of ticks passed
	private int tick;
	
	// address for bg and win tile set / map
	// bg data, contains absolute address
	private int bgTileDataAddress;
	// sprite data, contains absolute address
	private int spriteTileDataAddress;
	// bg map, contains absolute address
	private int bgMapAddress;
	// win map, contains absolute address
	private int winMapAddress;
	
	// the dimension of the sprites
	// can be either 8x8 or 8x16
	private static final Dimension SPRITE_SIZE_8x8 = new Dimension(8, 8);
	private static final Dimension SPRITE_SIZE_8x16 = new Dimension(8, 16);
	private Dimension2D spriteSize;
	
	// helper for pausing the threads
	private boolean isNewFrame;
	
	// index of the palette
	// TODO implement
	private int bgPaletteIndex;
	private int[] bgPaletteData;
	
	// for statistics
	private long totalCpuCycle = 0;
	
	/**
	 * The sprite attribute table (OAM).
	 */
	private int[] oam;
	
	/**
	 * The number of frame drops.
	 */
	private int numFrameDrops = 0;
	
	public JJPPU(JJMainBus bus) {
		this.bus = bus;
		
		screen = new JJScreen();
		
		// every sprite has 4 bytes of data in the OAM
		oam = new int[MAX_SPRITE_NUM * 4];
		
		bgColorPalette = new int[MAX_SHADE_NUM];
		setBackgroundColorPalette(0);
		objColorPalette = new int[2][MAX_SHADE_NUM];
		
		bgPaletteData = new int[MAX_PALETTE_NUM];
		
		reset();
	}
	
	public void reset() {
		currLcdMode = MODE_V_BLANK;
		
		ly = 0;
		
		tick = 0;
		bgPaletteIndex = 0;
		
		spriteSize = SPRITE_SIZE_8x8;
		
		// maximum of 256 tiles
		tileSet = new Tile[TILE_SET_SIZE];
		spriteSet = new Sprite[SPRITE_SET_SIZE];
		spriteTileSet = new Tile[TILE_SET_SIZE];
		
		setLcdMode(MODE_V_BLANK);
	}
	
	public boolean isLcdEn() {
		return (lcdc & 0x80) > 0;
	}
	
	public boolean isWinMap() {
		return (lcdc & 0x40) > 0;
	}
	
	public boolean isWinEn() {
		return (lcdc & 0x20) > 0;
	}
	
	public boolean isTileSel() {
		return (lcdc & 0x10) > 0;
	}
	
	public boolean isBgMap() {
		return (lcdc & 0x8) > 0;
	}
	
	public boolean isObjSize() {
		return (lcdc & 0x4) > 0;
	}
	
	public boolean isObjEn() {
		return (lcdc & 0x2) > 0;
	}
	
	public boolean isBigEn() {
		return (lcdc & 0x1) > 0;
	}
	
	public boolean isIntrLyc() {
		return (stat & 0x40) > 0;
	}
	
	public boolean isIntrM2() {
		return (stat & 0x20) > 0;
	}
	
	public boolean isIntrM1() {
		return (stat & 0x10) > 0;
	}
	
	public boolean isIntrM0() {
		return (stat & 0x8) > 0;
	}
	
	public int getLcdMode() {
		return (stat & 0x3);
	}
	
	/**
	 * Sets the current LCD mode.
	 * 
	 * @param m The new mode.
	 * 
	 * @return true if the mode has changed, false otherwise.
	 */
	private boolean setLcdMode(int m) {
		boolean retval = false;
		
		stat = (stat & ~0x03) | (m & 0x03);
		
		if (currLcdMode != m) {
			currLcdMode = m;
			retval = true;
		}
		
		return retval;
	}

	private void setCoincideFlag(boolean b) {
		if (b) {
			stat = stat | 0x04;
		} else {
			stat = stat & 0xFB;
		}
	}

	private void initRegister() {
		// to check if we have to reload the tiles
		int oldBgTileDataAddress = bgTileDataAddress;
		
		// sprites are always at 0x8000
		spriteTileDataAddress = MIN_TILE_SET_DATA_ADDRESS;
		// if set, total overlap
		bgTileDataAddress = isTileSel() ? MIN_TILE_SET_DATA_ADDRESS : MAX_TILE_SET_DATA_ADDRESS;
		
		bgMapAddress = isBgMap() ? MAX_MAP_ADDRESS : MIN_MAP_ADDRESS;
		winMapAddress = isWinMap() ? MAX_MAP_ADDRESS : MIN_MAP_ADDRESS;
		
		spriteSize = isObjSize() ? SPRITE_SIZE_8x16 : SPRITE_SIZE_8x8;
		
		if (oldBgTileDataAddress != bgTileDataAddress) {
			tileSet = new Tile[TILE_SET_SIZE];
		}
	}
	
	public void setTileSet(int ts, Tile[] tileSet) {
		System.arraycopy(tileSet, 0, this.tileSet, 0, tileSet.length);
	}
	
	public void setLCDC(int lcdc) {
		this.lcdc = lcdc;
	}
	
	public int getLCDC() {
		return lcdc;
	}
	
	public int getStat() {
		return stat;
	}
	
	public void setStat(int stat) {
		this.stat = stat;
	}
	
	public int getLy() {
		return ly;
	}
	
	public void setLy(int ly) {
		this.ly = ly;
	}
	
	public int getLyc() {
		return lyc;
	}
	
	public void setLyc(int lyc) {
		this.lyc = lyc;
	}
	
	public void setSCX(int scx) {
		this.scx = scx;
	}
	
	public int getSCX() {
		return scx;
	}
	
	public void setSCY(int scy) {
		this.scy = scy;
	}
	
	public int getSCY() {
		return scy;
	}
	
	public int getWx() {
		return wx;
	}
	
	public int getWy() {
		return wy;
	}
	
	public void setWx(int x) {
		wx = x;
	}
	
	public void setWy(int y) {
		wy = y;
	}
	
	/**
	 * Returns the address where the background tile data starts.
	 *  
	 * @return The address where the background tile data starts.
	 */
	public int getBgTileDataAddress() {
		return bgTileDataAddress;
	}
	
	/**
	 * Returns the address where the sprites data starts.
	 * 
	 * @return Always {@link #MIN_TILE_SET_DATA_ADDRESS}.
	 */
	public int getSpriteTileDataAddress() {
		return MIN_TILE_SET_DATA_ADDRESS;
	}
	
	/**
	 * Returns the address where the window tile data starts. The same as for background tiles.
	 *  
	 * @return The address where the window tile data starts.
	 */
	public int getWinTileDataAddress() {
		return bgTileDataAddress;
	}
	
	/**
	 * Returns the height in pixels of a tile (bg or win).
	 * 
	 * @return The height of a tile.
	 */
	public int getTileHeight() {
		return HEIGHT_TILE;
	}
	
	public int getTileWidth() {
		return WIDTH_SPRITE_OR_TILE;
	}
	
	public int getSpriteHeight() {
		return (int) spriteSize.getHeight();
	}

	public int getSpriteWidth() {
		return WIDTH_SPRITE_OR_TILE;
	}
	
	public boolean isNewFrame() {
		return isNewFrame;
	}
	
	public long getTotalCpuCycle() {
		return totalCpuCycle;
	}
	
	/**
	 * Makes ticks according to the number of cpu cycles.
	 * 
	 * <p>
	 * There are 4 modes:
	 * <ul>
	 * <li>OAM Search (20 cycles)</li>
	 * <li>Pixel Transfer (43 cycles)</li>
	 * <li>H-Blank (51 cycles)</li>
	 * <li>V-Blank</li>
	 * </ul>
	 * </p>
	 * 
	 * <p>
	 * The game boy has 144 lines plus 10 lines in V-Blank.
	 * </p>
	 * <p>
	 * A scanline takes all in all 114 = 20 + 43 + 51 cylces.
	 * </p>
	 */
	@Override
	public int tick(int cpuCycles) {
		updateStat();
		
		if (!isLcdEn()) {
			return 0;
		}
		
		totalCpuCycle += cpuCycles;
		tick += cpuCycles;
		if (tick >= MAX_H_BLANK_CYCLE) {
			ly++;
			// reset our tick
			tick = 0;
			
			// check for entering v-blank
			if (ly == MAX_VISIBLE_LINE) {
				setLcdMode(MODE_V_BLANK);
				// always request the v-blank interrupt
				bus.getIOPort().writeFromComponent(ADDRESS_INTERRUPT, INTERRUPT_VBLANK);
				// repaint screen
				repaint();
			} else if (ly >= MAX_V_BLANK_LINE) {
				// reset to line 0
				ly = 0;
			}
			
			if (ly < MAX_VISIBLE_LINE) {
				// let's draw the line
				drawLine();
			}
		}
		
		return 0;
	}
	
	public void repaint() {
		
		try {
			long now = System.currentTimeMillis();
			long sleepTime = (MIN_TIME_BETWEEN_FRAME - (now - lastFrame)*1000)/1000;
			if (numFrameDrops > 3 || sleepTime >= 0) {
				screen.repaint();
				numFrameDrops = 0;
			} else if (sleepTime < 0) {
				numFrameDrops++;
			}
			
			lastFrame = System.currentTimeMillis();
			if (sleepTime > 0) {
				Thread.sleep(sleepTime);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
		
	private int updateStat() {
		initRegister();
		
		if (!isLcdEn()) {
			reset();
			screen.turnOff();
			return 0;
		}
		
		screen.turnOn();
		
		int mode;
		// check if we have to request an interrupt
		boolean irq = false;
		
		// OAM -> Data Transfer -> h-blank
		if (ly >= MAX_VISIBLE_LINE) {
			// we are in the v-blank
			mode = MODE_V_BLANK;
			irq = setLcdMode(mode) && isIntrM1();
		} else {
			if (tick >= MAX_H_BLANK_CYCLE) {
				mode = MODE_SEARCH_OAM;
				irq = setLcdMode(mode) && isIntrM2();
			} else if (tick >= MAX_DATA_TRANSFER_CYCLE) {
				mode = MODE_H_BLANK;
				irq = setLcdMode(mode) && isIntrM0();
			} else if (tick >= MAX_OAM_SEARCH_CYCLE) {
				// mode data transfer does not request an interrupt
				mode = MODE_DATA_TRANSFER;
				setLcdMode(mode);
			} else {
				mode = MODE_SEARCH_OAM;
				irq = setLcdMode(mode) && isIntrM2();
			}
		}
		
		if (irq) {
			// request an interrupt, due to mode change
			bus.write(ADDRESS_INTERRUPT, INTERRUPT_LCD_STAT);
		}
		
		if (ly == lyc) {
			setCoincideFlag(true);
			if (isIntrLyc()) {
				bus.write(ADDRESS_INTERRUPT, INTERRUPT_LCD_STAT);
			}
		} else {
			setCoincideFlag(false);
		}
		
		return 0;
	}
	
	/**
	 * Draws the previous line.
	 */
	private void drawLine() {
		// handle the tiles
		if (isBigEn()) {
			int offset = 0;
			if ((scx % 8) != 0) {
				// we have to discard some pixels
				int toDiscard = scx % 8;
				offset = -toDiscard;
			}
			for (int lx = 0; lx <= WIDTH_VIEW_PIXEL_NUM; lx += 8) {
				// get the first few pixel
				int tileX = ((scx + lx) / getTileWidth()) % MAP_WIDTH;
				int tileY = ((scy + ly) / getTileHeight()) % MAP_HEIGHT;
				int y = (getSCY() + getLy()) % getTileHeight();
				
				Tile t = getBgTile(tileX, tileY);
				int[] pixels = t.getPixelLine(y);
				for (int x = 0; x < 8; x++) {
					if (lx + x + offset >= 0) {
						screen.setVisiblePixel(TILE, lx + x + offset, ly, pixels[x], t.getColor(pixels[x]));
					}
				}
			}
		}
		
		// handle the window
		if (isWinEn() && wy <= ly) {
			int mapWidthInPixel = WIDTH_VIEW_PIXEL_NUM - wx;
			int y = ly - wy;
			for (int lx = 0; lx < mapWidthInPixel; lx += 8) {
				// get the first few pixel
				int tileX = lx / getTileWidth();
				int tileY = y / getTileHeight();
				
				Tile t = getWinTile(tileX, tileY);
				int[] pixels = t.getPixelLine(y % getTileHeight());
				for (int x = 0; x < 8; x++) {
					if (lx + x >= 0) {
						screen.setVisiblePixel(WINDOW, wx + lx + x - 6, ly, pixels[x], t.getColor(pixels[x]));
					}
				}
			}
		}
		
		// handle the sprites
		if (isObjEn()) {
			for (int i = 0; i < getSpriteNum(); i++) {
				Sprite s = getSprite(i);
				if (s!= null && s.isInLine(ly)) {
					int[] pixels = s.getPixelLine(ly);
					if (s.isXFlip()) {
						int plen = pixels.length;
						for (int lx = 0; lx < plen; lx++) {
							int tmpx = s.getX() + plen - lx - 1;
							if (0 < pixels[lx]) {
								screen.setVisiblePixel(SPRITE, tmpx, ly, pixels[lx], s.getColor(pixels[lx]), s.isInFrontOfBackground());
							}
						}
					} else {
						for (int lx = 0; lx < pixels.length; lx++) {
							int tmpx = s.getX() + lx;
							if (0 < pixels[lx]) {
								screen.setVisiblePixel(SPRITE, tmpx, ly, pixels[lx], s.getColor(pixels[lx]), s.isInFrontOfBackground());
							}
						}
					}
				}
			}
		}
	}
	
	public Tile getBgTile(int x, int y) {
		int mapPos = x + y * TILES_PER_SCANLINE;
		
		int tile;
		if (0x8800 == getBgTileDataAddress()) {
			// we have a signed tile
			// thus, we have to offset it
			tile = bus.read7(bgMapAddress + mapPos) + 128;
		} else {
			tile = bus.read(bgMapAddress + mapPos);
		}
		
		Tile t = getTile(tile);
		return t;
	}
	
	public Tile getWinTile(int x, int y) {
		int mapPos = x + y * TILES_PER_SCANLINE;
		
		int tile;
		if (0x8800 == bus.getPPU().getWinTileDataAddress()) {
			// we have a signed tile
			// thus, we have to offset it
			tile = bus.read7(winMapAddress + mapPos) + 128;
		} else {
			tile = bus.read(winMapAddress + mapPos);
		}
		
		Tile t = getTile(tile);
		return t;
	}
	
	public int getSpriteNum() {
		return MAX_SPRITE_NUM;
	}
	
	public Tile getTile(int tileNum) {
		if (tileSet[tileNum] == null) {
			tileSet[tileNum] = new Tile(bus, tileNum);
		}
		return tileSet[tileNum];
	}
	
	public Tile getSpriteTile(int spriteTileNum) {
		if (spriteTileSet[spriteTileNum] == null) {
			spriteTileSet[spriteTileNum] = new Tile(bus, spriteTileNum, getSpriteTileDataAddress());
		}
		return spriteTileSet[spriteTileNum];
	}
	
	public Sprite getSprite(int spriteNum) {
		if (spriteSet[spriteNum] == null) {
			spriteSet[spriteNum] = new Sprite(bus, spriteNum);
		}
		return spriteSet[spriteNum];
	}
	
	public void dirtyRam(int address) {
		// compute the tile num and set it to null
		int tileNum = (address - getBgTileDataAddress()) / 16;
		if (0 <= tileNum && tileNum < tileSet.length) {
			tileSet[tileNum] = null;
		}
		// compute the tile num and set it to null
		int spriteNum = (address - getSpriteTileDataAddress()) / 16;
		if (0 <= spriteNum && spriteNum < spriteTileSet.length) {
			spriteTileSet[spriteNum] = null;
		}
	}
	
	public void loadTileSet() {
		for (int i = 0; i < TILE_SET_SIZE; i++) {
			tileSet[i] = new Tile(bus, i);
		}
	}
	
	public void loadSpriteSet() {
		for (int i = 0; i < SPRITE_SET_SIZE; i++) {
			spriteSet[i] = new Sprite(bus, i);
		}
	}
	
	public void dmaTransfer(int startAddress) {
		// adjust the position correctly
		startAddress = startAddress << 8;
		for (int i = 0; i < 0xA0; i++) {
			int value = bus.read(startAddress + i);
			oam[i] = value;
		}
		// load the sprite set after the transfer
		loadSpriteSet();
	}
	
	public JJScreen getScreen() {
		return screen;
	}
	
	/**
	 * Loads and returns the background tile set.
	 * 
	 * @return The background tile set.
	 */
	public Tile[] getBgTileSet() {
		return loadTileSet(bgTileDataAddress, bgTileDataAddress + 0x1000);
	}
	
	/**
	 * Loads and returns the sprite tile set.
	 * 
	 * @return The window 
	 */
	public Tile[] getSpriteTileSet() {
		return loadTileSet(spriteTileDataAddress, spriteTileDataAddress + 0x1000);
	}
	
	public Tile[] getSpriteAndBgTileSet() {
		// bg is either completely overlapping, or half overlapping but at the end
		return loadTileSet(spriteTileDataAddress, bgTileDataAddress + 0x1000);
	}
	
	private Tile[] loadTileSet(int offset, int end) {
		Tile[] tileSet = new Tile[(end - offset) / BYTES_PER_TILE];
		
		for (int i = 0; i < tileSet.length; i++) {
			int[] data = new int[TILE_WIDTH * TILE_HEIGHT];
			// each row is 2 bytes of data
			for (int y = 0; y < TILE_HEIGHT; y++) {
				int j = 0;
				int b1 = bus.read(offset + j + y * TILE_WIDTH);
				j++;
				int b2 = bus.read(offset + j + y * TILE_WIDTH);
				
				int b = (b1 << 8) | b2;
				
				// now we transform the data to a tile row
				for (int x = 0; x < TILE_WIDTH; x++) {
					data[x + y * TILE_WIDTH] = (b >> x * 2) & 0xFF ;
				}
			}
			
			tileSet[i] = new Tile(bus, 0);
		}
		
		return tileSet;
	}
	
	public void setBackgroundColorPalette(int bgp) {
		for (int i = 0; i < MAX_SHADE_NUM; i++) {
			bgColorPalette[i] = getBackgroundColorGrayShade(bgp, i);
		}
	}
	
	/**
	 * 
	 * 
	 * OBP0 - Object Palette 0 Data (R/W) - Non CGB Mode Only.
	 * OBP1 - Object Palette 1 Data (R/W) - Non CGB Mode Only.
	 * 
	 * @param objp
	 * @param p Either 0 or 1.
	 */
	public void setObjectColorPalette(int objp, int p) {
		for (int i = 0; i < MAX_SHADE_NUM; i++) {
			objColorPalette[p][i] = getBackgroundColorGrayShade(objp, i);
		}
	}
	
	public int getObjectColor(int paletteNumber, int colorNumber) {
		return objColorPalette[paletteNumber][colorNumber];
	}
	
	public void setBackgroundPaletteIndex(int index) {
		bgPaletteIndex = index;
	}
	
	public void setBackgroundColorPaletteData(int data) {
		bgPaletteData[bgPaletteIndex] = data;
	}
	
	public int getBackgroundColor(int colorNumber) {
		return bgColorPalette[colorNumber];
	}
	
	/**
	 * 
	 * FF47 - BGP - BG Palette Data (R/W) - Non CGB Mode Only
	 * This register assigns gray shades to the color numbers of the BG and Window tiles.
	 * 
	 *   Bit 7-6 - Shade for Color Number 3
	 *   Bit 5-4 - Shade for Color Number 2
	 *   Bit 3-2 - Shade for Color Number 1
	 *   Bit 1-0 - Shade for Color Number 0
	 *  
	 *  The four possible gray shades are:
	 *  		 0  White
	 *  		 1  Light gray
	 *  		 2  Dark gray
	 *  		 3  Black
	 *  In CGB Mode the Color Palettes are taken from CGB Palette Memory instead.
	 * 
	 * @return 
	 */
	private int getBackgroundColorGrayShade(int bgp, int colorNumber) {
		int color;
		
		int grayShade = (bgp >> (colorNumber * 2)) & 0x03;
		switch (grayShade) {
			
			case 0:
				color = WHITE;
				break;
				
			case 1:
				color = LIGHT_GRAY;
				break;
				
			case 2:
				color = DARK_GRAY;
				break;
				
			case 3:
				color = BLACK;
				break;
				
			default:
				throw new JJUnknownColorException("Unknown shade: " + grayShade);
				
		}
		
		return color;
	}

	public void writeOAM(int index, int value) {
		oam[index] = value;
	}
	
	public int readOAM(int index) {
		return oam[index];
	}
	
	/**
	 * Dumps the RAM to the output stream.
	 * 
	 * @param out The output stream to that the RAM is dumped to.
	 * 
	 * @throws IOException If an {@link IOException} occurs.
	 */
	public void dumpState(OutputStream out) throws IOException {
		out.write(lcdc);
		out.write(stat);
		out.write(scy);
		out.write(scx);
		out.write(ly);
		out.write(lyc);
		out.write(wx);
		out.write(wy);
		out.write(currLcdMode);
		for (int i = 0; i < bgColorPalette.length; i++) {
			out.write(bgColorPalette[i]);
		}
		for (int i = 0; i < objColorPalette.length; i++) {
			for (int j = 0; j < objColorPalette[i].length; j++) {
				out.write(objColorPalette[i][j]);
			}
		}
		out.write(tick);
		out.write(bgTileDataAddress);
		out.write(spriteTileDataAddress);
		out.write(bgMapAddress);
		out.write(winMapAddress);
		out.write((int)spriteSize.getWidth());
		out.write((int)spriteSize.getHeight());
		out.write(bgPaletteIndex);
		for (int i = 0; i < bgPaletteData.length; i++) {
			out.write(bgPaletteData[i]);
		}
		for (int i = 0; i < oam.length; i++) {
			out.write(oam[i]);
		}
	}
	
	/**
	 * Reloads the RAM from the input stream.
	 * 
	 * @param in The stream to load the RAM from.
	 * 
	 * @throws IOException If an {@link IOException} occurs.
	 */
	public void reloadState(InputStream in) throws IOException {
		lcdc = in.read();
		stat = in.read();
		scy = in.read();
		scx = in.read();
		ly = in.read();
		lyc = in.read();
		wx = in.read();
		wy = in.read();
		currLcdMode = in.read();
		for (int i = 0; i < bgColorPalette.length; i++) {
			bgColorPalette[i] = in.read();
		}
		for (int i = 0; i < objColorPalette.length; i++) {
			for (int j = 0; j < objColorPalette[i].length; j++) {
				objColorPalette[i][j] = in.read();
			}
		}
		tick = in.read();
		bgTileDataAddress = in.read();
		spriteTileDataAddress = in.read();
		bgMapAddress = in.read();
		winMapAddress = in.read();
		spriteSize.setSize(in.read(), in.read());
		bgPaletteIndex = in.read();
		for (int i = 0; i < bgPaletteData.length; i++) {
			bgPaletteData[i] = in.read();
		}
		for (int i = 0; i < oam.length; i++) {
			oam[i] = in.read();
		}
		
		// additional changes
		// the tiles will be reloaded when accessed
		tileSet = new Tile[TILE_SET_SIZE];
		spriteSet = new Sprite[SPRITE_SET_SIZE];
		spriteTileSet = new Tile[TILE_SET_SIZE];
	}
	
}
