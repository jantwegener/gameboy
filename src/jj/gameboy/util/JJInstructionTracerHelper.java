package jj.gameboy.util;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

public class JJInstructionTracerHelper {
	
	private static JJInstructionTracerHelper INSTANCE;
	
	private Queue<String> trace = new LinkedList<>();
	
	private boolean isAllLogging = false;
	
	private boolean isOpcodeLogging = false;
	
	private boolean isRangeLogging = false;
	
	private boolean isAnd = false;
	
	private Set<Integer> opcodeSet = new HashSet<>();
	
	private List<Range> rangeList = new ArrayList<>();
	
	/**
	 * If set, then the following delay opcodes are logged as well.
	 */
	private int trailing = 0;
	
	private int currTrailing = 0;
	
	private int bufferSize = -1;
	
	private JJInstructionTracerHelper() {
	}
	
	public static JJInstructionTracerHelper getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new JJInstructionTracerHelper();
		}
		return INSTANCE;
	}
	
	public boolean isLogging(int pc, int opcode) {
		boolean log = false;
		if (isAllLogging) {
			log = true;
		} else if (currTrailing > 0) {
			currTrailing--;
			log = true;
		} else {
			if (isAnd()) {
				// logical and connection
				log = isRangeLogging && isOpcodeLogging && checkOpcode(opcode) && checkRange(pc);
			} else {
				// logical or connection
				log = (isOpcodeLogging && checkOpcode(opcode)) || (isRangeLogging && checkRange(pc));
			}
			
			if (log) {
				currTrailing = trailing;
			}
		}
		
		return log;
	}
	
	public void setStandardLogCallRet() {
		setOpcodeLogging(true);
		// CALL
		addOpcode(0xc4);
		addOpcode(0xcc);
		addOpcode(0xcd);
		addOpcode(0xd4);
		addOpcode(0xdc);
		// RET
		addOpcode(0xc0);
		addOpcode(0xc8);
		addOpcode(0xc9);
		addOpcode(0xd0);
		addOpcode(0xd8);
		addOpcode(0xd9);
		// RST
		addOpcode(0xc7);
		addOpcode(0xd7);
		addOpcode(0xe7);
		addOpcode(0xf7);
		addOpcode(0xcf);
		addOpcode(0xdf);
		addOpcode(0xef);
		addOpcode(0xff);
		// Interrupt
		addOpcode(-1);
	}
	
	/**
	 * Creates a copy of the current trace and returns the copy. Should be rarely used.
	 * 
	 * @return A copy of the current trace.
	 */
	public synchronized Queue<String> getCallTrace() {
		return new LinkedList<>(trace);
	}
	
	public synchronized void resetTrace() {
		trace.clear();
	}
	
	public synchronized void addTrace(String traceStr) {
		trace.add(traceStr);
		while (bufferSize > 0 && trace.size() > bufferSize) {
			trace.poll();
		}
	}
	
	/**
	 * If set, then the following trailing opcodes are logged as well.
	 * 
	 * @param d The number of opcodes to log in addition.
	 */
	public void setTrailing(int d) {
		trailing = d;
	}
	
	private boolean checkRange(int pc) {
		boolean log = false;
		for (Range range : rangeList) {
			if (range.contains(pc)) {
				log = true;
				break;
			}
		}
		return log;
	}
	
	private boolean checkOpcode(int opcode) {
		return opcodeSet.contains(opcode);
	}
	
	public void addOpcode(int opcode) {
		opcodeSet.add(opcode);
	}
	
	/**
	 * The range to include in logging.
	 * 
	 * @param start The start of the range (inclusive).
	 * @param end The end of the range (inclusive).
	 */
	public void addRange(int start, int end) {
		rangeList.add(new Range(start, end));
	}
	
	public boolean isAllLogging() {
		return isAllLogging;
	}
	
	public void setAllLogging(boolean allLogging) {
		this.isAllLogging = allLogging;
	}
	
	public boolean isOpcodeLogging() {
		return isOpcodeLogging;
	}

	public void setOpcodeLogging(boolean isOpcodeLogging) {
		this.isOpcodeLogging = isOpcodeLogging;
	}

	public boolean isRangeLogging() {
		return isRangeLogging;
	}

	public void setRangeLogging(boolean isRangeLogging) {
		this.isRangeLogging = isRangeLogging;
	}
	
	public void setBufferSize(int size) {
		bufferSize = size;
	}
	
	public int getBufferSize() {
		return bufferSize;
	}
	
	public boolean isAnd() {
		return isAnd;
	}
	
	/**
	 * If and is set to true, opcode and pc have to be valid to be logged. Otherwise, only opcode or pc can be valid.
	 * When a logical and is wanted, both opcode and range checking have to be enabled, otherwise, false is always returned.
	 * 
	 * @param isAnd
	 */
	public void setAnd(boolean isAnd) {
		this.isAnd = isAnd;
	}
	
	/**
	 * Writes the current traces. The trace can be emptied during the write process or simply copied.
	 * Making a copy, can significantly impact the performance since a copy of the current trace is made.
	 * 
	 * @param file Where to write the trace to.
	 * @param emptyTrace If set to true, the current trace is emptied while writing it.
	 */
	public synchronized void writeQueue(String file, boolean emptyTrace) {
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
			Queue<String> queue;
			if (emptyTrace) {
				queue = this.trace;
			} else {
				queue = getCallTrace();
			}
			
			while (!queue.isEmpty()) {
				String nextCall = queue.poll();
				writer.write(nextCall);
				writer.newLine();
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
	private class Range {
		
		private final int start;
		private final int end;
		
		public Range(int s, int e) {
			start = s;
			end = e;
		}
		
		public boolean contains(int x) {
			return start <= x && x <= end;
		}
	}
	
}
