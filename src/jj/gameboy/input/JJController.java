package jj.gameboy.input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import jj.gameboy.unit.JJInterruptService;
import jj.gameboy.unit.JJMainBus;

public class JJController implements KeyListener {
	
	/**
	 * The address for the I/O port relative to 0xFF00.
	 */
	public static final int JOYPAD_ADDRESS = 0xFF00;
	
	private JJMainBus bus;
	
	private int pressedButtonResult = 0;
	
	private boolean isSelectButton = false;
	private boolean isSelectDirection = false;
	
	private boolean bStart = false;
	private boolean bSelect = false;
	private boolean bA = false;
	private boolean bB = false;
	
	private boolean dUp = false;
	private boolean dDown = false;
	private boolean dLeft = false;
	private boolean dRight = false;

	private int buttonStart = KeyEvent.VK_ENTER;
	private int buttonSelect = KeyEvent.VK_SHIFT;
	private int buttonA = KeyEvent.VK_Y;
	private int buttonB = KeyEvent.VK_X;

	private int buttonUp = KeyEvent.VK_UP;
	private int buttonDown = KeyEvent.VK_DOWN;
	private int buttonLeft = KeyEvent.VK_LEFT;
	private int buttonRight = KeyEvent.VK_RIGHT;
	
	public JJController(JJMainBus bus) {
		this.bus = bus;
		
		computeResult();
	}
	
	/**
	 * Selects either button or direction keys. The controller writes back the result.
	 * 
	 * Bit 7 - Not used
	 * Bit 6 - Not used
	 * Bit 5 - P15 Select Button Keys      (0=Select)
	 * Bit 4 - P14 Select Direction Keys   (0=Select)
	 * Bit 3 - P13 Input Down  or Start    (0=Pressed) (Read Only)
	 * Bit 2 - P12 Input Up    or Select   (0=Pressed) (Read Only)
	 * Bit 1 - P11 Input Left  or Button B (0=Pressed) (Read Only)
	 * Bit 0 - P10 Input Right or Button A (0=Pressed) (Read Only)
	 */
	public void selectRow(int value) {
		isSelectButton = false;
		isSelectDirection = false;
		
		if ((value & 0x20) > 0) {
			// bit 5 is high => direction is selected
			isSelectDirection = true;
			isSelectButton = false;
		} else if ((value & 0x10) > 0) {
			// bit 4 is high => button is selected
			isSelectDirection = false;
			isSelectButton = true;
		}
		
		computeResult();
	}
	
	private void computeResult() {
		pressedButtonResult = 0;
		if (isSelectButton) {
			pressedButtonResult = 0x20;
			
			if (bStart) {
				pressedButtonResult = pressedButtonResult | 0x08;
			}
			if (bSelect) {
				pressedButtonResult = pressedButtonResult | 0x04;
			}
			if (bB) {
				pressedButtonResult = pressedButtonResult | 0x02;
			}
			if (bA) {
				pressedButtonResult = pressedButtonResult | 0x01;
			}
		} else if (isSelectDirection) {
			pressedButtonResult = 0x10;
			
			if (dDown) {
				pressedButtonResult = pressedButtonResult | 0x08;
			}
			if (dUp) {
				pressedButtonResult = pressedButtonResult | 0x04;
			}
			if (dLeft) {
				pressedButtonResult = pressedButtonResult | 0x02;
			}
			if (dRight) {
				pressedButtonResult = pressedButtonResult | 0x01;
			}
		} else {
			// neither is selected
			pressedButtonResult = 0x10 | 0x20;
		}
		
		// we have 1 for selected, so we have to inverse the result before writing
		pressedButtonResult = pressedButtonResult ^ 0xFF;
	}
	
	public int getPressedButtonResult() {
		return pressedButtonResult;
	}
	
	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {
		boolean padKeyPressed = false;
		if (e.getKeyCode() == buttonStart) {
			bStart = true;
			padKeyPressed = true;
		} else if (e.getKeyCode() == buttonSelect) {
			bSelect = true;
			padKeyPressed = true;
		} else if (e.getKeyCode() == buttonA) {
			bA = true;
			padKeyPressed = true;
		} else if (e.getKeyCode() == buttonB) {
			bB = true;
			padKeyPressed = true;
		} else if (e.getKeyCode() == buttonUp) {
			dUp = true;
			padKeyPressed = true;
		} else if (e.getKeyCode() == buttonDown) {
			dDown = true;
			padKeyPressed = true;
		} else if (e.getKeyCode() == buttonLeft) {
			dLeft = true;
			padKeyPressed = true;
		} else if (e.getKeyCode() == buttonRight) {
			dRight = true;
			padKeyPressed = true;
		}
		
		if (padKeyPressed) {
			// and fire the interrupt
			bus.getIOPort().writeFromComponent(JJMainBus.ADDRESS_INTERRUPT, JJInterruptService.INTERRUPT_JOYPAD);
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if (e.getKeyCode() == buttonStart) {
			bStart = false;
		} else if (e.getKeyCode() == buttonSelect) {
			bSelect = false;
		} else if (e.getKeyCode() == buttonA) {
			bA = false;
		} else if (e.getKeyCode() == buttonB) {
			bB = false;
		} else if (e.getKeyCode() == buttonUp) {
			dUp = false;
		} else if (e.getKeyCode() == buttonDown) {
			dDown = false;
		} else if (e.getKeyCode() == buttonLeft) {
			dLeft = false;
		} else if (e.getKeyCode() == buttonRight) {
			dRight = false;
		}
		
		// only fire the interrupt when a button is pressed
	}
	
}
