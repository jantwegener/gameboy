package jj.gameboy.input;

import java.awt.Color;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import jj.gameboy.JJGameBoy;
import jj.gameboy.output.JJGraphicSettings;

public class JJSelectColorAction extends AbstractAction {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Color[] colorTilePalette;
	private Color[] colorSpritePalette;
	
	public JJSelectColorAction(JJGameBoy gameboy, String colorTheme, Color[] colorTilePalette, Color[] colorSpritePalette) {
		super(colorTheme);
		this.colorTilePalette = colorTilePalette;
		this.colorSpritePalette = colorSpritePalette;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		JJGraphicSettings.setColor(colorTilePalette[0], colorTilePalette[1], colorTilePalette[2], colorTilePalette[3]);
		JJGraphicSettings.setSpriteColor(colorSpritePalette[0], colorSpritePalette[1], colorSpritePalette[2], colorSpritePalette[3]);
	}

}
