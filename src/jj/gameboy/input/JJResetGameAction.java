package jj.gameboy.input;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.KeyStroke;

import jj.gameboy.JJGameBoy;

public class JJResetGameAction extends AbstractAction {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JJGameBoy gameboy;
	
	private static JJResetGameAction INSTANCE;
	
	public JJResetGameAction(JJGameBoy gameboy) {
		super("Reset");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl+r"));
		this.gameboy = gameboy;
	}
	
	public static JJResetGameAction getAction(JJGameBoy gameboy) {
		if (INSTANCE == null) {
			INSTANCE = new JJResetGameAction(gameboy);
		}
		return INSTANCE;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		gameboy.reset();
	}
	
}
