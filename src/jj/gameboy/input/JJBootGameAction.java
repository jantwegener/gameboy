package jj.gameboy.input;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.KeyStroke;

import jj.gameboy.JJGameBoy;

public class JJBootGameAction extends AbstractAction {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JJGameBoy gameboy;
	
	private static JJBootGameAction INSTANCE;
	
	public JJBootGameAction(JJGameBoy gameboy) {
		super("Open...");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl+o"));
		this.gameboy = gameboy;
	}
	
	public static JJBootGameAction getAction(JJGameBoy gameboy) {
		if (INSTANCE == null) {
			INSTANCE = new JJBootGameAction(gameboy);
		}
		return INSTANCE;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		JFileChooser chooser = new JFileChooser(System.getProperty("user.dir"));
		chooser.setFileFilter(new JJClassicGameBoyFilter());
		int retval = chooser.showOpenDialog(null);
		if (retval == JFileChooser.APPROVE_OPTION) {
			File chosenFile = chooser.getSelectedFile();
			String romFile = chosenFile.getAbsolutePath();
			
			try {
				gameboy.bootUp(romFile);
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}
	}
	
}
