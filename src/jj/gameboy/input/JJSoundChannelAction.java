package jj.gameboy.input;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import jj.gameboy.JJGameBoy;

public class JJSoundChannelAction extends AbstractAction {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JJGameBoy gameboy;
	
	private final int channel;
	
	public JJSoundChannelAction(JJGameBoy gameboy, int channel) {
		super("Sound " + channel);
		this.gameboy = gameboy;
		this.channel = channel;
		putValue(SELECTED_KEY, true);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		gameboy.toggleChannel(channel);
	}
	
}
