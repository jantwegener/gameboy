package jj.gameboy.input;

import java.io.File;

import javax.swing.filechooser.FileFilter;

public class JJClassicGameBoyFilter extends FileFilter {

	@Override
	public boolean accept(File f) {
		if (f.isDirectory()) {
			return true;
		}
		if (f.getName().endsWith(".gb") || f.getName().endsWith(".zip")) {
			return true;
		}
		return false;
	}

	@Override
	public String getDescription() {
		return "Game Boy File (*.gb, *.zip)";
	}

}
