package jj.gameboy.input;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import jj.gameboy.JJGameBoy;

public class JJMusicAction extends AbstractAction {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JJGameBoy gameboy;
	
	private static JJMusicAction INSTANCE;
	
	public JJMusicAction(JJGameBoy gameboy) {
		super("Music");
		this.gameboy = gameboy;
	}
	
	public static JJMusicAction getAction(JJGameBoy gameboy) {
		if (INSTANCE == null) {
			INSTANCE = new JJMusicAction(gameboy);
		}
		return INSTANCE;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		gameboy.toggleMusic();
	}
	
}
