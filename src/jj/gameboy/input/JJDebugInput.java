package jj.gameboy.input;

import static jj.gameboy.output.JJPixelOrigin.TILE;
import static jj.gameboy.output.JJPixelOrigin.WINDOW;
import static jj.gameboy.output.JJPixelOrigin.SPRITE;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;

import jj.gameboy.output.JJGraphicSettings;
import jj.gameboy.output.JJPixelOrigin;
import jj.gameboy.output.graphic.Sprite;
import jj.gameboy.output.graphic.Tile;
import jj.gameboy.unit.JJMainBus;
import jj.gameboy.util.JJInstructionTracerHelper;

public class JJDebugInput implements KeyListener {
	
	private JJMainBus bus;

	public JJDebugInput(JJMainBus bus) {
		this.bus = bus;
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_F11) {
			String cpuTrace = bus.getCPU().toString();
			System.out.println(cpuTrace);
		} else if (e.getKeyCode() == KeyEvent.VK_F12) {
			if (e.isShiftDown()) {
				String mbcInformation = bus.getCartridge().toString();
				System.out.println(mbcInformation);
			} else {
				String interruptFlags = bus.getCPU().getInterruptService().toString();
				System.out.println(interruptFlags);
			}
		} else if (e.getKeyCode() == KeyEvent.VK_F10) {
			String queueFile = "C:\\Users\\Otto\\eclipse-workspace\\JJGameBoy\\log\\callQueue.asm";
			JJInstructionTracerHelper.getInstance().writeQueue(queueFile, true);
		} else if (e.getKeyCode() == KeyEvent.VK_F6) {
			if (e.isShiftDown()) {
				showSpriteDialog();
			} else if (e.isControlDown()) {
				showPaletteDialog();
			} else if (e.isAltDown()) {
				showBackgroundDialog();
			} else {
				showTileDialog();
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}
	
	private void showBackgroundDialog() {
		JPanel panel = new JPanel() {
			
			private int MAX_WH = 0x1F;
			
			private final int squareSize = 2;
			private final int dist = 1;
			
			public void paintComponent(Graphics g) {
				g.setColor(Color.WHITE);
				g.fillRect(0, 0, getWidth(), getHeight());
				
				for (int ty = 0; ty < MAX_WH; ty++) {
					for (int tx = 0; tx < MAX_WH; tx++) {
						Tile tile = bus.getPPU().getBgTile(tx, ty);
						
						for (int x = 0; x < 8; x++) {
							for (int y = 0; y < 8; y++) {
								Color c = JJGraphicSettings.getColor(tile.getPixel(x, y), TILE);
								g.setColor(c);
								g.fillRect(tx * (squareSize * 8 + dist) + x * squareSize, ty * (squareSize * 8 + dist) + y * squareSize, squareSize, squareSize);
							}
						}
						
					}
				}
			}
			
		};
		
		JDialog dialog = new JDialog((JFrame)null, "Background Map", false);
		dialog.add(panel);
		dialog.setSize(200, 200);
		dialog.setVisible(true);
	}
	
	private void showPaletteDialog() {
		JPanel panel = new JPanel() {
			public void paintComponent(Graphics g) {
				g.setColor(Color.WHITE);
				g.fillRect(0, 0, getWidth(), getHeight());
				
				int width = 40;
				
				for (int color = 0; color < 4; color++) {
					g.setColor(JJGraphicSettings.getColor(bus.getPPU().getBackgroundColor(color), TILE));
					g.fillRect(color * (width + 2), 0, width, width);
				}
				g.setColor(Color.BLACK);
				g.drawString("TILE", 0, 10);
				
				for (int color = 0; color < 4; color++) {
					g.setColor(JJGraphicSettings.getColor(bus.getPPU().getBackgroundColor(color), WINDOW));
					g.fillRect(color * (width + 2), width + 2, width, width);
				}
				g.setColor(Color.BLACK);
				g.drawString("WINDOW", 0, width + 12);
				
				for (int color = 0; color < 4; color++) {
					g.setColor(JJGraphicSettings.getColor(bus.getPPU().getObjectColor(0, color), SPRITE));
					g.fillRect(color * (width + 2), 2 * width + 4, width, width);
				}
				g.setColor(Color.BLACK);
				g.drawString("SPRITE 1", 0, 2 * (width + 2) + 10);
				
				for (int color = 0; color < 4; color++) {
					g.setColor(JJGraphicSettings.getColor(bus.getPPU().getObjectColor(1, color), SPRITE));
					g.fillRect(color * (width + 2), 3 * width + 6, width, width);
				}
				g.setColor(Color.BLACK);
				g.drawString("SPRITE 2", 0, 3 * (width + 2) + 10);
			}
		};
		
		JDialog dialog = new JDialog((JFrame)null, "Palette", false);
		dialog.add(panel);
		dialog.setSize(200, 200);
		dialog.setVisible(true);
	}
	
	private void showSpriteDialog() {
		JPanel panel = new JPanel() {
			public void paintComponent(Graphics g) {
				g.setColor(Color.WHITE);
				g.fillRect(0, 0, getWidth(), getHeight());
				
				for (int tileY = 0; tileY < 8; tileY++) {
					for (int tileX = 0; tileX < 5; tileX++) {
						Sprite s = bus.getPPU().getSprite(tileX + tileY * 4);
						
						for (int x = 0; x < 8; x++) {
							for (int y = 0; y < s.getHeight(); y++) {
								Color c = JJGraphicSettings.getColor(s.getPixel(x, y), SPRITE);
								g.setColor(c);
								g.fillRect(tileX * 4 * 8 + x * 3, tileY * 4 * s.getHeight() + y * 3, 3, 3);
							}
						}
						
						g.setColor(Color.BLACK);
						g.drawString(String.valueOf(s.getPatternNumber()), tileX * 4 * 8, tileY * 4 * s.getHeight() + 8);
					}
				}
			}
		};
		
		panel.addMouseMotionListener(new MouseMotionListener() {
			
			@Override
			public void mouseMoved(MouseEvent e) {
				int tileX = (int) (e.getX() / (4d * 8d));
				int tileY = (int) (e.getY() / (4d * bus.getPPU().getSpriteHeight()));
				if (tileX + tileY * 4 < 40) {
					Sprite s = bus.getPPU().getSprite(tileX + tileY * 4);
					bus.getPPU().getScreen().setSpriteDebugRectangle(s.getX(), s.getY(), 8, bus.getPPU().getSpriteHeight());
				}
			}
			
			@Override
			public void mouseDragged(MouseEvent e) {
			}
		});
		
		JDialog dialog = new JDialog((JFrame)null, "Sprites", false);
		dialog.add(panel);
		dialog.setSize(200, 200);
		dialog.setVisible(true);
	}
	
	private void showTileDialog() {
		int width = 16;
		int height = 256 / width;
		
		JPanel panel = new JPanel() {
			public void paintComponent(Graphics g) {
				g.setColor(Color.WHITE);
				g.fillRect(0, 0, getWidth(), getHeight());
				
				for (int tileY = 0; tileY < width; tileY++) {
					for (int tileX = 0; tileX < height; tileX++) {
						Tile s = bus.getPPU().getTile(tileX + tileY * width);
						
						for (int x = 0; x < 8; x++) {
							for (int y = 0; y < s.getHeight(); y++) {
								Color c = JJGraphicSettings.getColor(s.getPixel(x, y), TILE);
								g.setColor(c);
								g.fillRect(tileX * 4 * 8 + x * 3, tileY * 4 * s.getHeight() + y * 3, 3, 3);
							}
						}
					}
				}
			}
			
		};
		
		JDialog dialog = new JDialog((JFrame)null, "Tiles", false);
		dialog.add(panel);
		dialog.setSize(200, 200);
		dialog.setVisible(true);
	}
	
}
