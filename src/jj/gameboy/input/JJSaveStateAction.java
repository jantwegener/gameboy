package jj.gameboy.input;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.AbstractAction;

import jj.gameboy.JJGameBoy;

public class JJSaveStateAction implements KeyListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JJGameBoy gameboy;
	
	private static JJSaveStateAction INSTANCE;
	
	public JJSaveStateAction(JJGameBoy gameboy) {
		this.gameboy = gameboy;
		
	}

	public static JJSaveStateAction getAction(JJGameBoy gameboy) {
		if (INSTANCE == null) {
			INSTANCE = new JJSaveStateAction(gameboy);
		}
		return INSTANCE;
	}
	
	
	private void dumpState(int stateNum) {
		gameboy.dumpState(stateNum);
	}
	
	private void reloadState(int stateNum) {
		gameboy.reloadState(stateNum);
	}
	
	@Override
	public void keyReleased(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_0) {
			if (e.isControlDown()) {
				dumpState(0);
			} else if (e.isAltDown()) {
				reloadState(0);
			}
		} else if (e.getKeyCode() == KeyEvent.VK_1) {
			if (e.isControlDown()) {
				dumpState(1);
			} else if (e.isAltDown()) {
				reloadState(1);
			}
		} else if (e.getKeyCode() == KeyEvent.VK_2) {
			if (e.isControlDown()) {
				dumpState(2);
			} else if (e.isAltDown()) {
				reloadState(2);
			}
		} else if (e.getKeyCode() == KeyEvent.VK_3) {
			if (e.isControlDown()) {
				dumpState(3);
			} else if (e.isAltDown()) {
				reloadState(3);
			}
		} else if (e.getKeyCode() == KeyEvent.VK_4) {
			if (e.isControlDown()) {
				dumpState(4);
			} else if (e.isAltDown()) {
				reloadState(4);
			}
		} else if (e.getKeyCode() == KeyEvent.VK_5) {
			if (e.isControlDown()) {
				dumpState(5);
			} else if (e.isAltDown()) {
				reloadState(5);
			}
		} else if (e.getKeyCode() == KeyEvent.VK_6) {
			if (e.isControlDown()) {
				dumpState(6);
			} else if (e.isAltDown()) {
				reloadState(6);
			}
		} else if (e.getKeyCode() == KeyEvent.VK_7) {
			if (e.isControlDown()) {
				dumpState(7);
			} else if (e.isAltDown()) {
				reloadState(7);
			}
		} else if (e.getKeyCode() == KeyEvent.VK_8) {
			if (e.isControlDown()) {
				dumpState(8);
			} else if (e.isAltDown()) {
				reloadState(8);
			}
		} else if (e.getKeyCode() == KeyEvent.VK_9) {
			if (e.isControlDown()) {
				dumpState(9);
			} else if (e.isAltDown()) {
				reloadState(9);
			}
		}
	}
	
	@Override
	public void keyTyped(KeyEvent e) {
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
	}
	
}
