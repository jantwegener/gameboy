package jj.gameboy.input;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;

import jj.gameboy.output.JJGraphicSettings;
import jj.gameboy.output.JJPixelOrigin;
import jtw.util.gui.AbstractSettingsDialog;

public class JJColorChoserAction extends AbstractAction  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public JJColorChoserAction() {
		super("Color");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		final CenterPanel center = new CenterPanel();
		
		AbstractSettingsDialog<Color[][]> dialog = new AbstractSettingsDialog<Color[][]>(null, "", true) {
			@Override
			protected JPanel initCenter() {
				return center;
			}

			@Override
			public Color[][] getResult() {
				return center.getSelectedColors();
			}
		};
		
		dialog.setSize(200, 300);
		dialog.showDialog();
		if (dialog.getButtonPressed() == AbstractSettingsDialog.YES_OPTION) {
			Color[][] selectedColorArray = dialog.getResult();
			JJGraphicSettings.setTileColor(selectedColorArray[0]);
			JJGraphicSettings.setWindowColor(selectedColorArray[1]);
			JJGraphicSettings.setSpriteColor(selectedColorArray[2]);
		}
	}
	
	private class CenterPanel extends JPanel {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		private final static int MAX_COLOR = 4;
		
		private JButton[] tileButton = new JButton[MAX_COLOR];
		private JButton[] winButton = new JButton[MAX_COLOR];
		private JButton[] spriteButton = new JButton[MAX_COLOR];
		
		private final JPanel instance;
		
		public CenterPanel() {
			super(new GridLayout(3, 4));
			
			instance = this;
			
			init();
		}
		
		public Color[][] getSelectedColors() {
			Color[][] result = new Color[3][4];
			for (int color = 0; color < MAX_COLOR; color++) {
				result[0][color] = tileButton[color].getBackground();
			}
			for (int color = 0; color < MAX_COLOR; color++) {
				result[1][color] = winButton[color].getBackground();
			}
			for (int color = 0; color < MAX_COLOR; color++) {
				result[2][color] = spriteButton[color].getBackground();
			}
			return result;
		}
		
		private void init() {
			for (int i = 0; i < MAX_COLOR; i++) {
				tileButton[i] = new JButton("Tile " + i);
				winButton[i] = new JButton("Win " + i);
				spriteButton[i] = new JButton("Sprite " + i);

				tileButton[i].setBackground(JJGraphicSettings.getColor(i, JJPixelOrigin.TILE));
				winButton[i].setBackground(JJGraphicSettings.getColor(i, JJPixelOrigin.WINDOW));
				spriteButton[i].setBackground(JJGraphicSettings.getColor(i, JJPixelOrigin.SPRITE));
				
				final int index = i;
				tileButton[i].addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						Color selectedColor = JColorChooser.showDialog(instance, "Select Color", tileButton[index].getBackground());
						if (selectedColor != null) {
							tileButton[index].setBackground(selectedColor);
						}
					}
				});
				winButton[i].addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						Color selectedColor = JColorChooser.showDialog(instance, "Select Color", winButton[index].getBackground());
						if (selectedColor != null) {
							winButton[index].setBackground(selectedColor);
						}
					}
				});
				spriteButton[i].addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						Color selectedColor = JColorChooser.showDialog(instance, "Select Color", spriteButton[index].getBackground());
						if (selectedColor != null) {
							spriteButton[index].setBackground(selectedColor);
						}
					}
				});
			}
			
			add(new JLabel("Tile"));
			for (int i = 0; i < MAX_COLOR; i++) {
				add(tileButton[i]);
			}
			
			add(new JLabel("Window"));
			for (int i = 0; i < MAX_COLOR; i++) {
				add(winButton[i]);
			}
			
			add(new JLabel("Sprite"));
			for (int i = 0; i < MAX_COLOR; i++) {
				add(spriteButton[i]);
			}
		}
	}

}
