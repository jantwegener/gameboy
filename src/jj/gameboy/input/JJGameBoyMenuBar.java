package jj.gameboy.input;

import java.awt.Color;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuBar;

import jj.gameboy.JJGameBoy;

public class JJGameBoyMenuBar extends JMenuBar {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public JJGameBoyMenuBar(JJGameBoy gameboy) {
		init(gameboy);
	}
	
	private void init(JJGameBoy gameboy) {
		JMenu fileMenu = new JMenu("File");
		fileMenu.setMnemonic('F');
		fileMenu.add(JJBootGameAction.getAction(gameboy));
		fileMenu.add(JJResetGameAction.getAction(gameboy));
		
		JMenu settingsMenu = new JMenu("Settings");
		settingsMenu.setMnemonic('S');
		JMenu settingsColorMenu = new JMenu("Predefined Colors");
		settingsColorMenu.add(new JJSelectColorAction(gameboy, "Original",
				new Color[] { new Color(155, 188, 15), new Color(139, 172, 15), new Color(48, 98, 48), new Color(15, 56, 15) },
				new Color[] { new Color(155, 188, 15), new Color(139, 172, 15), new Color(48, 98, 48), new Color(15, 56, 15) }));
		settingsColorMenu.add(new JJSelectColorAction(gameboy, "Alternative",
				new Color[] { new Color(248, 255, 178), new Color(170, 204, 71), new Color(60, 127, 38), new Color(0, 19, 25) },
				new Color[] { new Color(248, 255, 178), new Color(170, 204, 71), new Color(60, 127, 38), new Color(0, 19, 25) }));
		settingsColorMenu.add(new JJSelectColorAction(gameboy, "Light Green",
				new Color[] { new Color(170, 188, 65), new Color(120, 152, 15), new Color(48, 98, 48), new Color(15, 56, 15) },
				new Color[] { new Color(170, 188, 65), new Color(120, 152, 15), new Color(48, 98, 48), new Color(15, 56, 15) }));
		settingsColorMenu.add(new JJSelectColorAction(gameboy, "Film Negative",
				new Color[] { new Color(15, 56, 15), new Color(48, 98, 48), new Color(139, 172, 15), new Color(155, 188, 15) },
				new Color[] { new Color(15, 56, 15), new Color(48, 98, 48), new Color(139, 172, 15), new Color(155, 188, 15) }));
		settingsColorMenu.add(new JJSelectColorAction(gameboy, "Four Shades of Gray",
				new Color[] { Color.LIGHT_GRAY, Color.GRAY, Color.DARK_GRAY, Color.BLACK },
				new Color[] { Color.LIGHT_GRAY, Color.GRAY, Color.DARK_GRAY, Color.BLACK }));
		settingsColorMenu.add(new JJSelectColorAction(gameboy, "Reddish",
				new Color[] { new Color(200, 150, 150), new Color(170, 120, 120), new Color(140, 90, 90), new Color(80, 20, 20) },
				new Color[] { new Color(200, 150, 150), new Color(170, 120, 120), new Color(140, 90, 90), new Color(80, 20, 20) }));
		settingsColorMenu.add(new JJSelectColorAction(gameboy, "Greennish",
				new Color[] { new Color(150, 200, 150), new Color(120, 170, 120), new Color(90, 140, 90), new Color(20, 80, 20) },
				new Color[] { new Color(150, 200, 150), new Color(120, 170, 120), new Color(90, 140, 90), new Color(20, 80, 20) }));
		settingsColorMenu.add(new JJSelectColorAction(gameboy, "Blueish",
				new Color[] { new Color(150, 150, 200), new Color(120, 120, 170), new Color(90, 90, 140), new Color(20, 20, 80) },
				new Color[] { new Color(150, 150, 200), new Color(120, 120, 170), new Color(90, 90, 140), new Color(20, 20, 80) }));
		settingsColorMenu.add(new JJSelectColorAction(gameboy, "Mario",
				new Color[] { new Color(170, 188, 65), new Color(120, 152, 15), new Color(48, 98, 48), new Color(15, 56, 15) },
				new Color[] { new Color(155, 188, 15), new Color(180, 125, 73), new Color(10, 10, 200), new Color(200, 10, 10) }));
		//		settingsMenu.add(new JJSelectColorAction(gameboy, "Reddish", new Color[] { new Color(200, 150, 150), new Color(), new Color(), new Color()  }));
		
		
		settingsMenu.add(settingsColorMenu);
		
		settingsMenu.add(new JJColorChoserAction());
		settingsMenu.add(new JCheckBoxMenuItem(JJMusicAction.getAction(gameboy)));
		settingsMenu.add(new JCheckBoxMenuItem(new JJSoundChannelAction(gameboy, 0)));
		settingsMenu.add(new JCheckBoxMenuItem(new JJSoundChannelAction(gameboy, 1)));
		settingsMenu.add(new JCheckBoxMenuItem(new JJSoundChannelAction(gameboy, 2)));
		settingsMenu.add(new JCheckBoxMenuItem(new JJSoundChannelAction(gameboy, 3)));
		
		add(fileMenu);
		add(settingsMenu);
	}
	
}
