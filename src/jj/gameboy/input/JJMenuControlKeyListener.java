package jj.gameboy.input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import jj.gameboy.JJGameBoy;

public class JJMenuControlKeyListener implements KeyListener {
	
	private JJGameBoy gameboy;
	
	public JJMenuControlKeyListener(JJGameBoy gameboy) {
		this.gameboy = gameboy;
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_O && e.isControlDown()) {
			JJBootGameAction.getAction(gameboy).actionPerformed(null);
		} else if (e.getKeyCode() == KeyEvent.VK_P || (gameboy.isPaused() && e.getKeyCode() == KeyEvent.VK_ENTER)) {
			gameboy.setPause(!gameboy.isPaused());
		} else if (e.getKeyCode() == KeyEvent.VK_F) {
			gameboy.toggleFullScreen();
		}
	}
	
	
}
