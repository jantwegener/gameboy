package jj.gameboy;

import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingWorker;

import jj.gameboy.cartridge.JJCartridge;
import jj.gameboy.input.JJDebugInput;
import jj.gameboy.input.JJGameBoyMenuBar;
import jj.gameboy.input.JJMenuControlKeyListener;
import jj.gameboy.input.JJSaveStateAction;
import jj.gameboy.io.rom.JJBootRom;
import jj.gameboy.unit.JJMainBus;
import jj.gameboy.util.JJInstructionTracerHelper;

/**
 * This class represents the GameBoy.
 * 
 * There is a lot of useful information in the www that has been put into the GameBoy:
 * <ul>
 * <li>https://www.youtube.com/watch?v=B7seNuQncvU</li>
 * <li>https://www.youtube.com/watch?v=HyzD8pNlpwI</li>
 * <li>https://project-awesome.org/gbdev/awesome-gbdev</li>
 * <li>https://github.com/Gekkio/mooneye-gb</li>
 * <li>https://gekkio.fi/files/gb-docs/gbctr.pdf</li>
 * <li>https://retrocomputing.stackexchange.com/questions/11732/how-does-the-gameboys-memory-bank-switching-work</li>
 * <li>http://marc.rawer.de/Gameboy/Docs/GBCPUman.pdf</li>
 * <li>https://blog.ryanlevick.com/DMG-01/public/book/cpu/reading_and_writing_memory.html</li>
 * <li>https://blog.rekawek.eu/2017/02/09/coffee-gb/</li>
 * <li>https://www.pastraiser.com/cpu/gameboy/gameboy_opcodes.html</li>
 * <li>https://rednex.github.io/rgbds/gbz80.7.html</li>
 * <li>https://github.com/taisel/GameBoy-Online/blob/master/js/GameBoyCore.js</li>
 * <li>https://gbdev.io/pandocs/</li>
 * <li>https://www.chibiakumas.com/z80/Gameboy.php</li>
 * <li>https://bgb.bircd.org/pandocs.htm#lcdpositionandscrolling</li>
 * <li>http://imrannazar.com/GameBoy-Emulation-in-JavaScript:-Sprites/li>
 * <li>https://realboyemulator.wordpress.com/2013/01/18/emulating-the-core-2/</li>
 * <li>https://gbdev.gg8.se/wiki/articles/Gameboy_Bootstrap_ROM</li>
 * <li>https://gbdev.gg8.se/wiki/articles/Timer_and_Divider_Registers</li>
 * <li>https://gbdev.io/pandocs/</li>
 * <li>https://mgba.io/2017/05/29/holy-grail-bugs/</li>
 * <li>http://www.codeslinger.co.uk/pages/projects/gameboy/beginning.html</li>
 * <li>https://github.com/DerekBoucher/FuuGBemu/tree/master/TestROMS/Gekkio</li>
 * <li>https://thomas.spurden.name/gameboy/</li>
 * </ul>
 * 
 * Specifically for sound development
 * <ul>
 * <li>https://gbdev.gg8.se/wiki/articles/Gameboy_sound_hardware</li>
 * <li>https://gbdev.gg8.se/wiki/articles/Sound_Controller#FF12_-_NR12_-_Channel_1_Volume_Envelope_.28R.2FW.29</li>
 * <li>https://docs.oracle.com/javase/7/docs/api/javax/sound/midi/MidiChannel.html</li>
 * <li>https://www.reddit.com/r/EmuDev/comments/5gkwi5/gb_apu_sound_emulation/</li>
 * <li>http://forums.nesdev.com/viewtopic.php?f=3&t=13749&sid=2abf411adb911a9fca66f69f95dd6549&start=15</li>
 * </ul>
 * 
 * All this information has flown into this emulator in one way or another. Nothing would have been possible without the work
 * of the mentioned websites!
 * It is even possible that I have not mentioned all of them. So if you have the feeling that I have used information from
 * your source, and I have not mentioned it here, please let me know, so that I can add you to the list!
 * 
 * @author jan-thierry.wegener
 *
 */
public class JJGameBoy {
	
	public static final int VERSION = 14;
	
	private JFrame frame;
	
	private JJMainBus bus = new JJMainBus();
	
	/**
	 * The path to the boot ROM file.
	 */
	private String bootRomFilePath;
	
	/**
	 * The currently loaded rom.
	 */
	private String romFile;
	
	private boolean isTurnedOn = false;
	
	private boolean isMusicOn = false;
	
	/**
	 * A marker when to dump the current state.
	 */
	private boolean isDumpState = false;
	
	/**
	 * A marker when to reload a saved state.
	 */
	private boolean isReloadState = false;
	
	/**
	 * Whether or not to show the Nintendo logo scrolling down.
	 */
	private boolean isVisualizeLogo = false;
	
	/**
	 * The selected number to dump or reload the state.
	 */
	private int dumpedState = -1;
	
	private boolean isShutdownUserRequest = false;
	
	/**
	 * True if the GameBoy is paused, false otherwise.
	 */
	private boolean isPaused;
	
	/**
	 * To see whether we are already in full-screen mode or if we want to return from it.
	 */
	private boolean isFullscreen;
	
	/**
	 * True when the GameBoy shall pause when the focus is lost.
	 */
	private boolean pauseWhenFocusLost = true;
	
	public JJGameBoy(String bootRomFilePath) {
		this.bootRomFilePath = bootRomFilePath;
	}
	
	public JJMainBus getBus() {
		return bus;
	}
	
	/**
	 * Power Up Sequence
	 * -----------------
	 * 
	 * When the GameBoy is powered up, a 256 byte program
	 * starting at memory location 0 is executed. This program
	 * is located in a ROM inside the GameBoy. The first thing
	 * the program does is read the cartridge locations from
	 * $104 to $133 and place this graphic of a Nintendo logo
	 * on the screen at the top. This image is then scrolled
	 * until it is in the middle of the screen. Two musical
	 * notes are then played on the internal speaker. Again,
	 * the cartridge locations $104 to $133 are read but this
	 * time they are compared with a table in the internal rom.
	 * If any byte fails to compare, then the GameBoy stops
	 * comparing bytes and simply halts all operations.
	 * 
	 * GB & GB Pocket:
	 *      Next, the GameBoy starts adding all of the bytes
	 *      in the cartridge from $134 to $14d. A value of 25
	 *      decimal is added to this total. If the least
	 *      significant byte of the result is a not a zero,
	 *      then the GameBoy will stop doing anything.
	 * 
	 * Super GB:
	 *      Even though the GB & GBP check the memory locations
	 *      from $134 to $14d, the SGB doesn't.
	 * 
	 * If the above checks pass then the internal ROM is
	 * disabled and cartridge program execution begins at
	 * location $100 with the following register values:
	 * 
	 *   A=$01-GB/SGB, $FF-GBP, $11-GBC
	 *   F =$B0
	 *   B =$00-GB/SGB/GBP/GBC, $01-GBA
	 *   C =$13
	 *   DE=$00D8
	 *   HL=$014D
	 *   Stack Pointer=$FFFE
	 *   [$FF05] = $00   ; TIMA
	 *   [$FF06] = $00   ; TMA
	 *   [$FF07] = $00   ; TAC
	 *   [$FF10] = $80   ; NR10
	 *   [$FF11] = $BF   ; NR11
	 *   [$FF12] = $F3   ; NR12
	 *   [$FF14] = $BF   ; NR14
	 *   [$FF16] = $3F   ; NR21
	 *   [$FF17] = $00   ; NR22
	 *   [$FF19] = $BF   ; NR24
	 *   [$FF1A] = $7F   ; NR30
	 *   [$FF1B] = $FF   ; NR31
	 *   [$FF1C] = $9F   ; NR32
	 *   [$FF1E] = $BF   ; NR33
	 *   [$FF20] = $FF   ; NR41
	 *   [$FF21] = $00   ; NR42
	 *   [$FF22] = $00   ; NR43
	 *   [$FF23] = $BF   ; NR30
	 *   [$FF24] = $77   ; NR50
	 *   [$FF25] = $F3   ; NR51
	 *   [$FF26] = $F1-GB, $F0-SGB ; NR52
	 *   [$FF40] = $91   ; LCDC
	 *   [$FF42] = $00   ; SCY
	 *   [$FF43] = $00   ; SCX
	 *   [$FF45] = $00   ; LYC
	 *   [$FF47] = $FC   ; BGP
	 *   [$FF48] = $FF   ; OBP0
	 *   [$FF49] = $FF   ; OBP1
	 *   [$FF4A] = $00   ; WY
	 *   [$FF4B] = $00   ; WX
	 *   [$FFFF] = $00   ; IE
	 * 
	 * It is not a good idea to assume the above values
	 * will always exist. A later version GameBoy could
	 * contain different values than these at reset.
	 * Always set these registers on reset rather than
	 * assume they are as above.
	 * 
	 * Please note that GameBoy internal RAM on power up
	 * contains random data. All of the GameBoy emulators
	 * tend to set all RAM to value $00 on entry.
	 * 
	 * Cart RAM the first time it is accessed on a real
	 * GameBoy contains random data. It will only contain
	 * known data if the GameBoy code initializes it to
	 * some value.
	 */
	public void startUp(String romFile) throws IOException {
		// load the cartridge
		JJCartridge cartridge = new JJCartridge(romFile);
		System.out.println("CGB Flag: " + cartridge.isColorGameBoyOnly());
		
		// init the boot rom
		JJBootRom bootRom = new JJBootRom(bootRomFilePath);
		bus.connectCartridge(cartridge);
		
		bus.powerOn(bootRom);
		
		String title = String.format("%s (%s / %s (0x%02x))",
				cartridge.getTitle(), cartridge.getManufacturerCode(), cartridge.getCartridgeTypeDescription(), cartridge.getCartridgeType());
		frame.setTitle(title);
		
		turnOn();

		bus.getCPU().setRegAF(0);
		bus.getCPU().setRegBC(0);
		bus.getCPU().setRegDE(0);
		bus.getCPU().setRegHL(0);
		bus.getCPU().setPC(0);
		bus.getCPU().setSP(0xFFFE);
		
		// booting
		long lastFrame = 0;
		
		int cpuCycle = 0;
		int ppuCycle = 0;
		int apuCycle = 0;
		while (bus.isBooting()) {
			
			// 69905 clock cycles in one frame
			int cpu = bus.getCPU().tick(0);
			cpuCycle += cpu;
			
			// 17556 cycles per one frame
			ppuCycle += bus.getPPU().tick(cpu);
			
			// 
			apuCycle += bus.getAPU().tick(cpu);
		}
	}
	
	/**
	 * Starts the game loop.
	 */
	public void startGame() {
		System.out.println("Starting game");
		
		// the time of the last frame
		long lastFrame = 0;
		
		// we clear the assembler code to be sure to only have the cartridge code
		bus.getCPU().resetASM();
		JJInstructionTracerHelper.getInstance().resetTrace();
		
		bus.getAPU().flushAmplifier();
		
		int cpuCycle = 0;
		int ppuCycle = 0;
		int apuCycle = 0;
		while (isTurnedOn()) {
			if (!isPaused()) {
				int cpu = 0;
				cpu += bus.getCPU().tick(0)/4;
				cpuCycle += cpu;
				
				ppuCycle += bus.getPPU().tick(cpu);
					
				// TODO
				if (isMusicOn) {
					apuCycle += bus.getAPU().tick(cpu);
				}
			} else {
				// we are in pause state
				// so let's not burn the CPU and sleep for a while
				try {
					bus.getPPU().repaint();
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			if (isDumpState) {
				doDumpState();
			} else if (isReloadState) {
				doReloadState();
			}
		}
		bus.reset();
		System.out.println("Good Bye");
		
		shutdown();
	}
	
	public boolean isPaused() {
		return isPaused;
	}
	
	public void setPause(boolean p) {
		this.isPaused = p;
	}
	
	private void shutdown() {
		if (isShutdownUserRequest) {
			frame.setVisible(false);
			frame.dispose();
			System.exit(0);
		}
	}
	
	private void doReloadState() {
		// dumpedState needed to create the stream
		String zipFile = romFile + ".sav" + dumpedState;
		try (ZipInputStream in = new ZipInputStream(new FileInputStream(zipFile))) {
			bus.reloadState(in);
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		
		isReloadState = false;
	}

	private void doDumpState() {
		// dumpedState needed to create the stream
		String zipFile = romFile + ".sav" + dumpedState;
		try (ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFile))) {
			bus.dumpState(out);
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		
		isDumpState = false;
	}
	
	public void dumpState(int state) {
		isDumpState = true;
		dumpedState = state;
	}
	
	public void reloadState(int state) {
		isReloadState = true;
		dumpedState = state;
	}

	private void turnOn() {
		isTurnedOn = true;
	}
	
	public boolean isTurnedOn() {
		return isTurnedOn;
	}
	
	public void turnOff() {
		isTurnedOn = false;
	}
	
	/**
	 * Resets the gameboy and restarts the currently loaded game.
	 */
	public void reset() {
		try {
			bootUp(romFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void bootUp(String romFile) throws IOException {
		this.romFile = romFile;
		
		// turn off the gameboy before changing the cartridge
		turnOff();
		
		SwingWorker<Integer, Integer> worker = new SwingWorker<Integer, Integer>() {

			@Override
			protected Integer doInBackground() throws Exception {
				setPause(false);
				
				// boot the game boy
				startUp(romFile);
				// booting finished, internal rom is disabled, game rom is in place
				// let's start the game loop
				startGame();
				
				return null;
			}
			
		};
		worker.execute();
	}

	public void init() {
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.addWindowListener(new WindowAdapter() {
			
			@Override
			public void windowClosing(WindowEvent e) {
				isShutdownUserRequest = true;
				if (!isTurnedOn()) {
					shutdown();
				}
				turnOff();
			}
			
			@Override
			public void windowLostFocus(WindowEvent e) {
				if (pauseWhenFocusLost) {
					setPause(true);
				}
			}
			
			@Override
		    public void windowDeactivated(WindowEvent e) {
				if (pauseWhenFocusLost) {
					setPause(true);
				}
			}
			
		});
		
		JPanel mainPanel = new JPanel(new BorderLayout());
		JPanel screenPanel = bus.getPPU().getScreen();
		
		mainPanel.add(screenPanel, BorderLayout.CENTER);
		
		frame.add(mainPanel);
		frame.addKeyListener(bus.getController());
		frame.addKeyListener(new JJDebugInput(bus));
		frame.addKeyListener(new JJMenuControlKeyListener(this));
		frame.addKeyListener(JJSaveStateAction.getAction(this));
		
		frame.setJMenuBar(new JJGameBoyMenuBar(this));

		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		frame.validate();

		frame.setVisible(true);
	}
	
	public void toggleMusic() {
		isMusicOn = !isMusicOn;
		if (!isMusicOn) {
			bus.getAPU().flushAmplifier();
		}
	}
	
	public void toggleChannel(int channel) {
		bus.getAPU().toggleSoundChannel(channel);
	}
	
	public void toggleFullScreen() {
		if (!isFullscreen) {
			frame.setVisible(false);
			frame.setUndecorated(true);
			frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
			frame.setVisible(true);
		} else {
			frame.setVisible(false);
			frame.setUndecorated(false);
			frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
			frame.setVisible(true);
		}
	}
	
	public boolean isPauseWhenFocusLost() {
		return pauseWhenFocusLost;
	}
	
	public void setPauseWhenFocusLost(boolean pauseWhenFocusLost) {
		this.pauseWhenFocusLost = pauseWhenFocusLost;
	}
	
}
