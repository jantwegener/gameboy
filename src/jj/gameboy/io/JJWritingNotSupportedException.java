package jj.gameboy.io;

import jj.gameboy.JJGameBoyException;
import jj.gameboy.unit.JJCPU;

public class JJWritingNotSupportedException extends JJGameBoyException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public JJWritingNotSupportedException(String exc) {
		super(exc);
	}
	
	public JJWritingNotSupportedException(String exc, JJCPU cpu) {
		super(String.format("%s (%s)", exc, cpu));
	}
	
}
