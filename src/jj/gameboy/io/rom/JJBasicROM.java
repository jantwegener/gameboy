package jj.gameboy.io.rom;

import java.io.IOException;

import jj.gameboy.io.JJWritingNotSupportedException;
import jj.gameboy.io.ram.JJAbstractRAM;

public class JJBasicROM extends JJAbstractRAM {
	
	public JJBasicROM(String file) throws IOException {
		super(file);
	}
	
	public JJBasicROM(int sizeInB, int[] initArray) {
		super(sizeInB, initArray);
	}
	
	/**
	 * A ROM cannot be written to.
	 * 
	 * @throws Always throws an {@link UnsupportedOperationException}
	 */
	@Override
	public void write(int address, int value) {
		throw new JJWritingNotSupportedException(String.format("Write access not allowed: 0x%04x - %d", address, value));
	}
	
}
