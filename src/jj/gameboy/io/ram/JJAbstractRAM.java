package jj.gameboy.io.ram;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import jj.gameboy.unit.JJIUnit;
import jj.gameboy.unit.JJSaveStateUnit;

public class JJAbstractRAM implements JJIUnit, JJSaveStateUnit {

	public static final int KB = 1024;
	public static final int MB = 1024*1024;
	
	private int[] ram;
	
	public JJAbstractRAM(int sizeInB, int[] initArray) {
		init(sizeInB, initArray);
	}
	
	public JJAbstractRAM(int sizeInB) {
		this.ram = new int[sizeInB];
	}
	
	public JJAbstractRAM(String file) throws IOException {
		readFile(file);
	}
	
	/**
	 * Resets the RAM by filling it with 0.
	 */
	public void reset() {
		Arrays.fill(ram, 0);
	}
	
	/**
	 * Reads the content of the file and stores it into the RAM.
	 * 
	 * @param filePath The path to the file.
	 * 
	 * @throws IOException If an {@link IOException} occurs.
	 */
	public void readFile(String filePath) throws IOException {
		File file = new File(filePath);
		long fileLength = file.length();
		
		if (0 < fileLength && fileLength <= Integer.MAX_VALUE) {
			if (file.getName().endsWith(".zip")) {
				try (ZipFile zipFile = new ZipFile(file)) {
					Enumeration<? extends ZipEntry> enumer = zipFile.entries();
					boolean found = false;
					while (!found && enumer.hasMoreElements()) {
						ZipEntry entry = enumer.nextElement();
						if (entry.getName().endsWith(".gb")) {
							found = true;
							try (BufferedInputStream input = new BufferedInputStream(zipFile.getInputStream(entry))) {
								read(input, entry.getSize());
							}
						}
					}
				}
			} else {
				// we simply assume that this file is not compressed
				try (BufferedInputStream input = new BufferedInputStream(new FileInputStream(file))) {
					read(input, fileLength);
				}
			}
		}
	}
	
	private void read(BufferedInputStream input, long fileLength) throws IOException {
		int[] romData = new int[(int) fileLength];
		
		int byteRead;
		int i = 0;
		while ((byteRead = input.read()) != -1) {
			romData[i++] = byteRead;
		}
		
		init(romData.length, romData);
	}
	
	protected void init(int sizeInB, int[] initArray) {
		this.ram = new int[sizeInB];
		
		System.arraycopy(initArray, 0, ram, 0, initArray.length);
	}
	
	@Override
	public int read(int address) {
		return ram[address];
	}

	@Override
	public void write(int address, int value) {
		ram[address] = value;
	}
	
	/**
	 * Returns the size of the RAM.
	 * 
	 * @return The size of the RAM.
	 */
	public int size() {
		return ram.length;
	}

	/**
	 * Reads the data from start (inclusive) to end (exclusive).
	 * 
	 * @param start The start address.
	 * @param end The end address.
	 * 
	 * @return An array containing the data from start to end.
	 */
	public int[] readWindow(int start, int end) {
		int[] data = new int[end - start];
		for (int i = 0; i < end - start; i++) {
			data[i] = read(start + i);
		}
		return data;
	}

	/**
	 * Reads the data from start (inclusive) to end (exclusive).
	 * 
	 * @param start The start address.
	 * @param end The end address.
	 * 
	 * @return An array containing the data from start to end.
	 */
	public byte[] readWindowAsByte(int start, int end) {
		byte[] data = new byte[end - start];
		for (int i = 0; i < end - start; i++) {
			data[i] = (byte) read(start + i);
		}
		return data;
	}
	
	/**
	 * Dumps the RAM to the output stream.
	 * 
	 * @param out The output stream to that the RAM is dumped to.
	 * 
	 * @throws IOException If an {@link IOException} occurs.
	 */
	public void dumpState(OutputStream out) throws IOException {
		for (int i = 0; i < ram.length; i++) {
			out.write(ram[i]);
		}
	}
	
	/**
	 * Reloads the RAM from the input stream.
	 * 
	 * @param in The stream to load the RAM from.
	 * 
	 * @throws IOException If an {@link IOException} occurs.
	 */
	public void reloadState(InputStream in) throws IOException {
		for (int i = 0; i < ram.length; i++) {
			ram[i] = in.read();
		}
	}
	
}
