package jj.gameboy.io.ram;

/**
 * The work random access memory (WRAM).
 * 
 * @author jan-thierry.wegener
 *
 */
public class JJWRAM extends JJAbstractRAM {

	public JJWRAM(int sizeInB) {
		super(sizeInB);
	}

}
