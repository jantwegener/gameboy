package jj.gameboy.io.ram;

/**
 * The video random access memory (VRAM).
 * 
 * @author jan-thierry.wegener
 *
 */
public class JJVRAM extends JJAbstractRAM {
	
	public static int VRAM_SIZE = 8 * KB;
	
	public JJVRAM() {
		super(VRAM_SIZE);
	}
	
}
