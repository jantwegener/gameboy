package jj.gameboy.io;

import java.io.IOException;

import jj.gameboy.JJGameBoyException;
import jj.gameboy.io.ram.JJAbstractRAM;
import jj.gameboy.unit.JJMainBus;

/**
 * The I/O port for the game boy. The address is from 0xFF00 to 0xFF7F (=127 byte).
 * 
 * Documentation can be found at
 * <ul>
 * <li>https://www.chibiakumas.com/z80/Gameboy.php</li>
 * <li>https://gekkio.fi/files/gb-docs/gbctr.pdf</li>
 * </ul>
 * 
 * @author jan-thierry.wegener
 */
public class JJIOPort extends JJAbstractRAM {
	
	private JJMainBus bus;
	
	public JJIOPort(JJMainBus bus, int[] initArray) {
		super(128, initArray);
		this.bus = bus;
		
		prepareDefaultValues();
	}
	
	public JJIOPort(JJMainBus bus) {
		super(128);
		this.bus = bus;
		
		prepareDefaultValues();
	}
	
	public JJIOPort(JJMainBus bus, String file) throws IOException {
		super(file);
		this.bus = bus;
		
		prepareDefaultValues();
	}
	
	/**
	 * This method initializes the default values when no real logic is implemented behind the scenes.
	 */
	private void prepareDefaultValues() {
		super.write(0x02, 0x7E);
	}
	
	/**
	 * This allows the component to write back to the cpu. The cpu usually reads from the RAM to get the result.
	 */
	public void writeFromComponent(int address, int value) {
		if (address > 0xFF) {
			address = address & 0xFF;
		}
		
		switch (address) {
		
//			case 0x05:
//				// TIMA - Timer counter (R/W)
//				super.write(address, value);
//				break;
				
			case 0x0F:
				// IF - Interrupt Flag (R/W)
				// inform cpu that interrupt has occurred
				value = bus.getCPU().getInterruptService().setInterrupt(value);
				break;
				
			case 0x41:
				// STAT - LCDC Status (R/W)
				// only write the 3 lower bits
				bus.getPPU().setStat(value);
				break;
				
			default:
				System.out.printf("Unknown i/o port (wfc): 0x%x - %d%n", address, value);
				throw new JJGameBoyException(String.format("Unknown i/o port (w): 0x%x - %d", address, value));
				
		}
	}
	
	/**
	 * This serves the cpu to write to the component.
	 */
	public void write(int address, int value) {
		switch (address) {
		
			case 0x00:
				// P1/JOYP - Joypad (R/W)
				bus.getController().selectRow(value);
				break;
		
			case 0x01:
				// SB - Serial transfer data (R/W)
				// TODO connect?
				super.write(address, value);
				break;
				
			case 0x02:
				// SC - Serial Transfer Control (R/W)
				// TODO connect?
				super.write(address, value);
				break;
				
			case 0x04:
				// DIV - Divider Register (R/W)
				bus.getCPU().getTimer().resetDIV();
				break;
			
			case 0x05:
				// TIMA - Timer counter (R/W)
				bus.getCPU().getTimer().setTIMA(value);
				break;
				
			case 0x06:
				// TMA - Timer Modulo (R/W)
				bus.getCPU().getTimer().setTMA(value);
				break;
			
			case 0x07:
				// TAC - Timer Control (R/W)
				bus.getCPU().getTimer().setTimerSpeed(value);
				break;
			
			case 0x0F:
				// IF - Interrupt Flag (R/W)
				// acknowledge that interrupt has been handled
				bus.getCPU().getInterruptService().writeInterrupt(value);
				break;
				
			case 0x10:
				// NR10 - Channel 1 (Tone & Sweep) Sweep register (R/W)
				bus.getAPU().write(address, value);
				break;
				
			case 0x11:
				// NR11 - Channel 1 (Tone & Sweep) Sound length/Wave pattern duty (R/W)
				bus.getAPU().write(address, value);
				break;
				
			case 0x12:
				// NR12 - Channel 1 (Tone & Sweep) Volume Envelope (R/W)
				bus.getAPU().write(address, value);
				break;
				
			case 0x13:
				// NR13 - Channel 1 (Tone & Sweep) Frequency lo (Write Only)
				bus.getAPU().write(address, value);
				break;
				
			case 0x14:
				// NR14 - Channel 1 (Tone & Sweep) Frequency hi (R/W)
				bus.getAPU().write(address, value);
				break;
				
			case 0x16:
				// NR21 Channel 2 (Tone) Sound Length/Wave Pattern Duty (R/W)
				bus.getAPU().write(address, value);
				break;
				
			case 0x17:
				// NR22 - Channel 2 (Tone) Volume Envelope (R/W)
				bus.getAPU().write(address, value);
				break;
				
			case 0x18:
				// NR23 - Channel 2 (Tone) Frequency lo data (W)
				bus.getAPU().write(address, value);
				break;
				
			case 0x19:
				// NR24 - Channel 2 (Tone) Frequency hi data (R/W)
				bus.getAPU().write(address, value);
				break;
				
			case 0x1A:
				// NR30 - Channel 3 (Wave Output) Sound on/off (R/W)
				bus.getAPU().write(address, value);
				break;
				
			case 0x1B:
				// NR31 - Channel 3 Sound Length
				bus.getAPU().write(address, value);
				break;
				
			case 0x1C:
				// NR32 - Channel 3 (Wave Output) Select output level (R/W)
				bus.getAPU().write(address, value);
				break;
				
			case 0x1D:
				// NR33 - Channel 3 (Wave Output) Frequency's lower data (W)
				bus.getAPU().write(address, value);
				break;
				
			case 0x1E:
				// NR34 - Channel 3 (Wave Output) Frequency's higher data (R/W)
				bus.getAPU().write(address, value);
				break;
				
			case 0x20:
				// NR41 - Channel 4 (Noise) Sound Length (R/W)
				bus.getAPU().write(address, value);
				break;
				
			case 0x21:
				// NR42 - Channel 4 (Noise) Volume Envelope (R/W)
				bus.getAPU().write(address, value);
				break;
				
			case 0x22:
				// NR43 - Channel 4 (Noise) Polynomial Counter (R/W)
				bus.getAPU().write(address, value);
				break;
				
			case 0x23:
				// NR44 - Channel 4 (Noise) Counter/consecutive; Inital (R/W)
				bus.getAPU().write(address, value);
				break;
				
			case 0x24:
				// NR50 - Channel control / ON-OFF / Volume (R/W)
				bus.getAPU().write(address, value);
				break;
				
			case 0x25:
				// NR51 - Selection of Sound output terminal (R/W)
				bus.getAPU().write(address, value);
				break;
			
			case 0x26:
				// NR52 - Sound on/off
				bus.getAPU().write(address, value);
				break;
				
			case 0x30:
				// Wave Pattern RAM	
				bus.getAPU().write(address, value);
				break;
				
			case 0x31:
				// Wave Pattern RAM	
				bus.getAPU().write(address, value);
				break;
				
			case 0x32:
				// Wave Pattern RAM	
				bus.getAPU().write(address, value);
				break;
				
			case 0x33:
				// Wave Pattern RAM	
				bus.getAPU().write(address, value);
				break;
				
			case 0x34:
				// Wave Pattern RAM	
				bus.getAPU().write(address, value);
				break;
				
			case 0x35:
				// Wave Pattern RAM	
				bus.getAPU().write(address, value);
				break;
				
			case 0x36:
				// Wave Pattern RAM	
				bus.getAPU().write(address, value);
				break;
				
			case 0x37:
				// Wave Pattern RAM	
				bus.getAPU().write(address, value);
				break;
				
			case 0x38:
				// Wave Pattern RAM	
				bus.getAPU().write(address, value);
				break;
				
			case 0x39:
				// Wave Pattern RAM	
				bus.getAPU().write(address, value);
				break;
				
			case 0x3A:
				// Wave Pattern RAM	
				bus.getAPU().write(address, value);
				break;
				
			case 0x3B:
				// Wave Pattern RAM	
				bus.getAPU().write(address, value);
				break;
				
			case 0x3C:
				// Wave Pattern RAM	
				bus.getAPU().write(address, value);
				break;
				
			case 0x3D:
				// Wave Pattern RAM	
				bus.getAPU().write(address, value);
				break;
				
			case 0x3E:
				// Wave Pattern RAM	
				bus.getAPU().write(address, value);
				break;
				
			case 0x3F:
				// Wave Pattern RAM	
				bus.getAPU().write(address, value);
				break;
				
			case 0x40:
				// LCDC - LCD Control (R/W)	
				bus.getPPU().setLCDC(value);
				break;
				
			case 0x41:
				// STAT - LCDC Status (R/W)
				// do not touch the 3 lower bits
				bus.getPPU().setStat(value);
				break;
				
			case 0x42:
				// $00 ; SCY Tile Scroll Y	
				bus.getPPU().setSCY(value);
				break;
				
			case 0x43:
				// $00 ; SCX Tile Scroll X	
				bus.getPPU().setSCX(value);
				break;
				
			case 0x44:
				// LY - LCDC Y-Coordinate (R) - LCD Y
				// Line (0-153 144+ are V-Blank)
				// if CPU writes the ly, then it is set to 0
				bus.getPPU().setLy(0);
				break;
				
			case 0x45:
				// LYC - LY Compare (R/W)
				bus.getPPU().setLyc(value);
				break;
				
			case 0x46:
				// DMA - DMA Transfer and Start Address (W)
				bus.getPPU().dmaTransfer(value);
				break;
				
			case 0x47:
				// BGP - BG Palette Data (R/W) - Non CGB Mode Only
				super.write(address, value);
				bus.getPPU().setBackgroundColorPalette(value);
				break;
			
			case 0x48:
				// OBP0 - Object Palette 0 Data (R/W) - Non CGB Mode Only
				super.write(address, value);
				bus.getPPU().setObjectColorPalette(value, 0);
				break;
				
			case 0x49:
				// OBP1 - Object Palette 1 Data (R/W) - Non CGB Mode Only
				super.write(address, value);
				bus.getPPU().setObjectColorPalette(value, 1);
				break;
				
			case 0x4A:
				// WY - Window Y Position (R/W)
				bus.getPPU().setWy(value);
				break;
				
			case 0x4B:
				// WX- Window X Position minus 7 (R/W)
				bus.getPPU().setWx(value);
				break;
				
			case 0x4D:
				// KEY1 - CGB Mode Only - Prepare Speed Switch
				super.write(address, value);
				break;
				
			case 0x4f:
				// VBK - CGB Mode Only - VRAM Bank
				super.write(address, value);
				break;
				
			case 0x50:
				// turn off DMG rom (BOOT_OFF)
				super.write(address, value);
				bus.disableInternalRom();
				break;
				
			case 0x68:
				// BCPS/BGPI - CGB Mode Only - Background Palette Index
				super.write(address, value);
				bus.getPPU().setBackgroundPaletteIndex(value);
				break;
				
			case 0x69:
				// BCPD/BGPD - CGB Mode Only - Background Palette Data
				super.write(address, value);
				bus.getPPU().setBackgroundColorPaletteData(value);
				break;
				
			case 0x79:
				// undocumented
				super.write(address, value);
				break;
				
			case 0x7a:
				// undocumented
				super.write(address, value);
				break;
				
			case 0x7b:
				// undocumented
				super.write(address, value);
				break;
				
			case 0x7c:
				// undocumented
				super.write(address, value);
				break;
				
			case 0x7d:
				// undocumented
				super.write(address, value);
				break;
				
			case 0x7e:
				// undocumented
				super.write(address, value);
				break;
				
			case 0x7f:
				// undocumented
				super.write(address, value);
				break;
				
			default:
				System.out.printf("Unknown i/o port (w): 0x%x - %d%n", address, value);
				throw new JJGameBoyException(String.format("Unknown i/o port (w): 0x%x - %d", address, value));
		}
	}
	
	public int read(int address) {
		int retval;
		switch (address) {
			case 0x00:
				// P1/JOYP - Joypad (R/W)
				retval = bus.getController().getPressedButtonResult();
				break;
				
			case 0x01:
				// SB - Serial transfer data (R/W)
				// TODO connect?
				retval = super.read(address);
				break;
				
			case 0x02:
				// SC - Serial Transfer Control (R/W)
				// TODO connect?
				retval = super.read(address);
				break;
				
			case 0x03:
				// undocumented
				retval = 0xFF;
				break;
				
			case 0x04:
				// DIV - Divider Register (R/W)
				retval = bus.getCPU().getTimer().getDIV();
				break;
			
			case 0x05:
				// TIMA - Timer counter (R/W)
				retval = bus.getCPU().getTimer().getTIMA();
				break;
				
			case 0x06:
				// TMA - Timer Modulo (R/W)
				retval = bus.getCPU().getTimer().getTMA();
				break;
				
			case 0x07:
				// TAC - Timer Control (R/W)
				retval = bus.getCPU().getTimer().getTAC();
				break;
				
			case 0x0F:
				// IF - Interrupt Flag (R/W)
				retval = bus.getCPU().getInterruptService().getInterruptFlags();
				break;
		
			case 0x10:
				// NR10 - Channel 1 (Tone & Sweep) Sweep register (R/W)
				retval = super.read(address);
				break;
		
			case 0x11:
				// NR11 - Channel 1 (Tone & Sweep) Sound length/Wave pattern duty (R/W)
				retval = super.read(address);
				break;
				
			case 0x12:
				// NR12 - Channel 1 (Tone & Sweep)
				// Volume Envelope (R/W)
				retval = super.read(address);
				break;
				
			case 0x14:
				// NR14 - Channel 1 (Tone & Sweep) Frequency hi (R/W)
				retval = super.read(address);
				break;
				
			case 0x16:
				// NR21 - Channel 2 (Tone) Sound Length/Wave Pattern Duty (R/W)
				retval = super.read(address);
				break;
				
			case 0x17:
				// NR22 - Channel 2 (Tone) Volume Envelope (R/W)
				retval = super.read(address);
				break;
				
			case 0x19:
				// NR24 - Channel 2 (Tone) Frequency hi data (R/W)
				retval = super.read(address);
				break;
				
			case 0x1A:
				// NR30 - Channel 3 (Wave Output) Sound on/off (R/W)
				retval = super.read(address);
				break;
				
			case 0x1C:
				// NR32 - Channel 3 (Wave Output) Select output level (R/W)
				retval = super.read(address);
				break;
				
			case 0x21:
				// NR42 - Channel 4 (Noise) Volume Envelope (R/W)
				retval = super.read(address);
				break;
				
			case 0x22:
				// NR43 - Channel 4 (Noise) Polynomial Counter (R/W)
				retval = super.read(address);
				break;
				
			case 0x23:
				// NR44 - Channel 4 (Noise) Counter/consecutive; Inital (R/W)
				retval = super.read(address);
				break;
				
			case 0x24:
				// NR50 - Channel control / ON-OFF / Volume (R/W)
				retval = super.read(address);
				break;
				
			case 0x25:
				// NR51 - Selection of Sound output terminal (R/W)
				retval = super.read(address);
				break;
			
			case 0x26:
				// NR52 - Sound on/off
				retval = super.read(address);
				break;
				
			case 0x40:
				// LCDC - LCD Control (R/W)	
				retval = bus.getPPU().getLCDC();
				break;
				
			case 0x41:
				// STAT - LCDC Status (R/W)	
				retval = bus.getPPU().getStat();
				break;
				
			case 0x42:
				// $00 ; SCY Tile Scroll Y	
				retval = bus.getPPU().getSCY();
				break;
				
			case 0x43:
				// $00 ; SCX Tile Scroll X	
				retval = bus.getPPU().getSCX();
				break;
				
			case 0x44:
				// LY - LCDC Y-Coordinate (R) - LCD Y Line (0-153 144+ are V-Blank)
				retval = bus.getPPU().getLy();
				break;
				
			case 0x45:
				// LYC - LY Compare (R/W)
				retval = bus.getPPU().getLyc();
				break;
				
			case 0x47:
				// BGP - BG Palette Data (R/W) - Non CGB Mode Only
				retval = super.read(address);
				break;
				
			case 0x48:
				// OBP0 - Object Palette 0 Data (R/W) - Non CGB Mode Only
				retval = super.read(address);
				break;
				
			case 0x49:
				// OBP1 - Object Palette 1 Data (R/W) - Non CGB Mode Only
				retval = super.read(address);
				break;
				
			case 0x4A:
				// WY - Window Y Position (R/W)
				retval = bus.getPPU().getWy();
				break;
				
			case 0x4B:
				// WX- Window X Position minus 7 (R/W)
				retval = bus.getPPU().getWx();
				break;
				
			case 0x4D:
				// KEY1 - CGB Mode Only - Prepare Speed Switch
				retval = super.read(address);
				break;
			
			case 0x50:
				// turn off DMG rom (BOOT_OFF)
				bus.disableInternalRom();
				retval = 1;
				break;
				
			case 0x68:
				// BCPS/BGPI - CGB Mode Only - Background Palette Index
				retval = super.read(address);
				break;
				
			case 0x69:
				// BCPD/BGPD - CGB Mode Only - Background Palette Data
				retval = super.read(address);
				break;
				
			case 0x7c:
				// undocumented
				retval = super.read(address);
				break;
				
			case 0x7d:
				// undocumented
				retval = super.read(address);
				break;
				
			case 0x7e:
				// undocumented
				retval = super.read(address);
				break;
				
			case 0x7f:
				// undocumented
				retval = super.read(address);
				break;
								
			default:
				throw new JJGameBoyException(String.format("Unknown i/o port (r): 0xFF%02x", address));
//				retval = super.read(address);
		}
		return retval;
	}
	
}
