package jj.gameboy.io;

import jj.gameboy.JJGameBoyException;
import jj.gameboy.unit.JJCPU;

public class JJUnsupportedOpcodeException extends JJGameBoyException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public JJUnsupportedOpcodeException(String exc) {
		super(exc);
	}

	public JJUnsupportedOpcodeException(String exc, int opcode, JJCPU cpu) {
		super(String.format("%s: %x (%s)", exc, opcode, cpu));
	}

	public JJUnsupportedOpcodeException(int opcode, JJCPU cpu) {
		super(String.format("Unsupported opcode: %x (%s)", opcode, cpu));
	}
	
}
