package jj.gameboy.output.graphic;

import jj.gameboy.unit.JJMainBus;

public class Tile extends JJGraphicObject {
	
	/**
	 * Only to initialize the PPU.
	 * 
	 * @param bus
	 */
	public Tile(JJMainBus bus) {
		super(bus);
	}
	
	public Tile(JJMainBus bus, int tileNum) {
		this(bus, tileNum, bus.getPPU().getBgTileDataAddress());
	}
	
	public Tile(JJMainBus bus, int tileNum, int startAddress) {
		super(bus);
		
		setStartAddress(startAddress);
		
		setPatternNumber(tileNum);
		setHeight(bus.getPPU().getTileHeight());
		
		read(bus);
	}
	
	@Override
	public int getColor(int pixel) {
		return getBus().getPPU().getBackgroundColor(pixel);
	}
	
}
