package jj.gameboy.output.graphic;

import jj.gameboy.unit.JJMainBus;

/**
 * Class representing sprites.
 * 
 * @author Jan-Thierry Wegener
 */
public class Sprite_old extends JJGraphicObject {
	
	private final static int ADDRESS_SPRITE_ATTRIBUTE_TABLE = 0xFE00;
	
	private int x;
	
	private int y;
	
	private int attributes;
	
	private int spriteNumber;
	
	/**
	 * Creates a new sprite reading from the given address.
	 * 
	 * @param bus The main bus.
	 * @param address The address to start reading from.
	 */
	public Sprite_old(JJMainBus bus, int spriteNum) {
		// init the height of the sprite
		super(bus);
		
		this.spriteNumber = spriteNum;
		
		int address = ADDRESS_SPRITE_ATTRIBUTE_TABLE + spriteNum * 4;
		// bytes can be hidden on the left of the screen
		x = bus.read(address + 1) - 0x08;
		// sprites can be hidden on top of the screen
		y = bus.read(address) - 0x10;
		int patternNumber = bus.read(address + 2);
		attributes = bus.read(address + 3);
		
		int height = bus.getPPU().getSpriteHeight();
		
		setHeight(height);
		setPatternNumber(patternNumber);
		
		// the address of the sprite bitmaps
		setStartAddress(0x8000);
		
		read(bus);
	}
	
	public int getSpriteNumber() {
		return spriteNumber;
	}
	
	protected void setPixel(int x, int y, int color) {
		if (isXFlip()) {
			x = 8 - x - 1;
		}
		if (isYFlip()) {
			y = getHeight() - y - 1;
		}
		super.setPixel(x, y, color);
	}
	
	public boolean isInLine(int line) {
		boolean retval = false;
		if (y + getHeight() >= line && line >= y) {
			retval = true;
		}
		return retval;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public int getAttributes() {
		return attributes;
	}
	
	public boolean isInFrontOfBackground() {
		return (attributes & 0x80) == 0;
	}
	
	private boolean isXFlip() {
		return (attributes & 0x20) > 0;
	}
	
	private boolean isYFlip() {
		return (attributes & 0x40) > 0;
	}
	
	private int getPaletteNumber() {
		return (attributes & 0x10) >> 4;
	}
	
	public int getColor(int pxl) {
		return getBus().getPPU().getObjectColor(getPaletteNumber(), pxl);
	}
	
	protected int getPixelLineOffset() {
		return y;
	}
	
	public String toString() {
		StringBuffer b = new StringBuffer();
		b.append("Sprite ").append(spriteNumber).append(", patternNum: ").append(getPatternNumber());
		return b.toString();
	}
	
}
