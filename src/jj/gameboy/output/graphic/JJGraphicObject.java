package jj.gameboy.output.graphic;

import jj.gameboy.unit.JJMainBus;

public abstract class JJGraphicObject {
	
	private static final int WIDTH = 8;
	
	private int patternNumber;
	
	private int height;
	
	private JJMainBus bus;
	
	private int startAddress = 0;
	
	/**
	 * The pixels with y and x coordinates.
	 */
	private int[][] pixels;
	
	protected JJGraphicObject(JJMainBus bus) {
		this.bus = bus;
	}
	
	protected void setStartAddress(int address) {
		startAddress = address;
	}
	
	protected void setHeight(int height) {
		this.height = height;
		
		pixels = new int[height][getWidth()];
	}
	
	protected void setPatternNumber(int patternNumber) {
		this.patternNumber = patternNumber;
	}
	
	public int getPatternNumber() {
		return patternNumber;
	}
	
	protected JJMainBus getBus() {
		return bus;
	}
	
	public int getHeight() {
		return height;
	}
	
	public int getWidth() {
		return WIDTH;
	}
	
	protected void read(JJMainBus bus) {
		// each sprite has 2 * height bytes
		if (height == 16) {
			if ((patternNumber % 2) == 1) {
				patternNumber--;
			}
		}
		int address = startAddress + 2 * 8 * patternNumber;
		for (int h = 0; h < height; h++) {
			int data0 = readData(bus, address++);
			int data1 = readData(bus, address++);
			
			// convert the data into pixels
			// each pixel is stored in two bit of data
			for (int w = 0; w < bus.getPPU().getTileWidth(); w++) {
				// for a description of the data structure, follow
				// https://youtu.be/HyzD8pNlpwI?t=1847
				int pxl = ((data0 & 0x80) >> 7) | ((data1 & 0x80) >> 6);
				data0 = data0 << 1;
				data1 = data1 << 1;
				setPixel(w, h, pxl);
			}
		}
	}
	
	protected int readData(JJMainBus bus, int address) {
		 return bus.read(address++);
	}
	
	protected void setPixel(int x, int y, int color) {
		pixels[y][x] = color;
	}
	
	public int getPixel(int x, int y) {
		return pixels[y][x];
	}
	
	/**
	 * Returns the color according to the palette.
	 * 
	 * @param pixel
	 * 
	 * @return
	 */
	public abstract int getColor(int pixel);
	
	protected int getPixelLineOffset() {
		return 0;
	}
	
	public int[] getPixelLine(int line) {
		int[] retval = {};
		int index = line - getPixelLineOffset() - 0;
		if (0 <= index && index < pixels.length) {
			retval = pixels[index];
		}
		return retval;
	}
	
	public void print() {
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < 8; x++) {
				System.out.print(pixels[y][x] + " ");
			}
			System.out.println();
		}
	}
	
}
