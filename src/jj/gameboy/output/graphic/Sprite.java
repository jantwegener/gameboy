package jj.gameboy.output.graphic;

import jj.gameboy.unit.JJMainBus;

/**
 * Class representing sprites.
 * 
 * @author Jan-Thierry Wegener
 */
public class Sprite {
	
	private final static int ADDRESS_SPRITE_ATTRIBUTE_TABLE = 0xFE00;
	
	private JJMainBus bus;
	
	private int x;
	
	private int y;
	
	private int attributes;
	
	private int spriteNumber;

	private int patternNumber;
	
	private int height;
	
	private int[] spriteTileNumber = new int[2];
	
	/**
	 * Creates a new sprite reading from the given address.
	 * 
	 * @param bus The main bus.
	 * @param address The address to start reading from.
	 */
	public Sprite(JJMainBus bus, int spriteNum) {
		// set the bus
		this.bus = bus;
		
		// init the height of the sprite
		this.spriteNumber = spriteNum;
		
		int address = ADDRESS_SPRITE_ATTRIBUTE_TABLE + spriteNum * 4;
		// bytes can be hidden on the left of the screen
		x = bus.read(address + 1) - 0x08;
		// sprites can be hidden on top of the screen
		y = bus.read(address) - 0x10;
		patternNumber = bus.read(address + 2);
		attributes = bus.read(address + 3);
		
		height = bus.getPPU().getSpriteHeight();
		
		if (height == 8) {
			spriteTileNumber[0] = patternNumber;
		} else {
			if ((patternNumber % 2) == 0) {
				spriteTileNumber[0] = patternNumber;
				spriteTileNumber[1] = patternNumber + 1;
			} else {
				spriteTileNumber[0] = patternNumber - 1;
				spriteTileNumber[1] = patternNumber;
			}
		}
	}
	
	public int getSpriteNumber() {
		return spriteNumber;
	}
	
	public int getHeight() {
		return height;
	}
	
	public boolean isInLine(int line) {
		boolean retval = false;
		if (y + height > line && line >= y) {
			retval = true;
		}
		return retval;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public int getAttributes() {
		return attributes;
	}
	
	public boolean isInFrontOfBackground() {
		return (attributes & 0x80) == 0;
	}
	
	public boolean isXFlip() {
		return (attributes & 0x20) > 0;
	}
	
	public boolean isYFlip() {
		return (attributes & 0x40) > 0;
	}
	
	private int getPaletteNumber() {
		return (attributes & 0x10) >> 4;
	}
	
	public int getColor(int pxl) {
		return bus.getPPU().getObjectColor(getPaletteNumber(), pxl);
	}
	
	public int[] getPixelLine(int line) {
		return getPixelLine(line, true);
	}
	
	public int[] getPixelLine(int line, boolean relativeToScreen) {
		if (relativeToScreen) {
			line = line - y;
		}
		
		int tilePos = 0;
		int[] pixelLine;
		
		if (isYFlip()) {
			// count from bottom
			line = height - line - 1;
		}
		// if it is flipped, then it is 0 again
		if (line >= 8) {
			// bottom of two tile height sprite
			tilePos = 1;
			line = line % 8;
		}
		
		pixelLine = bus.getPPU().getSpriteTile(spriteTileNumber[tilePos]).getPixelLine(line);
		
		return pixelLine;
	}
	
	public int getPixel(int x, int y) {
		return getPixelLine(y, false)[x];
	}
	
	public int getPatternNumber() {
		return patternNumber;
	}
	
	public String toString() {
		StringBuffer b = new StringBuffer();
		b.append("Sprite ").append(spriteNumber).append(", patternNum: ").append(patternNumber);
		return b.toString();
	}
	
}
