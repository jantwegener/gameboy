package jj.gameboy.output;

import jj.gameboy.JJGameBoyException;

public class JJUnknownColorException extends JJGameBoyException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public JJUnknownColorException(String msg) {
		super(msg);
	}
	
}
