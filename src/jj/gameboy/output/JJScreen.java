package jj.gameboy.output;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

import jj.gameboy.JJDMGGameBoy;
import jj.gameboy.JJGameBoy;

public class JJScreen extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final int WIDTH_PIXEL_NUM = 256;
	public static final int HEIGHT_PIXEL_NUM = 256;
	
	public static final int WIDTH_VIEW_PIXEL_NUM = 160;
	public static final int HEIGHT_VIEW_PIXEL_NUM = 144;
	
	private int viewx = 0;
	private int viewy = 0;
	
	private boolean isScreenOn = false;
	
	// the complete background screen, maybe comes later
	private int[] pixelData = new int[WIDTH_PIXEL_NUM * HEIGHT_PIXEL_NUM];
	// the pixels in the view
	private int[] visiblePixelData = new int[WIDTH_VIEW_PIXEL_NUM * HEIGHT_VIEW_PIXEL_NUM];
	// the original value of the pixel
	private int[] visiblePixelOriginalData = new int[WIDTH_VIEW_PIXEL_NUM * HEIGHT_VIEW_PIXEL_NUM];
	// the origin of the pixel, either tile or sprite or window
	private JJPixelOrigin[] visibleOrigin = new JJPixelOrigin[WIDTH_VIEW_PIXEL_NUM * HEIGHT_VIEW_PIXEL_NUM];
	
	private Rectangle spriteDebugRectangle = new Rectangle(-1, -1, -1, -1);
	
	public JJScreen() {
		for (int i = 0; i < visibleOrigin.length; i++) {
			visibleOrigin[i] = JJPixelOrigin.TILE;
		}
	}
	
	public void setView(int x, int y) {
		if (viewx != x || viewy != y) {
			viewx = x;
			viewy = y;
			
			repaint();
		}
	}
	
	public void turnOn() {
		if (!isScreenOn) {
			isScreenOn = true;
			repaint();
		}
	}
	
	public void turnOff() {
//		if (isScreenOn) {
//			isScreenOn = false;
//			repaint();
//		}
	}
	
	public boolean isScreenOn() {
		return isScreenOn;
	}
	
	public void setVisiblePixel(JJPixelOrigin origin, int x, int y, int originalColor, int color) {
		setVisiblePixel(origin, x, y, originalColor, color, true);
	}
	
	public void setVisiblePixel(JJPixelOrigin origin, int x, int y, int originalColor, int color, boolean isInFrontOfBackground) {
		int nextIndex = x + y * WIDTH_VIEW_PIXEL_NUM;
		if (0 <= x && x <= WIDTH_VIEW_PIXEL_NUM) {
			if (0 <= nextIndex && nextIndex < visiblePixelData.length) {
				if (isInFrontOfBackground || visiblePixelOriginalData[nextIndex] == 0) {
					visiblePixelData[nextIndex] = color;
					visiblePixelOriginalData[nextIndex] = originalColor;
					visibleOrigin[nextIndex] = origin;
				}
			}
		}
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		if (isScreenOn) {
			paintScreenOn(g);
//			paintGrid(g);
		} else {
			paintScreenOff(g);
		}
		
		if (spriteDebugRectangle.x >= 0) {
			paintSpriteDebug(g);
		}
		
		if (JJDMGGameBoy.getInstance().isPaused()) {
			paintPause(g);
		}
	}
	
	private void paintPause(Graphics g) {
		String pauseString = "PAUSE";
		
		Font f = g.getFont().deriveFont(Font.BOLD, 48f);
		g.setFont(f);
		
		Rectangle2D rect = g.getFontMetrics().getStringBounds(pauseString, g);
		
		int dw = (int) (rect.getWidth() / 2);
		int dh = (int) (rect.getHeight() / 2);
		
		g.setColor(Color.WHITE);
		g.fillRect(getWidth() / 2 - dw - 10, getHeight() / 2 - dh - 10, (int) rect.getWidth() + 20, (int) rect.getHeight() + 20);
		g.setColor(Color.BLACK);
		g.drawString(pauseString, getWidth() / 2 - dw, getHeight() / 2 + dh / 2);
	}

	private void paintSpriteDebug(Graphics g) {
		double dw = Math.floor(getWidth() / (double) WIDTH_VIEW_PIXEL_NUM);
		double dh = Math.floor(getHeight() / (double) HEIGHT_VIEW_PIXEL_NUM);

		double offsetX = (getWidth() - dw * WIDTH_VIEW_PIXEL_NUM) / 2;
		double offsetY = (getHeight() - dh * HEIGHT_VIEW_PIXEL_NUM) / 2;
		
		g.setColor(Color.RED);
		g.drawRect((int) (spriteDebugRectangle.x * dw + offsetX), (int) (spriteDebugRectangle.y * dh + offsetY),
				(int) (spriteDebugRectangle.width * dw), (int) (spriteDebugRectangle.height * dh));
	}
	
	private void paintScreenOff(Graphics g) {
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, getWidth(), getHeight());
	}
	
	private void paintScreenOn(Graphics g) {
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, getWidth(), getHeight());
		
		double dw = Math.floor(getWidth() / (double) WIDTH_VIEW_PIXEL_NUM);
		double dh = Math.floor(getHeight() / (double) HEIGHT_VIEW_PIXEL_NUM);
		
		double wh = Math.min(dw, dh);
		dw = wh;
		dh = wh;
		
		double offsetX = (getWidth() - dw * WIDTH_VIEW_PIXEL_NUM) / 2;
		double offsetY = (getHeight() - dh * HEIGHT_VIEW_PIXEL_NUM) / 2;
		
		for (int y = 0; y < HEIGHT_VIEW_PIXEL_NUM; y++) {
			for (int x = 0; x < WIDTH_VIEW_PIXEL_NUM; x++) {
				int index = y * WIDTH_VIEW_PIXEL_NUM + x;
				g.setColor(JJGraphicSettings.getColor(visiblePixelData[index], visibleOrigin[index]));
				g.fillRect((int) (x * dw + offsetX), (int) (y * dh + offsetY), (int) dw, (int) dh);
			}
		}
	}
	
	public void setSpriteDebugRectangle(int x, int y, int w, int h) {
		spriteDebugRectangle.x = x;
		spriteDebugRectangle.y = y;
		spriteDebugRectangle.width = w;
		spriteDebugRectangle.height = h;
	}

	private void paintGrid(Graphics g) {
		double dw = getWidth() / 20.0;
		double dh = getHeight() / 16.0;
		
		g.setColor(Color.BLACK);
		for (int h = 0; h < 16; h++) {
			g.drawLine(0, (int)(h * dh), getWidth(), (int)(h * dh));
		}
		
		for (int w = 0; w < 20; w++) {
			g.drawLine((int)(w * dw), 0, (int)(w * dw), getHeight());
		}
		
	}
	
		
	private void painting(Graphics g) {
		g.setColor(Color.GREEN.darker());
		g.fillRect(0, 0, getWidth(), getHeight());
		
		double dw = getWidth() / (double) WIDTH_PIXEL_NUM;
		double dh = getHeight() / (double) HEIGHT_PIXEL_NUM;
		
		g.setColor(Color.BLACK);
		for (int h = 0; h < HEIGHT_PIXEL_NUM; h++) {
			g.drawLine(0, (int)(h * dh), getWidth(), (int)(h * dh));
		}
		
		for (int w = 0; w < WIDTH_PIXEL_NUM; w++) {
			g.drawLine((int)(w * dw), 0, (int)(w * dw), getHeight());
		}
		
		g.setColor(Color.RED);
		int i = 0;
		for (int h = 0; h < HEIGHT_VIEW_PIXEL_NUM; h++) {
			for (int w = 0; w < WIDTH_VIEW_PIXEL_NUM; w++) {
				int x = (viewx + w) % WIDTH_PIXEL_NUM;
				int y = (viewy + h) % HEIGHT_PIXEL_NUM;
				
				g.fillRect((int) (x * dw), (int) (y * dh), (int) dw, (int) dh);
			}
		}
	}
	
}
