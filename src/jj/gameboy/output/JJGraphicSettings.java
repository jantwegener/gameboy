package jj.gameboy.output;

import java.awt.Color;

public class JJGraphicSettings {
	
	// inverted r <-> b
//	private static final Color COLOR_0 = new Color(15, 188, 155);
//	private static final Color COLOR_1 = new Color(15, 172, 139);
//	private static final Color COLOR_2 = new Color(48, 98, 48);
//	private static final Color COLOR_3 = new Color(15, 56, 15);
	
	// Original colors
//	private static final Color COLOR_0 = new Color(155, 188, 15);
//	private static final Color COLOR_1 = new Color(139, 172, 15);
//	private static final Color COLOR_2 = new Color(48, 98, 48);
//	private static final Color COLOR_3 = new Color(15, 56, 15);

	// darker light green original colors
	private static Color COLOR_0 = new Color(248, 255, 178);
	private static Color COLOR_1 = new Color(170, 204, 71);
	private static Color COLOR_2 = new Color(60, 127, 38);
	private static Color COLOR_3 = new Color(0, 19, 25);
	
	private static Color WINDOW_COLOR_0 = new Color(248, 255, 178);
	private static Color WINDOW_COLOR_1 = new Color(170, 204, 71);
	private static Color WINDOW_COLOR_2 = new Color(60, 127, 38);
	private static Color WINDOW_COLOR_3 = new Color(0, 19, 25);

	private static Color SPRITE_COLOR_0 = new Color(248, 255, 178);
	private static Color SPRITE_COLOR_1 = new Color(170, 204, 71);
	private static Color SPRITE_COLOR_2 = new Color(60, 127, 38);
	private static Color SPRITE_COLOR_3 = new Color(0, 19, 25);
	
	// 50 Shades of Gray
//	private static final Color COLOR_0 = Color.LIGHT_GRAY;
//	private static final Color COLOR_1 = Color.GRAY;
//	private static final Color COLOR_2 = Color.DARK_GRAY;
//	private static final Color COLOR_3 = Color.BLACK;
	
	/**
	 * 
	 * FF47 - BGP - BG Palette Data (R/W) - Non CGB Mode Only
	 * This register assigns gray shades to the color numbers of the BG and Window tiles.
	 * 
	 *   Bit 7-6 - Shade for Color Number 3
	 *   Bit 5-4 - Shade for Color Number 2
	 *   Bit 3-2 - Shade for Color Number 1
	 *   Bit 1-0 - Shade for Color Number 0
	 *  
	 *  The four possible gray shades are:
	 *  		 0  White
	 *  		 1  Light gray
	 *  		 2  Dark gray
	 *  		 3  Black
	 *  In CGB Mode the Color Palettes are taken from CGB Palette Memory instead.
	 * 
	 * @return 
	 */
	public static Color getColor(int grayShade, JJPixelOrigin origin) {
		Color retval = Color.RED;
		
		switch (origin) {
			case TILE:
				retval = getTileColor(grayShade);
				break;
			case WINDOW:
				retval = getWindowColor(grayShade);
				break;
			case SPRITE:
				retval = getSpriteColor(grayShade);
				break;
		}
		
		return retval;
	}
	
	private static Color getTileColor(int grayShade) {
		Color color;
		
		switch (grayShade) {
			
			case 0:
				color = COLOR_0;
				break;
				
			case 1:
				color = COLOR_1;
				break;
				
			case 2:
				color = COLOR_2;
				break;
				
			case 3:
				color = COLOR_3;
				break;
				
			default:
				throw new JJUnknownColorException("Unknown shade: " + grayShade);
				
		}
		
		return color;
	}
	
	private static Color getSpriteColor(int grayShade) {
		Color color;
		
		switch (grayShade) {
			
			case 0:
				color = SPRITE_COLOR_0;
				break;
				
			case 1:
				color = SPRITE_COLOR_1;
				break;
				
			case 2:
				color = SPRITE_COLOR_2;
				break;
				
			case 3:
				color = SPRITE_COLOR_3;
				break;
				
			default:
				throw new JJUnknownColorException("Unknown shade: " + grayShade);
				
		}
		
		return color;
	}
	
	private static Color getWindowColor(int grayShade) {
		Color color;
		
		switch (grayShade) {
			
			case 0:
				color = WINDOW_COLOR_0;
				break;
				
			case 1:
				color = WINDOW_COLOR_1;
				break;
				
			case 2:
				color = WINDOW_COLOR_2;
				break;
				
			case 3:
				color = WINDOW_COLOR_3;
				break;
				
			default:
				throw new JJUnknownColorException("Unknown shade: " + grayShade);
				
		}
		
		return color;
	}
	
	/**
	 * 
	 * @param c0
	 * @param c1
	 * @param c2
	 * @param c3
	 * 
	 * @deprecated Use {@link #setTileColor(Color, Color, Color, Color)} instead.
	 */
	public static void setColor(Color c0, Color c1, Color c2, Color c3) {
		COLOR_0 = c0;
		COLOR_1 = c1;
		COLOR_2 = c2;
		COLOR_3 = c3;
	}
	
	public static void setTileColor(Color c0, Color c1, Color c2, Color c3) {
		COLOR_0 = c0;
		COLOR_1 = c1;
		COLOR_2 = c2;
		COLOR_3 = c3;
	}
	
	public static void setTileColor(Color[] c) {
		setTileColor(c[0], c[1], c[2], c[3]);
	}
	
	public static void setSpriteColor(Color c0, Color c1, Color c2, Color c3) {
		SPRITE_COLOR_0 = c0;
		SPRITE_COLOR_1 = c1;
		SPRITE_COLOR_2 = c2;
		SPRITE_COLOR_3 = c3;
	}
	
	public static void setSpriteColor(Color[] c) {
		setSpriteColor(c[0], c[1], c[2], c[3]);
	}
	
	public static void setWindowColor(Color c0, Color c1, Color c2, Color c3) {
		WINDOW_COLOR_0 = c0;
		WINDOW_COLOR_1 = c1;
		WINDOW_COLOR_2 = c2;
		WINDOW_COLOR_3 = c3;
	}
	
	public static void setWindowColor(Color[] c) {
		setWindowColor(c[0], c[1], c[2], c[3]);
	}
	
}
