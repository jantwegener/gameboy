package jj.gameboy.output.sound;

import java.util.ArrayList;
import java.util.List;

import javax.sound.midi.Instrument;
import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Synthesizer;

/**
 * One of the first approaches to produce sound. Would have been cool to be able to choose from a palette of instruments.
 * If you can make this work, it would be awesome :)
 * 
 * @author Jan-Thierry Wegener
 */
public class JJAmplifierMidi {
	
	private int CHANNEL_NUM = 4;

	private int BASE_FREQUENCE = 440;
	
	/**
	 * If no sound card is available set to false.
	 */
	private boolean generalPlaySound = true;
	
	private MidiChannel[] midiChannel = new MidiChannel[CHANNEL_NUM];
	
	// wave ram is from 0x30 - 0x3f
	private int[] wavePatternRam = new int[32];
	
	private int wavePatternCounter = 0;
	
	private int[] instrumentIndex = {
			12, 9, 16, 234
	};
	
	private List<Note> noteList = new ArrayList<Note>();
	
	public JJAmplifierMidi() {
        Synthesizer synthesizer;
		try {
			synthesizer = MidiSystem.getSynthesizer();
	        synthesizer.open();
	        
	        Instrument[] orchestra = synthesizer.getAvailableInstruments();
	        
	        synthesizer.loadInstrument(orchestra[instrumentIndex[0]]);
	        synthesizer.loadInstrument(orchestra[instrumentIndex[1]]);
	        synthesizer.loadInstrument(orchestra[instrumentIndex[2]]);
	        synthesizer.loadInstrument(orchestra[instrumentIndex[3]]);

	        midiChannel[0] = synthesizer.getChannels()[0];
	        midiChannel[1] = synthesizer.getChannels()[1];
	        midiChannel[2] = synthesizer.getChannels()[2];
	        midiChannel[3] = synthesizer.getChannels()[10];
	        
	        midiChannel[0].programChange(5);
	        midiChannel[1].programChange(7);
	        midiChannel[2].programChange(6);
	        midiChannel[3].programChange(234);
	        
		} catch (MidiUnavailableException e) {
			e.printStackTrace();
			generalPlaySound = false;
		}
	}
	
	public void writeWavePattern(int address, int value) {
		wavePatternRam[address] = (value & 0xFF00) >> 4;
		wavePatternRam[address + 1] = (value & 0xFF);
	}
	
	private int computeMidiNote(int channel, int frequency) {
		if (channel == 0 || channel == 1) {
			return computeMidiNoteChannel1And2(frequency);
		}
		return computeMidiNoteChannel3(frequency);
	}
	
	private int computeMidiNoteChannel1And2(int frequency) {
		// fm = 131072/(2048-x) Hz
		// midiNote  =  12*log2(fm/440 Hz) + 69
		double fm = 131072d / (2048 - frequency);
		int note = (int)(12d * Math.log(fm / BASE_FREQUENCE) / Math.log(2) + 69d);
		return note;
	}
	
	private int computeMidiNoteChannel3(int frequency) {
		// fm = 4194304/(64*(2048-x)) Hz = 65536/(2048-x) Hz
		// midiNote  =  12*log2(fm/440 Hz) + 69
		double fm = 131072d / (2048 - frequency);
		int note = (int)(12d * Math.log(fm / BASE_FREQUENCE) / Math.log(2) + 69d);
		return note;
	}
	
	public void play(int channel, int frequency, long length, int volume) {
		int midiNote = computeMidiNote(channel, frequency);
		Note n = new Note(channel, midiNote, length, volume, 0, 0, 0);
		playNote(n);
		noteList.add(n);
	}
	
	private void playNote(Note n) {
		int channel = n.channel;
		midiChannel[channel].noteOn(n.note, n.initialVolume);
	}
	
	private Note stopNote(Note n) {
		Note retval = null;
		
		int channel = n.channel;
		midiChannel[channel].noteOff(n.note, 127);
		
		// channel 3 plays from ram
		if (channel == 2) {
				wavePatternCounter++;
			if (wavePatternCounter >= wavePatternRam.length) {
				wavePatternCounter = 0;
			}
			int note = wavePatternRam[wavePatternCounter];
			
			n = new Note(2, note, n.length, n.initialVolume, 0, 0, 0);
			playNote(n);
			retval = n;
		}
		
		return retval;
	}
	
	public int tick() {
		if (!generalPlaySound) {
			return 4_000_000;
		}
		
		List<Note> toRemoveList = new ArrayList<>();
		List<Note> toAddList = new ArrayList<>();
		for (Note n : noteList) {
			if (!n.continuePlaying()) {
				Note toAdd = stopNote(n);
				if (toAdd != null) {
					toAddList.add(toAdd);
				}
				toRemoveList.add(n);
			}
		}
		
		noteList.removeAll(toRemoveList);
		noteList.addAll(toAddList);
		
		return 400;
	}
	
	private class Note {
		
		private int channel;
		private int note;
		private long stopTime;
		private long length;
		private int initialVolume;
		private long volumeEnvelopeTimer;
		private long frequencyTimer;
		private int wavePosition;
		
		public Note(int channel, int note, long length, int volume, long volumeEnvelopeTimer, long frequencyTimer, int wavePosition) {
			this.channel = channel;
			this.note = note;
			this.length = length;
			this.stopTime = System.currentTimeMillis() + length;
			this.initialVolume = volume;
			this.volumeEnvelopeTimer = volumeEnvelopeTimer;
			this.frequencyTimer = frequencyTimer;
			this.wavePosition = wavePosition;
		}
		
		/**
		 * Checks if the note continues playing.
		 * 
		 * @return true if the note still plays, false if the note shall stop.
		 */
		public boolean continuePlaying() {
			boolean retval = true;
			
			long now = System.currentTimeMillis();
			if (stopTime >= now) {
				retval = false;
			}
			
			return retval;
		}
		
	}
	
}
