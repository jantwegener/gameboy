package jj.gameboy.output.sound;

import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

import jj.gameboy.unit.JJAPU;
import jtw.util.sound.IFilter;
import jtw.util.sound.LowPassFilter;
import jtw.util.sound.SelectionFilter;

/**
 * An attempt to produce the sound of the GameBoy.
 * 
 * If you have not had tooth ache yet, well, here it comes.
 * 
 * @author Jan-Thierry Wegener
 */
public class JJAmplifierExactOneLine extends JJAmplifier {
	
	private int CHANNEL_NUM = 4;
	
	private static final int CPU_HZ = 4194304;
	
	private static final int SAMPLE_RATE = 48000;

	private static int SAMPLE_SKIP_RATE = CPU_HZ / SAMPLE_RATE;
	
	private static final int COLLECTION_RATE = 4 * 1024 * SAMPLE_SKIP_RATE;
	
	private int cpuClock = 0;
	
	/**
	 * If no sound card is available set to false.
	 */
	private boolean generalPlaySound = true;
	
	private SourceDataLine line;
	
	private ByteBuffer[] buffer = new ByteBuffer[4];
	
	private volatile Thread thread = null;
	
	private Object monitor = new Object();
	private boolean[] monitorLine = new boolean[CHANNEL_NUM];
	
	public JJAmplifierExactOneLine(JJAPU apu) {
		super(apu);
		
		final AudioFormat af = new AudioFormat(SAMPLE_RATE, 8, 1, true, true);

		try {
			for (int i = 0; i < getChannelNum(); i++) {
				buffer[i] = ByteBuffer.allocate(2 * COLLECTION_RATE);
			}
			
			line = AudioSystem.getSourceDataLine(af);
			line.open(af, SAMPLE_RATE);
			line.start();
		} catch (LineUnavailableException e) {
			e.printStackTrace();
			generalPlaySound = false;
		}
	}
	
	public void doStart() {
	}
	
	public void shutdown() {
		line.drain();
		line.close();
	}
	
	public void reset() {
		thread = null;
		
		for (int i = 0; i < CHANNEL_NUM; i++) {
			flush(i);
		}
	}
	
	public void flush(int channel) {
		line.stop();
		line.flush();
		line.start();
	}
	
	@Override
	public void writeNoop(int channel, int cycle) {
		cpuClock += cycle;
		
		try {
			buffer[channel].put((byte) 0);
		} catch (BufferOverflowException e) {
			for (int i = 0; i < getChannelNum(); i++) {
				buffer[i].clear();
			}
		}
	}
	
	@Override
	protected void doWrite(int channel, int cycle, byte output) {
		cpuClock += cycle;
		
		buffer[channel].put((byte)(output * 127d / 15d));
		
		if (buffer[channel].position() > COLLECTION_RATE) {
			mix();
			cpuClock -= COLLECTION_RATE;
		}
	}
	
	private void mix() {
		IFilter lowPassFilter = new LowPassFilter(3, SAMPLE_RATE / 2, SAMPLE_RATE);
		IFilter selectionFilter = new SelectionFilter(SAMPLE_SKIP_RATE);
		
		int minLength = Integer.MAX_VALUE;
		byte[][] b = new byte[getChannelNum()][];
		for (int channel = 0; channel < getChannelNum(); channel++) {
			b[channel] = buffer[channel].array();
//			b[channel] = lowPassFilter.filter(b[channel]);
			b[channel] = selectionFilter.filter(b[channel]);
			
			if (b[channel].length < minLength) {
				minLength = b[channel].length;
			}
			buffer[channel].clear();
		}
		
		int available = line.available();
		int write = Math.min(available, minLength);
		System.out.println(minLength + " / " + available + " / " + write + " - " + SAMPLE_SKIP_RATE);
//		if (available > write) {
//			SAMPLE_SKIP_RATE--;
//		} else if (available < write) {
//			SAMPLE_SKIP_RATE++;
//		}
		
		byte[] x = new byte[minLength];
		for (int i = 0; i < minLength; i++) {
			for (int channel = 0; channel < getChannelNum(); channel++) {
				x[i] += b[channel][i];
			}
		}
		
		line.write(x, 0, write);
	}
	
}
