package jj.gameboy.output.sound;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

/**
 * A very early attempt to make sound work for the GameBoy. Did not succeed.
 * 
 * @author Jan-Thierry Wegener
 */
public class JJAmplifierWave {
	
	private int CHANNEL_NUM = 4;
	
	private static final int SAMPLE_RATE = 44100;
	
	/**
	 * If no sound card is available set to false.
	 */
	private boolean generalPlaySound = true;
	
	// wave ram is from 0x30 - 0x3f
	private int[] wavePatternRam = new int[32];
	
	private static final int[] WAVE_DUTY = new int[] {
			0b10000000,
			0b11000000,
			0b11110000,
			0b11111100
	};
	
	private int wavePatternCounter = 0;
	
	private SourceDataLine[] line = new SourceDataLine[4];
	
	public JJAmplifierWave() {
		final AudioFormat af = new AudioFormat(SAMPLE_RATE, 8, 1, true, true);
		
		try {
			for (int i = 0; i < CHANNEL_NUM; i++) {
			line[i] = AudioSystem.getSourceDataLine(af);
			line[i].open(af, SAMPLE_RATE);
			line[i].start();
			}
		} catch (LineUnavailableException e) {
			e.printStackTrace();
			generalPlaySound = false;
		}
	}
	
	public void shutdown() {
			for (int i = 0; i < CHANNEL_NUM; i++) {
		line[i].drain();
		line[i].close();
			}
	}
	
	private byte[] createWaveBuffer(int frequency, int length, int volume) {
		long ms;
		if (length != 0) {
			// time in seconds = (64-t1)*(1/256)
			ms = 1000 * (64 - length) / 256;
		} else {
			ms = 1000;
		}
		
		// frequency = 131072/(2048-x) Hz = waves / s
		double freq = 65536d / (2048 - frequency);
		int samples = (int)(ms / 1000d * SAMPLE_RATE);
		byte[] output = new byte[samples];
		
		double period = SAMPLE_RATE / freq;
		int bytesPerStep = (int)(period / 8);
		for (int i = 0; i < output.length; i++) {
			int s = 1;
			if ((i % bytesPerStep) == 0) {
				if (wavePatternCounter >= wavePatternRam.length) {
					wavePatternCounter = 0;
				}
				// - 4 to get negative values => -4 to 3 instead of 0 to 7
				s = 127 / (wavePatternRam[wavePatternCounter++] - 4);
			}
			output[i] = (byte)(s * volume);
		}
		
		return output;
	}
	
	private byte[] createWaveDutyBuffer(int duty, int frequency, int length, int volume,
			int sweepTime, int sweepIncrease, int sweepNumber,
			int volumeEnvelopeDirection, int volumeEnvelopeNumber) {
		double ms = 0;
		if (length != 0) {
			// time in seconds = (64-t1)*(1/256)
			ms = 1000d * (64 - length) / 256d;
		}

		int sweepPeriod = Integer.MAX_VALUE;
		// 000: sweep off - no freq change
		// 001: 7.8 ms  (1/128Hz)
		// 010: 15.6 ms (2/128Hz)
		// 011: 23.4 ms (3/128Hz)
		// 100: 31.3 ms (4/128Hz)
		// 101: 39.1 ms (5/128Hz)
		// 110: 46.9 ms (6/128Hz)
		// 111: 54.7 ms (7/128Hz)
		if (sweepTime > 0) {
			sweepPeriod = (int) (SAMPLE_RATE / (128d / sweepTime));
			if (ms == 0) {
				ms = 1000d * sweepNumber * sweepTime / 128d;
			}
		}
		
		if (sweepIncrease == 0) {
			sweepIncrease = -1;
		}
		
		int volumePeriod = Integer.MAX_VALUE;
		// Bit 7-4 - Initial Volume of envelope (0-0Fh) (0=No Sound)
		// Bit 3   - Envelope Direction (0=Decrease, 1=Increase)
		// Bit 2-0 - Number of envelope sweep (n: 0-7)
		//           (If zero, stop envelope operation.)
		if (volumeEnvelopeNumber > 0) {
			volumePeriod = (int) (SAMPLE_RATE / 64d);
			if (ms == 0) {
				ms = 1000d * volumeEnvelopeNumber / 64d;
			}
		}
		
		if (volumeEnvelopeDirection == 0) {
			volumeEnvelopeDirection = -1;
		}
		
		// frequency = 131072/(2048-x) Hz = waves / s
		double freq = 131072d / (2048 - frequency);
		int samples = (int)(ms / 1000d * SAMPLE_RATE);
		byte[] output = new byte[samples];
		
		
		double period = SAMPLE_RATE / freq;
		int bytesPerStep = (int)(period / 8);
		int wavePointer = 0;
		int sweepCounter = 0;
		for (int i = 0; i < output.length; i++) {
			int s = 1;
			if ((i % bytesPerStep) == 0) {
				if (wavePointer >= 8) {
					wavePointer = 0;
				}
				int x = (WAVE_DUTY[duty] << wavePointer++) & 0x100;
				if (x > 0) {
					s = 1;
				} else {
					s = -1;
				}
			}
			
			if ((i % sweepPeriod) == 0) {
				// compute new frequency
				// X(t) = X(t-1) +/- X(t-1)/2^n
				freq = freq + sweepIncrease * freq / (2 << sweepCounter);
				sweepCounter++;
			}
			
			if ((i % volumePeriod) == 0) {
				// increase volume
				volume = volume + volumeEnvelopeDirection * 2;
				if (volume < 0) {
					volume = 0;
				} else if (volume > 127) {
					volume = 127;
				}
			}
			
			output[i] = (byte)(s * volume);
		}
		
		return output;
	}
	
	public void writeWavePattern(int address, int value) {
		wavePatternRam[address] = (value & 0xFF00) >> 4;
		wavePatternRam[address + 1] = (value & 0xFF);
	}
	
	private double computeRealFrequency(int channel, int frequency) {
		if (channel == 0 || channel == 1) {
			return computeRealFrequencyChannel1And2(frequency);
		}
		return computeRealFrequencyChannel3(frequency);
	}
	
	private double computeRealFrequencyChannel1And2(int frequency) {
		// fm = 131072 / (2048-x) Hz
		double fm = 131072d / (2048 - frequency);
		return fm;
	}
	
	private double computeRealFrequencyChannel3(int frequency) {
		// fm = 4194304/(64*(2048-x)) Hz = 65536/(2048-x) Hz
		double fm = 65536d / (2048 - frequency);
		return fm;
	}
	
	public void play(int channel, int waveDuty, int gbFrequency, int length, int volume,
			int sweepTime, int sweepIncrease, int sweepNumber,
			int volumeEnvelopeDirection, int volumeEnvelopeNumber) {
		byte [] toneBuffer;
		
		if (channel == 0 || channel == 1) {
			toneBuffer = createWaveDutyBuffer(waveDuty, gbFrequency, length, volume,
					sweepTime, sweepIncrease, sweepNumber, volumeEnvelopeDirection, volumeEnvelopeNumber);
		} else {
			toneBuffer = createWaveBuffer(gbFrequency, length, volume);
		}
		line[channel].write(toneBuffer, 0, toneBuffer.length);
	}
	
	public int tick(int cpuClockPassed) {
		if (!generalPlaySound) {
			return 4_000_000;
		}
		
		return 1;
	}
		
}
