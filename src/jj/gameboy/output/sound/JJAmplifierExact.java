package jj.gameboy.output.sound;

import java.nio.ByteBuffer;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

import jj.gameboy.unit.JJAPU;
import jtw.util.sound.IFilter;
import jtw.util.sound.LowPassFilter;
import jtw.util.sound.SelectionFilter;

/**
 * An attempt to produce the sound of the GameBoy.
 * 
 * If you have not had tooth ache yet, well, here it comes.
 * 
 * @author Jan-Thierry Wegener
 */
public class JJAmplifierExact extends JJAmplifier implements Runnable {
	
	private int CHANNEL_NUM = 4;
	
	private static final int CPU_HZ = 4194304/4;
	
	private static final int SAMPLE_RATE = 48000;
	
	private static final int SAMPLE_SKIP_RATE = CPU_HZ / SAMPLE_RATE;
	
	private static final int COLLECTION_RATE = 3 * 1024 * SAMPLE_SKIP_RATE;
	
	private int cpuClock = 0;
	
	/**
	 * If no sound card is available set to false.
	 */
	private boolean generalPlaySound = true;
	
	private SourceDataLine[] line = new SourceDataLine[4];
	
	private ByteBuffer[] buffer = new ByteBuffer[CHANNEL_NUM];
	
	private volatile Thread thread = null;
	
	private Object monitor = new Object();
	private boolean[] monitorLine = new boolean[CHANNEL_NUM];
	
	public JJAmplifierExact(JJAPU apu) {
		super(apu);
		
		final AudioFormat af = new AudioFormat(SAMPLE_RATE, 8, 1, true, true);

		try {
			for (int i = 0; i < CHANNEL_NUM; i++) {
				buffer[i] = ByteBuffer.allocate(2 * COLLECTION_RATE);
				
				line[i] = AudioSystem.getSourceDataLine(af);
				line[i].open(af, SAMPLE_RATE);
				line[i].start();
			}
		} catch (LineUnavailableException e) {
			e.printStackTrace();
			generalPlaySound = false;
		}
	}
	
	public void doStart() {
//		if (thread == null) {
//			thread = new Thread(this);
//			thread.start();
//		}
	}
	
	public void shutdown() {
		for (int i = 0; i < CHANNEL_NUM; i++) {
			line[i].drain();
			line[i].close();
		}
	}
	
	public void reset() {
		thread = null;
		
		for (int i = 0; i < CHANNEL_NUM; i++) {
			flush(i);
		}
	}
	
	public void flush(int channel) {
		line[channel].stop();
		line[channel].flush();
		line[channel].start();
//		line[channel].drain();
	}

	public void write2(int channel, byte output, int cycle) {
		cpuClock += cycle;
		
		if (cpuClock > SAMPLE_SKIP_RATE) {
			buffer[channel].put((byte)(output * 127d / 15d));
			
			int available = line[channel].available();
			if (buffer[channel].position() > 2048) {
				int write = Math.min(available, buffer[channel].position());
//				System.out.println(buffer[channel].position() + " / " + available + " / " + write);
				line[channel].write(buffer[channel].array(), 0, write);
				buffer[channel].clear();
//				synchronized (monitor) {
//					monitor.notify();
//				}
			}
			
			cpuClock -= SAMPLE_SKIP_RATE;
		}
	}

	@Override
	protected void doWrite(int channel, int cycle, byte output) {
		cpuClock += cycle;
		
		buffer[channel].put((byte)(output * 127d / 15d));
		
//		if (cpuClock > COLLECTION_RATE) {
		if (buffer[channel].position() > COLLECTION_RATE) {
			IFilter lowPassFilter = new LowPassFilter(3, SAMPLE_RATE / 2, SAMPLE_RATE);
			byte[] b = buffer[channel].array();
			b = lowPassFilter.filter(b);
			
			IFilter selectionFilter = new SelectionFilter(SAMPLE_SKIP_RATE);
//			byte[] b = selectionFilter.filter(buffer[channel].array());
			b = selectionFilter.filter(b);
			
			int available = line[channel].available();
//			if (b.length > 2048) {
				int write = Math.min(available, b.length);
				System.out.println(buffer[channel].position() + " / " + available + " / " + write);
				line[channel].write(b, 0, write);
				buffer[channel].clear();
//			}
			
			cpuClock -= COLLECTION_RATE;
		}
	}
	
	public void run() {
//		while (Thread.currentThread() == thread) {
//			try {
//				synchronized (monitor) {
//					monitor.wait();
//				}
//				line[0].write(buffer[0].array(), 0, buffer[0].position());
//				buffer[0].clear();
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//		}
	}
	
	private double computeRealFrequency(int channel, int frequency) {
		if (channel == 0 || channel == 1) {
			return computeRealFrequencyChannel1And2(frequency);
		}
		return computeRealFrequencyChannel3(frequency);
	}
	
	private double computeRealFrequencyChannel1And2(int frequency) {
		// fm = 131072 / (2048-x) Hz
		double fm = 131072d / (2048 - frequency);
		return fm;
	}
	
	private double computeRealFrequencyChannel3(int frequency) {
		// fm = 4194304/(64*(2048-x)) Hz = 65536/(2048-x) Hz
		double fm = 65536d / (2048 - frequency);
		return fm;
	}
	
}
