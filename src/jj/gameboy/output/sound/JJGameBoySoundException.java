package jj.gameboy.output.sound;

import jj.gameboy.JJGameBoyException;

public class JJGameBoySoundException extends JJGameBoyException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public JJGameBoySoundException(Exception e) {
		super(e);
	}

	public JJGameBoySoundException(String msg) {
		super(msg);
	}

}
