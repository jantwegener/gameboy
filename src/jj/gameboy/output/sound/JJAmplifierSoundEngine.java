package jj.gameboy.output.sound;

import javax.sound.sampled.LineUnavailableException;

import jj.gameboy.unit.JJAPU;
import jtw.util.sound.SoundEngine;

public class JJAmplifierSoundEngine extends JJAmplifier {
	
	private SoundEngine[] engine;

	public JJAmplifierSoundEngine(JJAPU apu) {
		super(apu);
		
		try {
			engine = new SoundEngine[getChannelNum()];
			for (int i = 0; i < getChannelNum(); i++) {
				engine[i] = new SoundEngine();
				engine[i].setWaveForm(SoundEngine.WAVE_SQUARE);
				engine[i].setDuration(150);
			}
			// channel 3 uses a custom wave form
			engine[2].setWaveForm(SoundEngine.WAVE_CUSTOM);
		} catch (LineUnavailableException e) {
			e.printStackTrace();
			
			setGeneralPlaySound(false);
		}
	}

	@Override
	protected void doStart() throws JJGameBoySoundException {
		try {
			for (int i = 0; i < getChannelNum(); i++) {
				engine[i].start();
			}
		} catch (LineUnavailableException e) {
			throw new JJGameBoySoundException(e);
		}
	}

	@Override
	public void shutdown() {
		for (int i = 0; i < getChannelNum(); i++) {
			engine[i].stop();
		}
	}

	@Override
	public void flush(int channel) {
		engine[channel].flush();
		engine[channel].setFrequency(-1);
	}
	
	public void writeNoop(int channel, int cycle) {
		engine[channel].pause();
	}

	@Override
	protected void doWrite(int channel, int cycle, byte output) {
		engine[channel].resume();
		
		engine[channel].setVolume(getAPU().getVolume(channel) / 16.0);
		
		// channel 3 has a custom wave form
		if (channel == 2) {
			engine[channel].setVolume(1);
			engine[2].setCustomWaveForm(transformCustomWaveForm());
		}
		
		int gbFrequency = getAPU().getFrequency(channel);
		// get the real frequency
		double frequency = computeRealFrequency(channel, gbFrequency);
		engine[channel].setFrequency(frequency);
	}
	
	private double[] transformCustomWaveForm() {
		int[] wavePattern = getAPU().getWavePatternRam();
		double[] transformedPattern = new double[wavePattern.length];
		
		for (int i = 0; i < transformedPattern.length; i++) {
			// the maximal value is 15
			transformedPattern[i] =  (wavePattern[i] >> getAPU().getChannel3OutputLevel()) / 15.0;
		}
		
		return transformedPattern;
	}
	
	private double computeRealFrequency(int channel, int frequency) {
		if (channel == 0 || channel == 1) {
			return computeRealFrequencyChannel1And2(frequency);
		}
		return computeRealFrequencyChannel3(frequency);
	}
	
	private double computeRealFrequencyChannel1And2(int frequency) {
		// fm = 131072 / (2048-x) Hz
		double fm = 131072d / (2048 - frequency);
		return fm;
	}
	
	private double computeRealFrequencyChannel3(int frequency) {
		// fm = 4194304/(64*(2048-x)) Hz = 65536/(2048-x) Hz
		double fm = 65536d / (2048 - frequency);
		return fm;
	}
	
}
