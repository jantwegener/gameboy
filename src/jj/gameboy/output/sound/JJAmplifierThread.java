package jj.gameboy.output.sound;

import javax.sound.sampled.LineUnavailableException;

import jj.gameboy.unit.JJAPUWave;
import jtw.util.sound.SoundEngine;

/**
 * Another attempt to produce sound. Well, it failed. Nothing to add.
 * 
 * @author Jan-Thierry Wegener
 */
public class JJAmplifierThread {
	
	private int CHANNEL_NUM = 4;
	
	private static final int CPU_HZ = 4194304;
	
	private static final int SAMPLE_RATE = 44100;
	
	private int cpuClock = 0;
	
	/**
	 * If no sound card is available set to false.
	 */
	private boolean generalPlaySound = true;
	
	private SoundEngine[] soundEngine = new SoundEngine[CHANNEL_NUM];
	
	public JJAmplifierThread() {
		try {
			for (int i = 0; i < CHANNEL_NUM; i++) {
				soundEngine[i] = new SoundEngine();
				soundEngine[i].setWaveForm(SoundEngine.WAVE_SQUARE);
			}
		} catch (LineUnavailableException e) {
			e.printStackTrace();
			generalPlaySound = false;
		}
	}
	
	public void start() {
		try {
			for (int i = 0; i < CHANNEL_NUM; i++) {
				soundEngine[i].start();
			}
		} catch (LineUnavailableException e) {
			e.printStackTrace();
			generalPlaySound = false;
		}
	}
	
	public void shutdown() {
		for (int i = 0; i < CHANNEL_NUM; i++) {
			soundEngine[i].stop();
		}
	}
	
	public void reset() {
		for (int i = 0; i < CHANNEL_NUM; i++) {
			flush(i);
		}
	}
	
	public void flush(int channel) {
		soundEngine[channel].setFrequency(0);
		soundEngine[channel].flush();
	}
	
	public void write(JJAPUWave apu, int channel, int cycle) {
		cpuClock += cycle;
		
		double frequency = computeRealFrequency(channel, apu.getFrequency(channel));
		soundEngine[channel].setFrequency(frequency);
	}
	
	private double computeRealFrequency(int channel, int frequency) {
		if (channel == 0 || channel == 1) {
			return computeRealFrequencyChannel1And2(frequency);
		}
		return computeRealFrequencyChannel3(frequency);
	}
	
	private double computeRealFrequencyChannel1And2(int frequency) {
		// fm = 131072 / (2048-x) Hz
		double fm = 131072d / (2048 - frequency);
		return fm;
	}
	
	private double computeRealFrequencyChannel3(int frequency) {
		// fm = 4194304/(64*(2048-x)) Hz = 65536/(2048-x) Hz
		double fm = 65536d / (2048 - frequency);
		return fm;
	}
	
}
