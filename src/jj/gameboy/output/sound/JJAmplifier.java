package jj.gameboy.output.sound;

import jj.gameboy.unit.JJAPU;
import jj.gameboy.unit.JJAPUWave;

/**
 * The base class for the amplifier. The amplifier is responsible to produce the sound.
 * 
 * @author Jan-Thierry Wegener
 */
public abstract class JJAmplifier {
	
	/**
	 * If no sound card is available set to false.
	 */
	private boolean generalPlaySound = true;
	
	/**
	 * The number of channels. Is 4 by default.
	 */
	private int channelNum = 4;
	
	/**
	 * The calling APU.
	 */
	private JJAPU apu;
	
	/**
	 * Creates a new amplifier from the given APU.
	 * 
	 * @param apu The APU that creates the amplifier.
	 */
	public JJAmplifier(JJAPU apu) {
		this.apu = apu;
	}
	
	/**
	 * Turns the sound on or off.
	 * 
	 * @param generalPlaySound true to turn on the sound, false to turn it off.
	 */
	protected void setGeneralPlaySound(boolean generalPlaySound) {
		this.generalPlaySound = generalPlaySound;
	}
	
	/**
	 * Returns the used APU.
	 * 
	 * @return The APU.
	 */
	protected JJAPU getAPU() {
		return apu;
	}
	
	/**
	 * Sets a new number of channels.
	 * 
	 * @param channelNum The new number of channels.
	 */
	protected void setChannelNum(int channelNum) {
		this.channelNum = channelNum;
	}
	
	/**
	 * Returns the number of handled channels.
	 * 
	 * @return The number of channels.
	 */
	protected int getChannelNum() {
		return channelNum;
	}
	
	/**
	 * Prepares the amplifier to start playing sound.
	 * 
	 * @see #doStart()
	 */
	public final void start() {
		try {
			if (generalPlaySound) {
				doStart();
			}
		} catch (JJGameBoySoundException e) {
			e.printStackTrace();
			generalPlaySound = false;
		}
	}
	
	/**
	 * Does the real preparation. Is called by {@link #start()}.
	 * <p>
	 * If the sound could not be initialized an exception is thrown.
	 * 
	 * @throws JJGameBoySoundException If the sound cannot be initialized.
	 * 
	 * @see #start()
	 */
	protected abstract void doStart() throws JJGameBoySoundException;
	
	/**
	 * Stops the amplifier and releases all resources.
	 */
	public abstract void shutdown();
	
	/**
	 * Resets the amplifier by calling flush on all channels.
	 */
	public void reset() {
		for (int i = 0; i < getChannelNum(); i++) {
			flush(i);
		}
	}
	
	/**
	 * Flushes the given channel.
	 * 
	 * @param channel The channel to flush.
	 */
	public abstract void flush(int channel);
	
	/**
	 * Signals that to the channel sound samples are written. The sound output is done in the method {@link #doWrite(JJAPUWave, int, int)}.
	 * 
	 * @param channel The channel to write to.
	 * @param cycle The number of cycles passed since last call.
	 * @param output The sound to output. However, the implementation can decide to ignore this and use the APU directly.
	 * 
	 * @see #doWrite(JJAPUWave, int, int)
	 */
	public final void write(int channel, int cycle, byte output) {
		if (generalPlaySound) {
			doWrite(channel, cycle, output);
		}
	}

	/**
	 * Signals that to the channel nothing is written to. Does nothing by default but can be used to turn of the sound for the channel.
	 * 
	 * @param channel The channel to write to.
	 * @param cycle The number of cycles passed since last call.
	 */
	public void writeNoop(int channel, int cycle) {
	}
	
	/**
	 * Plays the sound.
	 * 
	 * @param channel The channel to write to.
	 * @param cycle The number of cycles passed since last call.
	 * @param output The sound to output. However, the implementation can decide to ignore this and use the APU directly.
	 * 
	 * @see #write(JJAPUWave, int, int)
	 */
	protected abstract void doWrite(int channel, int cycle, byte output);
	
}
